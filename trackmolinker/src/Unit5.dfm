object Form5: TForm5
  Left = 324
  Top = 160
  Width = 870
  Height = 480
  Caption = 'Additional functions... Esc - go back'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 417
    Height = 423
    Align = alLeft
    Lines.Strings = (
      ';================================'
      ';CHANGEDISK ENGINE'
      ';THE DETERMINISTIC LOADER WORKING'
      ';WITH D64 CREATED FOR TRACKMOLINKER'
      ';based on write protection test'
      '; (C) BY WEGI IN 2013.02.09'
      ';================================'
      ';==='
      
        'TRACK_INDICATOR = $01  ; TRACK AND SECTOR WITH DATA OF IDICATOR ' +
        'SIDE'
      'SECT_INDICATOR = $00'
      ';==='
      'MYCHGDSK = $1000'
      '* = MYCHGDSK'
      'PROC2 = $0900 ;WHERE IS LOADER'
      'SENDDRIV2 = PROC2 + 12'
      '         ;PROC2    = LOAD FILE AND STOP MOTOR'
      '         ;PROC2+3  = DISCONNECT LOADER'
      '         ;PROC2+6  = LOAD FILE WITHOUT STOPPING MOTOR'
      '         ;PROC2+9 = TORN OFF THE MOTOR'
      '         ;PROC2+12 = SEND BYTE FROM ACC TO THE DRIVE '
      ';==============='
      ';SENDDRIV2 = PROC2+12'
      ';========================='
      '; IF YOU NEED CHANGE THE DISK ENGINE BELOW IS THE PROCEDURE'
      '; YOU MUST PUT YOUR UNIQUE DATA (MAX 20BYTES) TO SECTOR'
      
        '; AND PUT SECTOR AND TRACK NUMBER TO "TRACK_INDICATOR" AND "SECT' +
        '_INDICATOR" VARIABLE'
      '; AND CALL "JSR SECONDSIDETEST"'
      ';========================='
      'SECONDSIDETEST'
      ''
      '         LDY #$00'
      '         -'
      '         LDA CODESEND,Y'
      '         JSR SENDDRIV2   ;SEND JOB CODE TO LOADER'
      '         INY '
      '         CPY #$06'
      '         BNE -'
      ''
      '         LDY #$00'
      '         -'
      '         LDA CODESEND2,Y  ;SEND OUR EXTRA CODE TO DRIVE'
      '         JSR SENDDRIV2'
      '         INY '
      '         BNE -'
      '         BIT $DD00    ;WAIT FOR START WORK'
      '         BVC *-3'
      '         BIT $DD00    ;WAIT FOR END OF WORK'
      '         BVS *-3'
      ''
      '         RTS'
      ''
      'CODESEND'
      ';---'
      ';GET_TO_RAM PROTOCOL'
      '.BYTE 0'#9';LOW BYTE TARGET ADDRES IN DRIVE TO PUT DATA'
      '.BYTE 6'#9';HI BYTE ($0600)'
      '.BYTE 4'#9';JOB NR ALLWAYS 4'
      '.BYTE 0   ;LOW BYTE TO RUN PROG'
      '.BYTE 6   ;HI BYTE ($0600)'
      '.BYTE 0   ;BYTES TO SEND 0 = 256'
      ';---'
      'CODESEND2'
      ';---'
      '.LOGICAL $0600'
      ';---'
      '        LDY #$BA'
      '       -'
      '        LDA $0100,Y ;SAVE DATA FROM $01BA TO $01FF'
      '        STA $0600,Y'
      '        INY'
      '        BNE -'
      ''
      '        STY $1800    ;START WORK - INFO FOR C64'
      ';==='
      'RESTART'
      '         SEI'
      '         LDA $1C00'
      '         AND #%11111011   ;MOTOR STOP'
      '         STA $1C00'
      ''
      '         AND #$10'
      '         STA $85'
      ''
      '    -    JSR WRPTEST   ;WAIT 1ST TIME FOR CHANGE WRITE PROTECT'
      '         BEQ -'
      ''
      '    -    JSR WRPTEST'
      '         BNE -'
      ''
      '         LDA $1C00   ;RUN MOTOR AND SETLED ON'
      '         ORA #$0C'
      '         STA $1C00'
      '        '
      '         LDX #$00    ;DELAY FOR MOTOR START'
      '         DEY'
      '         BNE *-1'
      '         DEX'
      '         BNE *-4'
      ' '
      '         STX $1C03   ;HEAD TO READ MODE'
      ';==='
      'STARTTEST'
      '        '
      '        SEI'
      '        LDA #$EE'
      '        STA $1C0C      ;SOE TO READ'
      '        '
      '        LDA $1C01      ;WAIT FOR 1ST SYNC = CLOSE KEY'
      '        BIT $1C00'
      '        BMI *-3'
      '        '
      '        LDX #$10       ;WAIT FOR 16 STABILE SYNC'
      '       -'
      '        LDA #$FF'
      '        STA $1805'
      '        LDA $1C01'
      'CZKN'
      '        BIT $1805'
      '        BPL STARTTEST  ;NO STABILE SYNC REPEAT'
      '        BIT $1C00'
      '        BMI CZKN'
      '        DEX'
      '        BNE -'
      ';===        '
      '        CLI'
      ''
      
        '        JSR $D042      ;HEAD MOVE TO TRACK 18, READ BAM, INIT DI' +
        'SK'
      '        '
      '        JSR READSEC    ;READ OUR TYPED SECTOR'
      ''
      '        LDY #$04'
      '       -'
      '        LDA MYCOMPARE,Y'
      '        BEQ TEST_OK   '
      '        CMP $0700,Y    ;TEST DATA'
      '        BNE RESTART    ;NO THIS ONE DISK - RESTART'
      '        INY'
      '        BNE -'
      ';==='
      'TEST_OK'
      '        LDA #$08'
      '        STA $1800      ;THE END OF WORK INFO FOR C64'
      '        JSR READSEC    ;ONLY FOR DELAY'
      '        LDY #$BA'
      #9'-'
      '        LDA $0600,Y    ;RESTORE DATA FROM'
      '        STA $0100,Y    ;$01BA - $01FF'
      '        INY'
      '        BNE -'
      '        SEI'
      ';**************'
      ';* EXIT POINT *'
      ';**************'
      ';==='
      'WRPTEST  LDA $1C00   ;WAIT 2ND TIME FOR CHANGE WRITE PROTECT'
      '         AND #$10'
      '         CMP $85'
      '         RTS'
      ';==='
      'READSEC'
      '        LDA #TRACK_INDICATOR'
      '        STA $0E'
      '        LDA #SECT_INDICATOR'
      '        STA $0F'
      '        SEC'
      '        ROL $1C'
      '        -        '
      '        LDA #$80    ;READ SECTOR WITH SIDE INDICATOR'
      '        STA $04     ;TO $0700 BUFFER'
      '        CLI'
      '        LDA $04'
      '        BMI *-2'
      '        CMP #$02'
      '        BCS -'
      '        RTS'
      ';------------------'
      'MYCOMPARE = *-4        '
      ';OUR UNIQUE DATA OF SIDE INDICATOR'
      ';NO LONGER THAN 20 BYTES! - THAT'#39'S ENOUGHT'
      '.TEXT "MY UNIQUE DATA"'
      '.BYTE 0'
      ';=============================='
      ';        BELOW'
      '; EXAMPLE ASM FILE FOR OUR INDICATOR'
      '; 3 TEXTLINES'
      ';=============================='
      ';* = $0900 ;NOT IMPORTANT'
      ';.TEXT "MY UNIQUE DATA"'
      ';.BYTE 0'
      ';=============================='
      '__END_NEXT_SIDE'
      '.HERE'
      ' ')
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object Memo2: TMemo
    Left = 424
    Top = 0
    Width = 430
    Height = 423
    Align = alRight
    Lines.Strings = (
      ';==='
      'GET_SILENT_DIR'
      ''
      #9';IN ACC/Y LO/HI BYTE TARGET POINTER TO PUT INFODIR DATA'
      ''
      #9#9'STA TARGET_LO'
      #9#9'STY TARGET_HI'
      #9#9'LDA #<S_DIR_DATA'
      #9#9'LDY #>S_DIR_DATA'
      #9#9'JSR PROC2'
      #9#9'BCS *'
      #9#9'LDA $01'
      #9#9'PHA'
      #9#9'LDA #$34'
      #9#9'STA $01'
      ''
      #9#9'LDY #0'
      #9'-'#9'LDA PROC2-$0100+100,Y'
      #9#9'BEQ +'
      #9#9'JSR TARGET ;SAVE LENBLOCK OF FILE INTO '
      #9#9'LDA PROC2-$0100+150,Y ;LO BYTE LOAD ADDRESS'
      #9#9'JSR TARGET'
      #9#9'LDA PROC2-$0100+200,Y ;HI BYTE LOAD ADDRESS'
      #9#9'JSR TARGET'
      #9#9'LDA PROC2-$0100,Y ;start track'
      #9#9'JSR TARGET'
      #9#9'LDA PROC2-$0100+50,Y ; start sector'
      #9#9'JSR TARGET'
      #9#9'INY'
      #9#9'CPY #50'
      #9#9'BNE -'#9
      #9'+'#9'PLA'
      #9#9'STA $01'
      #9#9'RTS ;IN Y REG HOW MANY FILES ON THE DISK'
      ';==================='
      'TARGET'
      'TARGET_LO = *+1'
      'TARGET_HI = *+2'
      #9#9'STA $1111'
      #9#9'INC TARGET_LO'
      #9#9'BNE +'
      #9#9'INC TARGET_HI'
      #9'+'#9'RTS'
      'S_DIR_DATA'
      '.BYTE 1 ;1 SECTOR TO READ'
      '.BYTE <DIR_DATA ;ADDRES FOR DATA FROM SILENT DIR'
      '.BYTE >DIR_DATA'
      '.BYTE 18'#9';TRACK 18 SECTOR 2 TO READ'
      '.BYTE 2'
      ''
      ';CORRECTLY SHOULD BE ENOUGHT RESERVED 50 BYTES'
      ';THIS IS A TEMP TABLES FOR PLAYING LOAD PROCEDURE'
      ';AFTER CALLING IS UNUSED'
      'DIR_DATA'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
      ';=======================================')
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 423
    Width = 854
    Height = 19
    Panels = <
      item
        Width = 250
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object ActionList1: TActionList
    Left = 784
    Top = 24
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 27
      OnExecute = Action1Execute
    end
  end
end
