unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ToolWin, ComCtrls, ActnList, ExtCtrls, ImgList,
  Buttons;

type
  TForm3 = class(TForm)
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    ToolBar1: TToolBar;
    ToolBar2: TToolBar;
    ESCAPE: TAction;
    Panel1: TPanel;
    Panel2: TPanel;
    Memo2: TMemo;
    SaveDialog1: TSaveDialog;
    Memo3: TMemo;
    Memo4: TMemo;
    Memo5: TMemo;
    Memo6: TMemo;
    showproc1: TAction;
    ToolButton2: TToolButton;
    saveproc1: TAction;
    ToolButton1: TToolButton;
    saveproc2: TAction;
    showproc2: TAction;
    ToolButton3: TToolButton;
    showchangedisk: TAction;
    savechangedisk: TAction;
    ToolButton4: TToolButton;
    SHOWDEMO: TAction;
    savedemo: TAction;
    ToolButton5: TToolButton;
    ImageList1: TImageList;
    Panel3: TPanel;
    ListBox1: TListBox;
    ListBox2: TListBox;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    TrackBar1: TTrackBar;
    make4inst: TAction;
    SpeedButton1: TSpeedButton;
    StaticText5: TStaticText;
    StaticText6: TStaticText;
    StaticText7: TStaticText;
    StaticText8: TStaticText;
    TrackBar2: TTrackBar;
    make4loads: TAction;
    CheckBox2: TCheckBox;
    SpeedButton2: TSpeedButton;
    Memo1: TMemo;
    procedure ESCAPEExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure showproc1Execute(Sender: TObject);
    procedure saveproc1Execute(Sender: TObject);
    procedure showproc2Execute(Sender: TObject);
    procedure saveproc2Execute(Sender: TObject);
    procedure showchangediskExecute(Sender: TObject);
    procedure savechangediskExecute(Sender: TObject);
    procedure SHOWDEMOExecute(Sender: TObject);
    procedure savedemoExecute(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure make4instExecute(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure make4loadsExecute(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
//========== loader installers ====
    first_code : array [0..2048] of byte;
    second_code : array [0..2048] of byte;
    third_code : array [0..2048] of byte;
    diff_code : array [0..2048] of byte;

    vers_inst : byte;
    name_inst : string;
    install_len : Integer;
//================

    first_ldr : array [0..2048] of byte;
    zeropage_ldr : array [0..2048] of byte;
    second_ldr : array [0..2048] of byte;
    third_ldr : array [0..2048] of byte;

    diff_ldr : array [0..2048] of byte;
    zpage_diff_ldr : array [0..2048] of byte;

    vers_ldr : byte;
    name_ldr : string;
    ldr_len : Integer;

    decrun_zp : byte;
    used_zp :byte;


  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses Unit5;

{$R *.dfm}

procedure TForm3.ESCAPEExecute(Sender: TObject);
begin
  Form3.Close;
end;

procedure TForm3.FormCreate(Sender: TObject);
begin
  Memo2.Align:=alclient;
  make4instExecute(Sender);
  make4loadsExecute(Sender);
end;

procedure TForm3.showproc1Execute(Sender: TObject);
begin
SaveDialog1.FilterIndex :=1;
  ;Memo2.Visible := True;
saveproc1.Enabled:=True;
  Memo2.Lines:=Memo3.Lines;
  ToolButton1.Action := saveproc1;
end;

procedure TForm3.saveproc1Execute(Sender: TObject);
begin
  SaveDialog1.FileName := 'DETERM_PROC1.ASM' ;
  if not SaveDialog1.Execute then exit;
  Memo2.Lines.SaveToFile(SaveDialog1.FileName);
end;

procedure TForm3.showproc2Execute(Sender: TObject);
begin
SaveDialog1.FilterIndex :=1;
  Memo2.Visible := True;
  Memo2.Lines:=Memo4.Lines;
  ToolButton1.Action := saveproc2 ;

end;

procedure TForm3.saveproc2Execute(Sender: TObject);
begin
  SaveDialog1.FileName := 'DETERM_PROC2.ASM' ;
  if not SaveDialog1.Execute then exit;
  Memo2.Lines.SaveToFile(SaveDialog1.FileName);

end;

procedure TForm3.showchangediskExecute(Sender: TObject);
begin
SaveDialog1.FilterIndex :=1;
  Memo2.Visible := True;
  Memo2.Lines:=Memo5.Lines;
  ToolButton1.Action := savechangedisk ;

end;

procedure TForm3.savechangediskExecute(Sender: TObject);
begin
  SaveDialog1.FileName := 'CHANGEDISK_ENGINE.ASM' ;
  if not SaveDialog1.Execute then exit;
  Memo2.Lines.SaveToFile(SaveDialog1.FileName);

end;

procedure TForm3.SHOWDEMOExecute(Sender: TObject);
begin
SaveDialog1.FilterIndex :=1;
  Memo2.Visible := True;
  Memo2.Lines:=Memo6.Lines;
  ToolButton1.Action := savedemo ;
end;

procedure TForm3.savedemoExecute(Sender: TObject);
begin
  SaveDialog1.FileName := 'DEMO_DETERM_LOADER.ASM' ;
  if not SaveDialog1.Execute then exit;
  Memo2.Lines.SaveToFile(SaveDialog1.FileName);

end;

procedure TForm3.TrackBar1Change(Sender: TObject);
begin
  StaticText2.Caption := ListBox1.Items[TrackBar1.Position];
end;

procedure TForm3.make4instExecute(Sender: TObject);
var
  installer : TResourceStream;
  i:Integer;
  s1,s2 : string;
begin

  name_inst := IntToStr(vers_inst);

  s1 := 'inst_0900';
  s2 := 'inst_0a00';

  for i := 0 to 2048 do first_code[i] :=0;
  for i := 0 to 2048 do second_code[i] :=0;
  for i := 0 to 2048 do third_code[i] :=0;
  for i := 0 to 2048 do diff_code[i] :=0;


  installer := TResourceStream.Create(HInstance, s1, RT_RCDATA);
  install_len := installer.Size;
  installer.Read(first_code,install_len);
  installer.Free;

  installer := TResourceStream.Create(HInstance, s2, RT_RCDATA);
  installer.Read(second_code,install_len);
  installer.Free;
  StaticText4.Caption := IntToStr(install_len-2);


  ListBox1.Items.Clear;

  i := $0900;

  while i + install_len < $a000 do
  begin
    ListBox1.Items.Add('$'+IntToHex(i,4));
    i :=  i+ $0100;
  end;

  i := $c000;

  while i + install_len < $d000 do
  begin
    ListBox1.Items.Add('$'+IntToHex(i,4));
    i :=  i+ $0100;
  end;
  TrackBar1.Min := 0;
  TrackBar1.Max := ListBox1.Items.Count-1;
  if TrackBar1.Position > TrackBar1.Max then TrackBar1.Position := TrackBar1.Max;
  TrackBar1Change(Sender);
end;
procedure TForm3.TrackBar2Change(Sender: TObject);
begin
  StaticText6.Caption := ListBox2.Items[TrackBar2.Position];
end;

procedure TForm3.make4loadsExecute(Sender: TObject);
var
  loader : TResourceStream;
  i:Integer;
  s1,s2,s3 : string;

begin

  s1 := 'ldr_0900';
  s2 := 'ldr_0a00';

  for i := 0 to 2048 do first_ldr[i] :=0;
  for i := 0 to 2048 do second_ldr[i] :=0;
  for i := 0 to 2048 do third_ldr[i] :=0;
  for i := 0 to 2048 do diff_ldr[i] :=0;
  for i := 0 to 2048 do zeropage_ldr[i] :=0;
  for i := 0 to 2048 do zpage_diff_ldr[i] :=0;

  loader := TResourceStream.Create(HInstance, s1, RT_RCDATA);
  ldr_len := loader.Size;
  loader.Read(first_ldr,ldr_len);
  loader.Free;

  loader := TResourceStream.Create(HInstance, s2, RT_RCDATA);
  loader.Read(second_ldr,ldr_len);
  loader.Free;

  loader := TResourceStream.Create(HInstance, s1, RT_RCDATA);
  loader.Read(zeropage_ldr,ldr_len);
  loader.Free;

  StaticText8.Caption := IntToStr(ldr_len-2);


  ListBox2.Items.Clear;

  i := $0300;

  while i + ldr_len < $d000 do
  begin
    ListBox2.Items.Add('$'+IntToHex(i,4));
    i :=  i+ $0100;
  end;

  i := $e100;

  while i + ldr_len < $10000 do
  begin
    ListBox2.Items.Add('$'+IntToHex(i,4));
    i :=  i+ $0100;
  end;

  TrackBar2.Min := 0;
  TrackBar2.Max := ListBox2.Items.Count-1;
  if TrackBar2.Position > TrackBar2.Max then TrackBar2.Position := TrackBar2.Max;
  TrackBar2Change(Sender);

end;




procedure TForm3.SpeedButton1Click(Sender: TObject);
var
i,ady : Integer;
s : string;
  plik : file of byte;
begin
  make4instExecute(Sender);
  make4loadsExecute(Sender);



   SaveDialog1.DefaultExt := 'prg';
   SaveDialog1.FilterIndex :=2;
  i := StrToInt(StaticText2.Caption);
  SaveDialog1.FileName := 'inst'+IntToHex(i,4);
  ady := StrToInt(StaticText2.Caption) div 256;
  dec(ady);
  for i := 0 to install_len do
  if second_code[i] <> first_code[i] then diff_code[i] := second_code[i] - first_code[1] + ady;
  for i := 0 to install_len do
  begin
    if diff_code[i] = 0 then third_code[i] := first_code[i]
        else third_code[i] := diff_code[i];
  end;

  if not CheckBox2.Checked then
  begin
    for i := 0 to 2000 do
    begin
      if (
        (third_code[i] = $ad)
        and (third_code[i+1] = $01)
        and (third_code[i+2] = $18)
        and (third_code[i+3] = $09)
        and (third_code[i+4] = $20)
        ) then
        begin
          third_code[i+3] := $29;
          third_code[i+4] := $df;
        end;
    end;
  end;

  if not SaveDialog1.Execute then exit;
    AssignFile( plik , SaveDialog1.FileName );
    rewrite(plik);
      for i := 0 to install_len-1 do BlockWrite(plik,third_code[i],1) ;
    closefile(plik);

  ady := StrToInt(StaticText6.Caption) div 256;
  dec(ady);

  i := StrToInt(StaticText6.Caption);
  SaveDialog1.FileName := name_ldr+ 'ldr'+IntToHex(i,4);


  for i := 0 to ldr_len do
  if second_ldr[i] <> first_ldr[i] then diff_ldr[i] := second_ldr[i] - first_ldr[1] + ady;

  for i := 0 to ldr_len do
  begin
    if diff_ldr[i] = 0 then third_ldr[i] := first_ldr[i]
        else third_ldr[i] := diff_ldr[i];
  end;



  if not SaveDialog1.Execute then exit;
    AssignFile( plik , SaveDialog1.FileName );
    rewrite(plik);
      for i := 0 to ldr_len-1 do BlockWrite(plik,third_ldr[i],1) ;
    closefile(plik);


end;




procedure TForm3.SpeedButton2Click(Sender: TObject);
begin
  form5.ShowModal;
end;

end.
