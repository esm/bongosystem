unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, StdCtrls, ComCtrls, Buttons, FileCtrl, ImgList,
  ExtCtrls, Spin;

type
  TForm1 = class(TForm)
    StatusBar1: TStatusBar;
    ListBox1: TListBox;
    ActionList1: TActionList;
    EXITAPP: TAction;
    OpenDialog1: TOpenDialog;
    SpeedButton1: TSpeedButton;
    addfile: TAction;
    SpeedButton2: TSpeedButton;
    ListBox2: TListBox;
    SaveDialog1: TSaveDialog;
    checkexists: TAction;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText9: TStaticText;
    StaticText10: TStaticText;
    StaticText13: TStaticText;
    StaticText14: TStaticText;
    StaticText15: TStaticText;
    StaticText16: TStaticText;
    StaticText17: TStaticText;
    StaticText18: TStaticText;
    DELFILE: TAction;
    ImageList1: TImageList;
    movdown: TAction;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    movup: TAction;
    OpenDialog2: TOpenDialog;
    SaveDialog2: TSaveDialog;
    SpeedButton5: TSpeedButton;
    saveproj: TAction;
    SpeedButton6: TSpeedButton;
    loadproj: TAction;
    CREATED64: TAction;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    CREATEDATA: TAction;
    Memo1: TMemo;
    created64offs: TAction;
    resourcecopy: TAction;
    LOADFROMPARAMS: TAction;
    ALLOCATEBAM: TAction;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    Savememo: TAction;
    SaveDialog3: TSaveDialog;
    SpeedButton11: TSpeedButton;
    staticclear: TAction;
    SKOJARZ: TAction;
    StaticText19: TStaticText;
    RadioGroup2: TRadioGroup;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    StaticText23: TStaticText;
    StaticText24: TStaticText;
    GroupBox1: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    MAKEWITHOUT: TAction;
    StaticText25: TStaticText;
    StaticText26: TStaticText;
    WYSWNASTAT: TAction;
    MAKEV1: TAction;
    MAKEV2: TAction;
    clearall: TAction;
    SETFIRSTFILE: TAction;
    SETLASTFILE: TAction;
    SILENTCREATE: TAction;
    StaticText5: TStaticText;
    check12: TAction;
    HELP: TAction;
    RadioButton7: TRadioButton;
    checkfreeblock: TAction;
    puttodir: TAction;
    edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Edit3: TEdit;
    Label3: TLabel;
    SpeedButton14: TSpeedButton;
    SpeedButton15: TSpeedButton;
    procedure EXITAPPExecute(Sender: TObject);
    procedure addfileExecute(Sender: TObject);
    procedure checkexistsExecute(Sender: TObject);
    procedure ListBox2Click(Sender: TObject);
    procedure DELFILEExecute(Sender: TObject);
    procedure movdownExecute(Sender: TObject);
    procedure movupExecute(Sender: TObject);
    procedure saveprojExecute(Sender: TObject);
    procedure loadprojExecute(Sender: TObject);
    procedure CREATED64Execute(Sender: TObject);
    procedure CREATEDATAExecute(Sender: TObject);
    procedure created64offsExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure resourcecopyExecute(Sender: TObject);
    procedure LOADFROMPARAMSExecute(Sender: TObject);
    procedure ALLOCATEBAMExecute(Sender: TObject);
    procedure SavememoExecute(Sender: TObject);
    procedure staticclearExecute(Sender: TObject);
    procedure MAKEWITHOUTExecute(Sender: TObject);
    procedure WYSWNASTATExecute(Sender: TObject);
    procedure MAKEV1Execute(Sender: TObject);
    procedure MAKEV2Execute(Sender: TObject);
    procedure clearallExecute(Sender: TObject);
    procedure SETFIRSTFILEExecute(Sender: TObject);
    procedure SETLASTFILEExecute(Sender: TObject);
    procedure SILENTCREATEExecute(Sender: TObject);
    procedure check12Execute(Sender: TObject);
    procedure HELPExecute(Sender: TObject);
    procedure checkfreeblockExecute(Sender: TObject);
    procedure puttodirExecute(Sender: TObject);
    procedure SpeedButton14Click(Sender: TObject);
    procedure SpeedButton15Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

tabtrack  : array[1..664] of byte;
tabsec : array[1..664] of byte;
taboffs : array[1..664] of Integer;
tabdir  : array[1..18] of byte;
mojd64 : array[1..174848] of byte;
MOJTRAK, MOJSEC : BYTE;
   TBBIT : ARRAY[0..24] OF BYTE;
   TBMAX : ARRAY[1..35] OF BYTE;
SILENT : Boolean;
SILENT2 : Boolean;
isblockfree : Boolean;
  end;

var
  Form1: TForm1;

implementation

uses Unit3, Unit4;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
VAR I,i2 : INTEGER;
DANA : STRING;
begin

Application.OnHint := WYSWNASTATExecute;
SILENT:=False;
SILENT2:=False;
SaveDialog1.FileName := '';
SaveDialog3.FileName:= '';
OpenDialog2.FileName:='';

  for i := 1 to ParamCount-1 do
  begin
    if UpperCase(ParamStr(i)) = '-INTERLEAVE' then
    BEGIN
      TRY
        I2 := StrToInt(ParamStr(i+1));
        Edit3.Text :='$'+ IntToHex(I2,4);
      EXCEPT
        I2 := 10;
        Edit3.Text :='$'+ IntToHex(I2,4);
      END;
        Edit3.Text :='$'+ IntToHex(I2,4);
    END;
  end;



FOR I := 1 TO ParamCount DO
BEGIN
 DANA := UpperCase(ParamStr(I));
 IF DANA = '-SILENT' THEN SILENT := TRUE;
 IF DANA = '-L0' THEN RadioButton3.Checked := TRUE;
 IF DANA = '-L1' THEN RadioButton1.Checked := TRUE;
 IF DANA = '-L2' THEN RadioButton2.Checked := TRUE;
 IF DANA = '-DIRENT' THEN RadioButton7.Checked := TRUE;


 IF DANA = '-FF' THEN RadioButton4.Checked := TRUE;
 IF DANA = '-LF' THEN RadioButton5.Checked := TRUE;

 IF DANA = '-INC' THEN
 BEGIN
   SaveDialog3.FileName := ParamStr(I+1);

 END;

 IF DANA = '-DISKNAME' THEN
 BEGIN
   edit1.Text := UpperCase(ParamStr(I+1));
 END;

 IF DANA = '-DISKID' THEN
 BEGIN
   edit2.Text := UpperCase(ParamStr(I+1));
 END;


 IF DANA = '-D64' THEN
 BEGIN
   SaveDialog1.FileName := ParamStr(I+1);

 END;
 if UpperCase(ExtractFileExt( ParamStr(i))) = '.C64LINK' then
 BEGIN
   if FileExists(ParamStr(i)) then OpenDialog2.FileName := ParamStr(i)
   else
   begin
     ShowMessage('Error ! File does not exist : ' + ParamStr(i));
   end;
 END;

// ShowMessage(ParamStr(I));

END;
//ShowMessage(IntToStr(ParamCount));
  IF OpenDialog2.FileName <> '' then LOADFROMPARAMSExecute(NIL);
  check12Execute(nil);
  IF ((SaveDialog1.FileName <> '') OR (SaveDialog3.FileName <> '')) THEN SILENTCREATEExecute(NIL);
  IF ((SaveDialog1.FileName <> '') OR (SaveDialog3.FileName <> '')) THEN
  BEGIN
   SILENT:=True;
   Application.Terminate;
  END;
 //IF NOT SILENT THEN  Form1.Show;
 // resourcecopyExecute(nil);
 // created64offsExecute(nil);

TRY
//IF NOT SILENT THEN SKOJARZExecute(nil);
FINALLY

END;
end;

procedure TForm1.resourcecopyExecute(Sender: TObject);
var
  i,i2, ofsecik : Integer;
  d64_HEADER : TResourceStream;
begin
  d64_HEADER := TResourceStream.Create(HInstance, 'diskd64', RT_RCDATA);
  d64_HEADER.ReadBuffer(mojd64 , 174848);
  d64_HEADER.Free;

  if RadioButton2.Checked then
  begin
    d64_HEADER := TResourceStream.Create(HInstance, 'diskd264', RT_RCDATA);
    d64_HEADER.ReadBuffer(mojd64 , 174848);
    d64_HEADER.Free;
  end;

  if RadioButton3.Checked then
  begin
    d64_HEADER := TResourceStream.Create(HInstance, 'diskd364', RT_RCDATA);
    d64_HEADER.ReadBuffer(mojd64 , 174848);
    d64_HEADER.Free;
  end;

  if RadioButton7.Checked then
  begin
    d64_HEADER := TResourceStream.Create(HInstance, 'diskd364', RT_RCDATA);
    d64_HEADER.ReadBuffer(mojd64 , 174848);
    d64_HEADER.Free;
    ofsecik := 17*21*256+256 ;

    for i := 3 to 100 do mojd64[ofsecik+i] :=0;

  end;

    Edit1.Text := UpperCase(Edit1.Text);
    Edit2.Text := UpperCase(Edit2.Text);

    ofsecik:=17*21*256;
    for i := 1 to 23 do mojd64[ofsecik+i+144] := 160;
    i2 := length(edit1.Text);
    if i2 > 16 then i2 :=16;

    for i := 1 to i2 do mojd64[ofsecik+i+144] :=  ord(edit1.text[i]);

    i2 := length(edit2.Text);
    if i2 > 5 then i2 :=5;

    for i := 1 to i2 do mojd64[ofsecik+i+162] :=  ord(edit2.text[i]);

end;


procedure TForm1.created64offsExecute(Sender: TObject);
begin

 tabdir[1] := 1 ;
 tabdir[2] := 4 ;
 tabdir[3] := 7 ;
 tabdir[4] := 10 ;
 tabdir[5] := 13 ;
 tabdir[6] := 16 ;
 tabdir[7] := 2 ;
 tabdir[8] := 5 ;
 tabdir[9] := 8 ;
 tabdir[10] := 11 ;
 tabdir[11] := 14 ;
 tabdir[12] := 17 ;
 tabdir[13] := 3 ;
 tabdir[14] := 6 ;
 tabdir[15] := 9 ;
 tabdir[16] := 12 ;
 tabdir[17] := 15 ;
 tabdir[18] := 18 ;

TBBIT[0] := 1 ;
TBBIT[1] := 2 ;
TBBIT[2] := 4 ;
TBBIT[3] := 8 ;
TBBIT[4] := 16 ;
TBBIT[5] := 32 ;
TBBIT[6] := 64 ;
TBBIT[7] := 128 ;
TBBIT[8] := 1 ;
TBBIT[9] := 2 ;
TBBIT[10] := 4 ;
TBBIT[11] := 8 ;
TBBIT[12] := 16 ;
TBBIT[13] := 32 ;
TBBIT[14] := 64 ;
TBBIT[15] := 128 ;
TBBIT[16] := 1 ;
TBBIT[17] := 2 ;
TBBIT[18] := 4 ;
TBBIT[19] := 8 ;
TBBIT[20] := 16 ;
TBBIT[21] := 32 ;
TBBIT[22] := 64 ;
TBBIT[23] := 128 ;

TBMAX[1] := 21 ;
TBMAX[2] := 21 ;
TBMAX[3] := 21 ;
TBMAX[4] := 21 ;
TBMAX[5] := 21 ;
TBMAX[6] := 21 ;
TBMAX[7] := 21 ;
TBMAX[8] := 21 ;
TBMAX[9] := 21 ;
TBMAX[10] := 21 ;
TBMAX[11] := 21 ;
TBMAX[12] := 21 ;
TBMAX[13] := 21 ;
TBMAX[14] := 21 ;
TBMAX[15] := 21 ;
TBMAX[16] := 21 ;
TBMAX[17] := 21 ;
TBMAX[18] := 19 ;
TBMAX[19] := 19 ;
TBMAX[20] := 19 ;
TBMAX[21] := 19 ;
TBMAX[22] := 19 ;
TBMAX[23] := 19 ;
TBMAX[24] := 19 ;
TBMAX[25] := 18 ;
TBMAX[26] := 18 ;
TBMAX[27] := 18 ;
TBMAX[28] := 18 ;
TBMAX[29] := 18 ;
TBMAX[30] := 18 ;
TBMAX[31] := 17 ;
TBMAX[32] := 17 ;
TBMAX[33] := 17 ;
TBMAX[34] := 17 ;
TBMAX[35] := 17 ;


SpeedButton14Click(Sender);

exit;

tabtrack[1] := 1 ;  tabsec[1] := 0 ;  taboffs[1] := 0 ;  // $000000
tabtrack[2] := 1 ;  tabsec[2] := 4 ;  taboffs[2] := 1024 ;  // $000400
tabtrack[3] := 1 ;  tabsec[3] := 8 ;  taboffs[3] := 2048 ;  // $000800
tabtrack[4] := 1 ;  tabsec[4] := 12 ;  taboffs[4] := 3072 ;  // $000C00
tabtrack[5] := 1 ;  tabsec[5] := 16 ;  taboffs[5] := 4096 ;  // $001000
tabtrack[6] := 1 ;  tabsec[6] := 20 ;  taboffs[6] := 5120 ;  // $001400
tabtrack[7] := 1 ;  tabsec[7] := 3 ;  taboffs[7] := 768 ;  // $000300
tabtrack[8] := 1 ;  tabsec[8] := 7 ;  taboffs[8] := 1792 ;  // $000700
tabtrack[9] := 1 ;  tabsec[9] := 11 ;  taboffs[9] := 2816 ;  // $000B00
tabtrack[10] := 1 ;  tabsec[10] := 15 ;  taboffs[10] := 3840 ;  // $000F00
tabtrack[11] := 1 ;  tabsec[11] := 19 ;  taboffs[11] := 4864 ;  // $001300
tabtrack[12] := 1 ;  tabsec[12] := 2 ;  taboffs[12] := 512 ;  // $000200
tabtrack[13] := 1 ;  tabsec[13] := 6 ;  taboffs[13] := 1536 ;  // $000600
tabtrack[14] := 1 ;  tabsec[14] := 10 ;  taboffs[14] := 2560 ;  // $000A00
tabtrack[15] := 1 ;  tabsec[15] := 14 ;  taboffs[15] := 3584 ;  // $000E00
tabtrack[16] := 1 ;  tabsec[16] := 18 ;  taboffs[16] := 4608 ;  // $001200
tabtrack[17] := 1 ;  tabsec[17] := 1 ;  taboffs[17] := 256 ;  // $000100
tabtrack[18] := 1 ;  tabsec[18] := 5 ;  taboffs[18] := 1280 ;  // $000500
tabtrack[19] := 1 ;  tabsec[19] := 9 ;  taboffs[19] := 2304 ;  // $000900
tabtrack[20] := 1 ;  tabsec[20] := 13 ;  taboffs[20] := 3328 ;  // $000D00
tabtrack[21] := 1 ;  tabsec[21] := 17 ;  taboffs[21] := 4352 ;  // $001100
tabtrack[22] := 2 ;  tabsec[22] := 0 ;  taboffs[22] := 5376 ;  // $001500
tabtrack[23] := 2 ;  tabsec[23] := 4 ;  taboffs[23] := 6400 ;  // $001900
tabtrack[24] := 2 ;  tabsec[24] := 8 ;  taboffs[24] := 7424 ;  // $001D00
tabtrack[25] := 2 ;  tabsec[25] := 12 ;  taboffs[25] := 8448 ;  // $002100
tabtrack[26] := 2 ;  tabsec[26] := 16 ;  taboffs[26] := 9472 ;  // $002500
tabtrack[27] := 2 ;  tabsec[27] := 20 ;  taboffs[27] := 10496 ;  // $002900
tabtrack[28] := 2 ;  tabsec[28] := 3 ;  taboffs[28] := 6144 ;  // $001800
tabtrack[29] := 2 ;  tabsec[29] := 7 ;  taboffs[29] := 7168 ;  // $001C00
tabtrack[30] := 2 ;  tabsec[30] := 11 ;  taboffs[30] := 8192 ;  // $002000
tabtrack[31] := 2 ;  tabsec[31] := 15 ;  taboffs[31] := 9216 ;  // $002400
tabtrack[32] := 2 ;  tabsec[32] := 19 ;  taboffs[32] := 10240 ;  // $002800
tabtrack[33] := 2 ;  tabsec[33] := 2 ;  taboffs[33] := 5888 ;  // $001700
tabtrack[34] := 2 ;  tabsec[34] := 6 ;  taboffs[34] := 6912 ;  // $001B00
tabtrack[35] := 2 ;  tabsec[35] := 10 ;  taboffs[35] := 7936 ;  // $001F00
tabtrack[36] := 2 ;  tabsec[36] := 14 ;  taboffs[36] := 8960 ;  // $002300
tabtrack[37] := 2 ;  tabsec[37] := 18 ;  taboffs[37] := 9984 ;  // $002700
tabtrack[38] := 2 ;  tabsec[38] := 1 ;  taboffs[38] := 5632 ;  // $001600
tabtrack[39] := 2 ;  tabsec[39] := 5 ;  taboffs[39] := 6656 ;  // $001A00
tabtrack[40] := 2 ;  tabsec[40] := 9 ;  taboffs[40] := 7680 ;  // $001E00
tabtrack[41] := 2 ;  tabsec[41] := 13 ;  taboffs[41] := 8704 ;  // $002200
tabtrack[42] := 2 ;  tabsec[42] := 17 ;  taboffs[42] := 9728 ;  // $002600
tabtrack[43] := 3 ;  tabsec[43] := 0 ;  taboffs[43] := 10752 ;  // $002A00
tabtrack[44] := 3 ;  tabsec[44] := 4 ;  taboffs[44] := 11776 ;  // $002E00
tabtrack[45] := 3 ;  tabsec[45] := 8 ;  taboffs[45] := 12800 ;  // $003200
tabtrack[46] := 3 ;  tabsec[46] := 12 ;  taboffs[46] := 13824 ;  // $003600
tabtrack[47] := 3 ;  tabsec[47] := 16 ;  taboffs[47] := 14848 ;  // $003A00
tabtrack[48] := 3 ;  tabsec[48] := 20 ;  taboffs[48] := 15872 ;  // $003E00
tabtrack[49] := 3 ;  tabsec[49] := 3 ;  taboffs[49] := 11520 ;  // $002D00
tabtrack[50] := 3 ;  tabsec[50] := 7 ;  taboffs[50] := 12544 ;  // $003100
tabtrack[51] := 3 ;  tabsec[51] := 11 ;  taboffs[51] := 13568 ;  // $003500
tabtrack[52] := 3 ;  tabsec[52] := 15 ;  taboffs[52] := 14592 ;  // $003900
tabtrack[53] := 3 ;  tabsec[53] := 19 ;  taboffs[53] := 15616 ;  // $003D00
tabtrack[54] := 3 ;  tabsec[54] := 2 ;  taboffs[54] := 11264 ;  // $002C00
tabtrack[55] := 3 ;  tabsec[55] := 6 ;  taboffs[55] := 12288 ;  // $003000
tabtrack[56] := 3 ;  tabsec[56] := 10 ;  taboffs[56] := 13312 ;  // $003400
tabtrack[57] := 3 ;  tabsec[57] := 14 ;  taboffs[57] := 14336 ;  // $003800
tabtrack[58] := 3 ;  tabsec[58] := 18 ;  taboffs[58] := 15360 ;  // $003C00
tabtrack[59] := 3 ;  tabsec[59] := 1 ;  taboffs[59] := 11008 ;  // $002B00
tabtrack[60] := 3 ;  tabsec[60] := 5 ;  taboffs[60] := 12032 ;  // $002F00
tabtrack[61] := 3 ;  tabsec[61] := 9 ;  taboffs[61] := 13056 ;  // $003300
tabtrack[62] := 3 ;  tabsec[62] := 13 ;  taboffs[62] := 14080 ;  // $003700
tabtrack[63] := 3 ;  tabsec[63] := 17 ;  taboffs[63] := 15104 ;  // $003B00
tabtrack[64] := 4 ;  tabsec[64] := 0 ;  taboffs[64] := 16128 ;  // $003F00
tabtrack[65] := 4 ;  tabsec[65] := 4 ;  taboffs[65] := 17152 ;  // $004300
tabtrack[66] := 4 ;  tabsec[66] := 8 ;  taboffs[66] := 18176 ;  // $004700
tabtrack[67] := 4 ;  tabsec[67] := 12 ;  taboffs[67] := 19200 ;  // $004B00
tabtrack[68] := 4 ;  tabsec[68] := 16 ;  taboffs[68] := 20224 ;  // $004F00
tabtrack[69] := 4 ;  tabsec[69] := 20 ;  taboffs[69] := 21248 ;  // $005300
tabtrack[70] := 4 ;  tabsec[70] := 3 ;  taboffs[70] := 16896 ;  // $004200
tabtrack[71] := 4 ;  tabsec[71] := 7 ;  taboffs[71] := 17920 ;  // $004600
tabtrack[72] := 4 ;  tabsec[72] := 11 ;  taboffs[72] := 18944 ;  // $004A00
tabtrack[73] := 4 ;  tabsec[73] := 15 ;  taboffs[73] := 19968 ;  // $004E00
tabtrack[74] := 4 ;  tabsec[74] := 19 ;  taboffs[74] := 20992 ;  // $005200
tabtrack[75] := 4 ;  tabsec[75] := 2 ;  taboffs[75] := 16640 ;  // $004100
tabtrack[76] := 4 ;  tabsec[76] := 6 ;  taboffs[76] := 17664 ;  // $004500
tabtrack[77] := 4 ;  tabsec[77] := 10 ;  taboffs[77] := 18688 ;  // $004900
tabtrack[78] := 4 ;  tabsec[78] := 14 ;  taboffs[78] := 19712 ;  // $004D00
tabtrack[79] := 4 ;  tabsec[79] := 18 ;  taboffs[79] := 20736 ;  // $005100
tabtrack[80] := 4 ;  tabsec[80] := 1 ;  taboffs[80] := 16384 ;  // $004000
tabtrack[81] := 4 ;  tabsec[81] := 5 ;  taboffs[81] := 17408 ;  // $004400
tabtrack[82] := 4 ;  tabsec[82] := 9 ;  taboffs[82] := 18432 ;  // $004800
tabtrack[83] := 4 ;  tabsec[83] := 13 ;  taboffs[83] := 19456 ;  // $004C00
tabtrack[84] := 4 ;  tabsec[84] := 17 ;  taboffs[84] := 20480 ;  // $005000
tabtrack[85] := 5 ;  tabsec[85] := 0 ;  taboffs[85] := 21504 ;  // $005400
tabtrack[86] := 5 ;  tabsec[86] := 4 ;  taboffs[86] := 22528 ;  // $005800
tabtrack[87] := 5 ;  tabsec[87] := 8 ;  taboffs[87] := 23552 ;  // $005C00
tabtrack[88] := 5 ;  tabsec[88] := 12 ;  taboffs[88] := 24576 ;  // $006000
tabtrack[89] := 5 ;  tabsec[89] := 16 ;  taboffs[89] := 25600 ;  // $006400
tabtrack[90] := 5 ;  tabsec[90] := 20 ;  taboffs[90] := 26624 ;  // $006800
tabtrack[91] := 5 ;  tabsec[91] := 3 ;  taboffs[91] := 22272 ;  // $005700
tabtrack[92] := 5 ;  tabsec[92] := 7 ;  taboffs[92] := 23296 ;  // $005B00
tabtrack[93] := 5 ;  tabsec[93] := 11 ;  taboffs[93] := 24320 ;  // $005F00
tabtrack[94] := 5 ;  tabsec[94] := 15 ;  taboffs[94] := 25344 ;  // $006300
tabtrack[95] := 5 ;  tabsec[95] := 19 ;  taboffs[95] := 26368 ;  // $006700
tabtrack[96] := 5 ;  tabsec[96] := 2 ;  taboffs[96] := 22016 ;  // $005600
tabtrack[97] := 5 ;  tabsec[97] := 6 ;  taboffs[97] := 23040 ;  // $005A00
tabtrack[98] := 5 ;  tabsec[98] := 10 ;  taboffs[98] := 24064 ;  // $005E00
tabtrack[99] := 5 ;  tabsec[99] := 14 ;  taboffs[99] := 25088 ;  // $006200
tabtrack[100] := 5 ;  tabsec[100] := 18 ;  taboffs[100] := 26112 ;  // $006600
tabtrack[101] := 5 ;  tabsec[101] := 1 ;  taboffs[101] := 21760 ;  // $005500
tabtrack[102] := 5 ;  tabsec[102] := 5 ;  taboffs[102] := 22784 ;  // $005900
tabtrack[103] := 5 ;  tabsec[103] := 9 ;  taboffs[103] := 23808 ;  // $005D00
tabtrack[104] := 5 ;  tabsec[104] := 13 ;  taboffs[104] := 24832 ;  // $006100
tabtrack[105] := 5 ;  tabsec[105] := 17 ;  taboffs[105] := 25856 ;  // $006500
tabtrack[106] := 6 ;  tabsec[106] := 0 ;  taboffs[106] := 26880 ;  // $006900
tabtrack[107] := 6 ;  tabsec[107] := 4 ;  taboffs[107] := 27904 ;  // $006D00
tabtrack[108] := 6 ;  tabsec[108] := 8 ;  taboffs[108] := 28928 ;  // $007100
tabtrack[109] := 6 ;  tabsec[109] := 12 ;  taboffs[109] := 29952 ;  // $007500
tabtrack[110] := 6 ;  tabsec[110] := 16 ;  taboffs[110] := 30976 ;  // $007900
tabtrack[111] := 6 ;  tabsec[111] := 20 ;  taboffs[111] := 32000 ;  // $007D00
tabtrack[112] := 6 ;  tabsec[112] := 3 ;  taboffs[112] := 27648 ;  // $006C00
tabtrack[113] := 6 ;  tabsec[113] := 7 ;  taboffs[113] := 28672 ;  // $007000
tabtrack[114] := 6 ;  tabsec[114] := 11 ;  taboffs[114] := 29696 ;  // $007400
tabtrack[115] := 6 ;  tabsec[115] := 15 ;  taboffs[115] := 30720 ;  // $007800
tabtrack[116] := 6 ;  tabsec[116] := 19 ;  taboffs[116] := 31744 ;  // $007C00
tabtrack[117] := 6 ;  tabsec[117] := 2 ;  taboffs[117] := 27392 ;  // $006B00
tabtrack[118] := 6 ;  tabsec[118] := 6 ;  taboffs[118] := 28416 ;  // $006F00
tabtrack[119] := 6 ;  tabsec[119] := 10 ;  taboffs[119] := 29440 ;  // $007300
tabtrack[120] := 6 ;  tabsec[120] := 14 ;  taboffs[120] := 30464 ;  // $007700
tabtrack[121] := 6 ;  tabsec[121] := 18 ;  taboffs[121] := 31488 ;  // $007B00
tabtrack[122] := 6 ;  tabsec[122] := 1 ;  taboffs[122] := 27136 ;  // $006A00
tabtrack[123] := 6 ;  tabsec[123] := 5 ;  taboffs[123] := 28160 ;  // $006E00
tabtrack[124] := 6 ;  tabsec[124] := 9 ;  taboffs[124] := 29184 ;  // $007200
tabtrack[125] := 6 ;  tabsec[125] := 13 ;  taboffs[125] := 30208 ;  // $007600
tabtrack[126] := 6 ;  tabsec[126] := 17 ;  taboffs[126] := 31232 ;  // $007A00
tabtrack[127] := 7 ;  tabsec[127] := 0 ;  taboffs[127] := 32256 ;  // $007E00
tabtrack[128] := 7 ;  tabsec[128] := 4 ;  taboffs[128] := 33280 ;  // $008200
tabtrack[129] := 7 ;  tabsec[129] := 8 ;  taboffs[129] := 34304 ;  // $008600
tabtrack[130] := 7 ;  tabsec[130] := 12 ;  taboffs[130] := 35328 ;  // $008A00
tabtrack[131] := 7 ;  tabsec[131] := 16 ;  taboffs[131] := 36352 ;  // $008E00
tabtrack[132] := 7 ;  tabsec[132] := 20 ;  taboffs[132] := 37376 ;  // $009200
tabtrack[133] := 7 ;  tabsec[133] := 3 ;  taboffs[133] := 33024 ;  // $008100
tabtrack[134] := 7 ;  tabsec[134] := 7 ;  taboffs[134] := 34048 ;  // $008500
tabtrack[135] := 7 ;  tabsec[135] := 11 ;  taboffs[135] := 35072 ;  // $008900
tabtrack[136] := 7 ;  tabsec[136] := 15 ;  taboffs[136] := 36096 ;  // $008D00
tabtrack[137] := 7 ;  tabsec[137] := 19 ;  taboffs[137] := 37120 ;  // $009100
tabtrack[138] := 7 ;  tabsec[138] := 2 ;  taboffs[138] := 32768 ;  // $008000
tabtrack[139] := 7 ;  tabsec[139] := 6 ;  taboffs[139] := 33792 ;  // $008400
tabtrack[140] := 7 ;  tabsec[140] := 10 ;  taboffs[140] := 34816 ;  // $008800
tabtrack[141] := 7 ;  tabsec[141] := 14 ;  taboffs[141] := 35840 ;  // $008C00
tabtrack[142] := 7 ;  tabsec[142] := 18 ;  taboffs[142] := 36864 ;  // $009000
tabtrack[143] := 7 ;  tabsec[143] := 1 ;  taboffs[143] := 32512 ;  // $007F00
tabtrack[144] := 7 ;  tabsec[144] := 5 ;  taboffs[144] := 33536 ;  // $008300
tabtrack[145] := 7 ;  tabsec[145] := 9 ;  taboffs[145] := 34560 ;  // $008700
tabtrack[146] := 7 ;  tabsec[146] := 13 ;  taboffs[146] := 35584 ;  // $008B00
tabtrack[147] := 7 ;  tabsec[147] := 17 ;  taboffs[147] := 36608 ;  // $008F00
tabtrack[148] := 8 ;  tabsec[148] := 0 ;  taboffs[148] := 37632 ;  // $009300
tabtrack[149] := 8 ;  tabsec[149] := 4 ;  taboffs[149] := 38656 ;  // $009700
tabtrack[150] := 8 ;  tabsec[150] := 8 ;  taboffs[150] := 39680 ;  // $009B00
tabtrack[151] := 8 ;  tabsec[151] := 12 ;  taboffs[151] := 40704 ;  // $009F00
tabtrack[152] := 8 ;  tabsec[152] := 16 ;  taboffs[152] := 41728 ;  // $00A300
tabtrack[153] := 8 ;  tabsec[153] := 20 ;  taboffs[153] := 42752 ;  // $00A700
tabtrack[154] := 8 ;  tabsec[154] := 3 ;  taboffs[154] := 38400 ;  // $009600
tabtrack[155] := 8 ;  tabsec[155] := 7 ;  taboffs[155] := 39424 ;  // $009A00
tabtrack[156] := 8 ;  tabsec[156] := 11 ;  taboffs[156] := 40448 ;  // $009E00
tabtrack[157] := 8 ;  tabsec[157] := 15 ;  taboffs[157] := 41472 ;  // $00A200
tabtrack[158] := 8 ;  tabsec[158] := 19 ;  taboffs[158] := 42496 ;  // $00A600
tabtrack[159] := 8 ;  tabsec[159] := 2 ;  taboffs[159] := 38144 ;  // $009500
tabtrack[160] := 8 ;  tabsec[160] := 6 ;  taboffs[160] := 39168 ;  // $009900
tabtrack[161] := 8 ;  tabsec[161] := 10 ;  taboffs[161] := 40192 ;  // $009D00
tabtrack[162] := 8 ;  tabsec[162] := 14 ;  taboffs[162] := 41216 ;  // $00A100
tabtrack[163] := 8 ;  tabsec[163] := 18 ;  taboffs[163] := 42240 ;  // $00A500
tabtrack[164] := 8 ;  tabsec[164] := 1 ;  taboffs[164] := 37888 ;  // $009400
tabtrack[165] := 8 ;  tabsec[165] := 5 ;  taboffs[165] := 38912 ;  // $009800
tabtrack[166] := 8 ;  tabsec[166] := 9 ;  taboffs[166] := 39936 ;  // $009C00
tabtrack[167] := 8 ;  tabsec[167] := 13 ;  taboffs[167] := 40960 ;  // $00A000
tabtrack[168] := 8 ;  tabsec[168] := 17 ;  taboffs[168] := 41984 ;  // $00A400
tabtrack[169] := 9 ;  tabsec[169] := 0 ;  taboffs[169] := 43008 ;  // $00A800
tabtrack[170] := 9 ;  tabsec[170] := 4 ;  taboffs[170] := 44032 ;  // $00AC00
tabtrack[171] := 9 ;  tabsec[171] := 8 ;  taboffs[171] := 45056 ;  // $00B000
tabtrack[172] := 9 ;  tabsec[172] := 12 ;  taboffs[172] := 46080 ;  // $00B400
tabtrack[173] := 9 ;  tabsec[173] := 16 ;  taboffs[173] := 47104 ;  // $00B800
tabtrack[174] := 9 ;  tabsec[174] := 20 ;  taboffs[174] := 48128 ;  // $00BC00
tabtrack[175] := 9 ;  tabsec[175] := 3 ;  taboffs[175] := 43776 ;  // $00AB00
tabtrack[176] := 9 ;  tabsec[176] := 7 ;  taboffs[176] := 44800 ;  // $00AF00
tabtrack[177] := 9 ;  tabsec[177] := 11 ;  taboffs[177] := 45824 ;  // $00B300
tabtrack[178] := 9 ;  tabsec[178] := 15 ;  taboffs[178] := 46848 ;  // $00B700
tabtrack[179] := 9 ;  tabsec[179] := 19 ;  taboffs[179] := 47872 ;  // $00BB00
tabtrack[180] := 9 ;  tabsec[180] := 2 ;  taboffs[180] := 43520 ;  // $00AA00
tabtrack[181] := 9 ;  tabsec[181] := 6 ;  taboffs[181] := 44544 ;  // $00AE00
tabtrack[182] := 9 ;  tabsec[182] := 10 ;  taboffs[182] := 45568 ;  // $00B200
tabtrack[183] := 9 ;  tabsec[183] := 14 ;  taboffs[183] := 46592 ;  // $00B600
tabtrack[184] := 9 ;  tabsec[184] := 18 ;  taboffs[184] := 47616 ;  // $00BA00
tabtrack[185] := 9 ;  tabsec[185] := 1 ;  taboffs[185] := 43264 ;  // $00A900
tabtrack[186] := 9 ;  tabsec[186] := 5 ;  taboffs[186] := 44288 ;  // $00AD00
tabtrack[187] := 9 ;  tabsec[187] := 9 ;  taboffs[187] := 45312 ;  // $00B100
tabtrack[188] := 9 ;  tabsec[188] := 13 ;  taboffs[188] := 46336 ;  // $00B500
tabtrack[189] := 9 ;  tabsec[189] := 17 ;  taboffs[189] := 47360 ;  // $00B900
tabtrack[190] := 10 ;  tabsec[190] := 0 ;  taboffs[190] := 48384 ;  // $00BD00
tabtrack[191] := 10 ;  tabsec[191] := 4 ;  taboffs[191] := 49408 ;  // $00C100
tabtrack[192] := 10 ;  tabsec[192] := 8 ;  taboffs[192] := 50432 ;  // $00C500
tabtrack[193] := 10 ;  tabsec[193] := 12 ;  taboffs[193] := 51456 ;  // $00C900
tabtrack[194] := 10 ;  tabsec[194] := 16 ;  taboffs[194] := 52480 ;  // $00CD00
tabtrack[195] := 10 ;  tabsec[195] := 20 ;  taboffs[195] := 53504 ;  // $00D100
tabtrack[196] := 10 ;  tabsec[196] := 3 ;  taboffs[196] := 49152 ;  // $00C000
tabtrack[197] := 10 ;  tabsec[197] := 7 ;  taboffs[197] := 50176 ;  // $00C400
tabtrack[198] := 10 ;  tabsec[198] := 11 ;  taboffs[198] := 51200 ;  // $00C800
tabtrack[199] := 10 ;  tabsec[199] := 15 ;  taboffs[199] := 52224 ;  // $00CC00
tabtrack[200] := 10 ;  tabsec[200] := 19 ;  taboffs[200] := 53248 ;  // $00D000
tabtrack[201] := 10 ;  tabsec[201] := 2 ;  taboffs[201] := 48896 ;  // $00BF00
tabtrack[202] := 10 ;  tabsec[202] := 6 ;  taboffs[202] := 49920 ;  // $00C300
tabtrack[203] := 10 ;  tabsec[203] := 10 ;  taboffs[203] := 50944 ;  // $00C700
tabtrack[204] := 10 ;  tabsec[204] := 14 ;  taboffs[204] := 51968 ;  // $00CB00
tabtrack[205] := 10 ;  tabsec[205] := 18 ;  taboffs[205] := 52992 ;  // $00CF00
tabtrack[206] := 10 ;  tabsec[206] := 1 ;  taboffs[206] := 48640 ;  // $00BE00
tabtrack[207] := 10 ;  tabsec[207] := 5 ;  taboffs[207] := 49664 ;  // $00C200
tabtrack[208] := 10 ;  tabsec[208] := 9 ;  taboffs[208] := 50688 ;  // $00C600
tabtrack[209] := 10 ;  tabsec[209] := 13 ;  taboffs[209] := 51712 ;  // $00CA00
tabtrack[210] := 10 ;  tabsec[210] := 17 ;  taboffs[210] := 52736 ;  // $00CE00
tabtrack[211] := 11 ;  tabsec[211] := 0 ;  taboffs[211] := 53760 ;  // $00D200
tabtrack[212] := 11 ;  tabsec[212] := 4 ;  taboffs[212] := 54784 ;  // $00D600
tabtrack[213] := 11 ;  tabsec[213] := 8 ;  taboffs[213] := 55808 ;  // $00DA00
tabtrack[214] := 11 ;  tabsec[214] := 12 ;  taboffs[214] := 56832 ;  // $00DE00
tabtrack[215] := 11 ;  tabsec[215] := 16 ;  taboffs[215] := 57856 ;  // $00E200
tabtrack[216] := 11 ;  tabsec[216] := 20 ;  taboffs[216] := 58880 ;  // $00E600
tabtrack[217] := 11 ;  tabsec[217] := 3 ;  taboffs[217] := 54528 ;  // $00D500
tabtrack[218] := 11 ;  tabsec[218] := 7 ;  taboffs[218] := 55552 ;  // $00D900
tabtrack[219] := 11 ;  tabsec[219] := 11 ;  taboffs[219] := 56576 ;  // $00DD00
tabtrack[220] := 11 ;  tabsec[220] := 15 ;  taboffs[220] := 57600 ;  // $00E100
tabtrack[221] := 11 ;  tabsec[221] := 19 ;  taboffs[221] := 58624 ;  // $00E500
tabtrack[222] := 11 ;  tabsec[222] := 2 ;  taboffs[222] := 54272 ;  // $00D400
tabtrack[223] := 11 ;  tabsec[223] := 6 ;  taboffs[223] := 55296 ;  // $00D800
tabtrack[224] := 11 ;  tabsec[224] := 10 ;  taboffs[224] := 56320 ;  // $00DC00
tabtrack[225] := 11 ;  tabsec[225] := 14 ;  taboffs[225] := 57344 ;  // $00E000
tabtrack[226] := 11 ;  tabsec[226] := 18 ;  taboffs[226] := 58368 ;  // $00E400
tabtrack[227] := 11 ;  tabsec[227] := 1 ;  taboffs[227] := 54016 ;  // $00D300
tabtrack[228] := 11 ;  tabsec[228] := 5 ;  taboffs[228] := 55040 ;  // $00D700
tabtrack[229] := 11 ;  tabsec[229] := 9 ;  taboffs[229] := 56064 ;  // $00DB00
tabtrack[230] := 11 ;  tabsec[230] := 13 ;  taboffs[230] := 57088 ;  // $00DF00
tabtrack[231] := 11 ;  tabsec[231] := 17 ;  taboffs[231] := 58112 ;  // $00E300
tabtrack[232] := 12 ;  tabsec[232] := 0 ;  taboffs[232] := 59136 ;  // $00E700
tabtrack[233] := 12 ;  tabsec[233] := 4 ;  taboffs[233] := 60160 ;  // $00EB00
tabtrack[234] := 12 ;  tabsec[234] := 8 ;  taboffs[234] := 61184 ;  // $00EF00
tabtrack[235] := 12 ;  tabsec[235] := 12 ;  taboffs[235] := 62208 ;  // $00F300
tabtrack[236] := 12 ;  tabsec[236] := 16 ;  taboffs[236] := 63232 ;  // $00F700
tabtrack[237] := 12 ;  tabsec[237] := 20 ;  taboffs[237] := 64256 ;  // $00FB00
tabtrack[238] := 12 ;  tabsec[238] := 3 ;  taboffs[238] := 59904 ;  // $00EA00
tabtrack[239] := 12 ;  tabsec[239] := 7 ;  taboffs[239] := 60928 ;  // $00EE00
tabtrack[240] := 12 ;  tabsec[240] := 11 ;  taboffs[240] := 61952 ;  // $00F200
tabtrack[241] := 12 ;  tabsec[241] := 15 ;  taboffs[241] := 62976 ;  // $00F600
tabtrack[242] := 12 ;  tabsec[242] := 19 ;  taboffs[242] := 64000 ;  // $00FA00
tabtrack[243] := 12 ;  tabsec[243] := 2 ;  taboffs[243] := 59648 ;  // $00E900
tabtrack[244] := 12 ;  tabsec[244] := 6 ;  taboffs[244] := 60672 ;  // $00ED00
tabtrack[245] := 12 ;  tabsec[245] := 10 ;  taboffs[245] := 61696 ;  // $00F100
tabtrack[246] := 12 ;  tabsec[246] := 14 ;  taboffs[246] := 62720 ;  // $00F500
tabtrack[247] := 12 ;  tabsec[247] := 18 ;  taboffs[247] := 63744 ;  // $00F900
tabtrack[248] := 12 ;  tabsec[248] := 1 ;  taboffs[248] := 59392 ;  // $00E800
tabtrack[249] := 12 ;  tabsec[249] := 5 ;  taboffs[249] := 60416 ;  // $00EC00
tabtrack[250] := 12 ;  tabsec[250] := 9 ;  taboffs[250] := 61440 ;  // $00F000
tabtrack[251] := 12 ;  tabsec[251] := 13 ;  taboffs[251] := 62464 ;  // $00F400
tabtrack[252] := 12 ;  tabsec[252] := 17 ;  taboffs[252] := 63488 ;  // $00F800
tabtrack[253] := 13 ;  tabsec[253] := 0 ;  taboffs[253] := 64512 ;  // $00FC00
tabtrack[254] := 13 ;  tabsec[254] := 4 ;  taboffs[254] := 65536 ;  // $010000
tabtrack[255] := 13 ;  tabsec[255] := 8 ;  taboffs[255] := 66560 ;  // $010400
tabtrack[256] := 13 ;  tabsec[256] := 12 ;  taboffs[256] := 67584 ;  // $010800
tabtrack[257] := 13 ;  tabsec[257] := 16 ;  taboffs[257] := 68608 ;  // $010C00
tabtrack[258] := 13 ;  tabsec[258] := 20 ;  taboffs[258] := 69632 ;  // $011000
tabtrack[259] := 13 ;  tabsec[259] := 3 ;  taboffs[259] := 65280 ;  // $00FF00
tabtrack[260] := 13 ;  tabsec[260] := 7 ;  taboffs[260] := 66304 ;  // $010300
tabtrack[261] := 13 ;  tabsec[261] := 11 ;  taboffs[261] := 67328 ;  // $010700
tabtrack[262] := 13 ;  tabsec[262] := 15 ;  taboffs[262] := 68352 ;  // $010B00
tabtrack[263] := 13 ;  tabsec[263] := 19 ;  taboffs[263] := 69376 ;  // $010F00
tabtrack[264] := 13 ;  tabsec[264] := 2 ;  taboffs[264] := 65024 ;  // $00FE00
tabtrack[265] := 13 ;  tabsec[265] := 6 ;  taboffs[265] := 66048 ;  // $010200
tabtrack[266] := 13 ;  tabsec[266] := 10 ;  taboffs[266] := 67072 ;  // $010600
tabtrack[267] := 13 ;  tabsec[267] := 14 ;  taboffs[267] := 68096 ;  // $010A00
tabtrack[268] := 13 ;  tabsec[268] := 18 ;  taboffs[268] := 69120 ;  // $010E00
tabtrack[269] := 13 ;  tabsec[269] := 1 ;  taboffs[269] := 64768 ;  // $00FD00
tabtrack[270] := 13 ;  tabsec[270] := 5 ;  taboffs[270] := 65792 ;  // $010100
tabtrack[271] := 13 ;  tabsec[271] := 9 ;  taboffs[271] := 66816 ;  // $010500
tabtrack[272] := 13 ;  tabsec[272] := 13 ;  taboffs[272] := 67840 ;  // $010900
tabtrack[273] := 13 ;  tabsec[273] := 17 ;  taboffs[273] := 68864 ;  // $010D00
tabtrack[274] := 14 ;  tabsec[274] := 0 ;  taboffs[274] := 69888 ;  // $011100
tabtrack[275] := 14 ;  tabsec[275] := 4 ;  taboffs[275] := 70912 ;  // $011500
tabtrack[276] := 14 ;  tabsec[276] := 8 ;  taboffs[276] := 71936 ;  // $011900
tabtrack[277] := 14 ;  tabsec[277] := 12 ;  taboffs[277] := 72960 ;  // $011D00
tabtrack[278] := 14 ;  tabsec[278] := 16 ;  taboffs[278] := 73984 ;  // $012100
tabtrack[279] := 14 ;  tabsec[279] := 20 ;  taboffs[279] := 75008 ;  // $012500
tabtrack[280] := 14 ;  tabsec[280] := 3 ;  taboffs[280] := 70656 ;  // $011400
tabtrack[281] := 14 ;  tabsec[281] := 7 ;  taboffs[281] := 71680 ;  // $011800
tabtrack[282] := 14 ;  tabsec[282] := 11 ;  taboffs[282] := 72704 ;  // $011C00
tabtrack[283] := 14 ;  tabsec[283] := 15 ;  taboffs[283] := 73728 ;  // $012000
tabtrack[284] := 14 ;  tabsec[284] := 19 ;  taboffs[284] := 74752 ;  // $012400
tabtrack[285] := 14 ;  tabsec[285] := 2 ;  taboffs[285] := 70400 ;  // $011300
tabtrack[286] := 14 ;  tabsec[286] := 6 ;  taboffs[286] := 71424 ;  // $011700
tabtrack[287] := 14 ;  tabsec[287] := 10 ;  taboffs[287] := 72448 ;  // $011B00
tabtrack[288] := 14 ;  tabsec[288] := 14 ;  taboffs[288] := 73472 ;  // $011F00
tabtrack[289] := 14 ;  tabsec[289] := 18 ;  taboffs[289] := 74496 ;  // $012300
tabtrack[290] := 14 ;  tabsec[290] := 1 ;  taboffs[290] := 70144 ;  // $011200
tabtrack[291] := 14 ;  tabsec[291] := 5 ;  taboffs[291] := 71168 ;  // $011600
tabtrack[292] := 14 ;  tabsec[292] := 9 ;  taboffs[292] := 72192 ;  // $011A00
tabtrack[293] := 14 ;  tabsec[293] := 13 ;  taboffs[293] := 73216 ;  // $011E00
tabtrack[294] := 14 ;  tabsec[294] := 17 ;  taboffs[294] := 74240 ;  // $012200
tabtrack[295] := 15 ;  tabsec[295] := 0 ;  taboffs[295] := 75264 ;  // $012600
tabtrack[296] := 15 ;  tabsec[296] := 4 ;  taboffs[296] := 76288 ;  // $012A00
tabtrack[297] := 15 ;  tabsec[297] := 8 ;  taboffs[297] := 77312 ;  // $012E00
tabtrack[298] := 15 ;  tabsec[298] := 12 ;  taboffs[298] := 78336 ;  // $013200
tabtrack[299] := 15 ;  tabsec[299] := 16 ;  taboffs[299] := 79360 ;  // $013600
tabtrack[300] := 15 ;  tabsec[300] := 20 ;  taboffs[300] := 80384 ;  // $013A00
tabtrack[301] := 15 ;  tabsec[301] := 3 ;  taboffs[301] := 76032 ;  // $012900
tabtrack[302] := 15 ;  tabsec[302] := 7 ;  taboffs[302] := 77056 ;  // $012D00
tabtrack[303] := 15 ;  tabsec[303] := 11 ;  taboffs[303] := 78080 ;  // $013100
tabtrack[304] := 15 ;  tabsec[304] := 15 ;  taboffs[304] := 79104 ;  // $013500
tabtrack[305] := 15 ;  tabsec[305] := 19 ;  taboffs[305] := 80128 ;  // $013900
tabtrack[306] := 15 ;  tabsec[306] := 2 ;  taboffs[306] := 75776 ;  // $012800
tabtrack[307] := 15 ;  tabsec[307] := 6 ;  taboffs[307] := 76800 ;  // $012C00
tabtrack[308] := 15 ;  tabsec[308] := 10 ;  taboffs[308] := 77824 ;  // $013000
tabtrack[309] := 15 ;  tabsec[309] := 14 ;  taboffs[309] := 78848 ;  // $013400
tabtrack[310] := 15 ;  tabsec[310] := 18 ;  taboffs[310] := 79872 ;  // $013800
tabtrack[311] := 15 ;  tabsec[311] := 1 ;  taboffs[311] := 75520 ;  // $012700
tabtrack[312] := 15 ;  tabsec[312] := 5 ;  taboffs[312] := 76544 ;  // $012B00
tabtrack[313] := 15 ;  tabsec[313] := 9 ;  taboffs[313] := 77568 ;  // $012F00
tabtrack[314] := 15 ;  tabsec[314] := 13 ;  taboffs[314] := 78592 ;  // $013300
tabtrack[315] := 15 ;  tabsec[315] := 17 ;  taboffs[315] := 79616 ;  // $013700
tabtrack[316] := 16 ;  tabsec[316] := 0 ;  taboffs[316] := 80640 ;  // $013B00
tabtrack[317] := 16 ;  tabsec[317] := 4 ;  taboffs[317] := 81664 ;  // $013F00
tabtrack[318] := 16 ;  tabsec[318] := 8 ;  taboffs[318] := 82688 ;  // $014300
tabtrack[319] := 16 ;  tabsec[319] := 12 ;  taboffs[319] := 83712 ;  // $014700
tabtrack[320] := 16 ;  tabsec[320] := 16 ;  taboffs[320] := 84736 ;  // $014B00
tabtrack[321] := 16 ;  tabsec[321] := 20 ;  taboffs[321] := 85760 ;  // $014F00
tabtrack[322] := 16 ;  tabsec[322] := 3 ;  taboffs[322] := 81408 ;  // $013E00
tabtrack[323] := 16 ;  tabsec[323] := 7 ;  taboffs[323] := 82432 ;  // $014200
tabtrack[324] := 16 ;  tabsec[324] := 11 ;  taboffs[324] := 83456 ;  // $014600
tabtrack[325] := 16 ;  tabsec[325] := 15 ;  taboffs[325] := 84480 ;  // $014A00
tabtrack[326] := 16 ;  tabsec[326] := 19 ;  taboffs[326] := 85504 ;  // $014E00
tabtrack[327] := 16 ;  tabsec[327] := 2 ;  taboffs[327] := 81152 ;  // $013D00
tabtrack[328] := 16 ;  tabsec[328] := 6 ;  taboffs[328] := 82176 ;  // $014100
tabtrack[329] := 16 ;  tabsec[329] := 10 ;  taboffs[329] := 83200 ;  // $014500
tabtrack[330] := 16 ;  tabsec[330] := 14 ;  taboffs[330] := 84224 ;  // $014900
tabtrack[331] := 16 ;  tabsec[331] := 18 ;  taboffs[331] := 85248 ;  // $014D00
tabtrack[332] := 16 ;  tabsec[332] := 1 ;  taboffs[332] := 80896 ;  // $013C00
tabtrack[333] := 16 ;  tabsec[333] := 5 ;  taboffs[333] := 81920 ;  // $014000
tabtrack[334] := 16 ;  tabsec[334] := 9 ;  taboffs[334] := 82944 ;  // $014400
tabtrack[335] := 16 ;  tabsec[335] := 13 ;  taboffs[335] := 83968 ;  // $014800
tabtrack[336] := 16 ;  tabsec[336] := 17 ;  taboffs[336] := 84992 ;  // $014C00
tabtrack[337] := 17 ;  tabsec[337] := 0 ;  taboffs[337] := 86016 ;  // $015000
tabtrack[338] := 17 ;  tabsec[338] := 4 ;  taboffs[338] := 87040 ;  // $015400
tabtrack[339] := 17 ;  tabsec[339] := 8 ;  taboffs[339] := 88064 ;  // $015800
tabtrack[340] := 17 ;  tabsec[340] := 12 ;  taboffs[340] := 89088 ;  // $015C00
tabtrack[341] := 17 ;  tabsec[341] := 16 ;  taboffs[341] := 90112 ;  // $016000
tabtrack[342] := 17 ;  tabsec[342] := 20 ;  taboffs[342] := 91136 ;  // $016400
tabtrack[343] := 17 ;  tabsec[343] := 3 ;  taboffs[343] := 86784 ;  // $015300
tabtrack[344] := 17 ;  tabsec[344] := 7 ;  taboffs[344] := 87808 ;  // $015700
tabtrack[345] := 17 ;  tabsec[345] := 11 ;  taboffs[345] := 88832 ;  // $015B00
tabtrack[346] := 17 ;  tabsec[346] := 15 ;  taboffs[346] := 89856 ;  // $015F00
tabtrack[347] := 17 ;  tabsec[347] := 19 ;  taboffs[347] := 90880 ;  // $016300
tabtrack[348] := 17 ;  tabsec[348] := 2 ;  taboffs[348] := 86528 ;  // $015200
tabtrack[349] := 17 ;  tabsec[349] := 6 ;  taboffs[349] := 87552 ;  // $015600
tabtrack[350] := 17 ;  tabsec[350] := 10 ;  taboffs[350] := 88576 ;  // $015A00
tabtrack[351] := 17 ;  tabsec[351] := 14 ;  taboffs[351] := 89600 ;  // $015E00
tabtrack[352] := 17 ;  tabsec[352] := 18 ;  taboffs[352] := 90624 ;  // $016200
tabtrack[353] := 17 ;  tabsec[353] := 1 ;  taboffs[353] := 86272 ;  // $015100
tabtrack[354] := 17 ;  tabsec[354] := 5 ;  taboffs[354] := 87296 ;  // $015500
tabtrack[355] := 17 ;  tabsec[355] := 9 ;  taboffs[355] := 88320 ;  // $015900
tabtrack[356] := 17 ;  tabsec[356] := 13 ;  taboffs[356] := 89344 ;  // $015D00
tabtrack[357] := 17 ;  tabsec[357] := 17 ;  taboffs[357] := 90368 ;  // $016100
tabtrack[358] := 19 ;  tabsec[358] := 0 ;  taboffs[358] := 96256 ;  // $017800
tabtrack[359] := 19 ;  tabsec[359] := 4 ;  taboffs[359] := 97280 ;  // $017C00
tabtrack[360] := 19 ;  tabsec[360] := 8 ;  taboffs[360] := 98304 ;  // $018000
tabtrack[361] := 19 ;  tabsec[361] := 12 ;  taboffs[361] := 99328 ;  // $018400
tabtrack[362] := 19 ;  tabsec[362] := 16 ;  taboffs[362] := 100352 ;  // $018800
tabtrack[363] := 19 ;  tabsec[363] := 1 ;  taboffs[363] := 96512 ;  // $017900
tabtrack[364] := 19 ;  tabsec[364] := 5 ;  taboffs[364] := 97536 ;  // $017D00
tabtrack[365] := 19 ;  tabsec[365] := 9 ;  taboffs[365] := 98560 ;  // $018100
tabtrack[366] := 19 ;  tabsec[366] := 13 ;  taboffs[366] := 99584 ;  // $018500
tabtrack[367] := 19 ;  tabsec[367] := 17 ;  taboffs[367] := 100608 ;  // $018900
tabtrack[368] := 19 ;  tabsec[368] := 2 ;  taboffs[368] := 96768 ;  // $017A00
tabtrack[369] := 19 ;  tabsec[369] := 6 ;  taboffs[369] := 97792 ;  // $017E00
tabtrack[370] := 19 ;  tabsec[370] := 10 ;  taboffs[370] := 98816 ;  // $018200
tabtrack[371] := 19 ;  tabsec[371] := 14 ;  taboffs[371] := 99840 ;  // $018600
tabtrack[372] := 19 ;  tabsec[372] := 18 ;  taboffs[372] := 100864 ;  // $018A00
tabtrack[373] := 19 ;  tabsec[373] := 3 ;  taboffs[373] := 97024 ;  // $017B00
tabtrack[374] := 19 ;  tabsec[374] := 7 ;  taboffs[374] := 98048 ;  // $017F00
tabtrack[375] := 19 ;  tabsec[375] := 11 ;  taboffs[375] := 99072 ;  // $018300
tabtrack[376] := 19 ;  tabsec[376] := 15 ;  taboffs[376] := 100096 ;  // $018700
tabtrack[377] := 20 ;  tabsec[377] := 0 ;  taboffs[377] := 101120 ;  // $018B00
tabtrack[378] := 20 ;  tabsec[378] := 4 ;  taboffs[378] := 102144 ;  // $018F00
tabtrack[379] := 20 ;  tabsec[379] := 8 ;  taboffs[379] := 103168 ;  // $019300
tabtrack[380] := 20 ;  tabsec[380] := 12 ;  taboffs[380] := 104192 ;  // $019700
tabtrack[381] := 20 ;  tabsec[381] := 16 ;  taboffs[381] := 105216 ;  // $019B00
tabtrack[382] := 20 ;  tabsec[382] := 1 ;  taboffs[382] := 101376 ;  // $018C00
tabtrack[383] := 20 ;  tabsec[383] := 5 ;  taboffs[383] := 102400 ;  // $019000
tabtrack[384] := 20 ;  tabsec[384] := 9 ;  taboffs[384] := 103424 ;  // $019400
tabtrack[385] := 20 ;  tabsec[385] := 13 ;  taboffs[385] := 104448 ;  // $019800
tabtrack[386] := 20 ;  tabsec[386] := 17 ;  taboffs[386] := 105472 ;  // $019C00
tabtrack[387] := 20 ;  tabsec[387] := 2 ;  taboffs[387] := 101632 ;  // $018D00
tabtrack[388] := 20 ;  tabsec[388] := 6 ;  taboffs[388] := 102656 ;  // $019100
tabtrack[389] := 20 ;  tabsec[389] := 10 ;  taboffs[389] := 103680 ;  // $019500
tabtrack[390] := 20 ;  tabsec[390] := 14 ;  taboffs[390] := 104704 ;  // $019900
tabtrack[391] := 20 ;  tabsec[391] := 18 ;  taboffs[391] := 105728 ;  // $019D00
tabtrack[392] := 20 ;  tabsec[392] := 3 ;  taboffs[392] := 101888 ;  // $018E00
tabtrack[393] := 20 ;  tabsec[393] := 7 ;  taboffs[393] := 102912 ;  // $019200
tabtrack[394] := 20 ;  tabsec[394] := 11 ;  taboffs[394] := 103936 ;  // $019600
tabtrack[395] := 20 ;  tabsec[395] := 15 ;  taboffs[395] := 104960 ;  // $019A00
tabtrack[396] := 21 ;  tabsec[396] := 0 ;  taboffs[396] := 105984 ;  // $019E00
tabtrack[397] := 21 ;  tabsec[397] := 4 ;  taboffs[397] := 107008 ;  // $01A200
tabtrack[398] := 21 ;  tabsec[398] := 8 ;  taboffs[398] := 108032 ;  // $01A600
tabtrack[399] := 21 ;  tabsec[399] := 12 ;  taboffs[399] := 109056 ;  // $01AA00
tabtrack[400] := 21 ;  tabsec[400] := 16 ;  taboffs[400] := 110080 ;  // $01AE00
tabtrack[401] := 21 ;  tabsec[401] := 1 ;  taboffs[401] := 106240 ;  // $019F00
tabtrack[402] := 21 ;  tabsec[402] := 5 ;  taboffs[402] := 107264 ;  // $01A300
tabtrack[403] := 21 ;  tabsec[403] := 9 ;  taboffs[403] := 108288 ;  // $01A700
tabtrack[404] := 21 ;  tabsec[404] := 13 ;  taboffs[404] := 109312 ;  // $01AB00
tabtrack[405] := 21 ;  tabsec[405] := 17 ;  taboffs[405] := 110336 ;  // $01AF00
tabtrack[406] := 21 ;  tabsec[406] := 2 ;  taboffs[406] := 106496 ;  // $01A000
tabtrack[407] := 21 ;  tabsec[407] := 6 ;  taboffs[407] := 107520 ;  // $01A400
tabtrack[408] := 21 ;  tabsec[408] := 10 ;  taboffs[408] := 108544 ;  // $01A800
tabtrack[409] := 21 ;  tabsec[409] := 14 ;  taboffs[409] := 109568 ;  // $01AC00
tabtrack[410] := 21 ;  tabsec[410] := 18 ;  taboffs[410] := 110592 ;  // $01B000
tabtrack[411] := 21 ;  tabsec[411] := 3 ;  taboffs[411] := 106752 ;  // $01A100
tabtrack[412] := 21 ;  tabsec[412] := 7 ;  taboffs[412] := 107776 ;  // $01A500
tabtrack[413] := 21 ;  tabsec[413] := 11 ;  taboffs[413] := 108800 ;  // $01A900
tabtrack[414] := 21 ;  tabsec[414] := 15 ;  taboffs[414] := 109824 ;  // $01AD00
tabtrack[415] := 22 ;  tabsec[415] := 0 ;  taboffs[415] := 110848 ;  // $01B100
tabtrack[416] := 22 ;  tabsec[416] := 4 ;  taboffs[416] := 111872 ;  // $01B500
tabtrack[417] := 22 ;  tabsec[417] := 8 ;  taboffs[417] := 112896 ;  // $01B900
tabtrack[418] := 22 ;  tabsec[418] := 12 ;  taboffs[418] := 113920 ;  // $01BD00
tabtrack[419] := 22 ;  tabsec[419] := 16 ;  taboffs[419] := 114944 ;  // $01C100
tabtrack[420] := 22 ;  tabsec[420] := 1 ;  taboffs[420] := 111104 ;  // $01B200
tabtrack[421] := 22 ;  tabsec[421] := 5 ;  taboffs[421] := 112128 ;  // $01B600
tabtrack[422] := 22 ;  tabsec[422] := 9 ;  taboffs[422] := 113152 ;  // $01BA00
tabtrack[423] := 22 ;  tabsec[423] := 13 ;  taboffs[423] := 114176 ;  // $01BE00
tabtrack[424] := 22 ;  tabsec[424] := 17 ;  taboffs[424] := 115200 ;  // $01C200
tabtrack[425] := 22 ;  tabsec[425] := 2 ;  taboffs[425] := 111360 ;  // $01B300
tabtrack[426] := 22 ;  tabsec[426] := 6 ;  taboffs[426] := 112384 ;  // $01B700
tabtrack[427] := 22 ;  tabsec[427] := 10 ;  taboffs[427] := 113408 ;  // $01BB00
tabtrack[428] := 22 ;  tabsec[428] := 14 ;  taboffs[428] := 114432 ;  // $01BF00
tabtrack[429] := 22 ;  tabsec[429] := 18 ;  taboffs[429] := 115456 ;  // $01C300
tabtrack[430] := 22 ;  tabsec[430] := 3 ;  taboffs[430] := 111616 ;  // $01B400
tabtrack[431] := 22 ;  tabsec[431] := 7 ;  taboffs[431] := 112640 ;  // $01B800
tabtrack[432] := 22 ;  tabsec[432] := 11 ;  taboffs[432] := 113664 ;  // $01BC00
tabtrack[433] := 22 ;  tabsec[433] := 15 ;  taboffs[433] := 114688 ;  // $01C000
tabtrack[434] := 23 ;  tabsec[434] := 0 ;  taboffs[434] := 115712 ;  // $01C400
tabtrack[435] := 23 ;  tabsec[435] := 4 ;  taboffs[435] := 116736 ;  // $01C800
tabtrack[436] := 23 ;  tabsec[436] := 8 ;  taboffs[436] := 117760 ;  // $01CC00
tabtrack[437] := 23 ;  tabsec[437] := 12 ;  taboffs[437] := 118784 ;  // $01D000
tabtrack[438] := 23 ;  tabsec[438] := 16 ;  taboffs[438] := 119808 ;  // $01D400
tabtrack[439] := 23 ;  tabsec[439] := 1 ;  taboffs[439] := 115968 ;  // $01C500
tabtrack[440] := 23 ;  tabsec[440] := 5 ;  taboffs[440] := 116992 ;  // $01C900
tabtrack[441] := 23 ;  tabsec[441] := 9 ;  taboffs[441] := 118016 ;  // $01CD00
tabtrack[442] := 23 ;  tabsec[442] := 13 ;  taboffs[442] := 119040 ;  // $01D100
tabtrack[443] := 23 ;  tabsec[443] := 17 ;  taboffs[443] := 120064 ;  // $01D500
tabtrack[444] := 23 ;  tabsec[444] := 2 ;  taboffs[444] := 116224 ;  // $01C600
tabtrack[445] := 23 ;  tabsec[445] := 6 ;  taboffs[445] := 117248 ;  // $01CA00
tabtrack[446] := 23 ;  tabsec[446] := 10 ;  taboffs[446] := 118272 ;  // $01CE00
tabtrack[447] := 23 ;  tabsec[447] := 14 ;  taboffs[447] := 119296 ;  // $01D200
tabtrack[448] := 23 ;  tabsec[448] := 18 ;  taboffs[448] := 120320 ;  // $01D600
tabtrack[449] := 23 ;  tabsec[449] := 3 ;  taboffs[449] := 116480 ;  // $01C700
tabtrack[450] := 23 ;  tabsec[450] := 7 ;  taboffs[450] := 117504 ;  // $01CB00
tabtrack[451] := 23 ;  tabsec[451] := 11 ;  taboffs[451] := 118528 ;  // $01CF00
tabtrack[452] := 23 ;  tabsec[452] := 15 ;  taboffs[452] := 119552 ;  // $01D300
tabtrack[453] := 24 ;  tabsec[453] := 0 ;  taboffs[453] := 120576 ;  // $01D700
tabtrack[454] := 24 ;  tabsec[454] := 4 ;  taboffs[454] := 121600 ;  // $01DB00
tabtrack[455] := 24 ;  tabsec[455] := 8 ;  taboffs[455] := 122624 ;  // $01DF00
tabtrack[456] := 24 ;  tabsec[456] := 12 ;  taboffs[456] := 123648 ;  // $01E300
tabtrack[457] := 24 ;  tabsec[457] := 16 ;  taboffs[457] := 124672 ;  // $01E700
tabtrack[458] := 24 ;  tabsec[458] := 1 ;  taboffs[458] := 120832 ;  // $01D800
tabtrack[459] := 24 ;  tabsec[459] := 5 ;  taboffs[459] := 121856 ;  // $01DC00
tabtrack[460] := 24 ;  tabsec[460] := 9 ;  taboffs[460] := 122880 ;  // $01E000
tabtrack[461] := 24 ;  tabsec[461] := 13 ;  taboffs[461] := 123904 ;  // $01E400
tabtrack[462] := 24 ;  tabsec[462] := 17 ;  taboffs[462] := 124928 ;  // $01E800
tabtrack[463] := 24 ;  tabsec[463] := 2 ;  taboffs[463] := 121088 ;  // $01D900
tabtrack[464] := 24 ;  tabsec[464] := 6 ;  taboffs[464] := 122112 ;  // $01DD00
tabtrack[465] := 24 ;  tabsec[465] := 10 ;  taboffs[465] := 123136 ;  // $01E100
tabtrack[466] := 24 ;  tabsec[466] := 14 ;  taboffs[466] := 124160 ;  // $01E500
tabtrack[467] := 24 ;  tabsec[467] := 18 ;  taboffs[467] := 125184 ;  // $01E900
tabtrack[468] := 24 ;  tabsec[468] := 3 ;  taboffs[468] := 121344 ;  // $01DA00
tabtrack[469] := 24 ;  tabsec[469] := 7 ;  taboffs[469] := 122368 ;  // $01DE00
tabtrack[470] := 24 ;  tabsec[470] := 11 ;  taboffs[470] := 123392 ;  // $01E200
tabtrack[471] := 24 ;  tabsec[471] := 15 ;  taboffs[471] := 124416 ;  // $01E600
tabtrack[472] := 25 ;  tabsec[472] := 0 ;  taboffs[472] := 125440 ;  // $01EA00
tabtrack[473] := 25 ;  tabsec[473] := 4 ;  taboffs[473] := 126464 ;  // $01EE00
tabtrack[474] := 25 ;  tabsec[474] := 8 ;  taboffs[474] := 127488 ;  // $01F200
tabtrack[475] := 25 ;  tabsec[475] := 12 ;  taboffs[475] := 128512 ;  // $01F600
tabtrack[476] := 25 ;  tabsec[476] := 16 ;  taboffs[476] := 129536 ;  // $01FA00
tabtrack[477] := 25 ;  tabsec[477] := 2 ;  taboffs[477] := 125952 ;  // $01EC00
tabtrack[478] := 25 ;  tabsec[478] := 6 ;  taboffs[478] := 126976 ;  // $01F000
tabtrack[479] := 25 ;  tabsec[479] := 10 ;  taboffs[479] := 128000 ;  // $01F400
tabtrack[480] := 25 ;  tabsec[480] := 14 ;  taboffs[480] := 129024 ;  // $01F800
tabtrack[481] := 25 ;  tabsec[481] := 1 ;  taboffs[481] := 125696 ;  // $01EB00
tabtrack[482] := 25 ;  tabsec[482] := 5 ;  taboffs[482] := 126720 ;  // $01EF00
tabtrack[483] := 25 ;  tabsec[483] := 9 ;  taboffs[483] := 127744 ;  // $01F300
tabtrack[484] := 25 ;  tabsec[484] := 13 ;  taboffs[484] := 128768 ;  // $01F700
tabtrack[485] := 25 ;  tabsec[485] := 17 ;  taboffs[485] := 129792 ;  // $01FB00
tabtrack[486] := 25 ;  tabsec[486] := 3 ;  taboffs[486] := 126208 ;  // $01ED00
tabtrack[487] := 25 ;  tabsec[487] := 7 ;  taboffs[487] := 127232 ;  // $01F100
tabtrack[488] := 25 ;  tabsec[488] := 11 ;  taboffs[488] := 128256 ;  // $01F500
tabtrack[489] := 25 ;  tabsec[489] := 15 ;  taboffs[489] := 129280 ;  // $01F900
tabtrack[490] := 26 ;  tabsec[490] := 0 ;  taboffs[490] := 130048 ;  // $01FC00
tabtrack[491] := 26 ;  tabsec[491] := 4 ;  taboffs[491] := 131072 ;  // $020000
tabtrack[492] := 26 ;  tabsec[492] := 8 ;  taboffs[492] := 132096 ;  // $020400
tabtrack[493] := 26 ;  tabsec[493] := 12 ;  taboffs[493] := 133120 ;  // $020800
tabtrack[494] := 26 ;  tabsec[494] := 16 ;  taboffs[494] := 134144 ;  // $020C00
tabtrack[495] := 26 ;  tabsec[495] := 2 ;  taboffs[495] := 130560 ;  // $01FE00
tabtrack[496] := 26 ;  tabsec[496] := 6 ;  taboffs[496] := 131584 ;  // $020200
tabtrack[497] := 26 ;  tabsec[497] := 10 ;  taboffs[497] := 132608 ;  // $020600
tabtrack[498] := 26 ;  tabsec[498] := 14 ;  taboffs[498] := 133632 ;  // $020A00
tabtrack[499] := 26 ;  tabsec[499] := 1 ;  taboffs[499] := 130304 ;  // $01FD00
tabtrack[500] := 26 ;  tabsec[500] := 5 ;  taboffs[500] := 131328 ;  // $020100
tabtrack[501] := 26 ;  tabsec[501] := 9 ;  taboffs[501] := 132352 ;  // $020500
tabtrack[502] := 26 ;  tabsec[502] := 13 ;  taboffs[502] := 133376 ;  // $020900
tabtrack[503] := 26 ;  tabsec[503] := 17 ;  taboffs[503] := 134400 ;  // $020D00
tabtrack[504] := 26 ;  tabsec[504] := 3 ;  taboffs[504] := 130816 ;  // $01FF00
tabtrack[505] := 26 ;  tabsec[505] := 7 ;  taboffs[505] := 131840 ;  // $020300
tabtrack[506] := 26 ;  tabsec[506] := 11 ;  taboffs[506] := 132864 ;  // $020700
tabtrack[507] := 26 ;  tabsec[507] := 15 ;  taboffs[507] := 133888 ;  // $020B00
tabtrack[508] := 27 ;  tabsec[508] := 0 ;  taboffs[508] := 134656 ;  // $020E00
tabtrack[509] := 27 ;  tabsec[509] := 4 ;  taboffs[509] := 135680 ;  // $021200
tabtrack[510] := 27 ;  tabsec[510] := 8 ;  taboffs[510] := 136704 ;  // $021600
tabtrack[511] := 27 ;  tabsec[511] := 12 ;  taboffs[511] := 137728 ;  // $021A00
tabtrack[512] := 27 ;  tabsec[512] := 16 ;  taboffs[512] := 138752 ;  // $021E00
tabtrack[513] := 27 ;  tabsec[513] := 2 ;  taboffs[513] := 135168 ;  // $021000
tabtrack[514] := 27 ;  tabsec[514] := 6 ;  taboffs[514] := 136192 ;  // $021400
tabtrack[515] := 27 ;  tabsec[515] := 10 ;  taboffs[515] := 137216 ;  // $021800
tabtrack[516] := 27 ;  tabsec[516] := 14 ;  taboffs[516] := 138240 ;  // $021C00
tabtrack[517] := 27 ;  tabsec[517] := 1 ;  taboffs[517] := 134912 ;  // $020F00
tabtrack[518] := 27 ;  tabsec[518] := 5 ;  taboffs[518] := 135936 ;  // $021300
tabtrack[519] := 27 ;  tabsec[519] := 9 ;  taboffs[519] := 136960 ;  // $021700
tabtrack[520] := 27 ;  tabsec[520] := 13 ;  taboffs[520] := 137984 ;  // $021B00
tabtrack[521] := 27 ;  tabsec[521] := 17 ;  taboffs[521] := 139008 ;  // $021F00
tabtrack[522] := 27 ;  tabsec[522] := 3 ;  taboffs[522] := 135424 ;  // $021100
tabtrack[523] := 27 ;  tabsec[523] := 7 ;  taboffs[523] := 136448 ;  // $021500
tabtrack[524] := 27 ;  tabsec[524] := 11 ;  taboffs[524] := 137472 ;  // $021900
tabtrack[525] := 27 ;  tabsec[525] := 15 ;  taboffs[525] := 138496 ;  // $021D00
tabtrack[526] := 28 ;  tabsec[526] := 0 ;  taboffs[526] := 139264 ;  // $022000
tabtrack[527] := 28 ;  tabsec[527] := 4 ;  taboffs[527] := 140288 ;  // $022400
tabtrack[528] := 28 ;  tabsec[528] := 8 ;  taboffs[528] := 141312 ;  // $022800
tabtrack[529] := 28 ;  tabsec[529] := 12 ;  taboffs[529] := 142336 ;  // $022C00
tabtrack[530] := 28 ;  tabsec[530] := 16 ;  taboffs[530] := 143360 ;  // $023000
tabtrack[531] := 28 ;  tabsec[531] := 2 ;  taboffs[531] := 139776 ;  // $022200
tabtrack[532] := 28 ;  tabsec[532] := 6 ;  taboffs[532] := 140800 ;  // $022600
tabtrack[533] := 28 ;  tabsec[533] := 10 ;  taboffs[533] := 141824 ;  // $022A00
tabtrack[534] := 28 ;  tabsec[534] := 14 ;  taboffs[534] := 142848 ;  // $022E00
tabtrack[535] := 28 ;  tabsec[535] := 1 ;  taboffs[535] := 139520 ;  // $022100
tabtrack[536] := 28 ;  tabsec[536] := 5 ;  taboffs[536] := 140544 ;  // $022500
tabtrack[537] := 28 ;  tabsec[537] := 9 ;  taboffs[537] := 141568 ;  // $022900
tabtrack[538] := 28 ;  tabsec[538] := 13 ;  taboffs[538] := 142592 ;  // $022D00
tabtrack[539] := 28 ;  tabsec[539] := 17 ;  taboffs[539] := 143616 ;  // $023100
tabtrack[540] := 28 ;  tabsec[540] := 3 ;  taboffs[540] := 140032 ;  // $022300
tabtrack[541] := 28 ;  tabsec[541] := 7 ;  taboffs[541] := 141056 ;  // $022700
tabtrack[542] := 28 ;  tabsec[542] := 11 ;  taboffs[542] := 142080 ;  // $022B00
tabtrack[543] := 28 ;  tabsec[543] := 15 ;  taboffs[543] := 143104 ;  // $022F00
tabtrack[544] := 29 ;  tabsec[544] := 0 ;  taboffs[544] := 143872 ;  // $023200
tabtrack[545] := 29 ;  tabsec[545] := 4 ;  taboffs[545] := 144896 ;  // $023600
tabtrack[546] := 29 ;  tabsec[546] := 8 ;  taboffs[546] := 145920 ;  // $023A00
tabtrack[547] := 29 ;  tabsec[547] := 12 ;  taboffs[547] := 146944 ;  // $023E00
tabtrack[548] := 29 ;  tabsec[548] := 16 ;  taboffs[548] := 147968 ;  // $024200
tabtrack[549] := 29 ;  tabsec[549] := 2 ;  taboffs[549] := 144384 ;  // $023400
tabtrack[550] := 29 ;  tabsec[550] := 6 ;  taboffs[550] := 145408 ;  // $023800
tabtrack[551] := 29 ;  tabsec[551] := 10 ;  taboffs[551] := 146432 ;  // $023C00
tabtrack[552] := 29 ;  tabsec[552] := 14 ;  taboffs[552] := 147456 ;  // $024000
tabtrack[553] := 29 ;  tabsec[553] := 1 ;  taboffs[553] := 144128 ;  // $023300
tabtrack[554] := 29 ;  tabsec[554] := 5 ;  taboffs[554] := 145152 ;  // $023700
tabtrack[555] := 29 ;  tabsec[555] := 9 ;  taboffs[555] := 146176 ;  // $023B00
tabtrack[556] := 29 ;  tabsec[556] := 13 ;  taboffs[556] := 147200 ;  // $023F00
tabtrack[557] := 29 ;  tabsec[557] := 17 ;  taboffs[557] := 148224 ;  // $024300
tabtrack[558] := 29 ;  tabsec[558] := 3 ;  taboffs[558] := 144640 ;  // $023500
tabtrack[559] := 29 ;  tabsec[559] := 7 ;  taboffs[559] := 145664 ;  // $023900
tabtrack[560] := 29 ;  tabsec[560] := 11 ;  taboffs[560] := 146688 ;  // $023D00
tabtrack[561] := 29 ;  tabsec[561] := 15 ;  taboffs[561] := 147712 ;  // $024100
tabtrack[562] := 30 ;  tabsec[562] := 0 ;  taboffs[562] := 148480 ;  // $024400
tabtrack[563] := 30 ;  tabsec[563] := 4 ;  taboffs[563] := 149504 ;  // $024800
tabtrack[564] := 30 ;  tabsec[564] := 8 ;  taboffs[564] := 150528 ;  // $024C00
tabtrack[565] := 30 ;  tabsec[565] := 12 ;  taboffs[565] := 151552 ;  // $025000
tabtrack[566] := 30 ;  tabsec[566] := 16 ;  taboffs[566] := 152576 ;  // $025400
tabtrack[567] := 30 ;  tabsec[567] := 2 ;  taboffs[567] := 148992 ;  // $024600
tabtrack[568] := 30 ;  tabsec[568] := 6 ;  taboffs[568] := 150016 ;  // $024A00
tabtrack[569] := 30 ;  tabsec[569] := 10 ;  taboffs[569] := 151040 ;  // $024E00
tabtrack[570] := 30 ;  tabsec[570] := 14 ;  taboffs[570] := 152064 ;  // $025200
tabtrack[571] := 30 ;  tabsec[571] := 1 ;  taboffs[571] := 148736 ;  // $024500
tabtrack[572] := 30 ;  tabsec[572] := 5 ;  taboffs[572] := 149760 ;  // $024900
tabtrack[573] := 30 ;  tabsec[573] := 9 ;  taboffs[573] := 150784 ;  // $024D00
tabtrack[574] := 30 ;  tabsec[574] := 13 ;  taboffs[574] := 151808 ;  // $025100
tabtrack[575] := 30 ;  tabsec[575] := 17 ;  taboffs[575] := 152832 ;  // $025500
tabtrack[576] := 30 ;  tabsec[576] := 3 ;  taboffs[576] := 149248 ;  // $024700
tabtrack[577] := 30 ;  tabsec[577] := 7 ;  taboffs[577] := 150272 ;  // $024B00
tabtrack[578] := 30 ;  tabsec[578] := 11 ;  taboffs[578] := 151296 ;  // $024F00
tabtrack[579] := 30 ;  tabsec[579] := 15 ;  taboffs[579] := 152320 ;  // $025300
tabtrack[580] := 31 ;  tabsec[580] := 0 ;  taboffs[580] := 153088 ;  // $025600
tabtrack[581] := 31 ;  tabsec[581] := 4 ;  taboffs[581] := 154112 ;  // $025A00
tabtrack[582] := 31 ;  tabsec[582] := 8 ;  taboffs[582] := 155136 ;  // $025E00
tabtrack[583] := 31 ;  tabsec[583] := 12 ;  taboffs[583] := 156160 ;  // $026200
tabtrack[584] := 31 ;  tabsec[584] := 16 ;  taboffs[584] := 157184 ;  // $026600
tabtrack[585] := 31 ;  tabsec[585] := 3 ;  taboffs[585] := 153856 ;  // $025900
tabtrack[586] := 31 ;  tabsec[586] := 7 ;  taboffs[586] := 154880 ;  // $025D00
tabtrack[587] := 31 ;  tabsec[587] := 11 ;  taboffs[587] := 155904 ;  // $026100
tabtrack[588] := 31 ;  tabsec[588] := 15 ;  taboffs[588] := 156928 ;  // $026500
tabtrack[589] := 31 ;  tabsec[589] := 2 ;  taboffs[589] := 153600 ;  // $025800
tabtrack[590] := 31 ;  tabsec[590] := 6 ;  taboffs[590] := 154624 ;  // $025C00
tabtrack[591] := 31 ;  tabsec[591] := 10 ;  taboffs[591] := 155648 ;  // $026000
tabtrack[592] := 31 ;  tabsec[592] := 14 ;  taboffs[592] := 156672 ;  // $026400
tabtrack[593] := 31 ;  tabsec[593] := 1 ;  taboffs[593] := 153344 ;  // $025700
tabtrack[594] := 31 ;  tabsec[594] := 5 ;  taboffs[594] := 154368 ;  // $025B00
tabtrack[595] := 31 ;  tabsec[595] := 9 ;  taboffs[595] := 155392 ;  // $025F00
tabtrack[596] := 31 ;  tabsec[596] := 13 ;  taboffs[596] := 156416 ;  // $026300
tabtrack[597] := 32 ;  tabsec[597] := 0 ;  taboffs[597] := 157440 ;  // $026700
tabtrack[598] := 32 ;  tabsec[598] := 4 ;  taboffs[598] := 158464 ;  // $026B00
tabtrack[599] := 32 ;  tabsec[599] := 8 ;  taboffs[599] := 159488 ;  // $026F00
tabtrack[600] := 32 ;  tabsec[600] := 12 ;  taboffs[600] := 160512 ;  // $027300
tabtrack[601] := 32 ;  tabsec[601] := 16 ;  taboffs[601] := 161536 ;  // $027700
tabtrack[602] := 32 ;  tabsec[602] := 3 ;  taboffs[602] := 158208 ;  // $026A00
tabtrack[603] := 32 ;  tabsec[603] := 7 ;  taboffs[603] := 159232 ;  // $026E00
tabtrack[604] := 32 ;  tabsec[604] := 11 ;  taboffs[604] := 160256 ;  // $027200
tabtrack[605] := 32 ;  tabsec[605] := 15 ;  taboffs[605] := 161280 ;  // $027600
tabtrack[606] := 32 ;  tabsec[606] := 2 ;  taboffs[606] := 157952 ;  // $026900
tabtrack[607] := 32 ;  tabsec[607] := 6 ;  taboffs[607] := 158976 ;  // $026D00
tabtrack[608] := 32 ;  tabsec[608] := 10 ;  taboffs[608] := 160000 ;  // $027100
tabtrack[609] := 32 ;  tabsec[609] := 14 ;  taboffs[609] := 161024 ;  // $027500
tabtrack[610] := 32 ;  tabsec[610] := 1 ;  taboffs[610] := 157696 ;  // $026800
tabtrack[611] := 32 ;  tabsec[611] := 5 ;  taboffs[611] := 158720 ;  // $026C00
tabtrack[612] := 32 ;  tabsec[612] := 9 ;  taboffs[612] := 159744 ;  // $027000
tabtrack[613] := 32 ;  tabsec[613] := 13 ;  taboffs[613] := 160768 ;  // $027400
tabtrack[614] := 33 ;  tabsec[614] := 0 ;  taboffs[614] := 161792 ;  // $027800
tabtrack[615] := 33 ;  tabsec[615] := 4 ;  taboffs[615] := 162816 ;  // $027C00
tabtrack[616] := 33 ;  tabsec[616] := 8 ;  taboffs[616] := 163840 ;  // $028000
tabtrack[617] := 33 ;  tabsec[617] := 12 ;  taboffs[617] := 164864 ;  // $028400
tabtrack[618] := 33 ;  tabsec[618] := 16 ;  taboffs[618] := 165888 ;  // $028800
tabtrack[619] := 33 ;  tabsec[619] := 3 ;  taboffs[619] := 162560 ;  // $027B00
tabtrack[620] := 33 ;  tabsec[620] := 7 ;  taboffs[620] := 163584 ;  // $027F00
tabtrack[621] := 33 ;  tabsec[621] := 11 ;  taboffs[621] := 164608 ;  // $028300
tabtrack[622] := 33 ;  tabsec[622] := 15 ;  taboffs[622] := 165632 ;  // $028700
tabtrack[623] := 33 ;  tabsec[623] := 2 ;  taboffs[623] := 162304 ;  // $027A00
tabtrack[624] := 33 ;  tabsec[624] := 6 ;  taboffs[624] := 163328 ;  // $027E00
tabtrack[625] := 33 ;  tabsec[625] := 10 ;  taboffs[625] := 164352 ;  // $028200
tabtrack[626] := 33 ;  tabsec[626] := 14 ;  taboffs[626] := 165376 ;  // $028600
tabtrack[627] := 33 ;  tabsec[627] := 1 ;  taboffs[627] := 162048 ;  // $027900
tabtrack[628] := 33 ;  tabsec[628] := 5 ;  taboffs[628] := 163072 ;  // $027D00
tabtrack[629] := 33 ;  tabsec[629] := 9 ;  taboffs[629] := 164096 ;  // $028100
tabtrack[630] := 33 ;  tabsec[630] := 13 ;  taboffs[630] := 165120 ;  // $028500
tabtrack[631] := 34 ;  tabsec[631] := 0 ;  taboffs[631] := 166144 ;  // $028900
tabtrack[632] := 34 ;  tabsec[632] := 4 ;  taboffs[632] := 167168 ;  // $028D00
tabtrack[633] := 34 ;  tabsec[633] := 8 ;  taboffs[633] := 168192 ;  // $029100
tabtrack[634] := 34 ;  tabsec[634] := 12 ;  taboffs[634] := 169216 ;  // $029500
tabtrack[635] := 34 ;  tabsec[635] := 16 ;  taboffs[635] := 170240 ;  // $029900
tabtrack[636] := 34 ;  tabsec[636] := 3 ;  taboffs[636] := 166912 ;  // $028C00
tabtrack[637] := 34 ;  tabsec[637] := 7 ;  taboffs[637] := 167936 ;  // $029000
tabtrack[638] := 34 ;  tabsec[638] := 11 ;  taboffs[638] := 168960 ;  // $029400
tabtrack[639] := 34 ;  tabsec[639] := 15 ;  taboffs[639] := 169984 ;  // $029800
tabtrack[640] := 34 ;  tabsec[640] := 2 ;  taboffs[640] := 166656 ;  // $028B00
tabtrack[641] := 34 ;  tabsec[641] := 6 ;  taboffs[641] := 167680 ;  // $028F00
tabtrack[642] := 34 ;  tabsec[642] := 10 ;  taboffs[642] := 168704 ;  // $029300
tabtrack[643] := 34 ;  tabsec[643] := 14 ;  taboffs[643] := 169728 ;  // $029700
tabtrack[644] := 34 ;  tabsec[644] := 1 ;  taboffs[644] := 166400 ;  // $028A00
tabtrack[645] := 34 ;  tabsec[645] := 5 ;  taboffs[645] := 167424 ;  // $028E00
tabtrack[646] := 34 ;  tabsec[646] := 9 ;  taboffs[646] := 168448 ;  // $029200
tabtrack[647] := 34 ;  tabsec[647] := 13 ;  taboffs[647] := 169472 ;  // $029600
tabtrack[648] := 35 ;  tabsec[648] := 0 ;  taboffs[648] := 170496 ;  // $029A00
tabtrack[649] := 35 ;  tabsec[649] := 4 ;  taboffs[649] := 171520 ;  // $029E00
tabtrack[650] := 35 ;  tabsec[650] := 8 ;  taboffs[650] := 172544 ;  // $02A200
tabtrack[651] := 35 ;  tabsec[651] := 12 ;  taboffs[651] := 173568 ;  // $02A600
tabtrack[652] := 35 ;  tabsec[652] := 16 ;  taboffs[652] := 174592 ;  // $02AA00
tabtrack[653] := 35 ;  tabsec[653] := 3 ;  taboffs[653] := 171264 ;  // $029D00
tabtrack[654] := 35 ;  tabsec[654] := 7 ;  taboffs[654] := 172288 ;  // $02A100
tabtrack[655] := 35 ;  tabsec[655] := 11 ;  taboffs[655] := 173312 ;  // $02A500
tabtrack[656] := 35 ;  tabsec[656] := 15 ;  taboffs[656] := 174336 ;  // $02A900
tabtrack[657] := 35 ;  tabsec[657] := 2 ;  taboffs[657] := 171008 ;  // $029C00
tabtrack[658] := 35 ;  tabsec[658] := 6 ;  taboffs[658] := 172032 ;  // $02A000
tabtrack[659] := 35 ;  tabsec[659] := 10 ;  taboffs[659] := 173056 ;  // $02A400
tabtrack[660] := 35 ;  tabsec[660] := 14 ;  taboffs[660] := 174080 ;  // $02A800
tabtrack[661] := 35 ;  tabsec[661] := 1 ;  taboffs[661] := 170752 ;  // $029B00
tabtrack[662] := 35 ;  tabsec[662] := 5 ;  taboffs[662] := 171776 ;  // $029F00
tabtrack[663] := 35 ;  tabsec[663] := 9 ;  taboffs[663] := 172800 ;  // $02A300
tabtrack[664] := 35 ;  tabsec[664] := 13 ;  taboffs[664] := 173824 ;  // $02A700


end;


procedure TForm1.EXITAPPExecute(Sender: TObject);
begin
 Application.Terminate;
end;

procedure TForm1.addfileExecute(Sender: TObject);
var
i,I2,i3 : integer;
begin
  //i3 := ListBox2.ItemIndex;
  //if i3 <= 0 then i3 :=0;

  if not OpenDialog1.Execute then exit;
  if OpenDialog1.FileName = '' then exit;
  if not FileExists(OpenDialog1.FileName) then exit;

  FOR I2 := 0 TO OpenDialog1.Files.Count-1 DO
  BEGIN


  for i := 0 to ListBox1.Items.Count-1 do
  begin
    if ListBox1.Items[i] = OpenDialog1.Files[i2] then
    begin
      ShowMessage(OpenDialog1.FileName + chr(13) + chr(13) + 'file is existing !!!' + chr(13) + chr(13)+ 'NOT ADDED!!!');
      exit;
    end;
  end;

  ListBox1.Items.Add(OpenDialog1.Files[I2]);

  ListBox2.Items.Clear;

    for i := 0 to ListBox1.Items.Count-1 do
    begin
        ListBox2.Items.Add(ExtractFileName(ListBox1.Items[i]));
    end;
    checkexistsExecute(nil);
    //ListBox2.ItemIndex:= i3;
    ListBox2.ItemIndex := ListBox2.Items.Count-1;
    ListBox2Click(nil);
    END;
//  ListBox1.Items.SaveToFile('ziom.txt');
end;

procedure TForm1.checkexistsExecute(Sender: TObject);
var
i, zajete , rozmiar : Integer;
f : file of byte;
begin

   zajete := 0;


   for i := 0 to ListBox1.Items.Count-1 do
   begin
     if not FileExists(ListBox1.Items[i]) then ShowMessage('FILE IS NOT EXISTS !!!' +chr(13) + chr(13) + ListBox1.Items[i]);
     if FileExists (ListBox1.Items[i]) then
     begin
       AssignFile(f,(ListBox1.Items[i]));
       rozmiar := 0;
       reset(f);
       try
         rozmiar := filesize(f);
         zajete := zajete +((rozmiar div 254) * 256 );
         if rozmiar mod 254 <> 0 then zajete := zajete + 256
       finally
         closefile(f);
       end
     end;

   end;



   StaticText10.Caption:= IntToStr(zajete div 256);
   StaticText4.Caption:= IntToStr(664-(zajete div 256));

   if zajete div 256 > 664 then ShowMessage('ERROR !!! DISK FULL...');

end;

procedure TForm1.ListBox2Click(Sender: TObject);
var
i, zajete , rozmiar : Integer;
f : file of byte;

begin
  StaticText19.Caption := 'Files ' + IntToStr(ListBox2.Items.Count);
  //Caption:=IntToStr(ListBox2.Items.Count);
  if ListBox2.Items.Count <= 0 then
  begin

   exit;
  end
  else
  begin

  end;
  if FileExists(ListBox1.Items[ListBox2.Itemindex]) then
  begin
       AssignFile(f,(ListBox1.Items[ListBox2.Itemindex]));
       rozmiar := 0;
       i:= 0;
       reset(f);
       try


         BlockRead(f,i,2);

//         i := dana;
//         BlockRead(f,dana,1);
//         i := i + 256*dana;

         StaticText18.Caption := '$' + IntToHex(i,4);


         rozmiar := filesize(f);
         StaticText26.Caption := '$' + IntToHex(i+rozmiar-2,4);


         zajete := ((rozmiar div 254) );
         if rozmiar mod 254 <> 0 then zajete := zajete + 1;

       finally
         closefile(f);
       end;

       StaticText15.Caption:= IntToStr(rozmiar);
       StaticText5.Caption := 'Last block bytes: '+IntToStr(rozmiar mod 254);
       if rozmiar mod 254 = 0 then StaticText5.Caption := 'Last block bytes: 254';
       StaticText16.Caption:= IntToStr(zajete);

  end;
end;

procedure TForm1.DELFILEExecute(Sender: TObject);
var
i,i2 : Integer;
begin
  staticclearExecute(nil);
  if ListBox2.ItemIndex = -1 then exit;
  i2 := ListBox2.ItemIndex;

  ListBox2.Items.Clear;
  for i := 0 to ListBox1.Items.Count-1 do
  begin
    if i <> i2 then ListBox2.Items.Add(ListBox1.Items[i]);
  end;

  ListBox1.Items.Clear;
  ListBox1.Items := ListBox2.Items;
  ListBox2.Items.Clear;

    for i := 0 to ListBox1.Items.Count-1 do
    begin
        ListBox2.Items.Add(ExtractFileName(ListBox1.Items[i]));
    end;


    checkexistsExecute(nil);
    if ListBox2.Items.Count > -1 then
    begin
     if i2 >= ListBox2.Items.Count then i2 := i2-1;
     ListBox2.ItemIndex := i2;
     ListBox2Click(nil);
    end;
    check12Execute(nil);
end;

procedure TForm1.movdownExecute(Sender: TObject);
var
i,i2,i3 : integer;
begin

  i3 := ListBox2.ItemIndex;
  if ListBox2.ItemIndex = ListBox2.Items.Count-1 then exit;
  ListBox2.Items := ListBox1.Items;
  ListBox1.Items.Clear;

  for i := 0 to ListBox2.Items.Count-1 do
  begin
    if ((i <> i3) and (i <> i3+1)) then ListBox1.Items.Add(ListBox2.Items[i]);
    if i = i3  then ListBox1.Items.Add(ListBox2.Items[i3+1]);
    if i = i3+1 then ListBox1.Items.Add(ListBox2.Items[i3]);
  end;
  ListBox2.Items.Clear;

    for i := 0 to ListBox1.Items.Count-1 do
    begin
//      if not FileExists(ListBox1.Items[i]) then
//      begin
//        ListBox2.Items.Clear;
//        exit;
//      end
//      else
//      begin
        ListBox2.Items.Add(ExtractFileName(ListBox1.Items[i]));
//      end;

    end;
    checkexistsExecute(nil);

    if ListBox2.Items.Count > -1 then
    begin
     ListBox2.ItemIndex := i3+1;
     ListBox2Click(nil);
    end;



end;

procedure TForm1.movupExecute(Sender: TObject);
var
i,i2,i3 : integer;
begin

  i3 := ListBox2.ItemIndex;
  if ListBox2.ItemIndex = 0 then exit;
  ListBox2.Items := ListBox1.Items;
  ListBox1.Items.Clear;

  for i := 0 to ListBox2.Items.Count-1 do
  begin
    if ((i <> i3) and (i <> i3-1)) then ListBox1.Items.Add(ListBox2.Items[i]);
    if i = i3-1  then ListBox1.Items.Add(ListBox2.Items[i3]);
    if i = i3 then ListBox1.Items.Add(ListBox2.Items[i3-1]);
  end;
  ListBox2.Items.Clear;

    for i := 0 to ListBox1.Items.Count-1 do
    begin
 //     if not FileExists(ListBox1.Items[i]) then
//      begin
//        ListBox2.Items.Clear;
//        exit;
//      end
//      else
//      begin
        ListBox2.Items.Add(ExtractFileName(ListBox1.Items[i]));
//      end;

    end;
    checkexistsExecute(nil);

    if ListBox2.Items.Count > -1 then
    begin
     ListBox2.ItemIndex := i3-1;
     ListBox2Click(nil);
    end;



end;

procedure TForm1.saveprojExecute(Sender: TObject);
begin
  if ListBox1.Items.Count = 0 then exit;
  if SaveDialog2.Execute then  ListBox1.Items.SaveToFile(SaveDialog2.FileName);
end;

procedure TForm1.loadprojExecute(Sender: TObject);
var
i : Integer;
begin
  if not OpenDialog2.Execute then exit;
  if not FileExists(OpenDialog2.FileName) then exit;
  clearallExecute(Sender);
  ListBox1.Items.LoadFromFile(OpenDialog2.FileName);


    for i := 0 to ListBox1.Items.Count-1 do
    begin
      if not FileExists(ListBox1.Items[i]) then
      begin
         IF NOT SILENT THEN ShowMessage('ERROR - FILE DOES NO EXISTS : ' + ListBox1.Items[i]);
        //ListBox2.Items.Clear;
        //exit;
      end;

       ListBox2.Items.Add(ExtractFileName(ListBox1.Items[i]));

    end;
    IF NOT SILENT THEN checkexistsExecute(nil);
    ListBox2.ItemIndex:= 0;
    ListBox2Click(nil);


end;

procedure TForm1.CREATED64Execute(Sender: TObject);
var
tabplik : array[1..180000] of byte;
plik : file of byte;

filepoz ,blokpoz,i : Integer;

i2,i3,i4,i5,i6 : Integer;

lenfil : integer;
blockfil : integer;
reszta2 , reszta : byte;

load_ady : String;

fileblock : Integer;
MYTEKST : STRING;
tabFILE : array[0..255] of byte;

begin
  if RadioButton7.Checked then
  begin
    puttodirExecute(sender);
    exit;
  end;

  if RadioButton4.Checked then
  begin
    SpinEdit1.Value := 1;
    SpinEdit2.Value := 0;
  end;
  FOR i := 0 to 255 do tabFILE[i] := 0;

  blokpoz:=1;
  resourcecopyExecute(nil);
  created64offsExecute(nil);


//===============================================

   for i := 0 to ListBox2.Items.Count -1 do
   begin

     tabFILE[i] := tabtrack[blokpoz];
     tabFILE[i+50] := tabsec[blokpoz];
     for i4 := 1 to 180000 do tabplik[i4] := 0;
     ListBox2.ItemIndex:=i;
     ListBox2Click(nil);
     lenfil := StrToInt(StaticText15.Caption);
     blockfil := StrToInt(StaticText16.Caption);
     tabFILE[i+100] := blockfil;

     reszta := lenfil mod 254;
     reszta2 := reszta +1;
     reszta2 := reszta +1;
     IF RESZTA = 0 THEN reszta2 := 255;
     load_ady:= StaticText18.Caption;
     filepoz := 0;
     fileblock := 1;
     if i = ListBox2.Items.Count-1 then
     begin
      if RadioButton5.Checked then
      begin
       SpinEdit1.Value := tabtrack[blokpoz];
       SpinEdit2.Value := tabsec[blokpoz];
      end;
     end;


     if blokpoz+blockfil > 665 then
     begin
       ShowMessage('DISK FULL - TO MANY DATA !!!');
       exit;
     end;

IF FileExists(ListBox1.Items[ListBox2.Itemindex]) THEN
BEGIN

       AssignFile(plik,(ListBox1.Items[ListBox2.Itemindex]));
       reset(plik);
     try
       for i2 := 1 to lenfil do
       begin
         BlockRead(plik,tabplik[i2],1) ;
       end;
     finally
      closefile(plik);
     end;
     tabFILE[i+150] := tabplik[1];
     tabFILE[i+200] := tabplik[2];
     for i2 := 1 to blockfil do
     begin

       if i2 <> blockfil then
       begin
        mojd64[taboffs[blokpoz]+1] := tabtrack[blokpoz+1];
        mojd64[taboffs[blokpoz]+2] := tabsec[blokpoz+1];
       end;

       if i2 = blockfil then
       begin
         mojd64[taboffs[blokpoz]+1] := 0;
         mojd64[taboffs[blokpoz]+2] := reszta2;
       end;

       for i3 := 1 to 254 do
       begin
         mojd64[taboffs[blokpoz]+2+i3] := tabplik[filepoz+i3];
       end;

       MOJTRAK := tabtrack[blokpoz];
       MOJSEC := tabsec[blokpoz];
       ALLOCATEBAMExecute(nil);

       filepoz := filepoz+254;
       blokpoz := blokpoz+1;
     end;
END;

     //Sleep(1000);

   end;

   mojd64[i6 + (21*256*17) + 256 +32+3 +1 ] := SpinEdit1.Value;
   mojd64[i6 + (21*256*17) + 256 +32+4 +1 ] := SpinEdit2.Value;
   for i6 := 0 to 255 do mojd64[i6 + (21*256*17) + 512 +1 ] := tabFILE[i6];
   MOJTRAK := 18;
   MOJSEC := 2;
   ALLOCATEBAMExecute(nil);
   //Memo1.Hide;
   memo1.Clear;

   Memo1.Lines.Add(';SOME INFO FOR LOADER...  THIS DATA ADDED ALSO');
   Memo1.Lines.Add(';ON THE TRACK $12 SECT $02 IN ORDER:');
   Memo1.Lines.Add(';OFFSET 0 - STARTTRACK FILES');
   Memo1.Lines.Add(';OFFSET 50 - STARTSECT FILES');
   Memo1.Lines.Add(';OFFSET 100 - BLOCKS LENGTH FILES');
   Memo1.Lines.Add(';OFFSET 150 - LO BYTE LOAD ADRESS OF FILE');
   Memo1.Lines.Add(';OFFSET 200 - HI BYTE LOAD ADRESS OF FILE');
   Memo1.Lines.Add(';HOW YOU SEE - 50 FILES PER ONE SIDE FOR TRACKMO');
   Memo1.Lines.Add(';WOULD IT BE SAVED - THATS ENOUGHT I HOPE :)');
   Memo1.Lines.Add(';YOU CAN USE THIS DATA FROM THIS FILE - OR FROM DISK TRACK 18 SECTOR 2');
   Memo1.Lines.Add(';GOOD LUCK! :)');



   Memo1.Lines.Add('');

   Memo1.Lines.Add(';TBTRCK');
   Memo1.Lines.Add('');

   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      Memo1.Lines.Add(';        .BYTE $' + IntToHex(TABFILE[I],2) + '   ; START TRACK FROM : ' + ListBox2.Items[I]);
   END;

   Memo1.Lines.Add('');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('');

   Memo1.Lines.Add(';TBSECT');
   Memo1.Lines.Add('');

   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      Memo1.Lines.Add(';        .BYTE $' + IntToHex(TABFILE[I+50],2)  + '   ; START SECTOR FROM : ' + ListBox2.Items[I]) ;
   END;

   Memo1.Lines.Add('');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('');

   Memo1.Lines.Add(';TBLENBLCK');
   Memo1.Lines.Add('');

   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      Memo1.Lines.Add(';        .BYTE ' + IntToSTR(TABFILE[I+100]) + '   ; LENGTH BLOCKS OF FILE : ' + ListBox2.Items[I]) ;
   END;

   Memo1.Lines.Add('');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('');

   Memo1.Lines.Add(';LOADADDRES OF FILES');
   Memo1.Lines.Add('');

   Memo1.Lines.Add(';TBLOWLDAD');
   Memo1.Lines.Add('');


   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      I6 := (256 * TABFILE[I+200]);
      I6 := I6 + TABFILE[I+150];
      Memo1.Lines.Add(';        .BYTE $' + COPY(IntToHex(I6,4),3,2) +
      '  ; ($' + IntToHex(I6,4) + ') LOW BYTE OF LOAD ADRESS FILE' + IntToStr(I) + ' -> ' + ListBox2.Items[I]);
   END;

   Memo1.Lines.Add('');
   Memo1.Lines.Add(';TBHILDAD');
   Memo1.Lines.Add('');


   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      I6 := (256 * TABFILE[I+200]);
      I6 := I6 + TABFILE[I+150];
      Memo1.Lines.Add(';        .BYTE $' + COPY(IntToHex(I6,4),1,2) +
      '  ; ($' + IntToHex(I6,4) + ')  HI BYTE OF LOAD ADRESS FILE' + IntToStr(I) + ' -> ' + ListBox2.Items[I]);
   END;

   Memo1.Lines.Add('');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('');

   Memo1.Lines.Add('');


   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN

      Memo1.Lines.Add('STARTTRCK_FILE' + IntToStr(I) + ' = $' + IntToHex(TABFILE[I],2) +
      '  ; ' + ListBox2.Items[I]);

   END;

   Memo1.Lines.Add('');


   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN

      Memo1.Lines.Add('STARTSECT_FILE' + IntToStr(I) + ' = $' + IntToHex(TABFILE[I+50],2) +
      '  ; ' + ListBox2.Items[I]);

   END;


   Memo1.Lines.Add('');


   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      I6 := (256 * TABFILE[I+200]);
      I6 := I6 + TABFILE[I+150];
      Memo1.Lines.Add('LDADR_FILE' + IntToStr(I) + ' = $' + IntToHex(I6,4) +
      '  ; ' + ListBox2.Items[I]);
   END;
   Memo1.Lines.Add('');

   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      Memo1.Lines.Add('LENBLK_FILE' + IntToStr(I) + ' = ' + IntToSTR(TABFILE[I+100]) + '   ; LENGTH BLOCKS OF FILE : ' + ListBox2.Items[I]) ;
   END;

   //Memo1.Show;

//===============================================
    if not SaveDialog1.Execute then exit;

      AssignFile( plik , SaveDialog1.FileName );
      Rewrite(plik);

    try
      BlockWrite(plik,mojd64,174848) ;
    finally
     closefile(plik);
    end;

//Application.Terminate
end;

procedure TForm1.CREATEDATAExecute(Sender: TObject);
var
plik : file of byte;
i : Integer;
tablica  : array[1..664] of byte;
tablica2 : array[1..664] of byte;
tablica3 : array[1..664] of Integer;
tablica4 : array[1..35] of Integer;

begin
if not SaveDialog1.Execute then exit;
memo1.Clear;
Memo1.Text:='12345';

//for i := 1 to 144 do Memo1.Lines.SaveToFile(ExtractFilePath(SaveDialog1.FileName)+'\file' + IntToStr(i)+'.prg');


//exit;


    for i := 1 to 17 do tablica4[i] := (i-1) * 21*256;
    for i := 18 to 24 do tablica4[i] := ((i-18)*19*256) +(tablica4[17]+ 21*256);
    for i := 25 to 30 do tablica4[i] := ((i-25)*18*256) +(tablica4[24]+ 19*256);
    for i := 31 to 35 do tablica4[i] := ((i-31)*17*256) +(tablica4[30]+ 18*256);


    Memo1.Lines.Clear;
    OpenDialog1.InitialDir := ExtractFilePath(Application.ExeName);
    if not OpenDialog1.Execute then exit;
   if FileExists(OpenDialog1.FileName) then
   begin

    AssignFile( plik , OpenDialog1.FileName );
    Reset(plik);
    seek(plik,0);
    try
      for i := 1 to 664 do BlockRead(plik,tablica[i],1) ;
    finally
     closefile(plik);
    end;
   end;
    if not OpenDialog1.Execute then exit;
   if FileExists(OpenDialog1.FileName) then
   begin
    AssignFile( plik , OpenDialog1.FileName );
    Reset(plik);
    seek(plik,0);
    try
      for i := 1 to 664 do BlockRead(plik,tablica2[i],1) ;
    finally
     closefile(plik);
    end;
   end;

    for i := 1 to 664 do
    begin
      tablica3[i] := tablica4[tablica[i]] + (256 * tablica2[i]);
      Memo1.Lines.Add('tabtrack['+IntToStr(i)+'] := ' + IntToStr(tablica[i]) + ' ;  ' +
       'tabtsec['+IntToStr(i)+'] := ' + IntToStr(tablica2[i]) + ' ;  ' +
       'taboffs['+IntToStr(i)+'] := ' + IntToStr(tablica3[i]) + ' ;  // $'+ IntToHex(tablica3[i],6)
       );

    end;

end;




procedure TForm1.LOADFROMPARAMSExecute(Sender: TObject);


var
i : Integer;
begin

  clearallExecute(Sender);
  ListBox1.Items.LoadFromFile(OpenDialog2.FileName);


    for i := 0 to ListBox1.Items.Count-1 do
    begin
      if not FileExists(ListBox1.Items[i]) then
      begin
         IF NOT SILENT THEN ShowMessage('ERROR - FILE DOES NO EXISTS : ' + ListBox1.Items[i]);
        //ListBox2.Items.Clear;
        //exit;
      end;

       ListBox2.Items.Add(ExtractFileName(ListBox1.Items[i]));

    end;
    IF NOT SILENT THEN checkexistsExecute(nil);
    ListBox2.ItemIndex:= 0;
    ListBox2Click(nil);

end;

procedure TForm1.ALLOCATEBAMExecute(Sender: TObject);
VAR

   MAXSEC, bfree : BYTE;
   OFSECIK, dostep : INTEGER;
   mojbit : byte;
begin
   IF MOJTRAK > 35 THEN EXIT;
   IF MOJTRAK < 1 THEN EXIT;
   MAXSEC := TBMAX[MOJTRAK];
   IF MOJSEC >= MAXSEC THEN EXIT;
   IF MOJSEC < 0 THEN EXIT;
   mojbit := tbbit[mojsec];
   ofsecik := 21*256*17+1;
   dostep := OFSECIK + 4*MOJTRAK;

   bfree := mojd64[dostep];
   bfree := bfree-1;
   mojd64[dostep] :=bfree;

   if MOJSEC < 8 then dostep := dostep + 1
   else
   if MOJSEC < 16 then dostep := dostep + 2
   else
    dostep := dostep + 3;

   mojd64[dostep] := mojd64[dostep] or mojbit;
   mojd64[dostep] := mojd64[dostep] xor mojbit;

end;




procedure TForm1.SavememoExecute(Sender: TObject);

begin




  IF Memo1.Lines.Count <= 0 THEN EXIT;
  If Not SaveDialog3.Execute then exit;

  Memo1.Lines.SaveToFile(SaveDialog3.FileName);

end;

procedure TForm1.staticclearExecute(Sender: TObject);
begin
  StaticText15.Caption:='';
  StaticText16.Caption:='';
  StaticText18.Caption:='';
end;

procedure TForm1.MAKEWITHOUTExecute(Sender: TObject);
begin
  RadioButton3.Checked:=True;
  CREATED64Execute(NIL);
  Application.Terminate;
end;

procedure TForm1.WYSWNASTATExecute(Sender: TObject);
begin
StatusBar1.Panels[0].Text := Application.Hint;
end;

procedure TForm1.MAKEV1Execute(Sender: TObject);
begin
  RadioButton1.Checked := True;
  CREATED64Execute(NIL);
end;

procedure TForm1.MAKEV2Execute(Sender: TObject);
begin
  RadioButton2.Checked := True;
  CREATED64Execute(NIL);
end;

procedure TForm1.clearallExecute(Sender: TObject);
begin
  Memo1.Clear;
  staticclearExecute(nil);
  ListBox1.Clear;
  ListBox2.Clear;
  StaticText3.Caption := '664';
  StaticText4.Caption := '664';
  StaticText10.Caption := '0';
  check12Execute(nil);


end;

procedure TForm1.SETFIRSTFILEExecute(Sender: TObject);
begin
  RadioButton4.Checked:=True;
end;

procedure TForm1.SETLASTFILEExecute(Sender: TObject);
begin
   RadioButton5.Checked:=True;
end;

procedure TForm1.SILENTCREATEExecute(Sender: TObject);
var
tabplik : array[1..100000] of byte;
plik : file of byte;

filepoz ,blokpoz,i : Integer;

i2,i3,i4,i5,i6 : Integer;

lenfil : integer;
blockfil : integer;
reszta2 , reszta : byte;

load_ady : String;

fileblock : Integer;
MYTEKST : STRING;
tabFILE : array[0..255] of byte;

begin
SILENT2:=True;

  if RadioButton7.Checked then
  begin
    puttodirExecute(sender);
    exit;
  end;


  if RadioButton4.Checked then
  begin
    SpinEdit1.Value := 1;
    SpinEdit2.Value := 0;
  end;
  FOR i := 0 to 255 do tabFILE[i] := 0;

  blokpoz:=1;
  resourcecopyExecute(nil);
  created64offsExecute(nil);


//===============================================

   for i := 0 to ListBox2.Items.Count -1 do
   begin
     tabFILE[i] := tabtrack[blokpoz];
     tabFILE[i+50] := tabsec[blokpoz];
     for i4 := 1 to 70000 do tabplik[i4] := 0;
     ListBox2.ItemIndex:=i;
     ListBox2Click(nil);
     lenfil := StrToInt(StaticText15.Caption);
     blockfil := StrToInt(StaticText16.Caption);
     tabFILE[i+100] := blockfil;

     reszta := lenfil mod 254;
     reszta2 := reszta +1;
     reszta2 := reszta +1;
     IF RESZTA = 0 THEN reszta2 := 255;     
     load_ady:= StaticText18.Caption;
     filepoz := 0;
     fileblock := 1;
     if i = ListBox2.Items.Count-1 then
     begin
      if RadioButton5.Checked then
      begin
       SpinEdit1.Value := tabtrack[blokpoz];
       SpinEdit2.Value := tabsec[blokpoz];
      end;
     end;


     if blokpoz+blockfil > 665 then
     begin
       ShowMessage('DISK FULL - TO MANY DATA !!!');
       exit;
     end;
if FileExists(ListBox1.Items[ListBox2.Itemindex]) then
begin

       AssignFile(plik,(ListBox1.Items[ListBox2.Itemindex]));
       reset(plik);
     try
       for i2 := 1 to lenfil do
       begin
         BlockRead(plik,tabplik[i2],1) ;
       end;
     finally
      closefile(plik);
     end;


     tabFILE[i+150] := tabplik[1];
     tabFILE[i+200] := tabplik[2];
     for i2 := 1 to blockfil do
     begin

       if i2 <> blockfil then
       begin
        mojd64[taboffs[blokpoz]+1] := tabtrack[blokpoz+1];
        mojd64[taboffs[blokpoz]+2] := tabsec[blokpoz+1];
       end;

       if i2 = blockfil then
       begin
         mojd64[taboffs[blokpoz]+1] := 0;
         mojd64[taboffs[blokpoz]+2] := reszta2;
       end;

       for i3 := 1 to 254 do
       begin
         mojd64[taboffs[blokpoz]+2+i3] := tabplik[filepoz+i3];
       end;

       MOJTRAK := tabtrack[blokpoz];
       MOJSEC := tabsec[blokpoz];
       ALLOCATEBAMExecute(nil);

       filepoz := filepoz+254;
       blokpoz := blokpoz+1;
     end;
end;
     //Sleep(1000);

   end;

   mojd64[i6 + (21*256*17) + 256 +32+3 +1 ] := SpinEdit1.Value;
   mojd64[i6 + (21*256*17) + 256 +32+4 +1 ] := SpinEdit2.Value;
   for i6 := 0 to 255 do mojd64[i6 + (21*256*17) + 512 +1 ] := tabFILE[i6];
   MOJTRAK := 18;
   MOJSEC := 2;
   ALLOCATEBAMExecute(nil);
   //Memo1.Hide;
   memo1.Clear;

   Memo1.Lines.Add(';SOME INFO FOR LOADER...  THIS DATA ADDED ALSO');
   Memo1.Lines.Add(';ON THE TRACK $12 SECT $02 IN ORDER:');
   Memo1.Lines.Add(';OFFSET 0 - STARTTRACK FILES');
   Memo1.Lines.Add(';OFFSET 50 - STARTSECT FILES');
   Memo1.Lines.Add(';OFFSET 100 - BLOCKS LENGTH FILES');
   Memo1.Lines.Add(';OFFSET 150 - LO BYTE LOAD ADRESS OF FILE');
   Memo1.Lines.Add(';OFFSET 200 - HI BYTE LOAD ADRESS OF FILE');
   Memo1.Lines.Add(';HOW YOU SEE - 50 FILES PER ONE SIDE FOR TRACKMO');
   Memo1.Lines.Add(';WOULD IT BE SAVED - THATS ENOUGHT I HOPE :)');
   Memo1.Lines.Add(';YOU CAN USE THIS DATA FROM THIS FILE - OR FROM DISK TRACK 18 SECTOR 2');
   Memo1.Lines.Add(';GOOD LUCK! :)');



   Memo1.Lines.Add('');

   Memo1.Lines.Add(';TBTRCK');
   Memo1.Lines.Add('');

   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      Memo1.Lines.Add(';        .BYTE $' + IntToHex(TABFILE[I],2) + '   ; START TRACK FROM : ' + ListBox2.Items[I]);
   END;

   Memo1.Lines.Add('');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('');

   Memo1.Lines.Add(';TBSECT');
   Memo1.Lines.Add('');

   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      Memo1.Lines.Add(';        .BYTE $' + IntToHex(TABFILE[I+50],2)  + '   ; START SECTOR FROM : ' + ListBox2.Items[I]) ;
   END;

   Memo1.Lines.Add('');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('');

   Memo1.Lines.Add(';TBLENBLCK');
   Memo1.Lines.Add('');

   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      Memo1.Lines.Add(';        .BYTE ' + IntToSTR(TABFILE[I+100]) + '   ; LENGTH BLOCKS OF FILE : ' + ListBox2.Items[I]) ;
   END;

   Memo1.Lines.Add('');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('');

   Memo1.Lines.Add(';LOADADDRES OF FILES');
   Memo1.Lines.Add('');

   Memo1.Lines.Add(';TBLOWLDAD');
   Memo1.Lines.Add('');


   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      I6 := (256 * TABFILE[I+200]);
      I6 := I6 + TABFILE[I+150];
      Memo1.Lines.Add(';        .BYTE $' + COPY(IntToHex(I6,4),3,2) +
      '  ; ($' + IntToHex(I6,4) + ') LOW BYTE OF LOAD ADRESS FILE' + IntToStr(I) + ' -> ' + ListBox2.Items[I]);
   END;

   Memo1.Lines.Add('');
   Memo1.Lines.Add(';TBHILDAD');
   Memo1.Lines.Add('');


   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      I6 := (256 * TABFILE[I+200]);
      I6 := I6 + TABFILE[I+150];
      Memo1.Lines.Add(';        .BYTE $' + COPY(IntToHex(I6,4),1,2) +
      '  ; ($' + IntToHex(I6,4) + ')  HI BYTE OF LOAD ADRESS FILE' + IntToStr(I) + ' -> ' + ListBox2.Items[I]);
   END;

   Memo1.Lines.Add('');
   Memo1.Lines.Add('');
   Memo1.Lines.Add('');

   Memo1.Lines.Add('');


   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN

      Memo1.Lines.Add('STARTTRCK_FILE' + IntToStr(I) + ' = $' + IntToHex(TABFILE[I],2) +
      '  ; ' + ListBox2.Items[I]);

   END;

   Memo1.Lines.Add('');


   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN

      Memo1.Lines.Add('STARTSECT_FILE' + IntToStr(I) + ' = $' + IntToHex(TABFILE[I+50],2) +
      '  ; ' + ListBox2.Items[I]);

   END;


   Memo1.Lines.Add('');


   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      I6 := (256 * TABFILE[I+200]);
      I6 := I6 + TABFILE[I+150];
      Memo1.Lines.Add('LDADR_FILE' + IntToStr(I) + ' = $' + IntToHex(I6,4) +
      '  ; ' + ListBox2.Items[I]);
   END;
   Memo1.Lines.Add('');

   for i := 0 to ListBox2.Items.Count -1 do
   BEGIN
      Memo1.Lines.Add('LENBLK_FILE' + IntToStr(I) + ' = ' + IntToSTR(TABFILE[I+100]) + '   ; LENGTH BLOCKS OF FILE : ' + ListBox2.Items[I]) ;
   END;

   //Memo1.Show;

//===============================================
  IF SaveDialog3.FileName <> '' THEN
  BEGIN
    IF UpperCase(ExtractFileExt(SaveDialog3.FileName)) <> '.INC' THEN SaveDialog3.FileName := ExtractFilePath(SaveDialog3.FileName)+ExtractFileName(SaveDialog3.FileName)+'.INC';
    Memo1.Lines.SaveToFile(SaveDialog3.FileName);
  END;
  IF SaveDialog1.FileName = '' THEN EXIT;
    IF UpperCase(ExtractFileExt(SaveDialog1.FileName)) <> '.D64' THEN SaveDialog1.FileName := ExtractFilePath(SaveDialog1.FileName)+ExtractFileName(SaveDialog1.FileName)+'.D64';

      AssignFile( plik , SaveDialog1.FileName );
      Rewrite(plik);

    try
      BlockWrite(plik,mojd64,174848) ;
    finally
     closefile(plik);
    end;
  IF SaveDialog1.FileName <> '' THEN Application.Terminate;
  IF SaveDialog3.FileName <> '' THEN Application.Terminate;

//Application.Terminate
end;

procedure TForm1.check12Execute(Sender: TObject);
begin
  //Caption:= IntToStr(ListBox2.Items.Count);
  if ListBox2.Items.Count <= 0 then
  begin

   StaticText26.Caption:='';

   StaticText5.Caption:='Last block bytes : 0';
   exit;
  end
  else
  begin

  end;

end;

procedure TForm1.HELPExecute(Sender: TObject);
begin
 Form3.ShowModal;
end;

procedure TForm1.checkfreeblockExecute(Sender: TObject);
VAR

   MAXSEC, bfree : BYTE;
   OFSECIK, dostep : INTEGER;
   mojbit : byte;
begin
   isblockfree := False;

   IF MOJTRAK > 35 THEN EXIT;
   IF MOJTRAK < 1 THEN EXIT;
   MAXSEC := TBMAX[MOJTRAK];
   IF MOJSEC >= MAXSEC THEN EXIT;
   IF MOJSEC < 0 THEN EXIT;
   mojbit := tbbit[mojsec];
   ofsecik := 21*256*17+1;
   dostep := OFSECIK + 4*MOJTRAK;

   if MOJSEC < 8 then dostep := dostep + 1
   else
   if MOJSEC < 16 then dostep := dostep + 2
   else
    dostep := dostep + 3;

   mojbit := mojbit xor 255;
   if mojd64[dostep] and mojbit <> 0 then isblockfree := True;


end;

procedure TForm1.puttodirExecute(Sender: TObject);

var
tabplik : array[1..180000] of byte;
plik : file of byte;

filepoz ,blokpoz,i : Integer;

i2,i3,i4,i5,i6,i7 : Integer;

lenfil : integer;
blockfil : integer;
reszta2 , reszta : byte;

load_ady, nazwa1,nazwa2 : String;


fileblock : Integer;
MYTEKST : STRING;
tabFILE : array[0..255] of byte;
lastofs,nextofs,actofs : Integer;
begin

  FOR i := 0 to 255 do tabFILE[i] := 0;

  blokpoz:=1;
  resourcecopyExecute(nil);
  created64offsExecute(nil);


//===============================================

   for i := 0 to ListBox2.Items.Count -1 do
   begin

     //tabFILE[i] := tabtrack[blokpoz];
     //tabFILE[i+50] := tabsec[blokpoz];
     for i4 := 1 to 70000 do tabplik[i4] := 0;
     ListBox2.ItemIndex:=i;
     ListBox2Click(nil);
     lenfil := StrToInt(StaticText15.Caption);
     blockfil := StrToInt(StaticText16.Caption);
     tabFILE[i+100] := blockfil;

     actofs := (i div 8);
     actofs := tabdir[actofs+1];
     lastofs := actofs;

     if i > 7 then
     lastofs := tabdir[i div 8];

     actofs := actofs * 256;
     lastofs := lastofs * 256;
     lastofs := lastofs + 17*21*256+1;
     actofs := actofs + 17*21*256+1;
     if i mod 8 = 0 then
     begin
       if i <> 0 then
       begin
         mojd64[lastofs] := 18;
         mojd64[lastofs+1] := tabdir[(i div 8)+1];
         mojd64[actofs]:=0;
         mojd64[actofs+1]:=255;
         MOJTRAK := 18;
         MOJSEC := tabdir[(i div 8)+1];
         ALLOCATEBAMExecute(nil);
       end;
     end;
     actofs := actofs + (32*(i mod 8));
     mojd64[actofs+2]:=130;
     mojd64[actofs+3]:=tabtrack[blokpoz];
     mojd64[actofs+4]:=tabsec[blokpoz];
     mojd64[actofs+31]:=0;
     mojd64[actofs+30]:=blockfil MOD 256;
     mojd64[actofs+31]:=blockfil DIV 256;

     for i7 := 1 to 16 do mojd64[actofs+i7+4]:=160;
     nazwa1 := ListBox2.Items[i];
     nazwa2 := nazwa1;
     if Length(nazwa1) > 16 then nazwa2 := copy(nazwa1,1,16);
     nazwa2 := UpperCase(nazwa2);
     for i7 := 1 to length(nazwa2) do mojd64[actofs+i7+4] := ord(nazwa2[i7]);



     reszta := lenfil mod 254;
     reszta2 := reszta +1;
     reszta2 := reszta +1;
     IF RESZTA = 0 THEN reszta2 := 255;
     load_ady:= StaticText18.Caption;
     filepoz := 0;
     fileblock := 1;
     if i = ListBox2.Items.Count-1 then
     begin
      if RadioButton5.Checked then
      begin
       SpinEdit1.Value := tabtrack[blokpoz];
       SpinEdit2.Value := tabsec[blokpoz];
      end;
     end;


     if blokpoz+blockfil > 665 then
     begin
       ShowMessage('DISK FULL - TO MANY DATA !!!');
       exit;
     end;

IF FileExists(ListBox1.Items[ListBox2.Itemindex]) THEN
BEGIN

       AssignFile(plik,(ListBox1.Items[ListBox2.Itemindex]));
       reset(plik);
     try
       for i2 := 1 to lenfil do
       begin
         BlockRead(plik,tabplik[i2],1) ;
       end;
     finally
      closefile(plik);
     end;
     tabFILE[i+150] := tabplik[1];
     tabFILE[i+200] := tabplik[2];
     for i2 := 1 to blockfil do
     begin

       if i2 <> blockfil then
       begin
        mojd64[taboffs[blokpoz]+1] := tabtrack[blokpoz+1];
        mojd64[taboffs[blokpoz]+2] := tabsec[blokpoz+1];
       end;

       if i2 = blockfil then
       begin
         mojd64[taboffs[blokpoz]+1] := 0;
         mojd64[taboffs[blokpoz]+2] := reszta2;
       end;

       for i3 := 1 to 254 do
       begin
         mojd64[taboffs[blokpoz]+2+i3] := tabplik[filepoz+i3];
       end;

       MOJTRAK := tabtrack[blokpoz];
       MOJSEC := tabsec[blokpoz];
       ALLOCATEBAMExecute(nil);

       filepoz := filepoz+254;
       blokpoz := blokpoz+1;
     end;
END;

     //Sleep(1000);

   end;

//   mojd64[i6 + (21*256*17) + 256 +32+3 +1 ] := SpinEdit1.Value;
//   mojd64[i6 + (21*256*17) + 256 +32+4 +1 ] := SpinEdit2.Value;
//   for i6 := 0 to 255 do mojd64[i6 + (21*256*17) + 512 +1 ] := tabFILE[i6];
//   MOJTRAK := 18;
//   MOJSEC := 2;
//   ALLOCATEBAMExecute(nil);
   //Memo1.Hide;
   memo1.Clear;


   //Memo1.Show;

//===============================================
    IF NOT SILENT2 THEN
    BEGIN
      if not SaveDialog1.Execute then exit;
    END;
      IF UpperCase(ExtractFileExt(SaveDialog1.FileName)) <> '.D64' THEN SaveDialog1.FileName := ExtractFilePath(SaveDialog1.FileName)+ExtractFileName(SaveDialog1.FileName)+'.D64';
      AssignFile( plik , SaveDialog1.FileName );
      Rewrite(plik);

    try
      BlockWrite(plik,mojd64,174848) ;
    finally
     closefile(plik);
    end;

//Application.Terminate
end;
procedure TForm1.SpeedButton14Click(Sender: TObject);
var
tbtrack  : array[1..664] of byte;
tbsec : array[1..664] of byte;
tboffs : array[1..664] of Integer;

tb_manysec : array[1..35] of byte;
tb_max_nr_sec : array[1..35] of byte;

_bufenq : array [0..22] of byte;
_tabsec : array [0..22] of byte;

xreg : byte;
yreg :byte;
acc : byte;
cntr1 :byte;
cntr2 :byte;
maxsec : byte;
cntrsec :byte;

exitcode :byte;

i,i2,i3,i4 : Integer;
my_offs : Integer;

interleave : integer;



begin

try
  interleave := StrToInt(Edit3.Text);
except
  interleave := 8;

end;
  if interleave > 21 then interleave := 1;
  Edit3.Text := '$'+ IntToHex(interleave,2) ;


for i := 1 to 664 do tbtrack[i] :=0;
for i := 1 to 664 do tbsec[i] :=0;
for i := 1 to 664 do tboffs[i] :=0;
for i := 1 to 35 do tb_manysec[i] :=0;




for i := 1 to 35 do
begin
  if i < 18 then tb_manysec[i] := 21;
  if ((i>17)and(i<25)) then tb_manysec[i] := 19;
  if ((i>24)and(i<31)) then tb_manysec[i] := 18;
  if i > 30 then tb_manysec[i] := 17;
end;

for i := 1 to 35 do tb_max_nr_sec[i] := tb_manysec[i]-1;

  i3 := 1;
  for i := 1 to 17 do
  begin
    for i2 := 1 to tb_manysec[i] do
    begin
      tbtrack[i3] := i;
      inc(i3);
    end;
  end;
  for i := 19 to 35 do
  begin
    for i2 := 1 to tb_manysec[i] do
    begin
      tbtrack[i3] := i;
      inc(i3);
    end;
  end;

  //for i := 1 to 664 do if tbtrack[i] <> tabtrack[i] then ShowMessage(IntToStr(i));

  my_offs := 0;
  i3 := 1;

  for i := 1 to 17 do
  begin
    tboffs[i3] := my_offs;
    my_offs := my_offs + 256 * tb_manysec[i];
    i3 := i3 + tb_manysec[i];
  end;

  my_offs := my_offs + 256 * tb_manysec[18];

  for i := 19 to 35 do
  begin
    tboffs[i3] := my_offs;
    my_offs := my_offs + 256 * tb_manysec[i];
    i3 := i3 + tb_manysec[i];
  end;

  i3 := 1;
for i := 1 to 17 do
begin

  maxsec := tb_manysec[i];
  cntrsec := maxsec;

  for i4 := 0 to maxsec do _bufenq[i4] := 255;
  for i4 := 0 to maxsec do _tabsec[i4] := 255;

  acc := maxsec; //maxsec
  xreg := 0;
  cntr1 :=  xreg;
  cntr2 :=  xreg;
  exitcode := 22;


repeat
//@scloop

    xreg := cntr1;          //ldx cntr1
    acc := cntr2;           //lda cntr2
    _bufenq[xreg] := acc;   //sta bufenq,x
    yreg := acc;            //tay
    acc := xreg;            //txa
    _tabsec[xreg] := acc;   //sta tabsec,x

    cntr2 := cntr2+1;       //inc cntr2
    cntrsec := cntrsec-1;   //dec cntrsec

    if cntrsec = 0 then break ; //beq scl4

    acc := cntr1+interleave;                      //lda cntr1
    if acc >= maxsec then acc := acc - maxsec;    //clc
                                                  //adc #interleave
                                                  //bcc scl2b
                                                  //sbc maxsec


    xreg := acc ;              //tax


  repeat
     cntr1 := xreg;             //stx cntr1
     acc := _bufenq[xreg];      //lda bufenq,x
     if acc = 255 then break;   //bmi scloop
     xreg := xreg+1;            //inx
     if xreg >= maxsec then xreg := 0;
  until exitcode = 222;
until exitcode = 0;


{
asm
pushad
xor eax,eax
xor ebx,ebx

xor ecx,ecx

@scloop:
        mov bl, byte ptr [cntr1]          //ldx cntr1
        mov al, byte ptr [cntr2]          //lda cntr2
        mov byte ptr [_bufenq] + [ebx],al //sta bufenq,x
        mov cl,al                         //tay
        mov al,bl                         //txa
        mov byte ptr [_tabsec] + [ebx],al //sta tabsec,x
        inc byte ptr [cntr2]              //inc cntr2
        dec byte ptr [cntrsec]            //dec cntrsec
    je @scl4                              //beq scl4

        mov al, byte ptr[cntr1]           //lda cntr1
        clc                               //clc
        adc al,byte ptr [interleave]      //adc #interleave
        cmp al, byte ptr[maxsec]          //cmp maxsec
   jc @scl2b                              //bcc scl2b
        sbb al, byte ptr [maxsec]         //sbc maxsec
@scl2b:
        mov bl,al                         //tax
@scl2:
        mov byte ptr[cntr1],bl            //stx cntr1
        mov al,byte ptr[_bufenq]+[ebx]    //lda bufenq,x
        cmp al , 255
   je @scloop                            //bmi scloop
        inc bl                           //inx
        cmp bl, byte ptr [maxsec]
        jc @scl2
        mov bl, 0
je @scl2

@scl4:
popad
end; //end asm
}


for i4 := 0 to maxsec-1 do
begin
  _tabsec[_bufenq[i4]] :=i4;
end;

for i4 := 0 to maxsec-1 do
begin
  tbsec[i3] :=_tabsec[i4];
  inc(i3);
end;
end;

for i := 19 to 35 do
begin

  maxsec := tb_manysec[i];
  cntrsec := maxsec;

  for i4 := 0 to maxsec do _bufenq[i4] := 255;
  for i4 := 0 to maxsec do _tabsec[i4] := 255;
  acc := maxsec; //maxsec
  xreg := 0;
  cntr1 :=  xreg;
  cntr2 :=  xreg;



    xreg := cntr1;
    acc := cntr2;

//===========
  exitcode := 22;


repeat
//@scloop

    xreg := cntr1;          //ldx cntr1
    acc := cntr2;           //lda cntr2
    _bufenq[xreg] := acc;   //sta bufenq,x
    yreg := acc;            //tay
    acc := xreg;            //txa
    _tabsec[xreg] := acc;   //sta tabsec,x

    cntr2 := cntr2+1;       //inc cntr2
    cntrsec := cntrsec-1;   //dec cntrsec

    if cntrsec = 0 then break ; //beq scl4

    acc := cntr1+interleave;                      //lda cntr1
    if acc >= maxsec then acc := acc - maxsec;    //clc
                                                  //adc #interleave
                                                  //bcc scl2b
                                                  //sbc maxsec


    xreg := acc ;              //tax


  repeat
     cntr1 := xreg;             //stx cntr1
     acc := _bufenq[xreg];      //lda bufenq,x
     if acc = 255 then break;   //bmi scloop
     xreg := xreg+1;            //inx
     if xreg >= maxsec then xreg := 0;
  until exitcode = 222;
until exitcode = 0;




//==========

{
asm
pushad
xor eax,eax
xor ebx,ebx

xor ecx,ecx

@scloop:
mov bl, byte ptr [cntr1]        //ldx cntr1
mov al, byte ptr [cntr2]        //lda cntr2
mov byte ptr [_bufenq] + [ebx],al //sta bufenq,x
mov cl,al                       //tay
mov al,bl                       //txa
mov byte ptr [_tabsec] + [ebx],al //sta tabsec,x
inc byte ptr [cntr2]            //inc cntr2
dec byte ptr [cntrsec]          //dec cntrsec
je @scl4                        //beq scl4
mov al, byte ptr[cntr1]         //lda cntr1
clc                             //clc
adc al,byte ptr [interleave]    //adc #interleave
cmp al, byte ptr[maxsec]        //cmp maxsec
jc @scl2b                      //bcc scl2b
sbb al, byte ptr [maxsec]       //sbc maxsec
@scl2b:
mov bl,al                       //tax
@scl2:
mov byte ptr[cntr1],bl          //stx cntr1
mov al,byte ptr[_bufenq]+[ebx]  //lda bufenq,x
cmp al , 255
je @scloop                      //bmi scloop
inc bl                          //inx
cmp bl, byte ptr [maxsec]
jc @scl2
mov bl, 0
je @scl2

@scl4:
popad
end; //end asm

}

for i4 := 0 to maxsec-1 do
begin
  _tabsec[_bufenq[i4]] :=i4;
end;

for i4 := 0 to maxsec-1 do
begin
  tbsec[i3] :=_tabsec[i4];
  inc(i3);
end;
end;


i := 1;
my_offs := 0;

for i := 1 to 664 do
begin

if tboffs[i] = 0 then

  tboffs[i] :=  my_offs+ 256*tbsec[i]
  else my_offs := tboffs[i];


end;


for i := 1 to 664 do taboffs[i] := tboffs[i];
for i := 1 to 664 do tabtrack[i] := tbtrack[i];
for i := 1 to 664 do tabsec[i] := tbsec[i];


end;

procedure TForm1.SpeedButton15Click(Sender: TObject);
var
i,i2 : Integer;
dana,dana2 : String;
begin

        edit1.Text := UpperCase(edit1.Text);
        Edit2.Text := UpperCase(Edit2.Text);

      TRY
        I2 := StrToInt(Edit3.Text);
        Edit3.Text :='$'+ IntToHex(I2,2);
      EXCEPT
        I2 := 10;
        Edit3.Text :='$'+ IntToHex(I2,2);
      END;
        if i2 > 21 then i2 := 21;
        Edit3.Text :='$'+ IntToHex(I2,2);

      dana2 := ' -INC "tracksdata" "project.C64LINK" -D64 "myd64" ';

  dana := ExtractFileName(Application.ExeName) + dana2 ;

     dana2 := '-diskname "'+ edit1.Text + '" ';
     dana := dana + dana2;

     dana2 := '-diskID "'+ edit2.Text + '" ';
     dana := dana + dana2;

   IF RadioButton3.Checked THEN  DANA2 := '-L0 ' ;
   IF RadioButton1.Checked THEN  DANA2 := '-L1 ' ;
   IF RadioButton2.Checked THEN  DANA2 := '-L2 ' ;
   IF RadioButton7.Checked then  DANA2 := '-DIRENT ' ;
   dana := dana + dana2;

   IF RadioButton4.Checked THEN  DANA2 := '-FF ' ;
   IF RadioButton5.Checked THEN  DANA2 := '-LF ' ;
   dana := dana + dana2 + '-INTERLEAVE ' ;
   dana := dana + Edit3.Text;



  Form4.Edit1.Text := dana;
  form4.ShowModal;
end;

end.
