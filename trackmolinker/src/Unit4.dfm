object Form4: TForm4
  Left = 192
  Top = 124
  Width = 870
  Height = 229
  Caption = 'Example of CMD line text for this case'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 376
    Top = 112
    Width = 121
    Height = 41
    Action = Action1
    Caption = 'ESC to Exit'
    Flat = True
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 172
    Width = 854
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object Edit1: TEdit
    Left = 8
    Top = 56
    Width = 833
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Text = 'Edit1'
  end
  object ActionList1: TActionList
    Left = 808
    object Action1: TAction
      Caption = 'Go back - Esc'
      ShortCut = 27
      OnExecute = Action1Execute
    end
  end
end
