object Form3: TForm3
  Left = 230
  Top = 46
  Width = 950
  Height = 643
  Caption = 'HELP FOR TRACKMOLINKER AND DETERMINISTIC LOADER'
  Color = clBtnFace
  Constraints.MaxWidth = 950
  Constraints.MinHeight = 480
  Constraints.MinWidth = 950
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 586
    Width = 934
    Height = 19
    Panels = <
      item
        Width = 222
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 934
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 227
    Caption = 'ToolBar1'
    Flat = True
    Images = ImageList1
    ShowCaptions = True
    TabOrder = 1
    object ToolButton2: TToolButton
      Left = 0
      Top = 0
      Action = showproc1
    end
    object ToolButton3: TToolButton
      Left = 227
      Top = 0
      Action = showproc2
    end
    object ToolButton4: TToolButton
      Left = 454
      Top = 0
      Action = showchangedisk
    end
    object ToolButton5: TToolButton
      Left = 681
      Top = 0
      Action = SHOWDEMO
      Caption = 'Show simple demo that is using determ. loader '
    end
  end
  object ToolBar2: TToolBar
    Left = 0
    Top = 540
    Width = 934
    Height = 46
    Align = alBottom
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 139
    Caption = 'ToolBar2'
    Flat = True
    Images = ImageList1
    ShowCaptions = True
    TabOrder = 2
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Action = saveproc1
      AutoSize = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 46
    Width = 473
    Height = 314
    Align = alLeft
    Caption = 'Panel1'
    TabOrder = 3
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 471
      Height = 312
      Align = alClient
      Lines.Strings = (
        'Some changes to this version.'
        ''
        'Bugfix:'
        ''
        
          '- Incorrectly saved 2nd byte of  a sector link when file length ' +
          'is zero (mod 254 = 0).'
        ''
        'Upgrades:'
        ''
        
          '1. New command line parameter added for setting up interleave va' +
          'lue - default $0a. This will be '
        'set for whole disk and not single file only.'
        'In order to change it simply use: hex  "-interleave $xx"  '
        #9#9#9#9' or dec   "-interleave xx"'
        ''
        
          '2. Files in size over 255 blocks can now be saved on disk but th' +
          'ey are not compatible with built '
        'in deterministic loader.'
        ''
        
          '3. Loaders code has been changed for Jiffy Dos compatibility (JS' +
          'R $f82b).'
        ''
        
          '4. Trackmolinker script that is generating *.INC file has been m' +
          'odified so it gives variables  only '
        'and does not take additional space in code.'
        
          'These variables are: start track, start sector, file blocks leng' +
          'th and LO/HI load address.'
        ''
        
          '5. "Bind C64LINK files'#8221' button added. This function allows to bi' +
          'nd your D64 projects saved in '
        
          'C64LINK format. In order to use this function please run Trackmo' +
          'linker with administrative rights '
        'as it need to access registry.'
        ''
        
          '6. "build CMD" button added. Function will generate command line' +
          ' text.'
        ''
        '7. Procedure for calling loader has been changed (API). '
        ''
        '8. New loader functions added:'
        '- Two different types of changedisk engines .'
        '- Loading silent directory to C64 memory.'
        ''
        
          '9. Added code relocator for loader and installer with step of $0' +
          '100.'
        ''
        ''
        'Important info:'
        ''
        
          '1. Use GUI to build your own D64 trackmo disk then save project ' +
          'and make compilation from '
        'command line.'
        ''
        
          '2. Trackmolinker has been built for full compatibility with 1541' +
          ' normal file system. It can be '
        
          'extended to fast file system by using built-in deterministic loa' +
          'der.'
        ''
        
          '3. Files will be stored  on disk with requested interleave in or' +
          'der from track 1,2,3... to 35 '
        'excluding track 18.'
        ''
        
          '4. Trackmolinker will generate data file "*.INC" after compilati' +
          'on which includes info about all '
        
          'files on disk. Needed if you want to use deterministic loader. P' +
          'rogram can deal with 50 files per '
        
          'disk side. This info-data is also stored on track 18 sector 02 (' +
          '18,2) in order:'
        ''
        #9'OFFSET 0'#9'- FILE START TRACK'
        #9'OFFSET 50'#9'- FILE START SECTOR'
        #9'OFFSET 100'#9'- FILE BLOCKS LENGTH'
        #9'OFFSET 150'#9'- FILE LOAD ADDRESS LO BYTE'
        #9'OFFSET 200'#9'- FILE LOAD ADDRESS HI BYTE'
        ''
        
          'As you see deterministic loader require 5 bytes of info about ea' +
          'ch file. Trackmoliker script can '
        
          'sort and save info about up to 50 files on each disk side. Shoul' +
          'd this be enough?'
        
          'Please note "*.INC" file can be easily included to your code in ' +
          'TASM so there is no need for '
        
          'using silent dir from disk (18,2). This way loading process is s' +
          'ignificantly improved.'
        ''
        '5. Command line parameters:'
        ''
        #9'"filename.c64link"'#9'- name of your D64 project'
        ''
        #9'-SILENT'#9'- hide error messages'
        ''
        
          #9'-interleave $xx (or -interleave xx)'#9'- set hex/dec interleave va' +
          'lue for whole '
        'disk,'
        #9#9#9#9#9#9'  default $0a'
        ''
        #9'-L0'#9'- create D64 without Dirloader'
        #9'-L1'#9'- create D64 with Dirloader V1  (special option for hoxs64)'
        #9'-L2'#9'- create D64 with Dirloader V2  (special option for hoxs64)'
        ''
        #9'-DIRENT'#9'- files will be placed in DIRECTORY without loader'
        #9#9#9'  and info block (18,2)'
        ''
        
          #9'-FF'#9'- link to trackmo start file will be set on first file in D' +
          '64 project,'
        #9#9'  use only with L0 or L1 or L2'
        #9'-LF'#9'- like FF but pointer is set to last file'
        ''
        
          #9'-INC "filename"'#9'- make file with info data for deterministic lo' +
          'ader,'
        ''
        #9'-D64 "filename"'#9'- set D64 file name'
        ''
        #9'-DISKNAME "NAME OF DISK"'#9'- this can be max. 16 chars long'
        #9'-DISKID "ID..."'#9#9#9'- this can be max. 5 chars long'
        ''
        '-Example explained:'
        ''
        'CMD text:'
        
          '"trackmolinker.exe" "LINKNEW.C64LINK" -iNC "TRACKS DATA.INC" -L0' +
          ' -LF -D64 '
        '"DETERMINISTICLOAD"'
        ''
        
          'This will open project LINKNEW, save file with info "TRACKS DATA' +
          '.INC", make d64 file '
        
          'named "DETERMINISTICLOAD.D64" without Dirloader and start file l' +
          'ink in the dir will be set to '
        'last file.'
        ''
        '- Check out the batch file "createsample.bat".'
        ''
        ''
        ''
        '--------------------'
        'Advanced info:'
        '--------------------'
        ' '
        
          'When creating trackmo you can predict, assume and most important' +
          ' intend many things. This is '
        
          'due to fact that you know what you want to achieve, you got some' +
          ' kind of plan or structure in '
        
          'your mind of how things in your demo should roll. Part of that a' +
          're all files with your effects, '
        
          'graphics, music, tables that you want to put together. There is ' +
          'no guessing here I believe. You '
        
          'know which bank in RAM has been released and how much of a raste' +
          'rtime is free at certain '
        
          'point. You also exactly know what kind of stuff should be loaded' +
          ' to memory at what time. File '
        
          'name, file length, load address... All that is a base for my det' +
          'erministic loader.'
        ''
        
          'Idea is pretty simple yet very efficient. Let say you could crea' +
          'te a list of all files that should be '
        
          'on your disk. List that apart of file names stores also data abo' +
          'ut start tracks and start sectors, '
        
          'file lengths and load addresses. What are the advatages of havin' +
          'g all these sorted that way? '
        
          'Well this is what makes my loader deterministic as it has list o' +
          'f info about all files on disk. This '
        
          'way we do not lose 0.2 sec on every one of 35 tracks for scannin' +
          'g. We also do not have to '
        
          'wait for first block of the file with it'#39's load address and that' +
          ' is another 0.1 sec per file on '
        
          'average. When there is more than one file on track we do not los' +
          'e yes again 0.2 for scanning '
        
          'the same track again. Time savings are massive and all just beca' +
          'use we know where our files '
        'are placed before we even try to load them!'
        ''
        'Some additional optimizations for deterministic loader:'
        ''
        
          '- Function called WITHOUT STOP MOTOR will save some time on re-a' +
          'ccelerating floppys '
        'motor...'
        ''
        
          '- Files should be saved with interleave factor 4. It'#39's the best ' +
          'choice for small or partial files and it '
        
          'assures smooth loading. For example if file has only 3 blocks of' +
          ' data you do not have to wait '
        '0.2 sec for next sector as all has been saved optimally.'
        ''
        
          '- In case when your program can free just part of memory needed ' +
          'for next file due to low '
        
          'rastertime or because part of RAM is still in use you have not m' +
          'uch choice but to split that file.'
        
          'Option SPLIT FILE in Trackmolinker will make your life easier. I' +
          't allows you to change load '
        
          'addresses of each file. To divade file more than once simply sav' +
          'e, load and split again and '
        'again...'
        ''
        ''
        '--------------------'
        'Simple explanation of  how to use Det. Loader:'
        '--------------------'
        ''
        
          'First procedure is an installer and you have to use it at the st' +
          'art. This will just initialize the drive '
        'by simply calling:'
        ''
        #9';JSR PROC1'
        #9';BCC OK'
        #9';BCS DEV_NOT_PRESENT'
        ''
        
          'Number of a device can be set in $BA - it usually already contai' +
          'n last drive used anyway. First '
        'procedure will check which unit is present.'
        ''
        
          'IMPORTANT: Loader will use one block of RAM as a buffer (default' +
          ' $0100) for arriving data. If '
        'needed this can be altered in code '#8211' see BUFER1.'
        ''
        'Loader functions can be called from a jump table below:'
        ''
        #9';PROC2'#9'- LOAD FILE AND STOP MOTOR'
        #9';PROC2+3'#9'- DISCONNECT LOADER'
        #9';PROC2+6 '#9'- LOAD FILE WITHOUT STOPPING MOTOR'
        #9';PROC2+9'#9'- TURN OFF THE MOTOR'
        #9';PROC2+12 '#9'- SEND BYTE FROM ACC TO THE DRIVE '
        ''
        
          'For example to load file into $C000 from Track 1 Sector 0 do thi' +
          's:'
        ';---'
        
          #9';LDA #<LENFILE'#9'- This is a Lo/Hi pointer to 5 bytes file info-d' +
          'ata '
        #9';LDY #>LENFILE'#9'  (please see IMPORTANT INFO #4)'
        ''
        #9';JSR PROC2'#9#9';or JSR PROC2+6'
        #9';BCS *'#9#9#9'- LOAD ERROR - DEMO CRASHED'
        ';---'
        ''
        
          'When LOAD is completed you'#39'll find vector pointer to end of data' +
          ' in A and X reg (Lo/Hi)  for '
        'you decruncher. Add +1 for exomizer.'
        #9#9
        ';---'
        #9';LENFILE'#9#9'.BYTE LENBLK_FILE0'
        #9';VECADR'#9#9'.WORD $C000'
        #9';STRTTRK'#9#9'.BYTE $01'
        #9';STRTSEC'#9#9'.BYTE $00'
        ';---'
        ''
        'Check out my simple demo added to project.'
        ''
        
          'See change disk engine for special options. Using it you can sen' +
          'd your own programs to drive '
        'and then run them after...'
        ''
        ''
        ''
        
          'I hope you'#39'll like this little tool. Please feel free to ask any' +
          ' questions or to give suggestions.  '
        'Constructive criticism is always welcome.')
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 473
    Top = 46
    Width = 461
    Height = 314
    Align = alClient
    TabOrder = 4
    object Memo2: TMemo
      Left = 24
      Top = 24
      Width = 185
      Height = 89
      Lines.Strings = (
        'Memo2')
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
      Visible = False
    end
    object Memo3: TMemo
      Left = 248
      Top = 0
      Width = 185
      Height = 89
      Lines.Strings = (
        ''
        ';================================'
        ';1ST PROCEDURE OF:'
        ';THE DETERMINISTIC LOADER WORKING'
        ';WITH D64 CREATED FOR TRACKMOLINKER'
        '; (C) BY WEGI IN 2013-02-09'
        ';================================'
        ''
        'PROC1=$0A00'
        '* = PROC1'
        ';-'
        '.ALIGN 256'
        ';-'
        'VECTR    = $FB'
        'VECTR2   = $FD'
        ''
        'GOTORAM  = $0100 ;A SHORT PROC FOR GETTINGS DATA TO RAM OF DRIVE'
        ';-'
        ''
        '         JMP INITLOAD'
        ';--------'
        'TABKON1   '
        '         .BYTE $07,$87,$27,$A7,$47,$C7,$67,$E7'
        '         .BYTE $17,$97,$37,$B7,$57,$D7,$77,$F7'
        ';--------'
        'SENDDRIV1 '
        '         '
        '         BIT $DD00'
        '         BVC *-3'
        '         SEI'
        '         PHA'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         TAX'
        '         SEC'
        '-'
        '         LDA $D012'
        '         SBC #$32         '
        '         BCC +'
        '         AND #$07'
        '         BEQ -'
        ''
        'VIC1LO1   = *+1'
        '+'
        '         LDA #$03'
        '         STA $DD00'
        '         LDA TABKON1,X'
        '         STA $DD00'
        '         LSR A'
        '         LSR A'
        ''
        'VICF7A1   = *+1'
        ''
        '         AND #$F7'
        '         STA $DD00'
        '         PLA'
        '         AND #$0F'
        '         TAX'
        '         LDA TABKON1,X'
        '         STA $DD00'
        '         LSR A'
        '         LSR A'
        'VICF7B1   = *+1'
        '         AND #$F7'
        '         STA $DD00'
        ''
        '         NOP'
        '         NOP'
        ''
        '         NOP'
        ''
        'VICH21    = *+1'
        ''
        '         LDA #$23'
        '         STA $DD00'
        '         CLI'
        '         RTS'
        ';-'
        ''
        ''
        ''
        ';--------'
        ';-'
        'STOS     .BYTE 0 ;STACK SAVE VALUE'
        ';-'
        ';--------'
        'TBDRIV   .BYTE 8,9,10,11'
        ';---'
        'INITLOAD'
        '         TSX'
        '         STX STOS'
        ''
        'CONT'
        '         LDA $BA'
        '         LDX #$03'
        'CONT1'
        '         CMP TBDRIV,X'
        '         BEQ CONT2'
        '         DEX'
        '         BPL CONT1'
        '         LDA #$08'
        '         STA $BA'
        'CONT2'
        ';-'
        'FASTMW   LDA #$00'
        '         STA $D015'
        '         JSR LISTEN'
        '         LDA #"I"'
        '         JSR $FFA8'
        '         JSR $FFAE'
        ''
        '         LDA #<DRV'
        '         STA VECTR'
        '         LDA #>DRV'
        '         STA VECTR+1'
        ''
        '         LDA #<ADRDRV'
        '         STA VECTR2'
        '         LDA #>ADRDRV'
        '         STA VECTR2+1'
        ''
        '         LDA #<ADENDDRV'
        '         STA X3+1'
        '         LDA #>ADENDDRV'
        '         STA X3+2'
        ''
        '         JSR MEW'
        ''
        ';--------'
        'X0       LDA #$04'
        '         STA $02'
        '         LDY #$00'
        'X3       LDA $1000,Y'
        '         JSR SENDDRIV1'
        '         INY'
        '         BNE X3'
        '         INC X3+2'
        '         DEC $02'
        '         BNE X3'
        '         LDY #<RESTLDR'
        '         STY $02'
        '         LDY #$00'
        ''
        'X4A      LDA ADREST,Y'
        '         JSR SENDDRIV1'
        '         INY'
        '         INC $02'
        '         BNE X4A'
        ''
        '         CLC'
        '         CLI'
        '         RTS'
        ';========'
        'LISTEN         '
        '         LDA $BA'
        '         JSR $FFB1'
        '         LDA #$00'
        '         STA $90'
        '         LDA #$6F'
        '         JMP $FF93'
        ';--------'
        ';-'
        'MEW      LDX #3'
        'LOOP     '
        '         JSR LISTEN'
        '         BIT $90'
        '         BPL DALEJ'
        '         LDX STOS'
        '         TXS'
        '         LDA #$80  ;DNOTPRESENT'
        '         SEC'
        '         RTS'
        'DALEJ    LDA #$4D'
        '         JSR $FFA8'
        '         LDA #$2D'
        '         JSR $FFA8'
        '         LDA #$57'
        '         JSR $FFA8'
        '         LDA VECTR'
        '         JSR $FFA8'
        '         LDA VECTR+1'
        '         JSR $FFA8'
        '         LDA #$20'
        '         JSR $FFA8'
        '         LDY #$00'
        'LOOP1    LDA (VECTR2),Y'
        '         JSR $FFA8'
        '         INY'
        '         CPY #$20'
        '         BNE LOOP1'
        '         JSR $FFAE'
        '         LDA VECTR'
        '         CLC'
        '         ADC #$20'
        '         STA VECTR'
        '         BCC *+4'
        '         INC VECTR+1'
        '         LDA VECTR2'
        '         CLC'
        '         ADC #$20'
        '         STA VECTR2'
        '         BCC *+4'
        '         INC VECTR2+1'
        '         DEX'
        '         BNE LOOP'
        ';-'
        'MEX      '
        '         '
        '         JSR LISTEN'
        '         LDX #$00'
        '         LDA MEX1,X'
        '         JSR $FFA8'
        '         INX'
        '         CPX #$06'
        '         BNE *-9'
        '         JSR $FFAE'
        '         SEI'
        'VICHI1    = *+1'
        '         LDA #$23'
        '         STA $DD00'
        '         CLI'
        '         BIT $DD00'
        '         BVS *-3'
        '         CLC'
        '         RTS'
        ';-'
        'MEX1     .TEXT "M-E"'
        '         .BYTE <ABSRUN,>ABSRUN'
        ';-'
        'VBANK1    .BYTE 3'
        ';--------'
        ''
        ';--------'
        ';---------------------------------------'
        ';-'
        'CODE'
        'ADRDRV'
        ';-'
        '         .LOGICAL $0146'
        ';------  REDEFINED COS BAD ASSEMBLING'
        'MYDATA   = $01FF'
        ';-'
        'DRV'
        'GETBLK'
        '         JSR CLRLINE'
        'DRV2     LDA SERIAL'
        '         BNE *-3'
        '         PHP'
        '         LDA SERIAL'
        '         ASL A'
        '         PLP'
        '         EOR SERIAL'
        '         ASL A'
        '         ASL A'
        '         ASL A'
        '         NOP'
        '         NOP'
        '         NOP'
        '         EOR SERIAL'
        '         ASL A'
        '         NOP'
        '         NOP'
        '         NOP'
        '         EOR SERIAL'
        'ADRBLK   = *+2'
        '         STA $0300,Y'
        '         INY'
        '         BNE DRV2'
        'SETLINE  LDA #$08'
        'STLINE   STA SERIAL'
        '         RTS'
        'CLRLINE  LDA #$01'
        '         BNE STLINE'
        ';-'
        'ABSRUN   SEI'
        '         LDA #$7A'
        '         STA $1802'
        '         JSR SETLINE'
        '         JSR $F5E9'
        '         LDX #$04'
        'DRV1     JSR GETBLK'
        '         INC ADRBLK'
        '         DEX'
        '         BNE DRV1'
        '         INX'
        '         STX ADRBLK'
        '         LDY #<RESTLDR'
        '         JSR GETBLK'
        ';-'
        'RESTLDR'
        ''
        'ADREST   = CODE-DRV+RESTLDR'
        ';-'
        '         JMP INITRAM100'
        ';-'
        'SENDONE  TAY'
        '         AND #%00001111'
        '         TAX'
        '         TYA'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         TAY'
        '         JSR CLRLINE'
        '         LDA BIN2SER,X'
        '         LDX SERIAL'
        '         BNE *-3'
        '         STA SERIAL'
        '         ASL A'
        '         AND #%00001010'
        '         STA SERIAL'
        '         LDA BIN2SER,Y'
        '         STA SERIAL'
        '         ASL A'
        '         AND #%00001010'
        '         STA SERIAL'
        '         JMP SETLINE'
        'GETJOB   JSR GETONE'
        '         STA TRACK'
        '         JSR GETONE'
        '         STA SECTOR'
        ''
        'GETONE   SEI'
        '         LDY #$FF'
        '         JSR GETBLK'
        '         LDA MYDATA'
        '         STA TYPEWRK'
        '         RTS'
        ';-'
        'INITER   STA $00'
        '         CLI'
        '         LDA $00'
        '         BMI *-2'
        '         CMP #$02'
        'SETLED   LDA $1C00'
        '         ORA #$08'
        '         STA $1C00         '
        'ENDINI   RTS'
        'STPMTR   LDA $1C00'
        '         AND #$FB'
        '         STA $1C00'
        'LASTBT   RTS'
        '.HERE'
        ''
        ';----- PARAGRAPH @DRIVECODE@ -----'
        ''
        ';-'
        'ADENDDRV'
        ';-'
        '         .LOGICAL $0300'
        ';-'
        'TABSEC   = $0620 ;'
        'BUFENQ   = $0200 ;USED ON 2 METHOD'
        ';-'
        'TRACK    = $06'
        'SECTOR   = $07'
        'NXTRACK  = $08   ;AFTER SCAN '
        'NXSECTOR = $09   ;SCAN IS ALLWAYS ENABLED'
        'CNTRSEC  = $0A'
        'ENQN     = $0B   ;ORDER SECTOR IN SEND'
        ''
        'TYPEWRK  = $0C'
        '         ;=========================='
        '         ;#$01 WATCH AND WAIT WITHOUT STOPPING MOTOR'
        '         ;#$02 SEND SCANNED TRACK - HOWEVER YOU SIGN THIS'
        '         ;#$FF WATCH AND WAIT AND MOTOR STOP'
        '         ;#$00 DISCONNECT - END'
        
          '         ;#$04 GET DATA TO RAM - MAYBE FOR DRIVECALC, CHANGEDISK' +
          ' ENGINE...'
        '         ;==========================='
        ';---'
        ';GET_TO_RAM PROTOCOL'
        ';EXAMPLE VALUES'
        ';.BYTE 0'#9';LOW BYTE TARGET ADDRES IN DRIVE TO PUT DATA'
        ';.BYTE 6'#9';HI BYTE ($0600)'
        ';.BYTE 4'#9';JOB NR ALLWAYS 4'
        ';.BYTE 0   ;LOW BYTE TO RUN PROG'
        ';.BYTE 6   ;HI BYTE ($0600)'
        ';.BYTE 0   ;BYTES TO SEND 0 = 256'
        ';... RECIVING DATA'
        ';---'
        '         ; WHERE NO COMMENTS'
        '         ; - THE LABELS ARE CLEAR'
        ''
        'CNTR1    = $0D'
        'CNTR2    = $0E'
        'TMPR1    = $0F'
        'BLKCKSM  = $10'
        'LENFILE  = $11 ;HOW MANY BLOCK'#39'S FROM FILE TO SEND'
        ''
        'IDHEADER = $24    ;HEADER ID (FROM DOS)'
        'MAXSEC   = $43    ;HOW MANY SECTOR ON THE TRACK (FROM DOS)'
        ''
        'GCR1     = $52'
        'GCR2     = $53'
        'GCR3     = $54'
        ''
        'FINDSYNC = $F556'
        'HEADBAD  = $F41B'
        'IRQOK    = $F505'
        ''
        'LASTLONYBGCR = $07FF'
        ''
        'BFHINYBGCR = $0600'
        'BFLONYBGCR = $0700'
        'SERIAL   = $1800  ;THE SERIAL PORT...'
        ';-'
        'RUNLDR   JMP READ'
        ';-'
        'TRNS0    ;SEI'
        '         LDY #$00'
        'TRNS'
        '         JSR CLRLINE'
        ''
        'WAIT1    BIT SERIAL'
        '         BNE WAIT1'
        ''
        '         LDX #$08     ;BLINKING LINE'
        '         STX SERIAL'
        ''
        'WAIT2    BIT SERIAL'
        '         BEQ WAIT2'
        '         STA SERIAL'
        ''
        'TRNS2    LDX BFLONYBGCR,Y'
        '         LDA GCR2SER,X'
        '         STA SERIAL'
        '         ASL A'
        '         AND #%00001010'
        '         STA SERIAL'
        '         LDX BFHINYBGCR,Y'
        '         LDA GCR2SER,X'
        '         STA SERIAL'
        '         ASL A'
        '         AND #%00001010'
        '         STA SERIAL'
        ''
        '         LDX BFLONYBGCR+$80,Y'
        '         LDA GCR2SER,X'
        '         STA SERIAL'
        '         ASL A'
        '         AND #%00001010'
        '         STA SERIAL'
        '         LDX BFHINYBGCR+$80,Y'
        '         LDA GCR2SER,X'
        '         STA SERIAL'
        '         ASL A'
        '         AND #%00001010'
        '         STA SERIAL'
        ''
        '         EOR #$08    ;BLINK FOR SYNC'
        '         TAX'
        '         INY'
        '         TYA'
        '         STX SERIAL'
        ''
        '         AND #%00000011'
        '         BNE TRNS2'
        '         JSR SETLINE'
        '         CPY #$80'
        '         BNE TRNS'
        '         EOR $1C00'
        '         STA $1C00'
        '         RTS'
        ';-'
        'BIN2SER  .BYTE $0F,$07,$0D,$05,$0B,$03,$09,$01'
        'GCR2SER  .BYTE $0E,$06,$0C,$04,$0A,$02,$08,$00'
        '         .BYTE $FF,$0E,$0F,$07,$FF,$0A,$0B,$03'
        '         .BYTE $FF,$FF,$0D,$05,$FF,$00,$09,$01'
        '         .BYTE $FF,$06,$0C,$04,$FF,$02,$08,$FF'
        ';-'
        '         .TEXT "(C) BY WEGI"'
        ';-'
        'READ     JSR SCANNER'
        '         LDA CNTRSEC'
        '         JSR SENDONE'
        ';-'
        'READL'
        '         JSR HADER'
        ''
        '         LDA $1801'
        '         ORA #$20'
        '         STA $1801'
        ''
        '         JSR FINDSYNC'
        ';-'
        'K1       BVC *'
        '         CLV'
        '         LDA $1C01'
        '         TAX'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         STA BFHINYBGCR-1,Y'
        '         BVC *'
        '         CLV'
        '         LDA $1C01'
        '         LSR A'
        '         STA BFHINYBGCR,Y'
        '         TXA'
        '         STA BFLONYBGCR-1,Y'
        '         BVC *'
        '         CLV'
        '         LDA $1C01'
        '         TAX'
        '         ROR A'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         STA BFLONYBGCR,Y'
        '         BVC *'
        '         CLV'
        '         LDA $1C01'
        '         STA BFLONYBGCR+1,Y'
        '         ASL A'
        '         TXA'
        '         ROL A'
        '         AND #%00011111'
        '         STA BFHINYBGCR+1,Y'
        '         BVC *'
        '         CLV'
        '         LDA $1C01'
        '         STA BFHINYBGCR+2,Y'
        '         AND #%00011111'
        '         STA BFLONYBGCR+2,Y'
        '         INY'
        '         INY'
        '         INY'
        '         INY'
        '         BNE K1'
        'K2       BVC *'
        '         CLV'
        '         LDA $1C01'
        '         STA GCR1,Y'
        '         INY'
        '         CPY #$03'
        '         BNE K2'
        '         LDY #$00'
        'K3       LDA BFLONYBGCR-1,Y'
        '         STA TMPR1'
        '         LDA BFHINYBGCR,Y'
        '         TAX'
        '         AND #%00011111'
        '         STA BFHINYBGCR,Y'
        '         TXA'
        '         ASL A'
        '         ASL A'
        '         ROL TMPR1'
        '         ASL A'
        '         LDA TMPR1'
        '         ROL A'
        '         AND #%00011111'
        '         STA BFLONYBGCR-1,Y'
        '         LDA BFLONYBGCR+1,Y'
        '         TAX'
        '         AND #%00000011'
        '         STA TMPR1'
        '         TXA'
        '         LSR A'
        '         LSR A'
        '         AND #%00011111'
        '         STA BFLONYBGCR+1,Y'
        '         LDA BFHINYBGCR+2,Y'
        '         ASL A'
        '         ROL TMPR1'
        '         ASL A'
        '         ROL TMPR1'
        '         ASL A'
        '         LDA TMPR1'
        '         ROL A'
        '         STA BFHINYBGCR+2,Y'
        '         INY'
        '         INY'
        '         INY'
        '         INY'
        '         BNE K3'
        '         LDA GCR1'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         STA BFLONYBGCR-1'
        '         LDA GCR1'
        '         AND #%00000111'
        '         STA LASTLONYBGCR'
        '         LDA GCR2'
        '         ASL A'
        '         ROL LASTLONYBGCR'
        '         ASL A'
        '         ROL LASTLONYBGCR'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         TAX'
        '         ROR GCR3'
        '         LDA GCR3'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         TAY'
        '         LDA $1801'
        '         AND #$DF'
        '         STA $1801'
        '         LDA $F8A0,X'
        '         ORA $F8C0,Y'
        '         STA BLKCKSM'
        '         LDA $1800   ;TEST OF LAST CHECKSUM'
        '         LSR'
        '         BCS +'
        '         JMP ($FFFC) ;BAD OF BLOCK CHECKSUM - RESET THE DRIVE'
        '         +'
        ''
        '         JSR TRNS0'
        '         LDA BLKCKSM'
        '         JSR SENDONE  ;CHECKSUM SEND'
        '         LDA ENQN'
        '         JSR SENDONE  ;NR ORDER BY SECT.'
        ''
        ';-'
        '         DEC CNTRSEC'
        '         BEQ IREX7'
        '         JMP READL'
        'IREX7    '
        '         JMP IRQOK'
        ';-'
        ';----- PARAGRAPH @SCANNER@ -----'
        'SCANNER'
        ''
        '         LDA MAXSEC       ;NEW DETERMINISTIC SCAN PROCEDURE'
        '         STA CNTRSEC'
        ''
        '         JSR CLRENQ'
        ''
        '         INX'
        '         STX CNTR1'
        '         STX CNTR2'
        ''
        'SCLOOP'
        '         LDX CNTR1'
        '         LDA CNTR2'
        ''
        '         STA BUFENQ,X'
        '         TAY'
        '         TXA'
        '         STA TABSEC,Y'
        '         INC CNTR2'
        '         DEC CNTRSEC'
        '         BEQ SCL4'
        ''
        ''
        '         LDA CNTR1'
        '         CLC'
        ';************************'
        'MY_INTERLEAVE = *+1'
        '         ADC #$0A ;WILL BE CHECKED AND REFRESH'
        '         CMP MAXSEC'
        '         BCC SCL2B'
        '         SBC MAXSEC'
        'SCL2B'
        '         TAX'
        ''
        'SCL2'
        '         STX CNTR1'
        '         LDA BUFENQ,X'
        '         BMI SCLOOP'
        '         INX'
        '         CPX MAXSEC'
        '         BCC SCL2'
        '         LDX #$00'
        '         BEQ SCL2'
        'SCL4'
        ''
        '         JSR CLRENQ'
        ''
        '         LDX MAXSEC'
        '         LDA NXSECTOR'
        ''
        '         DEX'
        '         CMP TABSEC,X'
        '         BNE *-4'
        '         STX TMPR1'
        ''
        ''
        'SCL5'
        '         LDA TABSEC,X'
        '         TAY'
        '         TXA'
        '         SEC'
        '         SBC TMPR1'
        '         STA BUFENQ,Y'
        '         INC CNTRSEC'
        '         DEC LENFILE'
        '         BEQ FINSCAN'
        '         INX'
        '         CPX MAXSEC'
        '         BCC SCL5'
        '         LDX NXTRACK'
        '         INX'
        '         CPX #18'
        '         BNE *+3'
        '         INX'
        '         STX NXTRACK'
        '         LDA #$00'
        '         STA NXSECTOR'
        '         RTS'
        ';--------'
        'FINSCAN'
        '         LDX #$00'
        '         STX NXTRACK'
        '         INX'
        '         STX TYPEWRK'
        '         RTS'
        ';--------'
        'CLRENQ'
        '         LDX MAXSEC'
        '         LDA #$FF'
        '         STA BUFENQ,X'
        '         DEX'
        '         BPL *-4'
        '         RTS'
        ';------'
        ' '
        '_LENSCAN = * - SCANNER'
        ';-'
        'HADER    LDY #$00'
        '         STY CNTR1'
        '         STY CNTR2'
        '         LDA #$07'
        '         STA $31'
        'HLOOP    DEC CNTR2'
        '         BNE FINDH'
        'HERROR   JMP HEADBAD'
        'FINDH    JSR FINDSYNC'
        '         BVC *'
        '         CLV'
        '         LDA $1C01'
        '         CMP IDHEADER'
        '         BNE HLOOP'
        '         INY'
        'HREAD    BVC *'
        '         CLV'
        '         LDA $1C01'
        '         STA ($30),Y'
        '         INY'
        '         CPY #$04'
        '         BNE HREAD'
        '         LDY #$00'
        '         JSR $F7E8'
        '         LDX GCR3'
        '         CPX MAXSEC'
        '         BCS HERROR'
        '         LDA BUFENQ,X'
        '         BPL HEND'
        '         INC CNTR1'
        '         BPL FINDH'
        '         BMI HERROR'
        'HEND     STX SECTOR'
        '         STA ENQN'
        '         LDA #$FF'
        '         STA BUFENQ,X'
        '         RTS'
        ';-'
        '         ;WATCH AND WAIT'
        'MTORSTOP'
        'STRTLDR'
        ''
        #9#9'JSR STPMTR'
        'WITHOUT_STOP'
        ';-       '
        #9#9'JSR SETLED'
        '         JSR GETJOB'
        '         LDA SECTOR'
        '         STA NXSECTOR'
        '         LDA TRACK'
        '         STA NXTRACK'
        '         ;=========================='
        '         ;#$01 WATCH AND WAIT WITHOUT STOPPING MOTOR'
        '         ;#$02 SEND SCANNED TRACK - HOWEVER YOU SIGN THIS'
        '         ;#$FF WATCH AND WAIT AND MOTOR STOP'
        '         ;#$00 DISCONNECT - END'
        
          '         ;#$04 GET DATA TO RAM - MAYBE FOR DRIVECALC, CHANGEDISK' +
          ' ENGINE...'
        '         ;==========================='
        ';---'
        ';GET_TO_RAM PROTOCOL'
        ';EXAMPLE VALUES'
        ';.BYTE 0'#9';LOW BYTE TARGET ADDRES IN DRIVE TO PUT DATA'
        ';.BYTE 6'#9';HI BYTE ($0600)'
        ';.BYTE 4'#9';JOB NR ALLWAYS 4'
        ';.BYTE 0   ;LOW BYTE TO RUN PROG'
        ';.BYTE 6   ;HI BYTE ($0600)'
        ';.BYTE 0   ;BYTES TO SEND 0 = 256'
        ';... RECIVING DATA'
        ';---'
        'ELO      LDA TYPEWRK'
        '         BEQ EOFI'
        '         BMI MTORSTOP'
        '         CMP #$01'
        '         BEQ WITHOUT_STOP'
        '         CMP #$04'
        '         BNE SENDSTX ;SO $02 CODE LAST ONE POSSIBLITY'
        '         JSR GOTORAM'
        '         JMP STRTLDR'
        'SENDSTX'
        '         JSR GETONE'
        '         STA LENFILE'
        'SENDST         '
        '         LDA NXTRACK'
        '         BEQ ELO'
        '         STA TRACK'
        '         LDA NXSECTOR'
        '         STA SECTOR'
        'ELO2'
        '         JSR MOTORSTART'
        '         LDA #$E0'
        '         JSR INITER'
        '         BCC SENDST'
        'BAD1'
        ''
        '         JSR EOFI'
        '         LDA #$05'
        '         '
        'BAD2     JSR DELAY'
        '         TAX'
        '         DEX'
        '         TXA'
        '         BNE BAD2'
        '         JSR SETLINE'
        '         JSR DELAY'
        '         JMP STRTLDR'
        'EOFI'
        '         JSR CLRLINE'
        '         STA $1C'
        '         RTS'
        ''
        'MOTORSTART'
        '         SEI'
        '         LDA $1C00'
        '         TAX'
        '         ORA #$04'
        '         STA $1C00'
        '         TXA'
        '         AND #$04'
        '         BNE MOTOROK'
        'DELAY'
        '         LDX #$00'
        '         DEY'
        '         BNE *-1'
        '         DEX'
        '         BNE *-4'
        'MOTOROK'
        '         RTS'
        ''
        '__EOFDRIVECODE'
        ';====================='
        'INITRAM100'
        #9#9'LDA #$10'
        #9#9'STA $1C07'
        #9#9'LDA $1801'
        #9#9'AND #$DF'
        #9#9'STA $1801'
        #9#9#9#9
        #9#9'LDX #$20'
        #9'-'
        #9#9'LDA GETTORAM,X'
        #9#9'STA GOTORAM,X'
        #9#9'DEX'
        #9#9'BPL -'
        #9#9'LDX #$BA'
        #9#9'LDA $0100,X'
        #9#9'STA $0600,X'
        #9#9'INX'
        #9#9'BNE *-7'
        '         '
        #9#9'LDA #$01  ; GO NEAR START OF DATA BEFORE'
        #9#9'STA $06'
        #9#9'STA $0E'
        #9#9'LDA #$00'
        #9#9'STA $07'
        #9#9'STA $0F'
        #9#9'         '
        #9#9'LDA #$B0'
        #9#9'JSR INITER2'
        #9#9'BCC +'
        #9#9'LDA #$C0'
        #9#9'JSR INITER2'
        #9#9'LDA #$B0'
        #9#9'JSR INITER2'
        #9'+'
        #9'-'#9'LDA #$80'
        #9#9'JSR INITER2'
        #9#9'BCS -'
        #9#9'LDA $0700'
        #9#9'BNE +'
        #9#9'INC $0E'
        #9#9'LDA $0E'
        #9#9'CMP #7'
        #9#9'BNE -'
        #9#9'BEQ SKIP1'
        #9'+'#9'LDA $0701'
        #9#9'STA MY_INTERLEAVE'
        'SKIP1'#9'LDA #$B0'
        #9#9'STA $00'
        #9#9'LDA $00'
        #9#9'BMI *-2'
        #9#9
        #9#9'LDX #$BA'
        #9#9'LDA $0600,X'
        #9#9'STA $0100,X'
        #9#9'INX'
        #9#9'BNE *-7'
        '   '
        #9#9'SEI'
        #9#9'JMP STRTLDR'
        'INITER2'
        #9#9'SEI'
        #9#9'LDX #4'
        #9#9'STA $0298'
        #9#9'JSR $D57D'
        #9#9'LDA #5'
        #9#9'STA $6A'
        #9#9'CLI'
        #9#9'JSR $D599'
        #9#9'CMP #$02'
        #9#9'RTS        '
        ';==========         '
        'GETTORAM'
        '         LDA TRACK         ;SHORT PROCEDURE'
        '         STA $08           ;FOR GETTING DATA TO'
        '         LDA SECTOR        ;RAMDRIVE'
        '         STA $09'
        '         LDA #$00'
        '         STA $0A'
        '         JSR GETJOB'
        '         TAX'
        '         -'
        '         JSR GETONE'
        '         LDY $0A'
        '         STA ($08),Y'
        '         INC $0A'
        '         DEX'
        '         BNE -'
        '         '
        '         JMP ($0006) ;RUN'
        ';==========         '
        ';-'
        '_ENDLDR'
        '.HERE'
        ';==========================='
        '_END_PROC1'
        ''
        '')
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 1
      Visible = False
    end
    object Memo4: TMemo
      Left = 248
      Top = 72
      Width = 185
      Height = 89
      Lines.Strings = (
        ';----- PARAGRAPH @STARTPROC2@ -----'
        ';================================'
        ';2ND PROCEDURE OF:'
        ';THE DETERMINISTIC LOADER WORKING'
        ';WITH D64 CREATED FOR TRACKMOLINKER'
        '; (C) BY WEGI IN 2013-02-09'
        ';================================'
        'PROC2 = $a000'
        ''
        '* = PROC2'
        '         .ALIGN 256'
        ';-'
        'BUFER1   = *-$0100'
        ';-'
        '         ;A JUMP TABLE OF THIS'
        '         ;PROC2    = LOAD FILE AND STOPPING MOTOR'
        '         ;PROC2+3  = DISCONNECT LOADER'
        '         ;PROC2+6  = LOAD FILE WITHOUT STOP MOTOR'
        '         ;PROC2+9 = TURN OFF THE MOTOR'
        '         ;PROC2+12 = SEND BYTE FROM ACC TO THE DRIVE '
        '         '
        '         JMP STARTLOAD          ;LOAD AND AFTER THIS STOP MOTOR'
        '         JMP LOADEROFF          ;DISCONNECT LOADER'
        '         JMP LOAD_WITH_MOTOR_ON ;LOAD WITHOUT STOP MOTOR'
        '         JMP STOPMOTOR          ;WATCH AND WAIT LOADER WITH '
        ';========================'
        ';--------'
        'SENDDRIV2       ;SEND ONE BYTE'
        ''
        '         PHP    ;FOR SAVE SEI CLI - INTERRUPT FLAG'
        '         PHA    ;THE Y REGISTER IS NOT TOUCHED'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         LSR A'
        '         TAX'
        '         '
        '         BIT $DD00'
        '         BVC *-3'
        '       '
        '         SEC'
        '         SEI'
        ''
        '        -'
        '         LDA $D012'
        '         SBC #$32'
        '         BCC +'
        '         AND #$07'
        '         BEQ -'
        ''
        'VIC1LO2   = *+1'
        '        +'
        '         LDA #$03'
        '         STA $DD00'
        '         LDA TABKON,X'
        '         STA $DD00'
        '         LDA TABKONVER2,X'
        '         NOP'
        '         STA $DD00'
        ''
        '         PLA'
        '         AND #$0F'
        '         TAX'
        ''
        '         LDA TABKON,X'
        '         STA $DD00'
        '         LDA TABKONVER2,X'
        '         NOP'
        '         STA $DD00'
        ''
        '         NOP'
        '         NOP'
        '         NOP'
        'VIC2HI2   = *+1'
        '         LDA #$23'
        '         STA $DD00'
        '         PLP     ;RECALL INTERRUPT FLAG'
        '         ;CLI'
        '         RTS'
        ';-'
        ''
        'TABKON   '
        '         .BYTE $03,$83,$23,$A3,$43,$C3,$63,$E3'
        '         .BYTE $13,$93,$33,$B3,$53,$D3,$73,$F3'
        'TABKONVER2'
        '         .BYTE $03,$23,$03,$23,$13,$33,$13,$33'
        '         .BYTE $03,$23,$03,$23,$13,$33,$13,$33'
        ';--------'
        'TRNSM1   LDY #$00   ;GET ALL 256 BYTES OF BLOCK'
        '         BIT $DD00'
        '         BVC *-3'
        'WAITER         '
        '        -'
        '         SEI'
        '         LDA $D012'
        '         AND #$07'
        '         CMP #$03'
        '         BNE -'
        ''
        'VIC2LO2   = *+1'
        ''
        '         LDA #$03'
        '         NOP'
        '         STA $DD00'
        '         ;NOP'
        '         ;NOP'
        'BLINK1         '
        '         ;STA $D020'
        '         BIT $D020'
        '         NOP'
        '         EOR #$20'
        '         STA $DD00'
        ''
        '         LDA $DD00'
        '         NOP'
        '         CMP $DD00'
        '         BNE LAD1'
        ''
        '         NOP'
        '         NOP'
        ''
        'LAD1     AND #$03'
        '         STA $DD00'
        'LAD3     LDA $DD00'
        '         LSR A'
        '         LSR A'
        '         EOR $DD00'
        ''
        'VIC30A2   = *+1'
        ''
        '         EOR #$30'
        '         LSR A'
        '         LSR A'
        '         NOP'
        '         EOR $DD00'
        '         LSR A'
        '         LSR A'
        '         EOR $DD00'
        ''
        'ADBF1    = *+2'
        '         STA BUFER1,Y'
        '         NOP'
        '         NOP'
        '         LDA $DD00'
        '         LSR A'
        '         LSR A'
        '         EOR $DD00'
        ''
        'VIC30B2   = *+1'
        ''
        '         EOR #$30'
        '         LSR A'
        '         LSR A'
        '         INY'
        '         EOR $DD00'
        '         LSR A'
        '         LSR A'
        '         EOR $DD00'
        '         LDX $DD00 ;HERE IS'
        '         CPX $DD00 ;A BIT SYNC WITH CLOCK OF DRIVE'
        '         BEQ *+2'
        ''
        'ADBF1A   = *+2'
        '         STA BUFER1+$7F,Y'
        ''
        '         TYA'
        '         AND #$03'
        '         BNE LAD3'
        'VIC1HI2   = *+1'
        '         LDA #$23'
        '         STA $DD00'
        'BLINK2         '
        '         ;STY $D020'
        '         BIT $D020'
        '         CLI'
        'CMPR2    = *+1'
        '         CPY #$80'
        '         BNE WAITER'
        '         RTS'
        ';--------'
        'INITVICBANK2'
        ''
        '         AND #$03        ;ALL INVOKED TO $DD00 ARE CORRECTED'
        '         STA VBANK2      ;FOR CHOICED BANK'
        '         LDX #$0F'
        '        -'
        '         LDA TABKON,X   '
        '         AND #$F0'
        '         ORA VBANK2'
        '         STA TABKON,X'
        ''
        '         LDA TABKONVER2,X'
        '         AND #$F0'
        '         ORA VBANK2'
        '         STA TABKONVER2,X'
        '         '
        '         DEX'
        '         BPL -'
        '         LDA VBANK2'
        '         TAX'
        '         '
        '         AND #$03'
        '         ASL A'
        '         ASL A'
        '         ASL A'
        '         ASL A'
        '         STA VIC30A2'
        '         STA VIC30B2'
        '         TXA'
        '         STA VIC1LO2'
        '         STA VIC2LO2'
        '         STA VIC3LO2'
        '        '
        '         ORA #$20'
        '         STA VIC1HI2'
        '         STA VIC2HI2'
        '         RTS'
        ''
        ';-'
        'POB      BIT $DD00     ;GET ONE BYTE'
        '         BVC *-3       ;Y REGISTER IS NOT USED HERE'
        '         PHP           ;INTERRUPT FLAG'
        '         SEI'
        '         SEC'
        ''
        '       -         '
        '         LDA $D012'
        '         SBC #$32'
        '         BCC +'
        '         AND #$07'
        '         BEQ -'
        ''
        'VIC3LO2   = *+1'
        '        +'
        '         LDA #$03'
        '         STA $DD00'
        '         STA VIC4LO2'
        '         NOP'
        '         ORA #$20'
        '         TAX'
        '         LDA $DD00'
        '         LSR A'
        '         LSR A'
        '         EOR $DD00'
        '         LSR A'
        '         LSR A'
        '         EOR $DD00'
        '         LSR A'
        '         LSR A'
        '         EOR $DD00'
        '         STX $DD00'
        'VIC4LO2   = *+1'
        '         EOR #$03'
        '         ;CLI'
        '         PLP    ;RECAL INTERRUPT FLAG'
        '         RTS'
        ';--------'
        'BADCK'
        '         LDA $DD00'
        '         AND #$03'
        '         STA $DD00'
        '         LDX STACK'
        '         TXS'
        '         LDA #$23'
        '         SEC'
        '         RTS'
        ';--------'
        'STRTTRS'
        '         JSR TRNSM1   ;GET BLOCK'
        '         JSR POB      ;GET CHECKSUM'
        '         STA CKSMBLK  '
        '         JSR POB      ;GET WHICH ONE SECTOR'
        '__KONTOL         STA SECTNR   ;ON THE TRACK IS NOW'
        ''
        'CHECKBIN LDA CKSMBLK         ;CALCULATE CHECKSUM'
        '         LDX #$1F'
        'CKB2     EOR BUFER1,X'
        '         EOR BUFER1+$20,X'
        '         EOR BUFER1+$40,X'
        '         EOR BUFER1+$60,X'
        '         EOR BUFER1+$80,X'
        '         EOR BUFER1+$A0,X'
        '         EOR BUFER1+$C0,X'
        '         EOR BUFER1+$E0,X'
        '         DEX'
        '         BPL CKB2'
        '         CMP #$00  ;CHECSUM OK??'
        '         BNE BADCK ;IF NO - ERROR AND END LOADING'
        '         '
        '         RTS'
        ''
        ';--------'
        'LADUJ'
        '         JSR POB    ;GET HOW MANY SECTOR OF FILE ON THIS TRACK'
        '         STA LICZSEC'
        '         STA LICZSEC2'
        '         CMP #22'#9';IF MORE THAN 21'
        '         BCS *'#9#9';SOMETHING IS WRONG - ERROR!'
        ''
        ''
        
          'LADOR2B  JSR STRTTRS ;GET BLOCK TO BUFFER, NR SECT, CHECKSUM AND' +
          ' CALCULATE IT'
        '         JSR SETBASE ;SAVE DATA FROM BLOCK TO RAM'
        '         LDA BUFER1  ;IF IS THIS LAST BLOCK'
        '         BNE +'
        '         STA MARKER  ;SAVE THIS TO OUR INDICATOR'
        '         +'
        '         '
        '         DEC LICZSEC ;HOW MANY SECTOR ON THE TRACK...'
        '         BNE LADOR2B'
        
          '         INC WASFIRST ;AFTER THIS LOOP REWRITE INDICATOR OF FIRS' +
          'T TRAC'
        '         LDA LENFILE2 ;IF YOU WANNA READ 1 BLOCK FROM DISK'
        '         CMP #1'
        '         BEQ +'
        '         LDA MARKER ;WAS THE LAST BLOCK?'
        '         BEQ +'
        '         '
        '         JMP LADUJ  ;LOAD LOOP'
        '         '
        '         +'
        '         RTS'
        ';--------'
        'READ1SEC'
        'STRTTRK  =*+1   ;START TRAC FOR DRIVE'
        '         LDA #$00'
        '         JSR SENDDRIV2'
        'STRTSEC  =*+1   ;START SECTOR OF FILE'
        '         LDA #$00'
        '         JSR SENDDRIV2'
        '         '
        '         LDA #$02  ;SELECTED SECTOR SEND'
        '         JSR SENDDRIV2'
        'VBANK2 = LENFILE2'
        'LENFILE2 =*+1'
        '         LDA #$00 ;BLOCKS LENFILE AND SOME TIMES VICBANK'
        '         JMP SENDDRIV2'
        ''
        'STOPMOTOR'
        'WATCHLOADER'
        '         LDA #$FF'
        '         .BYTE $2C'
        ';---'
        'LOADEROFF'
        ';--------'
        '         LDA #$00'
        '         PHA'
        '         JSR SENDDRIV2'
        '         JSR SENDDRIV2'
        '         PLA'
        '         JMP SENDDRIV2'
        ';--------'
        ''
        ';---------------------------------------'
        ';-------------------------------------'
        '; GET BASE ADRESS OF TRACK AND ADD FOR'
        '; NUMBER OF SECTOR VALUE'
        ';-------------------------------------'
        'SETBASE'
        '         LDA BASEADR'
        '         STA GDZIELO'
        '         LDY BASEADR+1'
        '         STY GDZIEHI'
        '         '
        '         LDX SECTNR'
        '         BEQ SETB3'
        ''
        ';--------------------------------'
        '; BELLOW BIT STRANGE MAYBE BUT:'
        '; X * $FE = X * 254 <=> ((X*256) - (2*X))'
        '; WITHOUT LOOP - FASTER'
        ';--------------------------------'
        'SETB2    LDA GDZIEHI'
        '         CLC'
        '         ADC SECTNR  ;ADC X*256'
        '         STA GDZIEHI ;OK ADRESS '
        '         TXA         ;X -> A'
        '         ASL         ;X*2'
        '         EOR #$FF    ;HERE X * (-1)'
        '         ;CLC        ;AFTER ASL C=0'
        '         ADC #$01'
        
          '         ;CLC         ;X FROM RANGE 1 TO 22 IN THE NEGATIVE + 1 ' +
          'WILL BE NEVER OVERFLOV'
        '         ADC GDZIELO  ;SUBSTRACT BY ADD'
        '         STA GDZIELO  ;COS X < 0'
        '         BCS SETB3'
        '         DEC GDZIEHI'
        'SETB3    '
        ';====================================='
        '; OK NOW WE HAVE ALL FOR REWRITE DATA'
        '; TO RAM COMMODORE 64 - SO LET'#39'S BEGIN'
        ';====================================='
        'PREPI'
        
          '         LDY BUFER1    ;INDICATOR FROM LINK - IF = 0 THEN THIS I' +
          'S THE LAST SECTOR'
        
          '         LDX BUFER1+1  ;OFSET OF LAST BYTE - IF IS THIS LAST SEC' +
          'TOR'
        '         DEX'
        '         '
        ';=========  DEFAULT VALUES        '
        '         LDA #$FE'
        '         STA ILE      ;IF NOT FIRST SECTOR $FE BYTES OF DATA'
        '                      ;LABEL '#39'ILE'#39' <=> '#39'HOW MANY'#39
        '         LDA #$02'
        '         STA PREPI2+1 ;IF NOT FIRST SECTOR - OFFSET 2'
        ';=========         '
        '         LDA WASFIRST ;IT'#39'S THE 1ST TRACK?'
        '         BNE NOFIRSTTRACK'
        ''
        '         LDA SECTNR   ;IT'#39'S THE FIRST SECTOR?'
        '         '
        '         BNE NOFIRSTSEC'
        '         LDA #$04     ;1ST SECTOR - OFFSET 4'
        '         STA PREPI2+1         '
        '         LDA #$FC     ;IF IS THE 1ST SECTOR $FC BYTES OF DATA'
        '         STA ILE'
        '         CPY #$00     ;IS 1ST TRACK OUR LAST TRACK??'
        '         BNE FIRSTNOLAST '
        '                      ;IF FIRST SECTOR IS ALSO LAST'
        '                      ;GET LAST BYTE OFFSET'
        '         '
        '         DEX          ;OFSET OF LAST BYTE + 2'
        '         DEX          ; IN X REGISTER'
        '         STX ILE      ;COUNT OF LAST SECTOR DATA'
        'FIRSTNOLAST'
        '         JMP STARTPREPI'
        
          ';---------------------------------------------------------------' +
          '--'
        
          ';EVERY ONE SECTOR ON THE 1ST TRACK - MUS BE HAVE CORRECTED ADRES' +
          'S'
        
          ';---------------------------------------------------------------' +
          '--'
        'NOFIRSTSEC  '
        '         LDA GDZIELO'
        '         SEC'
        '         SBC #$02     ;CORRECT ON THE 1ST TRACK'
        '         STA GDZIELO  ;OFFSET MUST BE LESS 2'
        '         BCS NOFIRSTTRACK'
        '         DEC GDZIEHI'
        ';------------------------------------'
        '; IS IS THE LAST SECTOR WE MUST'
        '; WRITING LENGTH OF DATA FOR WRITE'
        ';------------------------------------'
        'NOFIRSTTRACK'
        '         CPY #$00    ;LAST SECTOR?? IN Y LINK OF SECTOR'
        '         BNE STARTPREPI'
        '                      ;OFSET OF LAST BYTE'
        '                      ; IN X REGISTER'
        '         '
        '         STX ILE      ;COUNT OF LAST SECTOR DATA'
        '                      ;IF IS NOT SECTOR 0'
        ';---------------------------'
        '; COPY BLOCK OF DATA TO RAM'
        ';---------------------------'
        'STARTPREPI'
        '         LDX #$00'
        ''
        '         LDA $01'
        '         PHA        ;SAVE $01'
        '         LDA #$34'
        '         STA $01'
        'PREPI2'
        '         LDA BUFER1,X       ;EXACTLY HERE'
        'GDZIELO  = *+1              ;BLOCK COPY'
        'GDZIEHI  = *+2'
        '         STA BUFER1,X'
        ''
        '         INX'
        ''
        'ILE      = *+1'
        '         CPX #$00'
        ''
        '         BNE PREPI2'
        '         PLA        ;RECALL $01'
        '         STA $01'
        '        '
        ';----------------------------'
        '; IF IS THE LAST SECTOR'
        '; CALCULATE END ADRESS'
        ';----------------------------'
        '         '
        '         LDA BUFER1   ;IT'#39'S THE LAST SECTOR?'
        '         BNE PREPI4'
        '         '
        '         '
        '         LDA GDZIELO  ;YES LAST SECTOR'
        '         CLC'
        '         ADC ILE      ;CALCULATE END ADRESS'
        '         STA EOF'
        '         LDA GDZIEHI  ;OF FILE'
        '         ADC #$00'
        '         STA EOF+1'
        ';----------------------------------------------'
        ';ON EVERY ONE LAST SECTOR ARRIVING TO C64'
        ';MUST CALCULATED BASEADRES FOR NEXT TRACK DATA'
        ';----------------------------------------------'
        'PREPI4'
        '         LDX LICZSEC2   ;LAST SECTOR NUMBER FROM TRACK'
        '         DEX'
        '         CPX SECTNR     ;IS HERE NOW THE LAST SECTOR?'
        '         BNE PREPI3     ;NO'
        '         CPX #$00'
        '         BNE PREPI4A    ;THAT WAS ONLY ONE SECTOR ON THE TRACK?'
        
          '         CPX WASFIRST   ;YES ONLY ONE BUT THAT WAS THE FIRST SEC' +
          'TOR OF FILE?'
        
          '         BEQ PREPI3     ;YES - DON'#39'T CALC NEXTBASE - WE HAVE GOT' +
          'TA HIS BEFORE!!'
        ''
        'PREPI4A             '
        '         LDA GDZIELO    ;YES THAT WAS THE LAST SECTOR'
        '         CLC            ;FROM TRACK'
        '         ADC #$FE'
        '         STA NEXTBASE   ;SO WITHOUT BIG CALCULATIONC'
        '         LDA GDZIEHI    ;GET THE ADRESS FOR NEW DATA FROM'
        '         ADC #$00       ;NEW (NEXT) TRACK'
        '         STA NEXTBASE+1'
        ';------'
        'PREPI3'
        
          '         LDX LICZSEC     ;WAS THAT THE LAST SECTOR FORM TRACK RE' +
          'ADING NOW?'
        '         CPX #1'
        '         BNE +           ;NO'
        '         '
        '         LDA NEXTBASE    ;YES MAKE BASEADR FOR NEW TRACK'
        '         STA BASEADR'
        '         LDA NEXTBASE+1'
        '         STA BASEADR+1'
        '         +    '
        '         RTS'
        ';--------'
        
          'BASEADR  .WORD 0   ;ADRESS START OF DATA IN THE C64 FROM ACTUAL ' +
          'TRACK'
        'NEXTBASE .WORD 0   ;ADRESS OF DATA FOR NEXT TRACK'
        'EOF      .WORD 0   ;END OF FILE ADRESS'
        
          'WASFIRST .BYTE 0   ;INDICATOR - IS THE FIRST TRACK WHICH WE READ' +
          'ING | 0 = YES | <> 0 = NO |'
        ';--------'
        
          'MARKER   .BYTE 0   ;INDICATOR - LAST SECTOR WE HAVE ALLREADY?? |' +
          ' 0 = YES | <> 0 = NO |'
        ';-'
        'SECTNR   .BYTE 0   ;SECTOR NR OF ORDER'
        'CKSMBLK  .BYTE 0   ;BLOCK CHECKSUM'
        'LICZSEC  .BYTE 0   ;VALUES OF SECTORS ON THE'
        'LICZSEC2 .BYTE 0   ;TRACK OUR FILE'
        ';-'
        'STACK     .BYTE 0  ;SAVE STACK'
        ';-'
        ''
        'GET_DATA_BYTE'
        #9#9'LDX $01'
        #9#9'LDA #$34'
        #9#9'STA $01'
        'LOW_GET = *+1'
        'HI_GET  = *+2'
        #9#9'LDA $1111,Y'
        #9#9'INY'
        #9#9'STX $01'
        #9#9'RTS'
        ';========================'
        'LOAD_WITH_MOTOR_ON'
        '         LDX #$2C'
        '         .BYTE $2C'
        'STARTLOAD'
        '         LDX #$20'
        '         STX ENDLOAD'
        #9#9'STA LOW_GET'
        #9#9'STY HI_GET'
        #9#9'LDA $DD00'
        
          '         JSR INITVICBANK2       ;IN ACC BANK OF VIC - LOW NYBBLE' +
          ' IS IMPORTANT HI IS IGNORED'
        ''
        '         LDY #$00'
        
          '         STY WASFIRST ;FIRST TRACK - INDICATOR FOR CORRECT #2 AD' +
          'RESS OF SECTOR'
        ''
        '         TSX'
        '         STX STACK'
        '         STX MARKER'
        '         '
        '         JSR GET_DATA_BYTE'
        '         STA LENFILE2'
        'STRL2'
        '         JSR GET_DATA_BYTE'
        '         STA BASEADR'
        '         CLC'
        '         ADC #$FC       ;IF FIRST SECTOR IS ONE ON FIRST TRACK'
        '         STA NEXTBASE   ;CALCULATE NEXTBASE ADRESS'
        '         '
        '         JSR GET_DATA_BYTE'
        '         STA BASEADR+1'
        '         ADC #$00'
        '         STA NEXTBASE+1 ;HI BYTE OF NEXTBASE'
        '         '
        'FROMTRACK'
        '         '
        '         JSR GET_DATA_BYTE'
        '         STA STRTTRK'
        '         '
        '         JSR GET_DATA_BYTE'
        '         STA STRTSEC'
        ''
        'FIRSTSECT'
        '         JSR READ1SEC'
        '         JSR LADUJ'
        ''
        'ENDLOAD'
        '         JSR WATCHLOADER'
        '         LDA EOF'
        '         LDX EOF+1'
        '         ;AT THE END - CARRY IS CLEAR AND FOR EXODECRUNCHER'
        
          '         ;IN ACC LOW BYTE OF EOF FILE IN X REG HI BYTE OF EOF FI' +
          'LE'
        '         CLC'
        '         RTS'
        ';====================='
        ''
        ';--------'
        'END_PROC2'
        ';******************************************************'
        ''
        ';----- PARAGRAPH @ENDPROC2@ -----'
        '')
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 2
      Visible = False
    end
    object Memo5: TMemo
      Left = 248
      Top = 168
      Width = 185
      Height = 89
      Lines.Strings = (
        ';================================'
        ';CHANGEDISK ENGINE FOR:'
        ';THE DETERMINISTIC LOADER WORKING'
        ';WITH D64 CREATED FOR TRACKMOLINKER'
        '; (C) BY WEGI IN 2013.02.09'
        ';THIS VERSION ISN'#39'T BASED ON WRITE PROTECTION TEST'
        ';FOR DRIVE WITH DAMAGED WRITE PROTECTION ELECTRONIC'#39'S'
        ';================================'
        ';==='
        
          'TRACK_INDICATOR = $01  ; TRACK AND SECTOR WITH DATA OF IDICATOR ' +
          'SIDE'
        'SECT_INDICATOR = $00'
        ';==='
        'MYCHGDSK = $1F00'
        '* = MYCHGDSK'
        'PROC2 = $1B00 ;WHERE IS LOADER'
        'SENDDRIV2 = PROC2 + 12'
        #9#9' ;PROC2    = LOAD FILE AND STOP MOTOR'
        #9#9' ;PROC2+3  = DISCONNECT LOADER'
        #9#9' ;PROC2+6  = LOAD FILE WITHOUT STOP MOTOR'
        #9#9' ;PROC2+9 = TURN OFF THE MOTOR'
        #9#9' ;PROC2+12 = SEND BYTE FROM ACC TO THE DRIVE '
        ';==============='
        ';SENDDRIV2 = PROC2+12'
        ';========================='
        '; IF YOU NEED CHANGE THE DISK ENGINE BELOW IS THE PROCEDURE'
        '; YOU MUST PUT YOUR UNIQUE DATA (MAX 20BYTES) TO SECTOR'
        
          '; AND PUT SECTOR AND TRACK NUMBER TO "TRACK_INDICATOR" AND "SECT' +
          '_INDICATOR" VARIABLE'
        '; AND CALL "JSR SECONDSIDETEST"'
        ';========================='
        'SECONDSIDETEST'
        ''
        #9#9' LDY #$00'
        #9#9' -'
        #9#9' LDA CODESEND,Y'
        #9#9' JSR SENDDRIV2   ;SEND JOB CODE TO LOADER'
        #9#9' INY '
        #9#9' CPY #$06'
        #9#9' BNE -'
        ''
        #9#9' LDY #$00'
        #9#9' -'
        #9#9' LDA CODESEND2,Y  ;SEND OUR EXTRA CODE TO DRIVE'
        #9#9' JSR SENDDRIV2'
        #9#9' INY '
        #9#9' BNE -'
        #9#9' BIT $DD00    ;WAIT FOR START WORK'
        #9#9' BVC *-3'
        #9#9' BIT $DD00    ;WAIT FOR END OF WORK'
        #9#9' BVS *-3'
        ''
        #9#9' RTS'
        ''
        'CODESEND'
        ';---'
        ';GET_TO_RAM PROTOCOL'
        '.BYTE 0'#9';LOW BYTE TARGET ADDRES IN DRIVE TO PUT DATA'
        '.BYTE 6'#9';HI BYTE ($0600)'
        '.BYTE 4'#9';JOB NR ALLWAYS 4'
        '.BYTE 0   ;LOW BYTE TO RUN PROG'
        '.BYTE 6   ;HI BYTE ($0600)'
        '.BYTE 0   ;BYTES TO SEND 0 = 256'
        ';---'
        'CODESEND2'
        ';---'
        '.LOGICAL $0600'
        ';---'
        #9#9'LDY #$BA'
        '       -'
        #9#9'LDA $0100,Y ;SAVE DATA FROM $01BA TO $01FF'
        #9#9'STA $0600,Y'
        #9#9'INY'
        #9#9'BNE -'
        #9#9'STY $1800    ;START WORK - INFO FOR C64'
        ';==='
        'RESTART'
        #9#9'SEI'
        #9#9'LDA $1C00   ;RUN MOTOR AND SETLED ON'
        #9#9'ORA #$0C'
        #9#9'STA $1C00'
        #9#9
        #9#9'LDX #$00    ;DELAY FOR MOTOR START'
        #9#9'DEY'
        #9#9'BNE *-1'
        #9#9'DEX'
        #9#9'BNE *-4'
        ' '
        #9#9'STX $1C03   ;HEAD TO READ MODE'
        #9#9'LDA #$EE'
        #9#9'STA $1C0C      ;SOE TO READ'
        #9#9
        'RSYNC'#9'LDA #$D0'#9';WAIT FOR OPEN DRIVE KEY'
        #9#9'STA $1805'
        #9#9'LDA $1C01'
        #9'-'#9'BIT $1805'
        #9#9'BPL STARTTEST ;YES KEY IS OPEN'
        #9#9'BIT $1C00'
        #9#9'BMI -'
        #9#9'BPL RSYNC'
        ';==='
        'STARTTEST'
        #9#9
        #9#9'SEI'
        #9#9
        #9#9'LDA $1C01      ;WAIT FOR 1ST SYNC = CLOSE KEY'
        #9#9'BIT $1C00'
        #9#9'BMI *-3'
        #9#9
        #9#9'LDX #$10       ;WAIT FOR 16 STABILE SYNC'
        '       -'
        #9#9'LDA #$FF'
        #9#9'STA $1805'
        #9#9'LDA $1C01'
        'CZKN'
        #9#9'BIT $1805'
        #9#9'BPL STARTTEST  ;NO STABILE SYNC REPEAT'
        #9#9'BIT $1C00'
        #9#9'BMI CZKN'
        #9#9'DEX'
        #9#9'BNE -'
        ';==='#9#9
        #9#9'CLI'
        ''
        #9#9'JSR $D042      ;HEAD MOVE TO TRACK 18, READ BAM, INIT DISK'
        #9#9
        #9#9'JSR READSEC    ;READ OUR TYPED SECTOR'
        ''
        #9#9'LDY #$04'
        '       -'
        #9#9'LDA MYCOMPARE,Y'
        #9#9'BEQ TEST_OK   '
        #9#9'CMP $0700,Y    ;TEST DATA'
        #9#9'BNE RESTART    ;NO THIS ONE DISK - RESTART'
        #9#9'INY'
        #9#9'BNE -'
        ';==='
        'TEST_OK'
        #9#9'LDA #$08'
        #9#9'STA $1800      ;THE END OF WORK INFO FOR C64'
        #9#9'JSR READSEC    ;ONLY FOR DELAY'
        #9#9'LDY #$BA'
        #9'-'
        #9#9'LDA $0600,Y    ;RESTORE DATA FROM'
        #9#9'STA $0100,Y    ;$01BA - $01FF'
        #9#9'INY'
        #9#9'BNE -'
        #9#9'SEI'
        ';**************'
        ';* EXIT POINT *'
        ';**************'
        ';==='
        #9#9'RTS'
        ';==='
        'READSEC'
        #9#9'LDA #TRACK_INDICATOR'
        #9#9'STA $0E'
        #9#9'LDA #SECT_INDICATOR'
        #9#9'STA $0F'
        #9#9'SEC'
        #9#9'ROL $1C'
        #9#9'-'#9#9
        #9#9'LDA #$80    ;READ SECTOR WITH SIDE INDICATOR'
        #9#9'STA $04     ;TO $0700 BUFFER'
        #9#9'CLI'
        #9#9'LDA $04'
        #9#9'BMI *-2'
        #9#9'CMP #$02'
        #9#9'BCS -'
        #9#9'RTS'
        ';------------------'
        'MYCOMPARE = *-4'#9#9
        ';OUR UNIQUE DATA OF SIDE INDICATOR'
        ';NO LONGER THAN 20 BYTES! - THAT'#39'S ENOUGHT'
        '.TEXT "MY UNIQUE DATA"'
        '.BYTE 0'
        ';=============================='
        ';'#9#9'BELOW'
        '; EXAMPLE ASM FILE FOR OUR INDICATOR'
        '; 3 TEXTLINES'
        ';=============================='
        ';* = $0900 ;NOT IMPORTANT'
        ';.TEXT "MY UNIQUE DATA"'
        ';.BYTE 0'
        ';=============================='
        '__END_NEXT_SIDE'
        '.HERE'
        ' ')
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 3
      Visible = False
    end
    object Memo6: TMemo
      Left = 248
      Top = 256
      Width = 185
      Height = 89
      Lines.Strings = (
        '                  *= $0801'
        ';================================'
        ';DEMO USING DETERMINISTIC LOADER'
        ';WITH D64 CREATED FOR TRACKMOLINKER'
        '; (C) BY WEGI IN 2013.02.12'
        ';================================'
        '         .BYTE $0B,$08,$90,$06,$9E,$32'
        '         .BYTE $30,$34,$39,$00,$A0,$00'
        ''
        '         ;A JUMP TABLE PROC2 OF DETERMINISTIC LOADER'
        '         ;A JUMP TABLE OF THIS'
        '         ;PROC2    = LOAD FILE AND STOPPING MOTOR'
        '         ;PROC2+3  = DISCONNECT LOADER'
        '         ;PROC2+6  = LOAD FILE WITHOUT STOP MOTOR'
        '         ;PROC2+9 = TURN OFF THE MOTOR'
        '         ;PROC2+12 = SEND BYTE FROM ACC TO THE DRIVE '
        '   '
        ';--------'
        'VEC1 = $06'
        'VEC2 = $08'
        ''
        'FVEC = VEC1'
        'VECADR = VEC2'
        'VECTR1 = VEC1'
        ';VECTR2 = VEC2'
        'INIT_MSX = $1000'
        'CNT_MSX = $1003'
        'NRBANK = $00'
        'BANK = $C000'
        'COLORRAM = BANK + $0400'
        'SCREEN = BANK + $2000'
        ';---'
        'START'
        ''
        '         SEI'
        '         CLD'
        '         LDX #$FB'
        '         TXS'
        '         LDA #$37'
        '         STA $01'
        '         JSR $FDA3'
        '         JSR $FD15'
        '         JSR $E3BF'
        '         JSR $FF5B'
        ';*********************************************'
        ';VIC BANK #$03'
        ';$01 = #$37         '
        ';JUST SIMPLE INITIALIZE LOADER BELOW'
        ''
        '        JSR PROC1'
        ';*********************************************'
        ''
        '         JSR INITSPRITE'
        ''
        '         SEI'
        '         JSR _TURNOFFMSX'
        '         LDA #$00'
        '         TAX'
        '         TAY'
        '         JSR INIT_MSX'
        '         JSR _INITIRQ'
        '         LDA #$0B'
        '         STA $D011'
        '         BIT $D011'
        '         BPL *-3'
        '         BIT $D011'
        '         BMI *-3'
        '         LDA #$00'
        '         STA $D020'
        '         '
        '         STA $D021'
        '         LDA #$35'
        '         STA $01'
        '         CLI'
        '         JSR _TURNONMSX'
        ''
        #9#9'LDA #$EA ;NOP'
        #9#9'STA MOVING2'
        ';*******************************'
        ';SWITCH TO BANK OF VIC'
        ''
        #9#9'LDA #<NRBANK'
        #9#9'AND #$03'
        #9#9'ORA #$20'
        #9#9'STA $DD00'
        ''
        ';**********************************'
        ';INIT COLOR AND CLEAR BITMAP'
        #9#9'LDX #$00'
        #9#9'LDA #$10'
        #9'-'
        #9#9'STA COLORRAM,X'
        #9#9'STA COLORRAM+$0100,X'
        #9#9'STA COLORRAM+$0200,X'
        #9#9'STA COLORRAM+$02E8,X'
        #9#9'INX'
        #9#9'BNE -'
        ''
        #9#9'TXA'
        #9#9'LDY #$1F'
        ''
        'CLRBMP'
        #9#9'STA SCREEN,X'
        #9#9'INX'
        #9#9'BNE CLRBMP'
        #9#9'INC CLRBMP + 2'
        #9#9'DEY'
        #9#9'BNE CLRBMP'
        ''
        #9#9'LDY #$3F'
        #9'-'
        #9#9'STA SCREEN+$1F00,Y'
        #9#9'DEY'
        #9#9'BPL -'
        ''
        #9#9'LDA #$18'
        #9#9'STA $D018 ;COLOR RAM $C400'
        #9#9'LDA #$C8'
        #9#9'STA $D016 ;HIRES ON'
        #9#9'LDA #$3B'
        #9#9'STA $D011 ;GRAPH MODE ON'
        ';*******************************'
        '; JUST SIMPLE USE - ONLY JSR FOR CALL LOAD NEXT FILE'
        'NEXTSIDE'
        #9#9';ADDITIONAL FUNCTION'
        #9#9'JSR GET_SILENT_DIR ;GET DATA FROM SILENT DIR AFTER THIS'
        #9#9#9#9#9';Y REG = COUNT FILES ON THE DISK'
        #9#9#9#9#9';AND DIR_DATA IS REFIL BY 5 BYTES INFO'
        #9#9#9#9#9';STRUCTURE TO LOADING FILES'
        ''
        #9#9'DEY'#9#9#9';WITHOUT EXE LAST FILE'
        #9#9'STY $FA'
        ''
        #9#9'LDA #$00'
        #9#9'STA $FB'#9#9
        #9#9'LDA #$EA'
        #9#9'STA MOVING2'
        'RELOAD'
        ''
        #9#9'LDY #>DIR_DATA'
        #9#9'LDA $FB'
        #9#9'ASL'
        #9#9'ASL'
        #9#9';CLC - DON'#39'T NEED'
        #9#9'ADC $FB'
        #9#9'ADC #<DIR_DATA'
        #9#9'BCC *+3'
        #9#9'INY'
        #9#9'STA $6A'
        #9#9'STY $6B'
        #9#9'LDA #$00'#9';SET THE LOAD ADDRESS TO $E000'
        #9#9'LDY #1'#9';COS I'#39'm LAZY AND FILES HAVE OTHER ;-)'
        #9#9'STA ($6A),Y'
        #9#9'INY'
        #9#9'LDA #$E0'
        #9#9'STA ($6A),Y'
        ''
        #9#9'LDA $6A'
        #9#9'LDY $6B'
        '         JSR PROC2+6 ;LOAD WITHOUT MOTOR STOP'
        '         BCC OK   ;'#193'=$62 FNOTFOUND  '
        '         LDA #$60 ;RTS'
        '         STA MOVING2'
        '         JSR INITSPRITE'
        '         JMP *'
        '         '
        'OK'
        '         LDA #$EF'
        '         CMP $DC01 ;SPACE? IF YES - WAIT'
        '         BEQ *-3'
        '         INC $FB               '
        '         DEC $FA'
        '         BNE RELOAD'
        '         '
        '         LDA #$60'
        '         STA MOVING2'
        '         JSR INITSPRITE'
        '         JSR PROC2+9'
        '         JSR SECONDSIDETEST'
        '         JMP NEXTSIDE'
        ';*********************************'
        ';================='
        ''
        ';==='
        'GOLEIRQ'
        '         PHA'
        '         TXA'
        '         PHA'
        '         TYA'
        '         PHA'
        'IRQ'
        '         LDA $01'
        '         PHA'
        '         LDA #$35'
        '         STA $01'
        '         '
        '         LDA #8'
        '         STA $D015'
        '         LDA $D007'
        '         SEC '
        '         SBC #16'
        '         STA $D012'
        '         JSR MOVING2'
        'MSXONOFF    '
        '         JSR CNT_MSX'
        '         INC $D019'
        ''
        '         LDA $D007'
        '         CLC'
        '         ADC #22'
        '         CMP $D012'
        '         BCS *-3'
        '         LDA #$00'
        '         STA $D015'
        ''
        ''
        '         PLA'
        '         STA $01'
        '         '
        '         PLA'
        '         TAY'
        '         PLA'
        '         TAX'
        '         PLA'
        'NMIEX'
        '         RTI'
        '_TURNONMSX'
        '         LDA #$20'
        '         .BYTE $2C'
        '_TURNOFFMSX         '
        '         LDA #$2C'
        '         STA MSXONOFF'
        '         RTS'
        ';--------'
        '_INITIRQ'
        '         SEI'
        '         LDA $01'
        '         PHA'
        '         LDA #$35'
        '         STA $01'
        '         LDA #$7F'
        '         STA $DC0D'
        '         STA $DD0D'
        '         LDA #$01'
        '         STA $D01A         '
        '         LDA $D007'
        '         SEC'
        '         SBC #16'
        '         STA $D012'
        '         LDA $D011'
        '         AND #$7F'
        '         STA $D011'
        '         LDA $DC0D'
        '         LDA $DD0D'
        '         INC $D019'
        '         PLA'
        '         STA $01'
        '_SETVECTORIRQ'
        '         SEI'
        '         LDA #<IRQ'
        '         STA $0314'
        '         LDA #>IRQ'
        '         STA $0315'
        '         LDA #<NMIEX'
        '         STA $0318'
        '         STA $FFFA'
        '         LDA #>NMIEX'
        '         STA $0319'
        '         STA $FFFB'
        '         '
        '         LDA #<GOLEIRQ'
        '         STA $FFFE'
        '         LDA #>GOLEIRQ'
        '         STA $FFFF'
        ''
        '         CLI'
        '         RTS'
        ';---'
        'KLATKA2  .BYTE 0'
        'KLATKA3  .BYTE 0'
        'KLATKA4  .BYTE 0'
        'SPRAJTBANK = BANK + $07F8'
        ';==='
        'MOVING2'
        #9#9'RTS'
        ';-'
        #9#9'INC KLATKA3'
        #9#9'LDA KLATKA3'
        #9#9'CMP #25'
        #9#9'BCC +'
        #9#9'LDA #$00'
        #9#9'STA KLATKA3'
        ''
        #9#9'INC KLATKA4'
        #9#9'LDA KLATKA4'
        #9#9'AND #$03'
        #9#9'CLC'
        #9#9'ADC SPRAJTBANK'
        #9#9'STA  BANK + $07FB'
        #9'+'
        #9#9'RTS'
        ';==='
        'GET_SILENT_DIR'
        #9#9'LDA #<S_DIR_DATA'
        #9#9'LDY #>S_DIR_DATA'
        #9#9'JSR PROC2'
        #9#9'BCS *'
        ''
        #9#9'LDA #<DIR_DATA'
        #9#9'STA TARGET_LO'
        #9#9'LDA #>DIR_DATA'
        #9#9'STA TARGET_HI'
        ''
        #9#9'LDY #0'
        #9'-'#9'LDA PROC2-$0100+100,Y'
        #9#9'BEQ +'
        #9#9'JSR TARGET ;SAVE LENBLOCK OF FILE INTO '
        #9#9'LDA PROC2-$0100+150,Y ;LO BYTE LOAD ADDRESS'
        #9#9'JSR TARGET'
        #9#9'LDA PROC2-$0100+200,Y ;HI BYTE LOAD ADDRESS'
        #9#9'JSR TARGET'
        #9#9'LDA PROC2-$0100,Y ;start track'
        #9#9'JSR TARGET'
        #9#9'LDA PROC2-$0100+50,Y ; start sector'
        #9#9'JSR TARGET'
        #9#9'INY'
        #9#9'CPY #50'
        #9#9'BNE -'#9
        #9'+'#9'RTS'
        'TARGET'
        'TARGET_LO = *+1'
        'TARGET_HI = *+2'
        #9#9'STA $1111'
        #9#9'INC TARGET_LO'
        #9#9'BNE +'
        #9#9'INC TARGET_HI'
        #9'+'#9'RTS'
        'S_DIR_DATA'
        '.BYTE 1 ;1 SECTOR TO READ'
        '.BYTE <DIR_DATA ;ADDRES FOR DATA FROM SILENT DIR'
        '.BYTE >DIR_DATA'
        '.BYTE 18'#9';TRACK 18 SECTOR 2 TO READ'
        '.BYTE 2'
        ''
        'DIR_DATA'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        '.BYTE 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
        ';======================================='
        '_END_CODE'
        ''
        ''
        '_END_CODE2'
        
          ';===============================================================' +
          '====='
        '*= $1000'
        ';.BINARY "lib\mymsx.prg",2   ;ADD MUSIC DATA TO PROJECT'
        
          '.BINARY "C:\C64Project\tools\DEMOLINKER\demo\lib\mymsx.prg",2'#9' ;' +
          'ADD MUSIC DATA TO PROJECT'
        ';=========='
        '*=$1B00'
        'PROC2'
        ';.BINARY "lib\ldr1b00.prg",2'#9' ;installer'#9' '
        
          '.BINARY "C:\C64Project\tools\DEMOLINKER\demo\lib\ldr1b00.prg",2'#9 +
          ' ;installer'
        ';==========='
        '*=$1F00'
        'SECONDSIDETEST'
        ';.BINARY "lib\chgdsk.prg",2'#9' ;CHANGEDISK ENGINE'
        
          '.BINARY "C:\C64Project\tools\DEMOLINKER\demo\lib\chgdsk.prg",2'#9' ' +
          ';CHANGEDISK ENGINE'
        '*=$2000'
        'PROC1'
        ';.BINARY "lib\inst2000.prg",2'#9' ;loader '
        
          '.BINARY "C:\C64Project\tools\DEMOLINKER\demo\lib\inst2000.prg",2' +
          #9' ;loader '
        
          ';===============================================================' +
          '====='
        '_ENDDATA'
        ''
        ''
        ''
        'INITSPRITE'
        ''
        #9#9'LDX #0'
        #9'-'
        #9#9'LDA SPRITE,X'
        #9#9'STA BANK,X'
        #9#9'INX'
        #9#9'BNE -'
        #9#9'TXA'
        #9#9'LDX #7'
        #9'-'
        #9#9'STA $C7F8,X'
        #9#9'DEX'
        #9#9'BPL -'
        ''
        #9#9'LDA #$15'
        #9#9'STA $D006'
        #9#9'LDA #$30'
        #9#9'STA $D007'
        ''
        ''
        #9#9'LDA #$00'
        #9#9'STA $D010'
        #9#9'STA $D01B'
        #9#9'STA $D01C'
        #9#9'STA $D017'
        #9#9'STA $D01D'
        ''
        #9#9'LDA #$05'
        #9#9'STA $D02A'
        #9#9'RTS'
        ''
        ''
        'SPRITE'
        ''
        
          '.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$03,$ff,$c' +
          '0,$04'
        
          '.byte $00,$20,$05,$ff,$a0,$05,$ff,$a0,$04,$00,$20,$04,$18,$20,$0' +
          '4,$24'
        
          '.byte $20,$04,$24,$20,$04,$18,$20,$04,$00,$20,$04,$18,$20,$04,$1' +
          '8,$20'
        
          '.byte $04,$18,$20,$03,$ff,$c0,$00,$00,$00,$00,$00,$00,$00,$00,$0' +
          '0,$05'
        
          '.byte $00,$00,$00,$00,$00,$00,$07,$ff,$e0,$08,$00,$10,$03,$ff,$c' +
          '0,$04'
        
          '.byte $00,$20,$05,$ff,$a0,$05,$ff,$a0,$04,$00,$20,$04,$18,$20,$0' +
          '4,$24'
        
          '.byte $20,$04,$24,$20,$04,$18,$20,$04,$00,$20,$04,$18,$20,$04,$1' +
          '8,$20'
        
          '.byte $04,$18,$20,$03,$ff,$c0,$08,$00,$10,$07,$ff,$e0,$00,$00,$0' +
          '0,$05'
        
          '.byte $00,$00,$00,$00,$00,$00,$07,$ff,$e0,$08,$00,$10,$13,$ff,$c' +
          '8,$14'
        
          '.byte $00,$28,$15,$ff,$a8,$15,$ff,$a0,$04,$00,$20,$04,$18,$20,$0' +
          '4,$24'
        
          '.byte $20,$04,$24,$20,$04,$18,$20,$04,$00,$20,$14,$18,$28,$14,$1' +
          '8,$28'
        
          '.byte $14,$18,$28,$13,$ff,$c8,$08,$00,$10,$07,$ff,$e0,$00,$00,$0' +
          '0,$05'
        
          '.byte $00,$00,$00,$00,$00,$00,$07,$ff,$e0,$08,$00,$10,$13,$ff,$c' +
          '8,$14'
        
          '.byte $00,$28,$15,$ff,$a8,$15,$ff,$a8,$14,$00,$28,$14,$18,$28,$1' +
          '4,$24'
        
          '.byte $28,$14,$24,$28,$14,$18,$28,$14,$00,$28,$14,$18,$28,$14,$1' +
          '8,$28'
        
          '.byte $14,$18,$28,$13,$ff,$c8,$08,$00,$10,$07,$ff,$e0,$00,$00,$0' +
          '0,$05'
        ''
        '_EOF')
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 4
      Visible = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 360
    Width = 934
    Height = 180
    Align = alBottom
    TabOrder = 5
    object SpeedButton1: TSpeedButton
      Left = 336
      Top = 112
      Width = 225
      Height = 46
      Caption = 'Save PRG file installer and loader'
      Flat = True
      Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        2000000000000009000000000000000000000000000000000000FF00FF000000
        5500000055000000550000007700000077000000770000007700000077000000
        7700000077000000770000007700000077000000770000007700000077000000
        77000000770000007700000055000000550000005500FF00FF00000055000033
        FF000033FF000033FF000000FF000000FF000000FF000000DD000000FF000000
        DD000000DD000000DD000000BB000000BB000000BB000000BB00000088000000
        BB0000008800000088000033FF000033FF000033FF0000005500000055000000
        FF000000FF000000FF00F0FBFF00FF00FF00F0FBFF00F0FBFF00F0FBFF00DDDD
        DD00DDDDDD00DDDDDD00DDDDDD00CCCCCC00DDDDDD00CCCCCC00CCCCCC00CCCC
        CC00C0C0C000C0C0C0000000FF000000FF000033FF000000550000005500FF00
        FF00FF00FF000000FF000033FF000000FF000033FF000000FF000000FF000000
        FF000000FF000000DD000000DD000000DD000000BB000000DD000000BB000000
        BB000000BB000000BB000000FF00FF00FF000033FF000000550000005500FF00
        FF00FF00FF000000FF00FF00FF00FF00FF00F0FBFF00F0FBFF00F0FBFF00F0FB
        FF00DDDDDD00F0FBFF00DDDDDD00DDDDDD00DDDDDD00CCCCCC00CCCCCC00CCCC
        CC00CCCCCC00C0C0C0000000FF00FF00FF000033FF0000005500000055000000
        FF000000FF000000FF000033FF000033FF000033FF000000FF000033FF000000
        FF000000FF000000FF000000DD000000DD000000DD000000DD000000BB000000
        DD000000BB000000BB000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00F0FBFF00FF00
        FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00DDDDDD00DDDDDD00DDDDDD00CCCC
        CC00DDDDDD00CCCCCC000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00F0FB
        FF00F0FBFF00F0FBFF00F0FBFF00DDDDDD00F0FBFF00DDDDDD00DDDDDD00DDDD
        DD00CCCCCC00CCCCCC000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00F0FBFF00FF00FF00F0FBFF00F0FBFF00F0FBFF00DDDDDD00DDDDDD00DDDD
        DD00DDDDDD00CCCCCC000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00F0FBFF00FF00FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00DDDD
        DD00DDDDDD00DDDDDD000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF000033FF000033FF000033FF000033FF000033FF000033
        FF000033FF000033FF000000FF000000FF000000FF000000FF000000DD000000
        FF000000DD000000DD000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00F0FBFF00FF00FF00F0FBFF00F0FBFF00F0FB
        FF00DDDDDD00DDDDDD000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00F0FBFF00F0FBFF00F0FBFF00F0FB
        FF00F0FBFF000033FF000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
        FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
        FF000000FF000000FF000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
        FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
        FF000000FF000000FF000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF0000008800000088000000880000008800000088000000
        8800000088000000880000008800000088000000880000008800000088000000
        FF000000FF000000FF000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF000000FF000000BB000000880066666600666666006666
        6600666666006666660066666600666666006666660066666600222222000000
        88000000FF000000FF000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF000000FF000000BB0000008800A4A0A000808080007777
        77004444440055555500000000000000000000000000DDDDDD00444444000000
        88000000FF000000FF000000FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF000000FF000000BB0000008800CCCCCC00A4A0A0008080
        80005555550044444400000000000000BB0000000000CCCCCC00444444000000
        88000000FF000000FF00FF00FF00FF00FF000033FF0000005500000055000000
        FF000000FF000000FF000000FF000000BB0000008800DDDDDD00CCCCCC00A4A0
        A0007777770055555500000000000000BB0000000000C0C0C000444444000000
        88000000FF000000FF00FF00FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF000000FF000000BB0000008800FF00FF00F0FBFF00DDDD
        DD00A4A0A00080808000000000000000BB000000000077777700444444000000
        88000000FF000000FF00FF00FF000000FF000033FF0000005500000055000000
        FF000000FF000000FF000000FF000000BB0000008800F0FBFF00FF00FF00F0FB
        FF00CCCCCC00A4A0A000000000000000BB000000000055555500444444000000
        88000000FF000000FF00FF00FF00FF00FF000033FF0000005500000055000000
        FF000000FF000000FF000000FF000000BB0000008800DDDDDD00F0FBFF00FF00
        FF00DDDDDD00CCCCCC0000000000000000000000000044444400444444000000
        88000000FF000000FF000000FF000000FF0000005500FF00FF00FF00FF000000
        5500000055000000550000005500000055000000550077777700777777007777
        7700777777007777770077777700777777007777770077777700777777000000
        550000005500000055000000550000005500FF00FF00FF00FF00}
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 80
      Top = 112
      Width = 169
      Height = 38
      Caption = 'Additional loaders functions'
      Flat = True
      OnClick = SpeedButton2Click
    end
    object ListBox1: TListBox
      Left = 712
      Top = 64
      Width = 121
      Height = 97
      ItemHeight = 13
      TabOrder = 0
      Visible = False
    end
    object ListBox2: TListBox
      Left = 784
      Top = 64
      Width = 121
      Height = 97
      ItemHeight = 13
      TabOrder = 1
      Visible = False
    end
    object StaticText1: TStaticText
      Left = 19
      Top = 24
      Width = 121
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'Installer start address'
      TabOrder = 2
    end
    object StaticText2: TStaticText
      Left = 137
      Top = 24
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
    object StaticText3: TStaticText
      Left = 254
      Top = 24
      Width = 85
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'Installer length'
      TabOrder = 4
    end
    object StaticText4: TStaticText
      Left = 336
      Top = 24
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 5
    end
    object TrackBar1: TTrackBar
      Left = 8
      Top = 56
      Width = 409
      Height = 45
      Orientation = trHorizontal
      Frequency = 1
      Position = 0
      SelEnd = 0
      SelStart = 0
      TabOrder = 6
      TickMarks = tmBottomRight
      TickStyle = tsAuto
      OnChange = TrackBar1Change
    end
    object StaticText5: TStaticText
      Left = 474
      Top = 24
      Width = 121
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'Loader start address'
      TabOrder = 7
    end
    object StaticText6: TStaticText
      Left = 592
      Top = 24
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
    end
    object StaticText7: TStaticText
      Left = 737
      Top = 24
      Width = 85
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      Caption = 'Loader length'
      TabOrder = 9
    end
    object StaticText8: TStaticText
      Left = 822
      Top = 24
      Width = 73
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 10
    end
    object TrackBar2: TTrackBar
      Left = 464
      Top = 56
      Width = 433
      Height = 45
      Hint = 
        'Remember this loader use one 256 bbytes block data before self  ' +
        'for buffer'
      Orientation = trHorizontal
      ParentShowHint = False
      Frequency = 1
      Position = 0
      SelEnd = 0
      SelStart = 0
      ShowHint = True
      TabOrder = 11
      TickMarks = tmBottomRight
      TickStyle = tsAuto
      OnChange = TrackBar2Change
    end
    object CheckBox2: TCheckBox
      Left = 592
      Top = 120
      Width = 145
      Height = 17
      Caption = 'Use 2MHZ in 1570/71'
      Checked = True
      State = cbChecked
      TabOrder = 12
    end
  end
  object ActionList1: TActionList
    Images = ImageList1
    Left = 400
    Top = 8
    object ESCAPE: TAction
      Caption = 'ESCAPE'
      ShortCut = 27
      OnExecute = ESCAPEExecute
    end
    object showproc1: TAction
      Caption = 'Show Procedure 1'
      ImageIndex = 1
      OnExecute = showproc1Execute
    end
    object saveproc1: TAction
      Caption = 'Save source of procedure 1'
      Enabled = False
      ImageIndex = 0
      OnExecute = saveproc1Execute
    end
    object showproc2: TAction
      Caption = 'Show Procedure 2'
      ImageIndex = 1
      OnExecute = showproc2Execute
    end
    object saveproc2: TAction
      Caption = 'Save source of procedure 2'
      ImageIndex = 0
      OnExecute = saveproc2Execute
    end
    object showchangedisk: TAction
      Caption = 'Show change disk procedure'
      ImageIndex = 1
      OnExecute = showchangediskExecute
    end
    object savechangedisk: TAction
      Caption = 'Save change disk procedure'
      ImageIndex = 0
      OnExecute = savechangediskExecute
    end
    object SHOWDEMO: TAction
      Caption = 'Show simple use Deterministic Loader demo'
      ImageIndex = 1
      OnExecute = SHOWDEMOExecute
    end
    object savedemo: TAction
      Caption = 'Save source of a simple demo that is using Deterministic Loader'
      ImageIndex = 2
      OnExecute = savedemoExecute
    end
    object make4inst: TAction
      Caption = 'make4inst'
      OnExecute = make4instExecute
    end
    object make4loads: TAction
      Caption = 'make4loads'
      OnExecute = make4loadsExecute
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.ASM'
    Filter = 
      'C64 assembler source files (*.ASM)|*.ASM|C64 execute PRG file (*' +
      '.prg)|*.prg'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 272
    Top = 8
  end
  object ImageList1: TImageList
    Height = 24
    Width = 24
    Left = 448
    Top = 8
    Bitmap = {
      494C010103000400040018001800FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000600000001800000001002000000000000024
      0000000000000000000000000000000000000000000033000000330000003300
      0000993300009933000099330000993300009933000099330000993300009933
      0000993300009933000099330000993300009933000099330000993300009933
      000033000000330000003300000000000000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C0000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C0000000000000005500000055000000
      5500000077000000770000007700000077000000770000007700000077000000
      7700000077000000770000007700000077000000770000007700000077000000
      7700000055000000550000005500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000FF660000FF660000CC66
      0000CC660000CC660000CC66000099660000FF66000099660000996600009966
      0000FF660000FF330000FF330000FF33000099330000FF330000993300009933
      0000FF660000FF660000FF66000033000000C0C0C000C0C0C000C0C0C000C0C0
      C000808080000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000000055000033FF000033FF000033
      FF000000FF000000FF000000FF000000DD000000FF000000DD000000DD000000
      DD000000BB000000BB000000BB000000BB00000088000000BB00000088000000
      88000033FF000033FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000F0FBFF0000000000F0FBFF00F0FBFF00F0FBFF00DDDDDD00DDDDDD00DDDD
      DD00DDDDDD00CCCCCC00DDDDDD00CCCCCC00CCCCCC00CCCCCC00C0C0C000C0C0
      C000CC330000CC330000FF66000033000000C0C0C000C0C0C000C0C0C0008080
      8000808080008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      000000000000C0C0C000C0C0C000C0C0C000000055000000FF000000FF000000
      FF00F0FBFF0000000000F0FBFF00F0FBFF00F0FBFF00DDDDDD00DDDDDD00DDDD
      DD00DDDDDD00CCCCCC00DDDDDD00CCCCCC00CCCCCC00CCCCCC00C0C0C000C0C0
      C0000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000330000000000000000000000CC33
      0000FF990000FF660000FF990000CC660000CC660000CC660000FF6600009966
      00009966000099660000FF33000099660000FF330000FF330000FF330000FF33
      0000CC33000000000000FF66000033000000C0C0C000C0C0C000808080008080
      800000000000C0C0C00080808000000000000000000000000000808080008080
      8000808080008080800000000000000000000000000000000000808080000000
      00000000000000000000C0C0C000C0C0C0000000550000000000000000000000
      FF000033FF000000FF000033FF000000FF000000FF000000FF000000FF000000
      DD000000DD000000DD000000BB000000DD000000BB000000BB000000BB000000
      BB000000FF00000000000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000330000000000000000000000CC33
      00000000000000000000F0FBFF00F0FBFF00F0FBFF00F0FBFF00DDDDDD00F0FB
      FF00DDDDDD00DDDDDD00DDDDDD00CCCCCC00CCCCCC00CCCCCC00CCCCCC00C0C0
      C000CC33000000000000FF66000033000000C0C0C00080808000808080008080
      8000000000000000000000000000000000000000000000000000808080008080
      8000808080008080800080808000808080000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C0000000550000000000000000000000
      FF000000000000000000F0FBFF00F0FBFF00F0FBFF00F0FBFF00DDDDDD00F0FB
      FF00DDDDDD00DDDDDD00DDDDDD00CCCCCC00CCCCCC00CCCCCC00CCCCCC00C0C0
      C0000000FF00000000000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000FF990000FF990000FF990000CC660000FF990000CC660000CC660000CC66
      000099660000996600009966000099660000FF33000099660000FF330000FF66
      0000CC330000CC330000FF660000330000008080800080808000808080000000
      0000000000000000000000000000808080000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080000000
      000000000000808080000000000000000000000055000000FF000000FF000000
      FF000033FF000033FF000033FF000000FF000033FF000000FF000000FF000000
      FF000000DD000000DD000000DD000000DD000000BB000000DD000000BB000000
      BB000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      000000000000000000000000000000000000F0FBFF0000000000F0FBFF00F0FB
      FF00F0FBFF00F0FBFF00DDDDDD00DDDDDD00DDDDDD00CCCCCC00DDDDDD00CCCC
      CC00CC330000CC330000FF660000330000008080800000000000C0C0C0008080
      8000000000000000000080808000808080000000000000000000808080008080
      8000808080008080800000000000000000008080800080808000808080000000
      000000000000808080000000000000000000000055000000FF000000FF000000
      FF0000000000000000000000000000000000F0FBFF0000000000F0FBFF00F0FB
      FF00F0FBFF00F0FBFF00DDDDDD00DDDDDD00DDDDDD00CCCCCC00DDDDDD00CCCC
      CC000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      00000000000000000000000000000000000000000000F0FBFF00F0FBFF00F0FB
      FF00F0FBFF00DDDDDD00F0FBFF00DDDDDD00DDDDDD00DDDDDD00CCCCCC00CCCC
      CC00CC330000CC330000FF660000330000008080800080808000808080000000
      0000000000008080800080808000808080000000000080808000808080008080
      8000808080008080800080808000808080000000000080808000808080000000
      000000000000000000000000000000000000000055000000FF000000FF000000
      FF000000000000000000000000000000000000000000F0FBFF00F0FBFF00F0FB
      FF00F0FBFF00DDDDDD00F0FBFF00DDDDDD00DDDDDD00DDDDDD00CCCCCC00CCCC
      CC000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000000000000000000000000000000000000000000000000000F0FBFF000000
      0000F0FBFF00F0FBFF00F0FBFF00DDDDDD00DDDDDD00DDDDDD00DDDDDD00CCCC
      CC00CC330000CC330000FF660000330000008080800080808000000000008080
      80008080800080808000C0C0C000C0C0C00080808000FFFFFF00C0C0C0008080
      8000808080008080800080808000808080008080800000000000000000008080
      800000000000000000000000000000000000000055000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000F0FBFF000000
      0000F0FBFF00F0FBFF00F0FBFF00DDDDDD00DDDDDD00DDDDDD00DDDDDD00CCCC
      CC000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      000000000000000000000000000000000000000000000000000000000000F0FB
      FF0000000000F0FBFF00F0FBFF00F0FBFF00F0FBFF00DDDDDD00DDDDDD00DDDD
      DD00CC330000CC330000FF660000330000008080800080808000000000000000
      0000000000000000000000000000C0C0C000FFFFFF00FFFFFF00000000000000
      0000000000000000000080808000808080008080800000000000000000008080
      800000000000000000000000000000000000000055000000FF000000FF000000
      FF0000000000000000000000000000000000000000000000000000000000F0FB
      FF0000000000F0FBFF00F0FBFF00F0FBFF00F0FBFF00DDDDDD00DDDDDD00DDDD
      DD000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000FF990000FF990000FF990000FF990000FF990000FF990000FF990000FF99
      0000CC660000CC660000CC660000CC66000099660000FF660000996600009966
      0000CC330000CC330000FF660000330000008080800080808000000000000000
      00000000000000000000C0C0C000FFFFFF00FFFFFF008080800000000000FF00
      0000FF000000FF00000000000000808080008080800080808000000000008080
      800080808000000000000000000000000000000055000000FF000000FF000000
      FF000033FF000033FF000033FF000033FF000033FF000033FF000033FF000033
      FF000000FF000000FF000000FF000000FF000000DD000000FF000000DD000000
      DD000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F0FBFF0000000000F0FBFF00F0FBFF00F0FBFF00DDDDDD00DDDD
      DD00CC330000CC330000FF660000330000008080800080808000000000008080
      80000000000000000000FFFFFF00FFFFFF008080800000000000808080008000
      00008000000080800000FF00000000000000808080008080800000000000C0C0
      C00080808000000000000000000000000000000055000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000F0FBFF0000000000F0FBFF00F0FBFF00F0FBFF00DDDDDD00DDDD
      DD000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00FF66
      0000CC330000CC330000FF660000330000008080800080808000000000000000
      0000000000000000000080808000FFFFFF008080800000000000C0C0C0008080
      80008000000080800000FF000000000000008080800000000000000000000000
      000000000000000000000000000000000000000055000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF000033
      FF000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000CC330000CC330000CC330000CC330000CC330000CC330000CC330000CC33
      0000CC330000CC330000CC330000CC330000CC330000CC330000CC330000CC33
      0000CC330000CC330000FF660000330000008080800080808000000000008080
      8000000000000000000000000000C0C0C0008080800000000000FFFFFF00C0C0
      C000800000008080000080000000000000000000000000000000000000008080
      800080808000000000000000000000000000000055000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000CC330000CC330000CC330000CC330000CC330000CC330000CC330000CC33
      0000CC330000CC330000CC330000CC330000CC330000CC330000CC330000CC33
      0000CC330000CC330000FF66000033000000C0C0C00080808000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000808080000000000000000000000000000000
      000000000000000000000000000000000000000055000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000330000003300000033000000330000003300000033000000330000003300
      00003300000033000000330000003300000033000000CC330000CC330000CC33
      0000CC330000CC330000FF66000033000000C0C0C00080808000000000000000
      0000808080008080800000000000C0C0C0008080800080808000000000000000
      0000000000000000000000000000C0C0C0008080800080808000808080000000
      000000000000000000000000000000000000000055000000FF000000FF000000
      FF00000088000000880000008800000088000000880000008800000088000000
      880000008800000088000000880000008800000088000000FF000000FF000000
      FF000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000CC0000007700000033000000666666006666660066666600666666006666
      6600666666006666660066666600666666002222220033000000CC330000CC33
      0000CC330000CC330000FF66000033000000C0C0C00080808000808080000000
      0000808080008080800000000000C0C0C000C0C0C000C0C0C00080808000C0C0
      C000808080000000000000000000C0C0C0008080800080808000808080000000
      000000000000000000000000000000000000000055000000FF000000FF000000
      FF000000FF000000BB0000008800666666006666660066666600666666006666
      66006666660066666600666666006666660022222200000088000000FF000000
      FF000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000CC0000007700000033000000A4A0A0008080800077777700444444005555
      5500000000000000000000000000DDDDDD004444440033000000CC330000CC33
      0000CC330000CC330000FF66000033000000C0C0C00080808000808080000000
      0000808080008080800080808000000000000000000000000000808080008080
      8000808080000000000000000000808080008080800080808000808080000000
      000000000000808080000000000000000000000055000000FF000000FF000000
      FF000000FF000000BB0000008800A4A0A0008080800077777700444444005555
      5500000000000000000000000000DDDDDD0044444400000088000000FF000000
      FF000000FF000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000CC0000007700000033000000CCCCCC00A4A0A00080808000555555004444
      4400000000009933000000000000CCCCCC004444440033000000CC330000CC33
      00000000000000000000FF66000033000000C0C0C00080808000C0C0C0008080
      8000000000008080800080808000808080000000000000000000000000000000
      0000000000000000000000000000808080008080800080808000000000000000
      000000000000C0C0C0000000000000000000000055000000FF000000FF000000
      FF000000FF000000BB0000008800CCCCCC00A4A0A00080808000555555004444
      4400000000000000BB0000000000CCCCCC0044444400000088000000FF000000
      FF0000000000000000000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000CC0000007700000033000000DDDDDD00CCCCCC00A4A0A000777777005555
      5500000000009933000000000000C0C0C0004444440033000000CC330000CC33
      000000000000CC330000FF66000033000000C0C0C00080808000808080008080
      8000000000000000000080808000808080008080800080808000000000000000
      0000000000000000000000000000808080008080800000000000000000008080
      8000000000008080800080808000C0C0C000000055000000FF000000FF000000
      FF000000FF000000BB0000008800DDDDDD00CCCCCC00A4A0A000777777005555
      5500000000000000BB0000000000C0C0C00044444400000088000000FF000000
      FF00000000000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000CC000000770000003300000000000000F0FBFF00DDDDDD00A4A0A0008080
      8000000000009933000000000000777777004444440033000000CC330000CC33
      000000000000CC330000FF66000033000000C0C0C000C0C0C000C0C0C0008080
      8000808080008080800000000000000000000000000000000000808080008080
      8000808080000000000000000000808080000000000080808000808080008080
      80008080800080808000C0C0C000C0C0C000000055000000FF000000FF000000
      FF000000FF000000BB000000880000000000F0FBFF00DDDDDD00A4A0A0008080
      8000000000000000BB00000000007777770044444400000088000000FF000000
      FF00000000000000FF000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000CC0000007700000033000000F0FBFF0000000000F0FBFF00CCCCCC00A4A0
      A000000000009933000000000000555555004444440033000000CC330000CC33
      00000000000000000000FF66000033000000C0C0C000C0C0C000C0C0C000C0C0
      C00080808000C0C0C00080808000808080000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000C0C0C0008080
      800080808000C0C0C000C0C0C000C0C0C000000055000000FF000000FF000000
      FF000000FF000000BB0000008800F0FBFF0000000000F0FBFF00CCCCCC00A4A0
      A000000000000000BB00000000005555550044444400000088000000FF000000
      FF0000000000000000000033FF00000055000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000033000000CC000000CC330000CC33
      0000CC0000007700000033000000DDDDDD00F0FBFF0000000000DDDDDD00CCCC
      CC00000000000000000000000000444444004444440033000000CC330000CC33
      0000CC330000CC3300003300000000000000C0C0C000C0C0C000C0C0C000C0C0
      C000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000C0C0C000C0C0C000C0C0C000C0C0C000000055000000FF000000FF000000
      FF000000FF000000BB0000008800DDDDDD00F0FBFF0000000000DDDDDD00CCCC
      CC000000000000000000000000004444440044444400000088000000FF000000
      FF000000FF000000FF0000005500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000033000000330000003300
      0000330000003300000033000000777777007777770077777700777777007777
      7700777777007777770077777700777777007777770033000000330000003300
      000033000000330000000000000000000000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C0008080800080808000808080008080
      800080808000808080008080800080808000808080008080800080808000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C0000000000000005500000055000000
      5500000055000000550000005500777777007777770077777700777777007777
      7700777777007777770077777700777777007777770000005500000055000000
      5500000055000000550000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000060000000180000000100010000000000200100000000000000000000
      000000000000000000000000FFFFFF0080000100000080000100000000000000
      0000000000000000040000000000040000000000600004000000600004000000
      6C00040000006C00040000000000000000000000000000000F40000000000F40
      000000000F80000000000F80000000000FD0000000000FD0000000000FE80000
      00000FE8000000000000000000000000000000000FFA000000000FFA00000000
      0FFC000000000FFC000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000C00000000000C000000000008000000000008000000
      01000800000001000800000000800C00000000800C0000000040010000000040
      0100000080000300000080000300000000000000000000000000000000000000
      000000000000}
  end
end
