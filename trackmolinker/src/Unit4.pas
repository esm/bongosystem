unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ComCtrls, Buttons, StdCtrls;

type
  TForm4 = class(TForm)
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    Action1: TAction;
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    procedure Action1Execute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

procedure TForm4.Action1Execute(Sender: TObject);
begin
  Form4.Close;   
end;

end.
