program cruncher;

{$MODE DELPHI}
{$APPTYPE CONSOLE}

uses
  SysUtils;

const
fast1 : array[1..388] of Byte =
(
     $01 , $08 , $0B , $08 , $00 , $00 , $9E , $32 , $30 , $36 , $31 , $00 , $00 , $00 , $A9 , $49 , 
     $A0 , $08 , $20 , $1E , $AB , $A0 , $D6 , $78 , $B9 , $4D , $08 , $99 , $FF , $00 , $C0 , $C0 , 
     $B0 , $06 , $B9 , $23 , $09 , $99 , $3B , $03 , $88 , $D0 , $ED , $A9 , $38 , $85 , $01 , $A2 , 
     $01 , $B9 , $83 , $08 , $99 , $00 , $FF , $C8 , $D0 , $F7 , $CE , $32 , $08 , $CE , $35 , $08 , 
     $CA , $D0 , $EE , $A9 , $00 , $A0 , $00 , $4C , $00 , $01 , $0D , $34 , $39 , $38 , $00 , $8D , 
     $4C , $03 , $8C , $4D , $03 , $A0 , $00 , $84 , $6B , $20 , $4B , $03 , $8D , $95 , $01 , $29 , 
     $3F , $85 , $6A , $D0 , $06 , $20 , $4B , $03 , $99 , $9A , $03 , $C8 , $C4 , $6A , $90 , $F5 , 
     $20 , $4B , $03 , $85 , $6C , $8D , $9A , $01 , $20 , $4B , $03 , $85 , $6D , $8D , $9C , $01 , 
     $20 , $3C , $03 , $90 , $2A , $A0 , $00 , $84 , $6F , $98 , $20 , $3C , $03 , $2A , $D0 , $03 ,
     $20 , $5E , $03 , $85 , $6A , $C9 , $00 , $F0 , $10 , $20 , $4B , $03 , $91 , $6C , $E6 , $6C ,
     $D0 , $03 , $20 , $82 , $03 , $C6 , $6A , $D0 , $F0 , $C4 , $6F , $C6 , $6F , $90 , $EA , $A9 , 
     $02 , $A0 , $03 , $20 , $3C , $03 , $90 , $42 , $98 , $20 , $3C , $03 , $90 , $3A , $20 , $6C , 
     $03 , $C9 , $03 , $F0 , $1E , $B0 , $31 , $A0 , $04 , $20 , $57 , $03 , $85 , $6A , $0A , $65 , 
     $6A , $A8 , $B9 , $9B , $03 , $85 , $6A , $B9 , $9D , $03 , $85 , $6F , $B9 , $9C , $03 , $A0 , 
     $00 , $F0 , $1C , $A9 , $00 , $0A , $10 , $09 , $A9 , $00 , $A0 , $00 , $A2 , $00 , $4C , $00 , 
     $01 , $A9 , $37 , $85 , $01 , $4C , $0D , $08 , $A0 , $04 , $85 , $6A , $20 , $6C , $03 , $85 , 
     $6E , $38 , $A5 , $6C , $E5 , $6E , $85 , $6E , $A5 , $6D , $E5 , $6F , $85 , $6F , $B1 , $6E , 
     $91 , $6C , $C8 , $C4 , $6A , $D0 , $F7 , $98 , $18 , $65 , $6C , $85 , $6C , $90 , $03 , $20 , 
     $82 , $03 , $4C , $31 , $01 , $06 , $6B , $F0 , $01 , $60 , $48 , $20 , $4B , $03 , $38 , $2A , 
     $85 , $6B , $68 , $60 , $BD , $11 , $11 , $E8 , $D0 , $03 , $EE , $4D , $03 , $60 , $A0 , $03 , 
     $A9 , $00 , $F0 , $1D , $A9 , $01 , $60 , $20 , $55 , $03 , $C9 , $07 , $D0 , $0A , $20 , $55 , 
     $03 , $69 , $07 , $D0 , $03 , $20 , $57 , $03 , $A0 , $00 , $84 , $6F , $A8 , $F0 , $E5 , $A9 , 
     $01 , $20 , $3C , $03 , $2A , $26 , $6F , $88 , $D0 , $F7 , $60 , $E6 , $6D , $A0 , $02 , $B1 , 
     $D1 , $38 , $E9 , $01 , $91 , $D1 , $C9 , $30 , $B0 , $07 , $A9 , $39 , $91 , $D1 , $88 , $10 , 
     $EE , $A0 , $00 , $60
);

fast2 : array[1..365] of Byte =
(
     $01 , $08 , $0B , $08 , $00 , $00 , $9E , $32 , $30 , $36 , $31 , $00 , $00 , $00 , $A9 , $49 , 
     $A0 , $08 , $20 , $1E , $AB , $A0 , $BF , $78 , $B9 , $4D , $08 , $99 , $FF , $00 , $C0 , $C0 , 
     $B0 , $06 , $B9 , $0C , $09 , $99 , $3B , $03 , $88 , $D0 , $ED , $A9 , $38 , $85 , $01 , $A2 , 
     $01 , $B9 , $6C , $08 , $99 , $00 , $FF , $C8 , $D0 , $F7 , $CE , $32 , $08 , $CE , $35 , $08 , 
     $CA , $D0 , $EE , $A9 , $00 , $A0 , $00 , $4C , $00 , $01 , $0D , $34 , $39 , $38 , $00 , $8D , 
     $4C , $03 , $8C , $4D , $03 , $A0 , $00 , $84 , $6B , $20 , $4B , $03 , $29 , $3F , $85 , $6A , 
     $D0 , $06 , $20 , $4B , $03 , $99 , $9A , $03 , $C8 , $C4 , $6A , $90 , $F5 , $20 , $4B , $03 , 
     $85 , $6C , $20 , $4B , $03 , $85 , $6D , $20 , $3C , $03 , $90 , $2A , $A0 , $00 , $84 , $6F , 
     $98 , $20 , $3C , $03 , $2A , $D0 , $03 , $20 , $5E , $03 , $85 , $6A , $C9 , $00 , $F0 , $10 , 
     $20 , $4B , $03 , $91 , $6C , $E6 , $6C , $D0 , $03 , $20 , $82 , $03 , $C6 , $6A , $D0 , $F0 , 
     $C4 , $6F , $C6 , $6F , $90 , $EA , $A9 , $02 , $A0 , $03 , $20 , $3C , $03 , $90 , $34 , $98 , 
     $20 , $3C , $03 , $90 , $2C , $20 , $6C , $03 , $C9 , $03 , $F0 , $1E , $B0 , $23 , $A0 , $04 , 
     $20 , $57 , $03 , $85 , $6A , $0A , $65 , $6A , $A8 , $B9 , $9B , $03 , $85 , $6A , $B9 , $9D , 
     $03 , $85 , $6F , $B9 , $9C , $03 , $A0 , $00 , $F0 , $0E , $A9 , $37 , $85 , $01 , $4C , $0D , 
     $08 , $A0 , $04 , $85 , $6A , $20 , $6C , $03 , $85 , $6E , $38 , $A5 , $6C , $E5 , $6E , $85 , 
     $6E , $A5 , $6D , $E5 , $6F , $85 , $6F , $B1 , $6E , $91 , $6C , $C8 , $C4 , $6A , $D0 , $F7 ,
     $98 , $18 , $65 , $6C , $85 , $6C , $90 , $03 , $20 , $82 , $03 , $4C , $28 , $01 , $06 , $6B ,
     $F0 , $01 , $60 , $48 , $20 , $4B , $03 , $38 , $2A , $85 , $6B , $68 , $60 , $BD , $11 , $11 , 
     $E8 , $D0 , $03 , $EE , $4D , $03 , $60 , $A0 , $03 , $A9 , $00 , $F0 , $1D , $A9 , $01 , $60 , 
     $20 , $55 , $03 , $C9 , $07 , $D0 , $0A , $20 , $55 , $03 , $69 , $07 , $D0 , $03 , $20 , $57 , 
     $03 , $A0 , $00 , $84 , $6F , $A8 , $F0 , $E5 , $A9 , $01 , $20 , $3C , $03 , $2A , $26 , $6F , 
     $88 , $D0 , $F7 , $60 , $E6 , $6D , $A0 , $02 , $B1 , $D1 , $38 , $E9 , $01 , $91 , $D1 , $C9 , 
     $30 , $B0 , $07 , $A9 , $39 , $91 , $D1 , $88 , $10 , $EE , $A0 , $00 , $60
);

fast3 : array[1..368] of Byte =
(
     $01 , $08 , $0B , $08 , $00 , $00 , $9E , $32 , $30 , $36 , $31 , $00 , $00 , $00 , $A9 , $6A , 
     $A0 , $09 , $20 , $1E , $AB , $A2 , $D7 , $78 , $BD , $34 , $08 , $9D , $00 , $01 , $E0 , $C0 , 
     $B0 , $06 , $BD , $0A , $09 , $9D , $3B , $03 , $CA , $D0 , $ED , $A9 , $38 , $85 , $01 , $A9 , 
     $6F , $A0 , $09 , $4C , $01 , $01 , $8D , $4C , $03 , $8C , $4D , $03 , $A0 , $00 , $84 , $6B , 
     $20 , $4B , $03 , $8D , $96 , $01 , $29 , $3F , $85 , $6A , $D0 , $06 , $20 , $4B , $03 , $99 , 
     $9A , $03 , $C8 , $C4 , $6A , $90 , $F5 , $20 , $4B , $03 , $85 , $6C , $8D , $9B , $01 , $20 , 
     $4B , $03 , $85 , $6D , $8D , $9D , $01 , $20 , $3C , $03 , $90 , $2A , $A0 , $00 , $84 , $6F ,
     $98 , $20 , $3C , $03 , $2A , $D0 , $03 , $20 , $5E , $03 , $85 , $6A , $C9 , $00 , $F0 , $10 ,
     $20 , $4B , $03 , $91 , $6C , $E6 , $6C , $D0 , $03 , $20 , $82 , $03 , $C6 , $6A , $D0 , $F0 , 
     $C4 , $6F , $C6 , $6F , $90 , $EA , $A9 , $02 , $A0 , $03 , $20 , $3C , $03 , $90 , $42 , $98 , 
     $20 , $3C , $03 , $90 , $3A , $20 , $6C , $03 , $C9 , $03 , $F0 , $1E , $B0 , $31 , $A0 , $04 , 
     $20 , $57 , $03 , $85 , $6A , $0A , $65 , $6A , $A8 , $B9 , $9B , $03 , $85 , $6A , $B9 , $9D , 
     $03 , $85 , $6F , $B9 , $9C , $03 , $A0 , $00 , $F0 , $1C , $A9 , $00 , $0A , $10 , $09 , $A9 , 
     $00 , $A0 , $00 , $A2 , $00 , $4C , $01 , $01 , $A9 , $37 , $85 , $01 , $4C , $00 , $90 , $A0 , 
     $04 , $85 , $6A , $20 , $6C , $03 , $85 , $6E , $38 , $A5 , $6C , $E5 , $6E , $85 , $6E , $A5 , 
     $6D , $E5 , $6F , $85 , $6F , $B1 , $6E , $91 , $6C , $C8 , $C4 , $6A , $D0 , $F7 , $98 , $18 , 
     $65 , $6C , $85 , $6C , $90 , $03 , $20 , $82 , $03 , $4C , $32 , $01 , $06 , $6B , $F0 , $01 , 
     $60 , $48 , $20 , $4B , $03 , $38 , $2A , $85 , $6B , $68 , $60 , $BD , $11 , $11 , $E8 , $D0 , 
     $03 , $EE , $4D , $03 , $60 , $A0 , $03 , $A9 , $00 , $F0 , $1D , $A9 , $01 , $60 , $20 , $55 , 
     $03 , $C9 , $07 , $D0 , $0A , $20 , $55 , $03 , $69 , $07 , $D0 , $03 , $20 , $57 , $03 , $A0 , 
     $00 , $84 , $6F , $A8 , $F0 , $E5 , $A9 , $01 , $20 , $3C , $03 , $2A , $26 , $6F , $88 , $D0 , 
     $F7 , $60 , $E6 , $6D , $A0 , $02 , $B1 , $D1 , $38 , $E9 , $01 , $91 , $D1 , $C9 , $30 , $B0 , 
     $07 , $A9 , $39 , $91 , $D1 , $88 , $10 , $EE , $A0 , $00 , $60 , $0D , $31 , $31 , $35 , $00
);

fast4 : array[1..323] of Byte =
(
     $01 , $08 , $0B , $08 , $00 , $00 , $9E , $32 , $30 , $36 , $31 , $00 , $00 , $00 , $A9 , $3D , 
     $A0 , $09 , $20 , $1E , $AB , $78 , $A9 , $38 , $85 , $01 , $A2 , $00 , $A9 , $42 , $A0 , $09 , 
     $8D , $EE , $08 , $8C , $EF , $08 , $A0 , $00 , $84 , $6B , $20 , $ED , $08 , $29 , $3F , $85 , 
     $6A , $D0 , $06 , $20 , $ED , $08 , $99 , $3F , $03 , $C8 , $C4 , $6A , $90 , $F5 , $20 , $ED , 
     $08 , $85 , $6C , $20 , $ED , $08 , $85 , $6D , $20 , $DE , $08 , $90 , $2A , $A0 , $00 , $84 , 
     $6F , $98 , $20 , $DE , $08 , $2A , $D0 , $03 , $20 , $00 , $09 , $85 , $6A , $C9 , $00 , $F0 , 
     $10 , $20 , $ED , $08 , $91 , $6C , $E6 , $6C , $D0 , $03 , $20 , $24 , $09 , $C6 , $6A , $D0 , 
     $F0 , $C4 , $6F , $C6 , $6F , $90 , $EA , $A9 , $02 , $A0 , $03 , $20 , $DE , $08 , $90 , $34 , 
     $98 , $20 , $DE , $08 , $90 , $2C , $20 , $0E , $09 , $C9 , $03 , $F0 , $1E , $B0 , $23 , $A0 , 
     $04 , $20 , $F9 , $08 , $85 , $6A , $0A , $65 , $6A , $A8 , $B9 , $40 , $03 , $85 , $6A , $B9 , 
     $42 , $03 , $85 , $6F , $B9 , $41 , $03 , $A0 , $00 , $F0 , $0E , $A9 , $37 , $85 , $01 , $4C , 
     $00 , $90 , $A0 , $04 , $85 , $6A , $20 , $0E , $09 , $85 , $6E , $38 , $A5 , $6C , $E5 , $6E , 
     $85 , $6E , $A5 , $6D , $E5 , $6F , $85 , $6F , $B1 , $6E , $91 , $6C , $C8 , $C4 , $6A , $D0 , 
     $F7 , $98 , $18 , $65 , $6C , $85 , $6C , $90 , $03 , $20 , $24 , $09 , $4C , $47 , $08 , $06 , 
     $6B , $F0 , $01 , $60 , $48 , $20 , $ED , $08 , $38 , $2A , $85 , $6B , $68 , $60 , $BD , $11 , 
     $11 , $E8 , $D0 , $03 , $EE , $EF , $08 , $60 , $A0 , $03 , $A9 , $00 , $F0 , $1D , $A9 , $01 ,
     $60 , $20 , $F7 , $08 , $C9 , $07 , $D0 , $0A , $20 , $F7 , $08 , $69 , $07 , $D0 , $03 , $20 ,
     $F9 , $08 , $A0 , $00 , $84 , $6F , $A8 , $F0 , $E5 , $A9 , $01 , $20 , $DE , $08 , $2A , $26 , 
     $6F , $88 , $D0 , $F7 , $60 , $E6 , $6D , $A0 , $02 , $B1 , $D1 , $38 , $E9 , $01 , $91 , $D1 , 
     $C9 , $30 , $B0 , $07 , $A9 , $39 , $91 , $D1 , $88 , $10 , $EE , $A0 , $00 , $60 , $0D , $30 , 
     $36 , $33 , $00
);

var
//GLOBAL VARIABLES
        mainfirst : byte;
        mainsecond : byte;
        my_output_filename : string;
        my_input_filename : string;
        offset_power : byte;
        value_01 : byte;
        s_JMP_ad : string;
        s_own_ld_ad : string;
        my_own_ld_ad : Boolean;
        no_safe_ld_ad : Boolean;
        many_gold_seq : Integer;
        golden_allowed : Boolean;
        deff_recursive : Boolean;
        poss_dep_ad : Integer;
        no_ld_ad : Boolean;
        depack_or_raw : Boolean;
        cr_buff : array [0..65536] of byte;

        size_of_cr_buff : Word;
        inputfile : string;
        outputfile : string;
        binfile : Boolean;
        corrupted_data : Boolean;

        max_lng_offs : Integer;
        max_sh_offs : Integer;

        my_match_len : Integer;
        my_match_offset : Integer;
        crbuf_size : Integer;
        len_plain_data : Integer;

        tab_read : array [0..65536] of byte;
        tab_plik : array [0..65536] of byte;
        tab_decrunch : array [0..65536] of byte;
        tab_pack : array [0..65536] of byte;
        tab_pack2 : array [0..65536] of byte;

        tabstepcruncheroperation : array [0..32768] of byte;
        tabstepoffset : array [0..32768] of word;
        tabstepcopy_or_crunch_len : array [0..32768] of word;

        seq_winner : array [0..256] of integer;
        seq_ratio : array [0..256] of integer;
        seq_matched : array [0..256] of integer;
        seq_coded : array [0..256] of integer;
        LOVE_DATA : array [0..256] of byte;

        winner_seq_count : Integer;
        GOLDEN_USED_COUNT : BYTE;
        first_dep_ady : boolean;
        first_dep_value : Integer;


        d_crunch : Integer;
        first_decr_block : Integer;
        second_decr_block : Integer;
        sum_decr_block : Integer;
        after_crunch : Integer;
        depacker : array [0..1024] of byte;
        depack_len : word;
        blocki : array [0..2] of byte;

        put_pointer : Word; //pointer to squad data
        get_pointer : Word; //pointer to get data
        stream_put_pointer : Word; //pointer of infostream
        bitcounter : byte; //counter rolled bits



        my_sfx : Boolean;
        depack_from : byte;
        packed_data : integer;
        dep_ady : word;
        rekursja : Integer;

        rawdata : byte;
        overlap : integer;
        overlay : Integer;
        ladr_compressed_data:  integer;
        ladr_compress_lo :byte;
        ladr_compress_hi :byte;
        length_long_seq : Integer;

        //decruncher data
        bitsoffset : byte;
        shortbitsoffset : byte;
        rol_byte : byte;
        rol_word : Word;
        code_add_infobit : Boolean;
        crunchbytes : word;
        oper_code : byte;
        inputbit : byte;
        manybits : byte;
        copybytes : Word;
        countcrunch : Integer;


        manycrunch : Integer;

        coded_bits : integer;
        as_golden : Integer;
        as_ratio  : Integer;
        was_golden_coded : Boolean;

        crunchresult : Boolean;

        packfrom : integer;
        packto : integer;
        f_len : Integer;


    count_rekursja : integer;
    spcase : integer;
    firstput,secput : Integer;



// global variables end
//==========================================================
//========  cruncher  FUNCTIONS ============================
//==========================================================
Function how_many_bits(myvalue : Word): Integer;
begin
 Result:=16;
 if myvalue = 0 then myvalue :=1;
 while (myvalue < 32768) do
 begin
    dec(Result);
    myvalue := myvalue shl (1);
 end;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure increase_bitcounter;
begin

      inc(bitcounter);
      if bitcounter = 8 then
      begin
        stream_put_pointer := put_pointer;
        bitcounter :=0;
        inc(put_pointer);
      end;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure out_clear_bit;
begin
   increase_bitcounter;
   tab_pack[stream_put_pointer] := tab_pack[stream_put_pointer] shl (1);
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure out_set_bit;
begin
   out_clear_bit;
   tab_pack[stream_put_pointer] := tab_pack[stream_put_pointer] or(1);
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure out_this_bit(my_value : Integer) ;
begin
  if my_value = 0 then out_clear_bit else out_set_bit;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure send_lowestroolbit(my_value :byte ; bits_value : byte);
var
i : integer;
begin
  rol_word := my_value;
  rol_word := rol_word shl(16-bits_value);
  for i := 1 to bits_value do
  begin
    out_this_bit(rol_word and 32768);
    rol_word := rol_word shl(1);
 end;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure sendrollword(my_value : word);
var
i,i2:integer;
begin
   rol_word := my_value;
   i2 := how_many_bits(rol_word)-1;
   if i2 = 0 then exit;
   while rol_word < 32768 do rol_word := rol_word shl(1);
   rol_word := rol_word shl(1);
   for i := 0 to i2-1 do
   begin
     out_this_bit(rol_word and 32768);
     rol_word := rol_word shl(1);
   end;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure sendlongbytescopy;
begin
   sendrollword (tabstepcopy_or_crunch_len[countcrunch]);
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure sendlongcrunchbytes;
begin
   sendlongbytescopy;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure sendoffsetbytes;
begin
   sendrollword(tabstepoffset[countcrunch]);
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure flush_and_finish;
begin
   if code_add_infobit then send_lowestroolbit(1,2) else send_lowestroolbit(1,1);
   out_set_bit;
   send_lowestroolbit(3,4);
   while bitcounter <> 7 do out_clear_bit;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//==========================================================
//========= decrunch functions   ===========================
//==========================================================
Procedure decrease_bitcounter;
begin
  if bitcounter = 0 then
  begin
    bitcounter :=8;
    inc(get_pointer);
    rol_byte := tab_pack[get_pointer];
  end;
  dec(bitcounter);
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure get_one_bit;
begin
   decrease_bitcounter;
   if rol_byte > 127 then inputbit := 1 else inputbit := 0;
   rol_byte := rol_byte shl (1);
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure getmanybits(MY_VALUE : Integer);
var i : integer;
begin
    manybits:=0;
    for i := 1 to MY_VALUE do
    begin
      get_one_bit;
      manybits:=manybits shl (1);
      manybits := manybits or inputbit;
    end;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Procedure get_bytes_to_copy;
var
i:integer;
begin
  copybytes := 1;
  IF manybits = 0 THEN EXIT;
  if manybits = 1 then
  begin
    copybytes := 2;
    get_one_bit;
    copybytes := copybytes or inputbit;
    Exit;
  end;
  for i := 0 to manybits-1 do
  begin
    get_one_bit;
    copybytes:=copybytes shl (1);
    copybytes := copybytes or inputbit;
  end;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure getcrunchbytes;
begin
  get_bytes_to_copy;
  crunchbytes := copybytes;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure getbackoffset;
begin
  get_bytes_to_copy;
  rol_word := copybytes;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//==========================================================
//========= statistic functions ============================
//==========================================================
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure calc_ratio(My_Len : Integer ; My_Offset : Integer);
var
i4,i5,i6,i7,i8 : Integer;
begin

    i4 := how_many_bits(My_Len); //bit_len
    i5 := how_many_bits(My_Offset);//bitoffset

    if My_Len = 2 then i6 := shortbitsoffset+i5;
    if My_Len = 3 then i6 := bitsoffset+i5+1;
    if My_Len > 3 then i6 := 4+i4 + bitsoffset + i5;

    i7 := 9;
    i8 := i6 -i7;

    coded_bits := i6;

    as_golden := i7;
    as_ratio := i8;

end;
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Procedure find_golden_seq(my_value : Integer);
var
i,i2 : Integer;
   tabwinner1 : array[0..65536] of integer;
   tabmatch : array[0..65536] of Word;
   tabratio : array[0..65536] of Integer;
   tab_coded_bits : array[0..65536] of Byte;
   lenwinner : Integer;
   codeseq : Integer;
   poz_maxi : Integer;
   moje_mx : Integer;
   found : boolean;
   s : String;

begin
  GOLDEN_USED_COUNT := 0;
  s := chr(9);
  lenwinner := 0;
  for i := 0 to 65536 do tabwinner1[i]:=0;
  for i := 0 to 65536 do tabmatch[i]:=0;

  for i := 0 to my_value do
  begin
    if tabstepcruncheroperation[i] = 2 then
    begin
      codeseq := tabstepoffset[i];
      codeseq := codeseq shl (8);
      codeseq := codeseq or tabstepcopy_or_crunch_len[i];

      if lenwinner = 0 then
      begin
        tabwinner1[0] := codeseq;
        tabmatch[0] := 1;
        lenwinner := 1;
      end
      else
      begin
        found := false;
        for i2 := 0 to lenwinner-1 do
        begin
          if tabwinner1[i2] = codeseq then
          begin
            inc(tabmatch[i2]);
            found := True;
            break;
          end;
        end;
        if not found then
        begin
          tabwinner1[lenwinner] := codeseq;
          tabmatch[lenwinner] := 1;
          inc(lenwinner);
        end;
      end;


    end;
  end; //  for i := 0 to my_value

  for i := 0 to lenwinner-1 do
  begin
    as_ratio := -1;
    if tabmatch[i] > 2 then
    begin
      calc_ratio((tabwinner1[i] and 255),(tabwinner1[i] shr (8)));
      as_ratio := coded_bits + (tabmatch[i] * as_ratio) - as_ratio;
      as_ratio:=as_ratio-32 -(8 - (as_ratio and 7));
      as_ratio := as_ratio ;
      if as_ratio < 9 then as_ratio := -1;
    end;
    tabratio[i] := as_ratio;
    tab_coded_bits[i] := coded_bits;
  end;

  i2 := 0;
  for i := 0 to lenwinner-1 do if tabratio[i] > 0 then inc(i2);
  if i2 > 255 then i2 := 255 else dec(i2);
  if i = -1 then i2 :=0;
  winner_seq_count := i2;

  for i := 0 to winner_seq_count do
  begin
    moje_mx := 1;
    for i2 := 0 to lenwinner -1 do
    begin
      if tabratio[i2] > moje_mx then
      begin
        moje_mx := tabratio[i2];
        poz_maxi := i2;
      end;
    end;
    tabratio[poz_maxi] :=0;
    seq_winner[i] := tabwinner1[poz_maxi];
    seq_ratio[i] := moje_mx;
    seq_matched[i] := tabmatch[poz_maxi];
    seq_coded[i] := tab_coded_bits[poz_maxi];
  end; // for i := 0 to winner_seq_count




  if golden_allowed then
  begin
    if winner_seq_count > 15 then GOLDEN_USED_COUNT := 16
      else GOLDEN_USED_COUNT := winner_seq_count+1 ;
    if many_gold_seq = 0 then GOLDEN_USED_COUNT:=0;
    if many_gold_seq > GOLDEN_USED_COUNT then many_gold_seq := GOLDEN_USED_COUNT;
    if many_gold_seq < GOLDEN_USED_COUNT then GOLDEN_USED_COUNT := many_gold_seq;
  end;

end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure try_golden_for_seq;
var i : Integer;
begin
  was_golden_coded := false;
  if winner_seq_count = 0 then exit;
  if not golden_allowed then exit;

  if many_gold_seq = 0 then exit;


  if not was_golden_coded then
  begin

    for i:= 0 to many_gold_seq-1 do
    begin
      if tabstepcopy_or_crunch_len[countcrunch] = (seq_winner[i] and 255 ) then
      begin
        if  tabstepoffset[countcrunch]  = (seq_winner[i] shr (8) )  then
        begin
          out_set_bit;
          out_set_bit;
          send_lowestroolbit(0,3);
          send_lowestroolbit(i,4);
          was_golden_coded :=true;
        end;
      end;
    end; // if not was_golden_coded
  end;
end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Procedure prepare_gold_data;
var
i,i2,i3,i4 : Integer;
begin
 LOVE_DATA[0] := GOLDEN_USED_COUNT*3+1;

 if rawdata = 2 then
 begin
   LOVE_DATA[(GOLDEN_USED_COUNT*3) + 1] := poss_dep_ad and 255;
   LOVE_DATA[(GOLDEN_USED_COUNT*3) + 2] := poss_dep_ad shr 8;
   LOVE_DATA[0] := LOVE_DATA[0] or 128;
 end;

 if GOLDEN_USED_COUNT = 0 then exit;

 for i := 0 to GOLDEN_USED_COUNT-1 do
 begin
   i2 := seq_winner[i] and 255;
   i3 := seq_winner[i];
   i3 := i3 shr (8);
   i3 := i3 and 255;
   i4 := seq_winner[i];
   i4 := i4 shr (8);
   i4 := i4 div 256;

   LOVE_DATA[(i*3) + 1] :=i2;
   LOVE_DATA[(i*3) + 2] :=i3;
   LOVE_DATA[(i*3) + 3] :=i4;
 end;

end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//===================
//= other functions =
//===================
//+++++++++++++++++++++++++++++++++++++++++++
procedure cleartablesExecute;
var i : Integer;
begin
  for i := 0 to 65536 do tab_read[i] := 0;
  for i := 0 to 65536 do tab_plik[i] := 0;
  for i := 0 to 65536 do tab_pack[i] := 0;
  f_len :=0;
  d_crunch := 0;
  after_crunch := 0;
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure clearstaticExecute;
begin
  d_crunch :=0;
  after_crunch := 0;
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure copyfileExecute;
var
i,i2,i3 : Integer;
byt_lo : byte;

begin
  if f_len = 0 then exit;
  i3 := f_len-1;
  i2 := 0;
  if no_ld_ad then
  begin
    i2 := 2;
    poss_dep_ad := tab_read[1]*256+tab_read[0];
  end;
  d_crunch := f_len - i2;
  first_decr_block := 0;
  second_decr_block :=0;
  byt_lo := Tab_read[0];
  for i :=  1 to d_crunch do
  begin
   inc(byt_lo);
   if byt_lo = 0 then inc(first_decr_block);
  end;
  for i := 0 to i3 do tab_plik[i] := tab_read[i+i2];
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure set_raw_data;
begin
  if depack_or_raw then
  begin
   rawdata :=2;
  end
  else
  begin
    rawdata:=0;
  end;
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure OpenfileExecute;
var
plik : file of byte;
i,i2 : integer;
begin
f_len := 0;
clearstaticExecute;
  IF not FileExists(my_input_filename) THEN exit;
  cleartablesExecute;
try
  AssignFile( plik , my_input_filename );
  Reset(plik);
  i2 := FileSize(plik);

  if i2 > 64002 then
  begin
    Writeln('File size is ' + IntToStr(i2) +' bytes');
    Writeln('Maximum file size is 64002 bytes long');
    exit;
  end;

  for i := 0 to i2-1 do BlockRead(plik,tab_read[i],1) ;
    mainfirst := tab_read[0];
  mainsecond := tab_read[1];

  set_raw_data;
  f_len := i2;
  copyfileExecute;
finally
  CloseFile(plik);
  if no_ld_ad then poss_dep_ad := tab_read[1]*256+tab_read[0];
end;
end;
//+++++++++++++++++++++++++++++++++++++++++++
function findsequences : Boolean;
var
scan_pointer : Integer;
matchLen : Integer;
matchOffset : Integer;
first, second : Byte;
len : Integer;
s : string;

begin
       s:=chr(9);

  Result:=False;

  matchLen := 0;
  matchOffset := 0;


  if (get_pointer < 2) then exit;

  scan_pointer := get_pointer -1;

  first  := cr_buff[get_pointer];
  second := cr_buff[get_pointer-1];

  //======================================
  while (((get_pointer - scan_pointer) <= max_lng_offs) and (scan_pointer > 0)) do
  begin
    //********************
    if((cr_buff[scan_pointer] = first) and (cr_buff[scan_pointer-1] = second)) then
    begin
      len := 2;
        //+++++++++++++++++++
        while((len < 255) and
              (scan_pointer >= len) and
              (cr_buff[scan_pointer - len] = cr_buff[get_pointer - len])) do len := len +1;
      //&&&&&&&&&&&&&&&&&&&&&
      if(len > matchLen) then
      begin
        matchLen := len;
        matchOffset := get_pointer - scan_pointer;
      end;
      //&&&&&&&&&&&&&&&&&&&&&

    end;// if((cr_buff[scan_pointer] = first) and (cr_buff[scan_pointer - 1] = second))
    //********************

  scan_pointer := scan_pointer -1;

  end;// while (((get_pointer - scan_pointer) <= max_lng_offs) and (scan_pointer > 0))
//==============================================
  if ((matchLen = 2) and (matchOffset <= max_sh_offs)) then
  begin
     result := true;
     my_match_len    := matchLen;
     my_match_offset := matchOffset;
  end;
//==============================================
  if ((matchLen > 2) and (matchOffset <= max_lng_offs)) then
  begin
     result := true;
     my_match_len    := matchLen;
     my_match_offset := matchOffset;
  end;
//==============================================
//==============================================
if max_lng_offs > 256 then
begin
  scan_pointer := get_pointer -1;
  while ((scan_pointer > 0)) do
  begin

    if((cr_buff[scan_pointer] = first) and (cr_buff[scan_pointer-1] = second)) then
    begin
      len := 2;

        while((len < 255) and
              (scan_pointer >= len) and
              (cr_buff[scan_pointer - len] = cr_buff[get_pointer - len])) do len := len +1;

      if(len > matchLen) then
      begin
        matchLen := len;
        matchOffset := get_pointer - scan_pointer;
      end;// if (len > matchLen)

    end;// if((cr_buff[scan_pointer] = first) and (cr_buff[scan_pointer - 1] = second))
    //********************

  scan_pointer := scan_pointer -1;

  end;// while (((get_pointer - scan_pointer) <= max_lng_offs) and (scan_pointer > 0))


    if ((matchLen > 4 ) and (max_lng_offs > 256)) then
    begin
       result := true;
       my_match_len    := matchLen;
       my_match_offset := matchOffset;
    end;
end;
//==============================

end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure recopyfileExecute;
var
  plik : file of byte;
  i:Integer;
begin

   my_output_filename:='recursivecrunchdata' ;

   if FileExists(my_output_filename) then DeleteFile(my_output_filename);
   AssignFile( plik , my_output_filename );
   rewrite(plik);
   if no_safe_ld_ad then
   begin

     ladr_compress_lo := ladr_compressed_data and 255;
     ladr_compress_hi := ladr_compressed_data shr (8);

     if my_own_ld_ad then
     begin
       ladr_compress_lo := StrToInt(s_own_ld_ad) and 255;
       ladr_compress_hi := StrToInt(s_own_ld_ad) shr (8);
     end;

     BlockWrite(plik,(ladr_compress_lo),1);
     BlockWrite(plik,(ladr_compress_hi),1) ;
   end;
   for i := (0) to (after_crunch-1) do BlockWrite(plik,tab_pack[i],1) ;
   closefile(plik);

   sum_decr_block := first_decr_block+second_decr_block;

  my_input_filename := my_output_filename;
  if FileExists(my_input_filename) then  OpenfileExecute;

  if FileExists(my_input_filename) then  DeleteFile(my_input_filename);

  second_decr_block := sum_decr_block;
  many_gold_seq := 16;

  
end;
//++++++++++++++++++++++++++++++++++++++++++
procedure DecruncherExecute;
var
i:Integer;
begin
//========================
//=== depack procedure ===
//========================
  overlap :=1;
  get_pointer := tab_pack[0] and 63;
  if tab_pack[0] > 127 then get_pointer:=get_pointer+2;
  put_pointer := 0;
  shortbitsoffset := 3;
  bitsoffset := 4;

  for i := 0 to 255 do seq_winner[i]:=0;
  for i := 0 to ((tab_pack[0] div 3)-1) do
  begin
    seq_winner[i] := tab_pack[(i*3)+1+2];
    seq_winner[i] := seq_winner[i] shl (8);
    seq_winner[i] := seq_winner[i] or tab_pack[(i*3)+1+1];
    seq_winner[i] := seq_winner[i] shl (8);
    seq_winner[i] := seq_winner[i] or tab_pack[(i*3)+1+0];
  end;

  rol_byte := tab_pack[get_pointer];
  bitcounter :=8;
  rol_word :=0;
  code_add_infobit := true;
  crunchbytes := 5;

while crunchbytes <> 99999 do
begin

        if put_pointer >= get_pointer then
        begin
           if put_pointer- get_pointer >= (overlap-1) then overlap := (put_pointer - get_pointer)+1;
        end;

  oper_code :=0;
  get_one_bit;
  oper_code := oper_code or inputbit;

  if code_add_infobit then
  begin
    oper_code := oper_code shl (1);
    get_one_bit;
    oper_code := oper_code or inputbit;
  end;

  //1 byte copy
  if oper_code = 3 then
  begin
    code_add_infobit := false;
    tab_decrunch[put_pointer] := tab_pack[get_pointer+1];
    inc(put_pointer);
    inc(get_pointer);
  end;



  //more than 1 byte copy
  if oper_code = 2 then
  begin
    code_add_infobit := false;
    getmanybits(3);

    if manybits = 7 then
    begin
     getmanybits(3);
     manybits:=manybits +7;
    end;

    get_bytes_to_copy;

    for i := 1 to copybytes do  tab_decrunch[put_pointer+i-1] := tab_pack[get_pointer+i];

    put_pointer := put_pointer + copybytes;
    get_pointer := get_pointer + copybytes;
  end;


  //decrunch 2 bytes seq
  if oper_code = 0 then
  begin
    code_add_infobit := True;
    crunchbytes := 2;

    getmanybits(shortbitsoffset);
    getbackoffset;

    for i := 0 to 1 do
    begin
      tab_decrunch[put_pointer+i] := tab_decrunch[put_pointer-rol_word+i];
    end;

    put_pointer := put_pointer+2 ;
  end;

  //decrunch >2 bytes seq
  if oper_code = 1 then
  begin
    code_add_infobit := True;
    crunchbytes := 3;

    getmanybits(1);
    crunchbytes := crunchbytes+manybits;

    if crunchbytes = 3 then
    begin
      getmanybits(bitsoffset);
      getbackoffset;
    end;

    if crunchbytes = 4 then
    begin

      getmanybits(3);
      //if manybits = 7 then  //to long depack for coded unlimmited long seq
      //begin
       //getmanybits(3);
       //manybits:=manybits +7;
      //end;
      getcrunchbytes;

      if crunchbytes < 4 then
      begin
         getmanybits(4);
         if (crunchbytes=3) then
         begin
           for i := 0 to put_pointer-1 do
           begin
             if tab_decrunch[i] <> cr_buff[i] then
             begin
               //Writeln('error '+IntToStr(i));
               break;
             end;
           end;
          // exit point;
          I := put_pointer-1;
          break;
        end
        else begin
            crunchbytes :=(seq_winner[manybits] and 255 );
            rol_word:=(seq_winner[manybits] shr (8) );
        end;
      end
      else begin
        getmanybits(bitsoffset);
        getbackoffset;
      end;
    end; //if crunchbytes =4
    for i := 0 to (crunchbytes -1) do tab_decrunch[put_pointer+i] := tab_decrunch[put_pointer-rol_word+i];

    put_pointer := put_pointer + crunchbytes ;

  end; //if oper. code = 1


end; // while crunchbytes <> 99999

   if put_pointer >= get_pointer then
   begin
      if put_pointer- get_pointer >= (overlap-1) then overlap := (put_pointer - get_pointer)+1;
   end;
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure crunchbufferExecute;
var

  i,i2 : Integer;
  countcrunch2 : Integer;
  crunched : Integer;
  tmpr1 : byte;
  s : string;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//************************************
//*** mainlooop procedure cruncher ***
//************************************
begin
  crunchresult :=False;
  length_long_seq := 0;
  s := chr(9);
  rol_byte := 8;
  bitsoffset :=4;
  shortbitsoffset :=3;

  if d_crunch = 0 then exit;

  manycrunch := 0;

  for i := 0 to 32768 do tabstepcruncheroperation[i]:=0;
  for i := 0 to 32768 do tabstepoffset[i]:=0;
  for i := 0 to 32768 do tabstepcopy_or_crunch_len[i]:=0;
  for i := 0 to 65535 do tab_decrunch[i]:=0;
  for i := 0 to 65536 do tab_pack[i]:=0;



  if f_len = 0 then exit;
  size_of_cr_buff := d_crunch;

  crbuf_size := 65535;


  if not no_ld_ad then
  begin
     for i := 0 to size_of_cr_buff-1 do cr_buff[i] := tab_read[i];
     cr_buff[0] := mainfirst;
     cr_buff[1] := mainsecond;
  end
  else
  begin
    for i := 0 to size_of_cr_buff-1 do cr_buff[i] := tab_read[i+2];
  end;

  get_pointer := size_of_cr_buff - 1;
  put_pointer := crbuf_size - 1;


  my_match_len:=0;
  len_plain_data:=0;
  countcrunch := 1;

  //simullate crunch - true eof indicator
  tabstepcruncheroperation[0] := 2;//oper. crunch
  tabstepcopy_or_crunch_len[0] := 1; //1 byte crunch
  tabstepoffset[0] :=2 ;//offset played crunchingu

  while (get_pointer >= 0) do begin
    if findsequences() then
    begin
      if len_plain_data > 0 then
      begin
       tabstepcruncheroperation[countcrunch]:=1;//oper. copy
       tabstepcopy_or_crunch_len[countcrunch] := len_plain_data;

        inc(countcrunch);
      end;

      if my_match_len > 1 then
      begin
        tabstepcruncheroperation[countcrunch]:=2;//oper. crunch
        tabstepcopy_or_crunch_len[countcrunch] := my_match_len;//len seq
        tabstepoffset[countcrunch]:=my_match_offset;//offset
        inc(countcrunch);
      end;

      get_pointer := get_pointer-my_match_len;
      len_plain_data:=0;
      my_match_len:=0;


    end
    else begin
      inc(len_plain_data);
      dec(get_pointer);
    end;

      if get_pointer > 64500 then break;
    end;

    tabstepcruncheroperation[countcrunch]:=1;//oper. copy
    tabstepcopy_or_crunch_len[countcrunch] := len_plain_data;
    countcrunch2:=countcrunch;


    //new setup for crunching

    get_pointer :=0; //begin is start from 0 poss.
    put_pointer :=1; //more 1 than get_pointer
    stream_put_pointer :=0; //put_pointer -1
    bitcounter := 255; //after first increase will be 0
    code_add_infobit := True;

    find_golden_seq(countcrunch);
//============================
//=== main crunching loop  ===
//============================

   while countcrunch > 0 do
   begin

//===================================
//=== coding info about copy      ===
//===================================


        if tabstepcruncheroperation[countcrunch] = 1 then
        begin
          code_add_infobit := false; //after copy only crunching possible - so 1 bit less to code
          out_set_bit; //send 1 to infostream

          //if code 1 byte to copy
          if tabstepcopy_or_crunch_len[countcrunch] = 1 then
          begin
            out_set_bit;
          end
          else begin   //else code more than 1 byte to copy
            out_clear_bit;

            tmpr1 := how_many_bits(tabstepcopy_or_crunch_len[countcrunch])-1;

            if tmpr1 < 7 then
            begin
              send_lowestroolbit(tmpr1,3);
            end
            else
            begin
              send_lowestroolbit(7,3);
              send_lowestroolbit((tmpr1-7),3);
            end;
          sendlongbytescopy;
          end; // tabstepcopy_or_crunch_len[countcrunch] = 1 else

          for i := 0 to (tabstepcopy_or_crunch_len[countcrunch]-1) do tab_pack[put_pointer+i] := cr_buff[get_pointer+i];
          get_pointer := get_pointer+tabstepcopy_or_crunch_len[countcrunch]; //increment pointer for inputbuffer
          put_pointer := put_pointer+tabstepcopy_or_crunch_len[countcrunch]; //increment pointer for outputbuffer
        end; // if tabstepcruncheroperation[countcrunch] = 1 then

//==========================================
//=== end service copy data              ===
//==========================================

        if tabstepcruncheroperation[countcrunch] = 2 then
        begin
          if code_add_infobit then out_clear_bit;

          if tabstepcopy_or_crunch_len[countcrunch] = 2 then
          begin
            try_golden_for_seq;

            if (not was_golden_coded) then
            begin
              out_clear_bit;
              send_lowestroolbit(how_many_bits(tabstepoffset[countcrunch])-1, shortbitsoffset);
              sendoffsetbytes;
            end;
          end;

          if tabstepcopy_or_crunch_len[countcrunch] = 3 then
          begin
            try_golden_for_seq;
            if (not was_golden_coded) then
            begin
              out_set_bit;
              out_clear_bit;
              send_lowestroolbit(how_many_bits(tabstepoffset[countcrunch])-1, bitsoffset);
              sendoffsetbytes;
            end;
          end; // if tabstepcopy_or_crunch_len[countcrunch] = 3


          if tabstepcopy_or_crunch_len[countcrunch] > 3 then
          begin
            try_golden_for_seq;
            if (not was_golden_coded) then
            begin
              out_set_bit;
              out_set_bit;
              tmpr1 := how_many_bits(tabstepcopy_or_crunch_len[countcrunch])-1;

              if tmpr1 < 7 then
              begin
                send_lowestroolbit(tmpr1,3);
              end
              else
              begin
                send_lowestroolbit(7,3);
              end;

              sendlongcrunchbytes;
              send_lowestroolbit(how_many_bits(tabstepoffset[countcrunch])-1, bitsoffset);
              sendoffsetbytes;
            end;
          end; // if tabstepcopy_or_crunch_len[countcrunch] = 2


          code_add_infobit := true;
          get_pointer := get_pointer+tabstepcopy_or_crunch_len[countcrunch]; //increment pointer + lenseq
        end; // if tabstepcruncheroperation[countcrunch] = 2 then

     dec(countcrunch); //decrease finded seq counter
     IF countcrunch < 0 THEN BREAK;
   end; //   while countcrunch > 0 do

   flush_and_finish;




//=========================
//===  end crunch loop  ===
//=========================
   countcrunch:=countcrunch2;
   crunched := put_pointer;


   prepare_gold_data;
   after_crunch := rawdata + put_pointer+(LOVE_DATA[0] and 127);
   for i := crunched downto 0 do tab_pack[rawdata+i+((LOVE_DATA[0])and 127)] := tab_pack[i];
   for i := 0 to rawdata +(((LOVE_DATA[0])and 127)-1) do tab_pack[i] := LOVE_DATA[i];

//========================
//=== depack procedure ===
//========================

    DecruncherExecute;

    overlay :=  overlap -(f_len - after_crunch);
    if not no_ld_ad then overlay := overlay-2;
    ladr_compressed_data:= poss_dep_ad+d_crunch-after_crunch+overlay;

//===========================
//=== decrunching end     ===
//===========================
  crunchresult := True;
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure set_max_lng_offsExecute;
begin
  if offset_power = 9  then  max_lng_offs := 511;
  if offset_power = 10 then  max_lng_offs := 1023;
  if offset_power = 11 then  max_lng_offs := 2047;
  if offset_power = 12 then  max_lng_offs := 4095;
  if offset_power = 13 then  max_lng_offs := 8191;
  if offset_power = 14 then  max_lng_offs := 16193;
  if offset_power = 15 then  max_lng_offs := 32767;
  if offset_power = 16 then  max_lng_offs := 65535;
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure FINDBESTCRUNCHExecute;
var
i1,i4,i5 : Integer;
function how_many_bits(myvalue : Word): Integer;
begin

 Result:=16;
 if myvalue = 0 then myvalue :=1;
 while (myvalue < 32768) do
 begin
    dec(Result);
    myvalue := myvalue shl (1);
 end;
end;

begin

  if f_len = 0 then
  begin
    Writeln('No any data file to crunch');
    exit;
  end;
  first_dep_value := poss_dep_ad;
  if poss_dep_ad < $07c3 then  first_dep_ady := True else  first_dep_ady := False;
  many_gold_seq := 16;


    i5:=100000;

    offset_power := 9;
    set_max_lng_offsExecute;

    for i1 := offset_power to 16 do
    begin
      offset_power := i1;
      set_max_lng_offsExecute;
      if ((how_many_bits(f_len) > 8) and(how_many_bits(f_len) < offset_power)) then
      begin
        offset_power := offset_power-1 ;
        set_max_lng_offsExecute();
        break;
      end;
      many_gold_seq := 16;
      Writeln('wait...');
      crunchbufferExecute();
      if after_crunch < i5 then
      begin
        i5 := after_crunch;
        i4 := i1;
      end;
    end;



  offset_power := i4;
  set_max_lng_offsExecute();
  many_gold_seq := 16;
  Writeln('crunch...');  
  crunchbufferExecute();
  Writeln('Encored bytes ' + inttostr(overlay));

  if not deff_recursive then exit;
  Writeln('iterate...'); 
  recopyfileExecute();
  many_gold_seq := 16;

  i5:=100000;

  offset_power := 9;
  set_max_lng_offsExecute();


    for i1 := offset_power to 16 do
    begin
      offset_power := i1;
      set_max_lng_offsExecute();

      if ((how_many_bits(f_len) > 8) and(how_many_bits(f_len) < offset_power)) then
      begin
        offset_power := offset_power-1 ;
        set_max_lng_offsExecute();
        break;
      end;
      many_gold_seq := 16;
      crunchbufferExecute();
      Writeln('wait...');
      if after_crunch < i5 then
      begin
        i5 := after_crunch;
        i4 := i1;
      end;
    end;

  offset_power := i4;
  set_max_lng_offsExecute();
  many_gold_seq := 16;
  Writeln('crunch...');
  crunchbufferExecute();
  Writeln('Encored bytes ' + inttostr(overlay));  
  tab_pack[0] := tab_pack[0] or 64;  


end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure Savecrunchedbin;
var
  plik : file of byte;
  i:Integer;
begin

   if after_crunch = 0 then
   begin
     Writeln('No crunched data in buffer... Pleasse try again later ;-)');
     Readln;
     exit;
   end;



   outputfile := my_output_filename;

   if outputfile = '' then exit;

   if FileExists(my_output_filename) then DeleteFile(my_output_filename);
   AssignFile( plik , my_output_filename );
   rewrite(plik);
   if no_safe_ld_ad then
   begin

     ladr_compress_lo := ladr_compressed_data and 255;
     ladr_compress_hi := ladr_compressed_data shr (8);

     if my_own_ld_ad then
     begin
       ladr_compress_lo := StrToInt(s_own_ld_ad) and 255;
       ladr_compress_hi := StrToInt(s_own_ld_ad) shr (8);
     end;

     BlockWrite(plik,(ladr_compress_lo),1);
     BlockWrite(plik,(ladr_compress_hi),1) ;
   end;
   set_raw_data;
   if rawdata = 0 then tab_pack[0] := tab_pack[0] and 127;
   if rawdata = 0 then for i:= tab_pack[0] to 65535 do tab_pack[i] := tab_pack[i+2];
   if rawdata = 0 then after_crunch := after_crunch-2;
   for i := (0) to (after_crunch-1) do BlockWrite(plik,tab_pack[i],1) ;
   closefile(plik);

end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure tryrecursive2Execute();
var
  i:Integer;
  plik : file of byte;
begin

  depack_len := length(fast3);
  for i := 1 to depack_len do depacker[i-1] := fast3[i];


  depacker[$16C]:= blocki[0];
  depacker[$16D]:= blocki[1];
  depacker[$16E]:= blocki[2];
  if first_dep_ady then  depacker[$154]:= $60;

  depacker[$D9]:= value_01;

  depacker[$DD]:= (StrToInt(s_JMP_ad) and 255);
  depacker[$DE]:= (StrToInt(s_JMP_ad) shr (8));

  

   outputfile := my_output_filename;

   if outputfile = '' then exit;

  if FileExists(my_output_filename) then DeleteFile(my_output_filename);
  AssignFile( plik , my_output_filename );
  rewrite(plik);

  for i := (0) to (depack_len-1) do BlockWrite(plik,depacker[i],1) ;
  for i := (0) to (after_crunch-1) do BlockWrite(plik,tab_pack[i],1) ;
  closefile(plik);
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure trynorecusive2Execute();
var
  i:Integer;
  plik : file of byte;
begin

  depack_len := length(fast4);
  for i := 1 to depack_len do depacker[i-1] := fast4[i];


  depacker[$13f]:= blocki[0];
  depacker[$140]:= blocki[1];
  depacker[$141]:= blocki[2];
  if first_dep_ady then  depacker[$127]:= $60;

  depacker[$ac]:= value_01;

  depacker[$b0]:= (StrToInt(s_JMP_ad) and 255);
  depacker[$b1]:= (StrToInt(s_JMP_ad) shr (8));


   outputfile := my_output_filename;

   if outputfile = '' then  exit;

  if FileExists(my_output_filename) then DeleteFile(my_output_filename);
  AssignFile( plik , my_output_filename );
  rewrite(plik);

  for i := (0) to (depack_len-1) do BlockWrite(plik,depacker[i],1) ;
  for i := (0) to (after_crunch-1) do BlockWrite(plik,tab_pack[i],1) ;
  closefile(plik);
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure tryrecursiveExecute();
var
  i:Integer;
  plik : file of byte;
begin
  depack_len := length(fast1);
  for i := 1 to depack_len do depacker[i-1] := fast1[i];


  //BLOCKs TO REG Y
  i := after_crunch shr (8);
  if after_crunch and 255 <> 0 then inc(i);
  depacker[$30]:= (i and 255);

  //FINISH-$0100
  i:= 2049+depack_len-2+after_crunch-256;
  depacker[$32]:= (i and 255);
  depacker[$33]:= (i shr (8));

  //DEPACKADR
  depacker[$4b]:= blocki[0];
  depacker[$4c]:= blocki[1];
  depacker[$4d]:= blocki[2];

  i:=65535-after_crunch +1;
  depacker[$44]:= (i and 255);
  depacker[$46]:= (i shr (8));

  if first_dep_ady then  depacker[$16d]:= $60;
  depacker[$f2]:= value_01;

  depacker[$f6]:= (StrToInt(s_JMP_ad) and 255);
  depacker[$f7]:= (StrToInt(s_JMP_ad) shr (8));


   outputfile := my_output_filename;

   if outputfile = '' then  exit;

  if FileExists(my_output_filename) then DeleteFile(my_output_filename);
  AssignFile( plik , my_output_filename );
  rewrite(plik);

  for i := (0) to (depack_len-1) do BlockWrite(plik,depacker[i],1) ;
  for i := (0) to (after_crunch-1) do BlockWrite(plik,tab_pack[i],1) ;
  closefile(plik);

end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure trynorecursiveExecute();
var
  i:Integer;
  plik : file of byte;
begin

  depack_len := length(fast2);
  for i := 1 to depack_len do depacker[i-1] := fast2[i];

  //BLOCKs TO REG Y
  i := after_crunch shr (8);
  if after_crunch and 255 <> 0 then inc(i);
  depacker[$30]:= (i and 255);

  //FINISH-$0100
  i:= 2049+depack_len-2+after_crunch-256;
  depacker[$32]:= (i and 255);
  depacker[$33]:= (i shr (8));
  if first_dep_ady then  depacker[$156]:= $60;
  //DEPACKADR

  depacker[$4b]:= blocki[0];
  depacker[$4c]:= blocki[1];
  depacker[$4d]:= blocki[2];

  i:=65535-after_crunch +1;
  depacker[$44]:= (i and 255);
  depacker[$46]:= (i shr (8));

  depacker[$db]:= value_01;

  depacker[$df]:= (StrToInt(s_JMP_ad) and 255);
  depacker[$e0]:= (StrToInt(s_JMP_ad) shr (8));


   outputfile := my_output_filename;

  if outputfile = '' then exit;

  if FileExists(my_output_filename) then DeleteFile(my_output_filename);
  AssignFile( plik , my_output_filename );
  rewrite(plik);

  for i := (0) to (depack_len-1) do BlockWrite(plik,depacker[i],1) ;
  for i := (0) to (after_crunch-1) do BlockWrite(plik,tab_pack[i],1) ;
  closefile(plik);
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure savecrunchedprg();
var
 i,i2 : integer;
s : String;
begin
  If not depack_or_raw then
  begin
    Writeln('For SFX You must set the depack address');
    Readln;
    exit;
  end;

  if first_dep_value < $0400 then
  begin
    Writeln('The depack address can not be lower than $0400');
    Writeln('or remake by self sfxdecruncher and link with bin data');
    Readln;
    exit;
  end;

  i2 := poss_dep_ad;

  for i := 0 to 2 do blocki[i] := $30;

  sum_decr_block := first_decr_block+second_decr_block;
  s := IntToStr(sum_decr_block);
  blocki[2] := ord(s[length(s)]);
  if Length(s) > 1 then  blocki[1] := ord(s[length(s)-1]);
  if Length(s) > 2 then  blocki[0] := ord(s[length(s)-2]);

  if 2049+372+after_crunch < i2 then
  begin
   if tab_pack[0] and 64 <> 0 then tryrecursive2Execute() else trynorecusive2Execute();
  end
  else
  begin
   if tab_pack[0] and 64 <> 0 then  tryrecursiveExecute() else trynorecursiveExecute();
  end;
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure SpecialdecrunchExecute();
var
i:Integer;
begin
//========================
//=== depack procedure ===
//========================

  for i := depack_from to 65530 do tab_pack[i-depack_from] := tab_pack2[i];

  dep_ady := 0;
  corrupted_data := true;
  overlap :=1;
  get_pointer := tab_pack[0] and 63;
  rekursja := tab_pack[0] and 64;
  if tab_pack[0] and 128 = 128 then
  begin
   dep_ady :=  tab_pack[(tab_pack[0] and 63)+1]*256 +tab_pack[(tab_pack[0] and 63)];
  end;

  if tab_pack[0] > 127 then get_pointer:=get_pointer+2;
  put_pointer := 0;
  shortbitsoffset := 3;
  bitsoffset := 4;

  for i := 0 to 255 do seq_winner[i]:=0;
  for i := 0 to ((tab_pack[0] div 3)-1) do
  begin
    seq_winner[i] := tab_pack[(i*3)+1+2];
    seq_winner[i] := seq_winner[i] shl (8);
    seq_winner[i] := seq_winner[i] or tab_pack[(i*3)+1+1];
    seq_winner[i] := seq_winner[i] shl (8);
    seq_winner[i] := seq_winner[i] or tab_pack[(i*3)+1+0];
  end;

  rol_byte := tab_pack[get_pointer];
  bitcounter :=8;
  rol_word :=0;
  code_add_infobit := true;
  crunchbytes := 5;

while crunchbytes <> 99999 do
begin



  oper_code :=0;
  get_one_bit;
  oper_code := oper_code or inputbit;

  if code_add_infobit then
  begin
    oper_code := oper_code shl (1);
    get_one_bit;
    oper_code := oper_code or inputbit;
  end;

  //copy 1 byte
  if oper_code = 3 then
  begin
    code_add_infobit := false;
    tab_decrunch[put_pointer] := tab_pack[get_pointer+1];
    inc(put_pointer);
    inc(get_pointer);
  end;



  //copy more than 1 byte
  if oper_code = 2 then
  begin
    code_add_infobit := false;
    getmanybits(3);

    if manybits = 7 then
    begin
     getmanybits(3);
     manybits:=manybits +7;
    end;

    get_bytes_to_copy;
    if put_pointer + copybytes > 65530 then exit;
    if get_pointer + copybytes > 65530 then exit;
    for i := 1 to copybytes do  tab_decrunch[put_pointer+i-1] := tab_pack[get_pointer+i];

    put_pointer := put_pointer + copybytes;
    get_pointer := get_pointer + copybytes;
  end;


  //decrunch 2 bytes seq
  if oper_code = 0 then
  begin
    code_add_infobit := True;
    crunchbytes := 2;

    getmanybits(shortbitsoffset);
    getbackoffset;
    if put_pointer - rol_word < 0 then exit;

    for i := 0 to 1 do
    begin
      tab_decrunch[put_pointer+i] := tab_decrunch[put_pointer-rol_word+i];
    end;
    put_pointer := put_pointer+2 ;
  end;

  //decrunch >2 bytes seq
  if oper_code = 1 then
  begin
    code_add_infobit := True;
    crunchbytes := 3;

    getmanybits(1);
    crunchbytes := crunchbytes+manybits;

    if crunchbytes = 3 then
    begin
      getmanybits(bitsoffset);
      getbackoffset;
      if put_pointer - rol_word < 0 then exit;
    end;

    if crunchbytes = 4 then
    begin

      getmanybits(3);
      //if manybits = 7 then //to long depack for coded unlimmited long seq
      //begin
       //getmanybits(3);
       //manybits:=manybits +7;
      //end;
      getcrunchbytes;

      if crunchbytes < 4 then
      begin
         getmanybits(4);
         if (crunchbytes=3) then //end of file
         begin
          corrupted_data := False;
          I := put_pointer-1;
          break;
        end
        else begin
            crunchbytes :=(seq_winner[manybits] and 255 );
            rol_word:=(seq_winner[manybits] shr (8) );
        end;
      end
      else begin
        getmanybits(bitsoffset);
        getbackoffset;
        if put_pointer - rol_word < 0 then exit;
      end;
    end; //if crunchbytes =4
    for i := 0 to (crunchbytes -1) do tab_decrunch[put_pointer+i] := tab_decrunch[put_pointer-rol_word+i];

    put_pointer := put_pointer + crunchbytes ;

  end; //if oper code = 1


end; // while crunchbytes <> 99999
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure crunchedfileopenExecute();
var
plik : file of byte;
i,i2 : integer;
begin
  my_sfx := true;
  f_len := 0;
  packed_data:=0;
  i2:= 0;
  depack_from :=0;

clearstaticExecute();

  IF not FileExists(my_input_filename) THEN exit;
  cleartablesExecute();


  AssignFile( plik , my_input_filename );
  Reset(plik);
  i2 := FileSize(plik);


  if i2 > 64002 then
  begin
    i2 :=0;
    CloseFile(plik);
    Writeln('File size is ' + IntToStr(i2) +' bytes');
    Writeln('Maximum file size is 64002 bytes long');
    Readln;
    exit;
  end;

  for i := 0 to i2-1 do BlockRead(plik,tab_pack2[i],1) ;

  CloseFile(plik);

  packed_data := i2;

  depack_len := length(fast1);
  for i := 1 to depack_len do depacker[i-1] := fast1[i];


  i2 := 0;

  for i := 0 to depack_len-1 do if depacker[i] =  tab_pack2[i] then inc(i2);
  if i2 > 290 then
  begin
    for i := depack_len to 65534 do tab_pack2[i-depack_len]:=tab_pack2[i];
    exit;
  end;

  depack_len := length(fast2);
  for i := 1 to depack_len do depacker[i-1] := fast2[i];

  i2 := 0;

  for i := 0 to depack_len-1 do if depacker[i] =  tab_pack2[i] then inc(i2);
  if i2 > 290 then
  begin
    for i := depack_len to 65534 do tab_pack2[i-depack_len]:=tab_pack2[i];
    exit;
  end;

  depack_len := length(fast3);
  for i := 1 to depack_len do depacker[i-1] := fast3[i];

  i2 := 0;

  for i := 0 to depack_len-1 do if depacker[i] =  tab_pack2[i] then inc(i2);
  if i2 > 290 then
  begin
    for i := depack_len to 65534 do tab_pack2[i-depack_len]:=tab_pack2[i];
    exit;
  end;
  depack_len := length(fast4);
  for i := 1 to depack_len do depacker[i-1] := fast4[i];

  i2 := 0;

  for i := 0 to depack_len-1 do if depacker[i] =  tab_pack2[i] then inc(i2);
  if i2 > 290 then
  begin
    for i := depack_len to 65534 do tab_pack2[i-depack_len]:=tab_pack2[i];
    exit;
  end;
  my_sfx := false;
  depack_from := 2;
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure rekursdecrExecute();
var
i : Integer;
m_chr : char;
begin
  count_rekursja := 0;
  spcase := 0;
  firstput := 0;
  secput := 0;

  depack_from := 0;
  SpecialdecrunchExecute();
  if not corrupted_data then
  begin
    firstput := put_pointer;
    inc(spcase);
  end;

  depack_from := 2;
  SpecialdecrunchExecute();
  if not corrupted_data then
  begin
    secput := put_pointer;
    inc(spcase);
  end
  else
  begin
    depack_from := 0;
    SpecialdecrunchExecute();
  end;

  If spcase = 2 then
  begin

    Writeln('1. File with load addres after decrunch length '+IntToStr(secput)+ ' bytes' );
    Writeln('2. File without load addres after decrunch length '+IntToStr(firstput)+ ' bytes' );
    Writeln;
    Writeln('Type 1 or 2 and press enter');
    Read(m_chr);
    If m_chr = '1' then depack_from := 2 else depack_from := 0;

    SpecialdecrunchExecute();
    secput := put_pointer;
    firstput := put_pointer;
  end;
  if spcase = 0 then
  begin
   Writeln('Data corrupted');
   Readln;
   exit;
  end;

  if rekursja = 0 then exit;

  while ((count_rekursja<20) and(rekursja <> 0)) do
  begin
    inc(count_rekursja);
    for i := 0 to 65534 do tab_pack2[i] := tab_decrunch[i];

    spcase := 0;
    firstput := 0;
    secput := 0;

    depack_from := 0;
    SpecialdecrunchExecute();
    if not corrupted_data then
    begin
      firstput := put_pointer;
      if firstput > 0 then inc(spcase);
    end;

    depack_from := 2;
    SpecialdecrunchExecute();
    if not corrupted_data then
    begin
      secput := put_pointer;
      if secput > 0 then inc(spcase);
    end
    else
    begin
//      depack_from := 0;
//      SpecialdecrunchExecute();
    end;

    depack_from := depack_from xor 2;
    SpecialdecrunchExecute();
    
    If spcase = 2 then
    begin

      Writeln('1. File with load addres after decrunch length '+IntToStr(secput)+ ' bytes' );
      Writeln('2. File without load addres after decrunch length '+IntToStr(firstput)+ ' bytes' );
      Writeln;
      Writeln('Type 1 or 2 and press enter');
      Read(m_chr);
      If m_chr = '1' then depack_from := 2 else depack_from := 0;

      SpecialdecrunchExecute();
      secput := put_pointer;
      firstput := put_pointer;
    end;
    if spcase = 0 then
    begin
      Writeln('Data corrupted');
      Readln;
      exit;
    end;
  end;
end;

//+++++++++++++++++++++++++++++++++++++++++++
procedure LoadanddecrunchExecute();
var
plik : File of byte;
i:Integer;
begin
  crunchedfileopenExecute();
  if packed_data = 0 then exit;
  rekursdecrExecute();
  if corrupted_data then exit;
  if my_output_filename = '' then my_output_filename := 'deflatedfile.bin';

   if FileExists(my_output_filename) then DeleteFile(my_output_filename);
   AssignFile( plik , my_output_filename );
   rewrite(plik);

   if dep_ady <> 0 then
   begin
     ladr_compress_lo := dep_ady and 255;
     ladr_compress_hi := dep_ady shr (8);
     BlockWrite(plik,(ladr_compress_lo),1);
     BlockWrite(plik,(ladr_compress_hi),1) ;
   end;

   for i := (0) to (put_pointer -1) do BlockWrite(plik,tab_decrunch[i],1) ;
   closefile(plik);

end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure PrintHelp();
begin
Writeln('Usage for decrunch :');
Writeln('cruncher.exe {-deflate} {-i inputfilename} [-o outputfilename]');
Writeln;
Writeln('Usage for crunch :');
Writeln('cruncher.exe {-i inputfilename} {-o outputfilename} [-parameters]');
Writeln;
Writeln('-NOLDAD default (no) file without load address');
Writeln('-NODEPACKADR  save crunched data without depack address (as RAW)');
Writeln('-RAWDATA this same as -NODEPACKADR');
Writeln('-DEPACKADR $xxxx or dec. xxxx - depack address value (default from load ad.)');
Writeln('-NOGOLDENSEQ defalt(no) - crunch data without golden sequences');
Writeln('-ITERATE default (no) - recursive crunch data');
Writeln('-BINFILE default true - save data without attached decruncher');
Writeln('-PRGFILE default (no) - save data with attached decruncher as SFX file');
Writeln('-NOSAFELDADR default (no) - save data without safe or any load address');
Writeln('-MYLDADR $xxxx or dec. xxxx - add typed load address to crunched data');
Writeln('-VALUE01 $xx or dec. xxx - after depack $01 value for (SFX file)');
Writeln('-JMP $xxxx or dec. xxxx -  jump address after depack for decruncher (SFX file)');
Writeln('-PACKFROM $xxxx or dec. xxxx - partial crunch file from typed byte');
Writeln('-PACKTO   $xxxx or dec. xxxx - partial crunch file to typed byte');
end;
//+++++++++++++++++++++++++++++++++++++++++++
procedure checkparameters();
var
i,i2 : Integer;

cc : char;
DO_DECRUNCH : Boolean;
begin

packfrom :=0;
packto := 100000;
inputfile :='';
outputfile:='';
depack_or_raw := true;
no_ld_ad := true;
poss_dep_ad := 2049;
deff_recursive := False;
golden_allowed := True;
many_gold_seq := 16;
my_own_ld_ad := False;
no_safe_ld_ad := True;
s_own_ld_ad := '$080D';
s_JMP_ad := '$080D';
value_01 := $37;
offset_power :=9;

  max_sh_offs := 255;
  set_max_lng_offsExecute();
  set_raw_data();

  outputfile:='crunchedfile.prg';
  cleartablesExecute();

  binfile :=true;

  if ParamCount < 3 then
  begin
   PrintHelp();
   Readln;
   exit;
  end;




  DO_DECRUNCH := false;
  for i := 1 to ParamCount do
    if UpperCase(ParamStr(i)) = '-DEFLATE' then DO_DECRUNCH := True;

  if DO_DECRUNCH then
  begin
    outputfile:='';
    for i := 1 to ParamCount-1 do
    if UpperCase(ParamStr(i)) = '-O' then outputfile := ParamStr(i+1);
    for i := 1 to ParamCount-1 do
    if UpperCase(ParamStr(i)) = '-I' then inputfile := ParamStr(i+1);

    my_input_filename := inputfile;
    my_output_filename := outputfile;
    if outputfile = '' then my_output_filename := 'deflatedfile.prg' ;
    LoadanddecrunchExecute();
    exit;
  end;

  if ParamCount < 4 then
  begin
   PrintHelp();
   Readln;
   exit;
  end;


  for i := 1 to ParamCount-1 do
    if UpperCase(ParamStr(i)) = '-O' then outputfile := ParamStr(i+1);

  for i := 1 to ParamCount-1 do
  begin
    if UpperCase(ParamStr(i)) = '-I' then inputfile := ParamStr(i+1);
    if ((inputfile = '') or (not FileExists(inputfile))) then
    begin
      Writeln('File does not exists');
      Writeln(inputfile);
      Readln;
      exit;
    end;
  end;
  my_input_filename := inputfile;
  OpenfileExecute();


  for i := 1 to ParamCount do
  begin
    if UpperCase(ParamStr(i)) = '-NOLDAD' then
    begin
      no_ld_ad := False;
      copyfileExecute();
    end;
  end;


  for i := 1 to ParamCount do
  begin
    if UpperCase(ParamStr(i)) = '-NODEPACKADR' then depack_or_raw := False;
  end;
  for i := 1 to ParamCount do
  begin
    if UpperCase(ParamStr(i)) = '-RAWDATA' then depack_or_raw := False;
  end;
  for i := 1 to ParamCount do
  begin
    if UpperCase(ParamStr(i)) = '-ITERATE' then deff_recursive := True;
  end;
  for i := 1 to ParamCount do
  begin
    if UpperCase(ParamStr(i)) = '-NOGOLDENSEQ' then golden_allowed := False;
  end;
  for i := 1 to ParamCount do
  begin
    if UpperCase(ParamStr(i)) = '-PRGFILE' then binfile := False;
  end;
  for i := 1 to ParamCount do
  begin
    if UpperCase(ParamStr(i)) = '-BINFILE' then binfile := True;
  end;

  for i := 1 to ParamCount do
  begin
    if UpperCase(ParamStr(i)) = '-NOSAFELDADR' then
    BEGIN
      no_safe_ld_ad := False;
      my_own_ld_ad := False;
    END;
  end;

  for i := 1 to ParamCount-1 do
  begin
    if UpperCase(ParamStr(i)) = '-MYLDADR' then
    BEGIN
      my_own_ld_ad := True;
      no_safe_ld_ad := True;

      s_own_ld_ad := '$080D';
      TRY
        I2 := StrToInt(ParamStr(i+1));
        s_own_ld_ad :='$'+ IntToHex(I2,4);
      EXCEPT
        I2 := 2061;
        s_own_ld_ad :='$'+ IntToHex(I2,4);
      END;
        s_own_ld_ad :='$'+ IntToHex(I2,4);
    END;
  end;

  for i := 1 to ParamCount-1 do
  begin
    if UpperCase(ParamStr(i)) = '-DEPACKADR' then
    BEGIN
      TRY
        I2 := StrToInt(ParamStr(i+1));
        poss_dep_ad := I2;

      EXCEPT
        I2 := 2049;
        poss_dep_ad := I2;

      END;
        poss_dep_ad := I2;

    END;
  end;

  for i := 1 to ParamCount-1 do
  begin
    if UpperCase(ParamStr(i)) = '-JMP' then
    BEGIN
      s_JMP_ad := '$080D';
      TRY
        I2 := StrToInt(ParamStr(i+1));
        s_JMP_ad :='$'+ IntToHex(I2,4);
      EXCEPT
        I2 := 2061;
        s_JMP_ad :='$'+ IntToHex(I2,4);
      END;
        s_JMP_ad :='$'+ IntToHex(I2,4);
    END;
  end;

  for i := 1 to ParamCount-1 do
  begin
    if UpperCase(ParamStr(i)) = '-VALUE01' then
    BEGIN
      value_01 := $37;
      TRY
        I2 := StrToInt(ParamStr(i+1));
        value_01 := i2 and 255;
      EXCEPT
        I2 := 7;
        value_01 := i2 and 255;
      END;
        value_01 := i2 and 255;
    END;
  end;


  for i := 1 to ParamCount-1 do
  begin
    if UpperCase(ParamStr(i)) = '-PACKFROM' then
    BEGIN
      TRY
        I2 := StrToInt(ParamStr(i+1));
      EXCEPT
        I2 := 0;
        packfrom := I2;
      END;
        packfrom := I2;
    END;
  end;

  for i := 1 to ParamCount-1 do
  begin
    if UpperCase(ParamStr(i)) = '-PACKTO' then
    BEGIN
      TRY
        I2 := StrToInt(ParamStr(i+1));
      EXCEPT
        I2 := 0;
        packto := I2;
      END;
        packto := I2;
    END;
  end;

  IF packfrom < f_len THEN
  BEGIN
    IF packfrom < packto THEN
    BEGIN
      IF packto > f_len THEN packto := f_len;
      if ((rawdata = 2) and (packfrom <2)) then packfrom := 2;

        FOR I := packfrom TO 65535 DO tab_read[I-packfrom+rawdata] := tab_read[I];
        f_len := packto-packfrom+rawdata;
        I2 := poss_dep_ad;
        I2 := I2 + packfrom - rawdata;


        IF depack_or_raw THEN poss_dep_ad := I2;

        first_decr_block:=0;

        if rawdata = 2  then
        begin
          tab_read[0] := poss_dep_ad and 255;
          tab_read[1] := poss_dep_ad shr 8 and 255;
        end;

        copyfileExecute();

    END;
  END;

  copyfileExecute();
  FINDBESTCRUNCHExecute();
  my_output_filename := outputfile;

  IF binfile THEN Savecrunchedbin() ELSE  savecrunchedprg();
end;
//*******************************************
//*******************************************
//*******************************************

//***********************
//*      MAIN LOOP      *
//***********************
begin
  checkparameters();
end.
//***********************
//*    MAIN LOOP END    *
//***********************

//*******************************************
//*******************************************
//*******************************************
