;BONGO RECURSIVE DE-CRUNCHER - WEGI 2013.01.01
;SHORT VERSION	



RECURSIVE = 2 ; <> 1 no recursive decruncher (23 bytes shorted)
GOLDEN_SEQ_USED = 1 ; <> 1 without golden seq decruncher (47 bytes shorted + optional 48 bytes for golden seq table)


;---
LENGTH		= $02 
STREAM_BYTE	= LENGTH +1
PUT			= LENGTH+2
COPY_SEQ		= LENGTH+4
;---

;=========================
		*= $0900 ;DECRUNCHER START

DECRUNCH	
	; A #<START_DATA ; Y #>START_DATA ; X #OFFSET TO DATA
	;---------------------------------------------
	; FIRST ENTRY POINT TO DECRUNCHER
	; IN ACC LO BYTE DATA ADDRESS IN Y HI BYTE
	; X REGISTER MUST BE 0
	; OR IF YOU DO PARTIAL DECRUNCH X MUST BE
	; HAVE OFFSET TO DATA FOR EXAMPLE
	; IF YOU USE 256 BYTES BUFFER BLOCK FOR SECTOR
	; X SHOULD BE #4 FOR FIRST BLOCK
	; AND #2 FOR NEXT OTHER
	;---------------------------------------------
	
;IF PARTIAL DECRUNCH REMOVE SIGN ";" BELLOW
;		STX DEP_OFFS	;INDICATOR 0 = STREAM CRUNCH <> 0 PARTIAL CRUNCH
			
		STA MGET1HI-1	;LO BYTE START DATA
		STY MGET1HI	;HI BYTE
		
		LDY #$00
		STY STREAM_BYTE
		JSR GET_DATA_BYTE
.IF RECURSIVE = 1
		STA ITERATOR 	;IS RECURSIVE DECRUNCH?
.ENDIF
	;----------------------------
	; BELLOW IF YOU MAKE DECRUNCH
	; INTO YOUR AREA BEFORE YOU
	; MUST SET THE PUT VECTOR
	; AND GET2 VECTOR IF DATA
	; ARE RECURSIVE
	;----------------------------

		CMP #$80	
.IF GOLDEN_SEQ_USED = 1	
		PHP ;RAW DATA? - MAKE ACTIVE IF TRUE
		
		AND #$3F 
		STA LENGTH
		BNE +
	-
		JSR GET_DATA_BYTE
		STA SEQUENCES-1,Y
	+	INY
		CPY LENGTH
		BCC -
		
	;----------------------------------
	; IF YOU USE RAW DATA 2 LINES BELOW
	; MUST BE ACTIVE AND BEFORE PHP
	;----------------------------------
		PLP
.ENDIF
		BCC +
		
		JSR GET_DATA_BYTE
		STA PUT
.IF RECURSIVE = 1
		STA GET2LO
.ENDIF
		JSR GET_DATA_BYTE
		STA PUT+1
.IF RECURSIVE = 1
		STA GET2HI		
.ENDIF	
	+		
;**********************
;* MAIN DECRUNCH LOOP *
;**********************			
DECRUNCH_LOOP	
		JSR GET_STREAM_BIT


		BCC IS_UNCRUNCH
		LDY #$00
		STY COPY_SEQ+1
		TYA
		JSR GET_STREAM_BIT
		ROL
		BNE COPY_ONLY_1
		JSR GET3_OR_6BITS
;---
COPY_ONLY_1
		STA LENGTH
		CMP #$00
		BEQ CHECK_PAGE
;=================		
LOOP_COPY	;Y = #$00	
		
		JSR GET_DATA_BYTE
		STA (PUT),Y
		INC PUT
		BNE *+4
		INC PUT+1
		DEC LENGTH
		BNE LOOP_COPY
;---
CHECK_PAGE
		CPY COPY_SEQ+1
		DEC COPY_SEQ+1
		BCC LOOP_COPY		
;==============
IS_UNCRUNCH

		LDA #$02 	;2 BYTES SEQ?		

;SHORTBITS 3 FOR 2BYTES SEQ
		LDY #$03		
		JSR GET_STREAM_BIT

		BCC SHORT_C
		;LDA #3	;3 BYTES SEQ?
		TYA ; Y = 3
		JSR GET_STREAM_BIT ;IN ACC #3
		BCC START_UNCRUNCH ;SEQ 3 BYTES

		JSR GET_Y_BYTES 
		CMP #3
		BEQ EOF ; 3 BYTES = EOF OR LESS = GOLDEN SEQ
		BCS START_UNCRUNCH ;SEQ LONGER THAN 3 BYTES
		
;-----------------------------
;HERE SERVICE GOLDEN SEQUENCES
;-----------------------------
.IF GOLDEN_SEQ_USED = 1
CHECK_SEQ		
		LDY #4
		JSR GETMANYBITS ;GET4BITS
		
		;NR OF SEQ *3
		STA LENGTH
		ASL
		ADC LENGTH
		TAY
;===
		LDA SEQUENCES,Y
		STA LENGTH

		LDA SEQUENCES+2,Y ;OFFSET HI BYTE
		STA COPY_SEQ+1
		LDA SEQUENCES+1,Y ;OFFSET LO BYTE		
		LDY #$00
		BEQ SHORT_WAY
.ENDIF
;=====		
EOF
.IF RECURSIVE =1
ITERATOR = *+1
		LDA #$00
		ASL 		; CHECK BIT 6 IS RECURSIVE DECRUNCH
		BPL +
GET2LO = *+1
		LDA #$00
GET2HI = *+1
		LDY #$00
		LDX #$00
		JMP DECRUNCH
	+	
.ENDIF
		;CLC 		;FOR FUTURE USE
		RTS
;===================================================
;=============      UNCRUNCHING      ===============
;===================================================
START_UNCRUNCH	
		LDY #$04 	;4 BITS FOR >2 BYTES SEQ
SHORT_C	STA LENGTH	;IN ACC LENGTH OF SEQ
		JSR GET_Y_BYTES ;GET 3 OR 4 BITS
;***************************************************
SHORT_WAY
		;SUBSTRACT OFFSET
		STA COPY_SEQ  	
		SEC
		LDA PUT
		SBC COPY_SEQ
		STA COPY_SEQ
		LDA PUT+1
		SBC COPY_SEQ+1
		STA COPY_SEQ+1	
;---
UNCRUNCH		;Y = 0
		LDA (COPY_SEQ),Y
		STA (PUT),Y
		INY
		CPY LENGTH
		BNE UNCRUNCH
		TYA
		CLC
		ADC PUT
		STA PUT
		BCC *+4
		INC PUT+1
		JMP DECRUNCH_LOOP
;---
;==========================
GET_STREAM_BIT
		ASL STREAM_BYTE
		BEQ GET_STREAM_BYTE
		RTS
;---
GET_STREAM_BYTE		
		PHA	
		JSR GET_DATA_BYTE
		SEC
		ROL
		STA STREAM_BYTE
		PLA
		RTS
;---
GET_DATA_BYTE
MGET1HI = *+2
		LDA $1111,X		
		INX
		BNE +
TRY_REF

;------
	
	;---------------------------------------
	; HERE IS POINT FOR LOADER -  
	; STORE REGISTERS AND STATUS
	; LOAD NEXT DATA
	; RECALL REGISTERS AND STATUS
	; MODIFFY X REG FOR OFFSET TO FIRST BYTE
	; OF A DATA BLOCK 
	; (PROBABLY X SHOULD BE #2) 
	; AND 2 VECTORS DATA BLOCKS MGET1, MGET2
	; YOU SHOULD BE SET
	; TO ADDRESS OF PART DATA - 2
	;---------------------------------------
	
;------
;IF PARTIAL DECRUNCH REMOVE SIGN ";" BELLOW
;DEP_OFFS = *+1
;		LDX #$00
;		BEQ BIG_INCR
;		PHP	;SAVE CARRY
;		PHA
;		TYA
;		PHA
;		JSR REFILL_DATA	;YOUR LOADER POINT TO REFILL DATA
;		PLA
;		TAY
;		PLA
;		PLP	;RESTORE CARRY
;		RTS
;---
BIG_INCR
		INC MGET1HI

;============
	+	;SEC ; FOR FUTURE USE
		RTS
;============

;---
GET3MANYBITS LDY #3 ; 3 BITS GET
GETMANYBITS	; IN Y REG HOW MANY BITS TO GET
		LDA #$00
		BEQ GETCRUNCHBYTES
;---
LOWER	LDA #$01
		RTS
;===
GET3_OR_6BITS
		JSR GET3MANYBITS
		CMP #7
		BNE GETBYTESTOCOPY
GETNEXT3BITS
		JSR GET3MANYBITS ;IN ACC #BITS - 7 , C=0
		ADC #7
		BNE GETBYTESTOCOPY
GET_Y_BYTES
		JSR GETMANYBITS		
;---
GETBYTESTOCOPY
		LDY #$00
		STY COPY_SEQ+1
		TAY
		BEQ LOWER

		LDA #$01		
GETCRUNCHBYTES
	-	
		JSR GET_STREAM_BIT
		ROL
		ROL COPY_SEQ+1
		DEY
		BNE -
		RTS
;---
;=======
.IF GOLDEN_SEQ_USED = 1
SEQUENCES = *
.ENDIF
;---------------------------------------------
; 48 BYTES MAX FOR MY LOVELLY GOLDEN SEQUENCES
; OF COURSE - YOU CAN CHANGE SEQ ADDRESS
;---------------------------------------------		
