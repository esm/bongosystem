;BONGO SFX RECURSIVE DE-CRUNCHER - WEGI 2013.01.01

;---
LENGTH		= $6A
STREAM_BYTE	= LENGTH +1
PUT			= LENGTH+2
COPY_SEQ		= LENGTH+4
;---

;=========================
		*= $0801

;BASICLINE	0 SYS 2061
		.BYTE $0B,$08,$00,$00,$9E,$32
		.BYTE $30,$36,$31,$00,$00,$00
;======================================
		LDA #<TXBLOCK
		LDY #>TXBLOCK
		JSR $AB1E
;--------
		SEI		
		LDA #$38
		STA $01
;-----	
		
		LDX #0
		
		LDA #<START_DATA
		LDY #>START_DATA 		

;============================================================
DECRUNCH	
			
		STA MGET1HI-1	;LO BYTE START DATA
		STY MGET1HI	;HI BYTE
		
		LDY #$00
		STY STREAM_BYTE
		
		JSR GET_DATA_BYTE

		AND #$3F 
		STA LENGTH
		BNE +
	-
		JSR GET_DATA_BYTE
		STA SEQUENCES-1,Y
	+	INY
		CPY LENGTH
		BCC -
		
		JSR GET_DATA_BYTE
		STA PUT

		JSR GET_DATA_BYTE
		STA PUT+1

	+		
;**********************
;* MAIN DECRUNCH LOOP *
;**********************			
DECRUNCH_LOOP	
		JSR GET_STREAM_BIT


		BCC IS_UNCRUNCH
		LDY #$00
		STY COPY_SEQ+1
		TYA
		JSR GET_STREAM_BIT
		ROL
		BNE COPY_ONLY_1
		JSR GET3_OR_6BITS
;---
COPY_ONLY_1
		STA LENGTH
		CMP #$00
		BEQ CHECK_PAGE
;=================		
LOOP_COPY	;Y = #$00	
		
		JSR GET_DATA_BYTE
		STA (PUT),Y
		INC PUT
		BNE *+5
		JSR DECIM
		DEC LENGTH
		BNE LOOP_COPY
;---
CHECK_PAGE
		CPY COPY_SEQ+1
		DEC COPY_SEQ+1
		BCC LOOP_COPY		
;==============
IS_UNCRUNCH

		LDA #$02 	;2 BYTES SEQ?		

;SHORTBITS 3 FOR 2BYTES SEQ
		LDY #$03		
		JSR GET_STREAM_BIT

		BCC SHORT_C
		;LDA #3	;3 BYTES SEQ?
		TYA ; Y = 3
		JSR GET_STREAM_BIT ;IN ACC #3
		BCC START_UNCRUNCH ;SEQ 3 BYTES

		JSR GET_Y_BYTES 
		CMP #3
		BEQ EOF ; 3 BYTES = EOF OR LESS = GOLDEN SEQ
		BCS START_UNCRUNCH ;SEQ LONGER THAN 3 BYTES
		
;-----------------------------
;HERE SERVICE GOLDEN SEQUENCES
;-----------------------------
CHECK_SEQ		
		LDY #4
		JSR GETMANYBITS ;GET4BITS
		
		;NR OF SEQ *3
		STA LENGTH
		ASL
		ADC LENGTH
		TAY
;===
		LDA SEQUENCES,Y
		STA LENGTH

		LDA SEQUENCES+2,Y ;OFFSET HI BYTE
		STA COPY_SEQ+1
		LDA SEQUENCES+1,Y ;OFFSET LO BYTE		
		LDY #$00
		BEQ SHORT_WAY
;=====		
EOF
		LDA #$37
		STA $01
		JMP $9000
;===================================================
;=============      UNCRUNCHING      ===============
;===================================================
START_UNCRUNCH	
		LDY #$04 	;4 BITS FOR >2 BYTES SEQ
SHORT_C	STA LENGTH	;IN ACC LENGTH OF SEQ
		JSR GET_Y_BYTES ;GET 3 OR 4 BITS
;***************************************************
SHORT_WAY
		;SUBSTRACT OFFSET
		STA COPY_SEQ  	
		SEC
		LDA PUT
		SBC COPY_SEQ
		STA COPY_SEQ
		LDA PUT+1
		SBC COPY_SEQ+1
		STA COPY_SEQ+1	
;---
UNCRUNCH		;Y = 0
		LDA (COPY_SEQ),Y
		STA (PUT),Y
		INY
		CPY LENGTH
		BNE UNCRUNCH
		TYA
		CLC
		ADC PUT
		STA PUT
		BCC *+5
		JSR DECIM
		JMP DECRUNCH_LOOP
;---
;===================================================
;==========================
GET_STREAM_BIT
		ASL STREAM_BYTE
		BEQ GET_STREAM_BYTE
		RTS
;---
GET_STREAM_BYTE		
		PHA	
		JSR GET_DATA_BYTE
		SEC
		ROL
		STA STREAM_BYTE
		PLA
		RTS
;---
GET_DATA_BYTE
MGET1HI = *+2
		LDA $1111,X		
		INX
		BNE +
BIG_INCR
		INC MGET1HI
;============
	+	;SEC ; FOR FUTURE USE
		RTS
;============
;---
GET3MANYBITS LDY #3 ; 3 BITS GET
GETMANYBITS	; IN Y REG HOW MANY BITS TO GET
		LDA #$00
		BEQ GETCRUNCHBYTES
;---
LOWER	LDA #$01
		RTS
;===
GET3_OR_6BITS
		JSR GET3MANYBITS
		CMP #7
		BNE GETBYTESTOCOPY
GETNEXT3BITS
		JSR GET3MANYBITS ;IN ACC #BITS - 7 , C=0
		ADC #7
		BNE GETBYTESTOCOPY
GET_Y_BYTES
		JSR GETMANYBITS		
;---
GETBYTESTOCOPY
		LDY #$00
		STY COPY_SEQ+1
		TAY
		BEQ LOWER

		LDA #$01		
GETCRUNCHBYTES
	-	
		JSR GET_STREAM_BIT
		ROL
		ROL COPY_SEQ+1
		DEY
		BNE -
		RTS
;---
_DECRUNCHER_LENGTH = * -DECRUNCH
;====
DECIM
		INC PUT+1
		LDY #2	
	-	LDA ($D1),Y
		SEC
		SBC #$01
		STA ($D1),Y
		CMP #$30
		BCS +
		LDA #$39
		STA ($D1),Y
		DEY
		BPL -
	+	LDY #$00
		RTS
;=======
;---
TXBLOCK
.BYTE 13
PARAM5 ;3BYTES COUNT OF BLOCK
.TEXT "063"
.BYTE 0
SEQUENCES = $0340

;---------------------------------------------
; 48 BYTES MAX FOR MY LOVELLY GOLDEN SEQUENCES
; OF COURSE - YOU CAN CHANGE SEQ ADDRESS
;---------------------------------------------		
START_DATA = *
;.BINARY "YOU_CRUNCHED_DATA_NAME",2 ;WITHOUT LOAD ADDRESS