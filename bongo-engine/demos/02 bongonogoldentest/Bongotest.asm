

;---
SCREEN = $2000

LENGTH		= $02
STREAM_BYTE	= LENGTH +1
PUT			= LENGTH+2
COPY_SEQ		= LENGTH+4
;---
SRC = PUT-2
SRCH = PUT-1
DEST = PUT
DESTH = PUT+1

IRQCNTR = LENGTH+6
IRQCNTR2 = LENGTH+8

VEC1 = LENGTH+10
MAINPLOT = LENGTH+12
VEC2 = LENGTH+14
;---
PROC2 = $1B00
PROC1 = $2000
;---
;-------------------------------------------
!TO "Bongotest.prg" , CBM
;========

          *= $0801
;===============================================
;= TEST OF SPEED DEPACK & LOAD BY WEGI CONCEPT =
;= (C) BY WEGI IN 2011.09.25                   =
;===============================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------
START
          CLD
          jsr SETTBADR
          JMP NO_WAIT
;================
IRQ
          PHA
          TXA
          PHA
          TYA
          PHA

IRQ33     LDA $01
          PHA
          LDA #$35
          STA $01
MSXONOFF    
          JSR $1003

          INC IRQCNTR
          BNE +
          INC IRQCNTR+1
+:
          JSR CHECK_DECRUNCH
          JSR MOV_SPRITE
          BIT $DC0D
          PLA
          STA $01

          PLA
          TAY
          PLA
          TAX
          PLA
NMIEX
          RTI
;--------
;*************************

MYLOOP
          LDA $DC01
          CMP $DC01
          BEQ *-3
NO_WAIT
          dec $01
          JSR LOADPART
          INC $01
;------------   
          JMP NO_WAIT
          JMP MYLOOP
;=============================================
LOADPART
;now only shortstream loader reside
;-- load and fly decrunch first pic (load under $5000 decrunch into depack addr. $2000)
          LDA #<VECADR
          LDY #>VECADR
          JSR PROC2
          BCS *
;-- load on the screen second crunched data of pic 
          LDA #<VECADR2
          LDY #>VECADR2
          JSR PROC2+3
          BCS *
          LDA VECADR2     ; decrunch after load
          LDY VECADR2+1
          JSR PROC2+6
;-- load only exchange stub of deterministic loader into $5000
          LDA #<VECADR4
          LDY #>VECADR4
          JSR PROC2+3
;-- load only stub of deterministic loader C64side int $5b00
          LDA #<VECADR5
          LDY #>VECADR5
          JSR PROC2+3  
          
          JSR RECOPY_DATA  ;copy stub of loader into target area $1b00
;-- load and fly decrunch third pic all on the screen
          LDA #<LENFILE3
          LDY #>LENFILE3
          JSR PROC2
          BCS *
;--  load and NON-fly decrunch fourth pic all on the screen
          LDA #<LENFILE1
          LDY #>LENFILE1
          JSR PROC2+3
          BCS *	
          LDA #<LDADR_FILE1
          LDY #>LDADR_FILE1
          JSR PROC2+6
;---
          LDA #$01 ;LOADER AND DECRUNCHTYPE INDICATOR
          JSR START_MEASSURE 

          LDA #<LENFILE6 ;load demodata
          LDY #>LENFILE6
          JSR PROC2+3
          BCS *	
          LDA #<LDADR_FILE6 ;decrunch demodata
          LDY #>LDADR_FILE6
          JSR PROC2+6
          
          JSR STOP_MEASSURE

          
;--  load and NON-fly decrunch fourth pic all on the screen
          LDA #<LENFILE1
          LDY #>LENFILE1
          JSR PROC2+3
          BCS *	
          LDA #<LDADR_FILE1
          LDY #>LDADR_FILE1
          JSR PROC2+6
;---

          LDA #$00 ;LOADER AND DECRUNCHTYPE INDICATOR
          JSR START_MEASSURE

          
          LDA #<LENFILE6 ;load and fly decrunch demodata
          LDY #>LENFILE6
          JSR PROC2
          BCS *	
          
          JSR STOP_MEASSURE


;--- load only exchange stub of nointerleave loader into $5000          
          LDA #<LENFILE7 ; 
          LDY #>LENFILE7
          JSR PROC2+3
          BCS *	
;---  load only stub of nointerleave loader C64side int $5b00
          LDA #<LENFILE8
          LDY #>LENFILE8
          JSR PROC2+3
          BCS *	  
          
          JSR RECOPY_DATA ;copy stub of loader into target area $1b00
;--  load and NON-fly decrunch fourth pic all on the screen
          LDA #<VECADR1
          LDY #>VECADR1
          JSR PROC2+3
          BCS *	
          LDA #<LDADR_FILE1
          LDY #>LDADR_FILE1
          JSR PROC2+6
;---	
          LDA #$02 ;LOADER AND DECRUNCHTYPE INDICATOR
          JSR START_MEASSURE 

          
          LDA #<VECADR6  ;load demodata without decrunch
          LDY #>VECADR6
          JSR PROC2+3
          BCS *	
          LDA #<LDADR_FILE6 ;decrunch demodata
          LDY #>LDADR_FILE6
          JSR PROC2+6
          
          JSR STOP_MEASSURE
;--  load and NON-fly decrunch fourth pic all on the screen
          LDA #<VECADR1
          LDY #>VECADR1
          JSR PROC2+3
          BCS *	
          LDA #<LDADR_FILE1
          LDY #>LDADR_FILE1
          JSR PROC2+6
;---	
          LDA #$03 ;LOADER AND DECRUNCHTYPE INDICATOR
          JSR START_MEASSURE          
          
          LDA #<VECADR6 ;;load and fly decrunch demodata by nointerleave loader
          LDY #>VECADR6
          JSR PROC2
          BCS *	

          JSR STOP_MEASSURE

; now load stubs of fast stream loader
          LDA #<VECADR11
          LDY #>VECADR11
          JSR PROC2+3
          BCS *	
          
          LDA #<VECADR12
          LDY #>VECADR12
          JSR PROC2+3
          BCS *	  
          
          JSR RECOPY_DATA ;copy stub into target area

          LDA #<VECADR1
          LDY #>VECADR1

          JSR PROC2+3
          BCS *          
          
;          LDA #<LDADR_FILE1 ;decrunch demodata
;          LDY #>LDADR_FILE1
          LDA VECADR1     ; decrunch after load
          LDY VECADR1+1
          JSR PROC2+6         
          
          LDA #$04 ;LOADER AND DECRUNCHTYPE INDICATOR          
          JSR START_MEASSURE

          
          LDA #<VECADR6 ;load and fly decrunch demodata by fast stream loader
          LDY #>VECADR6
          JSR PROC2
          BCS *	
          
          JSR STOP_MEASSURE
;--- load stubs of fast stream loader
          LDA #<VECADR11  ;load 
          LDY #>VECADR11
          JSR PROC2+3
          BCS *	
          
          LDA #<VECADR12
          LDY #>VECADR12
          JSR PROC2+3
          BCS * 

          JSR RECOPY_DATA ;copy stub into target area
          
          
          RTS
;---
FLYDEC_P
          LDA #10 ;COLUMN
          JSR CALC_POZX
          LDA #4 ;ROWS
          JSR CALC_POZY
          LDA #<FLYDEC_T
          STA TX_VEC          
          LDA #>FLYDEC_T
          STA TX_VEC+1
          JMP INFOPRINT          
;---  
NOFLYDEC_P
          LDA #10 ;COLUMN
          JSR CALC_POZX
          LDA #4 ;ROWS
          JSR CALC_POZY
          LDA #<NOFLYDEC_T
          STA TX_VEC          
          LDA #>NOFLYDEC_T
          STA TX_VEC+1
          JMP INFOPRINT 
;---
B_DET_P
          JSR FST_LINE_CALC
          LDA #<B_DET_T
          STA TX_VEC          
          LDA #>B_DET_T
          STA TX_VEC+1
          JMP INFOPRINT
;---
B_NOI_P
          JSR FST_LINE_CALC
          LDA #<B_NOI_T
          STA TX_VEC          
          LDA #>B_NOI_T
          STA TX_VEC+1
          JMP INFOPRINT
;---
B_FST_P
          JSR FST_LINE_CALC
          LDA #<B_FST_T
          STA TX_VEC          
          LDA #>B_FST_T
          STA TX_VEC+1
          JMP INFOPRINT
FST_LINE_CALC
          LDA #10 ;COLUMN
          JSR CALC_POZX
          LDA #0 ;ROWS
          JMP CALC_POZY
;---
INFO1_P

          LDA #<IRQ33
          STA $0314
          LDA #>IRQ33
          STA $0315
          
          LDA #10 ;COLUMN
          JSR CALC_POZX
          LDA #6 ;ROWS
          JSR CALC_POZY
          LDA #<INFO1_T
          STA TX_VEC          
          LDA #>INFO1_T
          STA TX_VEC+1
          JSR INFOPRINT
          
          LDA #10 ;COLUMN
          JSR CALC_POZX
          LDA #8 ;ROWS
          JSR CALC_POZY
          LDA #<INFO2_T
          STA TX_VEC          
          LDA #>INFO2_T
          STA TX_VEC+1
          JSR INFOPRINT  
          
          LDA #10 ;COLUMN
          JSR CALC_POZX
          LDA #2 ;ROWS
          JSR CALC_POZY
          LDA #<INFO3_T
          STA TX_VEC          
          LDA #>INFO3_T
          STA TX_VEC+1
          JSR INFOPRINT  
          
          LDA #10 ;COLUMN
          JSR CALC_POZX
          LDA #10 ;ROWS
          JSR CALC_POZY
          LDA #<INFO4_T
          STA TX_VEC          
          LDA #>INFO4_T
          STA TX_VEC+1 

INFOPRINT
          LDY #$00

TX_VEC = *+1
-:        LDA INFO1_T,Y
          AND #$3F
          TAX
          BNE +
          RTS
+:        TYA
          PHA

          TXA
          JSR FONT_PRINT
          LDA POZX
          CLC
          ADC #8
          STA POZX
          BCC +
          INC POZX+1
+:        PLA
          TAY
          INY
          BNE -
          RTS
;============
INFO1_T
          !TEXT "CRUNCHED FILE : 28613 BYTES"
          !BYTE 0
INFO2_T
          !TEXT "FILE LENGTH : 47107 BYTES"
          !BYTE 0
INFO3_T
          !TEXT "WITH BONGO-NOGOLDEN DECRUNCHER"
          !BYTE 0
INFO4_T
          !TEXT "PRESS SPACE TO HOLD TEST..."
          !BYTE 0          
FLYDEC_T
          !TEXT "FLY DECRUNCHING TEST /STOP=END"
          !BYTE 0
NOFLYDEC_T
          !TEXT "NON-FLY DECR. TEST (STOP=END)"
          !BYTE 0
B_DET_T
          !TEXT "BONGO DETERMINISTIC LOADER"
          !BYTE 0
B_NOI_T
          !TEXT "BONGO NOINTERLEAVE LOADER"
          !BYTE 0
B_FST_T
          !TEXT "BONGO FAST STREAM LOADER INT. #10"
          !BYTE 0
;---
FONT_PRINT
          LDY #$00
          STY FVEC1+1
          
          ASL
          ROL FVEC1+1
          ASL
          ROL FVEC1+1
          ASL
          ROL FVEC1+1
          STA FVEC1
          LDA #$D0
          CLC
          ADC FVEC1+1
          STA FVEC1+1
          
         LDA POZY
         LSR
         LSR
         LSR
         TAX

         LDA POZX
         AND #$F8
         CLC
         ADC TBADLO,X
         STA SVEC

         LDA TBADHI,X
         ADC POZX+1
         STA SVEC+1
          
          LDY #7

          LDA $01
          PHA
          LDA #$33
          STA $01
-:
FVEC1 = *+1
          LDA $1111,Y
SVEC = *+1
          STA $1111,Y
          DEY
          BPL -
          PLA
          STA $01

          
          RTS
;---
CALC_POZY
          ASL
          ASL
          ASL
          STA POZY
          RTS
;---
CALC_POZX
          LDX #0
          STX POZX+1
          LDX #3
-:        ASL
          ROL POZX+1
          DEX
          BNE -
          STA POZX
          RTS          
;---
JIFFI

          LDA CLOCK+3
          CLC
          ADC #2
          STA CLOCK+3
          CMP #$3A
          BCS *+3
          RTS
          LDA #$30
          STA CLOCK+3
          
          LDX #$02
          
-:        TXA
          PHA
          INC CLOCK,X
          LDA CLOCK,X
          CMP #$3A
          BCC +
          LDA #$30
          STA CLOCK,X
+:        JSR UPDATE_CLOCK
          
          PLA
          TAX
          LDA CLOCK,X
          CMP #$30
          BEQ +
          RTS
+:        DEX
          BPL -
          RTS
;---
UPDATE_CLOCK

          LDY #192
          STY POZY
          LDY XCLOCK,X
          STY POZX
          

          
          LDY #$00
          STY POZX+1                   
          STY VEC1+1
          
          ASL
          ROL VEC1+1
          ASL
          ROL VEC1+1
          ASL
          ROL VEC1+1
          STA VEC1
          LDA #$D0
          CLC
          ADC VEC1+1
          STA VEC1+1
          
          LDA POZY
          LSR
          LSR
          LSR
          TAX          

          LDA POZX
          AND #$F8
          CLC
          ADC TBADLO,X
          STA MAINPLOT

          LDA TBADHI,X
          ADC POZX+1
          STA MAINPLOT+1

          LDY #7
          LDA $01
          PHA
          
          LDA #$33
          STA $01
          
-:        LDA (VEC1),Y
          STA (MAINPLOT),Y
          DEY
          BPL -
          PLA
          STA $01
          RTS
XCLOCK    !BYTE 0,8,24,16
;---
STOP_MEASSURE
          LDA #$60
          STA CHECK_DECRUNCH ; DISABLE MEASSURE, PROGRESS BARS...
          STA PROC2+$82 ; HI BYTE POINTER TO PUT LOADED DATA
          LDA $01
          PHA
          LDA #$35
          STA $01
          LDA $DC01
          CMP #$7F
          BNE +
          cmp $dc01
          beq *-3
          JSR PROC2+9
          lda #$08
          sta $de00
          lda #$37
          sta $01
          JMP ($fffc)
+:
          
          LDA #$EF
          CMP $DC01
          BEQ *-3          
          PLA
          STA $01
          RTS
START_MEASSURE
          PHA ;LOADER AND DECRUNCHTYPE INDICATOR
          JSR STOP_MEASSURE
          
  
          
          JSR PRINT_FRAMES

          JSR INFO1_P
          PLA ;LOADER AND DECRUNCHTYPE INDICATOR
          BNE +
          JSR B_DET_P
          JSR FLYDEC_P
          JMP PREP_COUNT
+:        CMP #1
          BNE +
          JSR B_DET_P
          JSR NOFLYDEC_P
          JMP PREP_COUNT
+:        CMP #2
          BNE +
          JSR B_NOI_P
          JSR NOFLYDEC_P
          JMP PREP_COUNT
+:        CMP #3
          BNE +
          JSR B_NOI_P
          JSR FLYDEC_P
          JMP PREP_COUNT
+:        JSR B_FST_P
          JSR FLYDEC_P
          
PREP_COUNT
          LDA #'.'
          LDX #$03
          JSR UPDATE_CLOCK
          LDX #$03
          LDA CLOCK_STUB,X
          STA CLOCK,X
          DEX
          BPL *-7
          
          LDA #$47       ;setup for meassure time 
          STA LASTPUT
          LDA #$00
          STA PUT+1
          STA PROC2+$82
          LDA #$EA
          STA CHECK_DECRUNCH
          

          RTS
;---
BCK       JMP CLEAR_BOUND

CHECK_DECRUNCH
          
          RTS
          
          JSR JIFFI
          LDA PROC2+$82
          CMP #>LDADR_FILE6
          BCC +
          
          SBC #>LDADR_FILE6
          ADC #7
          STA POZY
          LDA #$24
          STA POZX
          JSR DRAW1
          
+:        LDA PUT+1
          BEQ BCK
          CMP #$47
          BCC BCK
          
+:
          SEC
          SBC LASTPUT
          STA PROGRESS
          
          LDA PUT+1
          STA LASTPUT
          SEC
          SBC #$47-8

          STA POZY
          LDA #64
          STA POZX
          
-:          
          LDA POZY
          CMP #200
          BCS BCK
+:
          JSR DRAW1
          DEC POZY
          DEC PROGRESS
          BPL -
          BMI BCK
;---
DRAW1
         LDA POZY
         LSR
         LSR
         LSR
         TAX

         LDA POZY
         AND #$07
         TAY

         LDA POZX
         AND #$F8
         CLC
         ADC TBADLO,X
         STA STOREPLOT

         LDA TBADHI,X
         ADC #$00
         STA STOREPLOT+1

         LDA #$FF
STOREPLOT = *+1
         STA $1111,Y
         RTS
;---
PLOT


         LDA POZY
         LSR
         LSR
         LSR
         TAX

         LDA POZY
         AND #$07
         TAY

         LDA POZX
         AND #$F8
         CLC
         ADC TBADLO,X
         STA MAINPLOT

         LDA TBADHI,X
         ADC POZX+1
         STA MAINPLOT+1

         LDA POZX
         AND #$07
         TAX

         LDA (MAINPLOT),Y
         ORA TBBIT,X
         STA (MAINPLOT),Y
         RTS
;========
POZY !BYTE 0
POZX !WORD 0
LASTPUT !BYTE 0
PROGRESS  !BYTE 0
;--------
TBADLO
         !BYTE 0,0,0,0,0
         !BYTE 0,0,0,0,0
         !BYTE 0,0,0,0,0
         !BYTE 0,0,0,0,0
         !BYTE 0,0,0,0,0
;---
TBADHI
         !BYTE 0,0,0,0,0
         !BYTE 0,0,0,0,0
         !BYTE 0,0,0,0,0
         !BYTE 0,0,0,0,0
         !BYTE 0,0,0,0,0
TBBIT
         !BYTE %10000000
         !BYTE %01000000
         !BYTE %00100000
         !BYTE %00010000
         !BYTE %00001000
         !BYTE %00000100
         !BYTE %00000010
         !BYTE %00000001         
;--------
CLOCK          !BYTE 0,0,0,0
CLOCK_STUB     !TEXT "9998"
RECOPY_DATA
          LDY #$00
          
          STY PUT
          STY SRC
          LDA #$5B
          STA SRCH
          LDA #$1B
          STA PUT+1
          LDX #5
          
-:        LDA (SRC),Y
          STA (PUT),Y
          INY
          BNE -
          INC SRCH
          INC PUT+1
          DEX
          BNE -
          INC $01
          JSR $5000 ;execute fast stream loader
          DEC $01  
          RTS
;---
CLEAR_BOUND
          LDA #$00

BND1 = *+1
          STA $033C
BND2 = *+1
          STA $033C
          RTS

;--------
CLRSCR
          LDA #<SCREEN
          STA VEC1
          LDA #>SCREEN
          STA VEC1+1
          
          LDX #$1F
          LDA #$00
          TAY
-:
          STA (VEC1),Y
          INY
          BNE -
          INC VEC1+1
          DEX
          BNE -
          
          LDY #$3F
          STA (VEC1),Y
          DEY
          BPL *-3

          RTS
;---
PRINT_FRAMES
          LDA #$06
          STA POZY
          
          LDA #$00
          STA POZX+1
          LDA #29
          STA POZX
          
-:        JSR PLOT
          DEC POZY
          JSR PLOT
          INC POZY
          INC POZX
          LDA POZX
          CMP #43
          BCC -
          
          LDA #61
          STA POZX
          
-:        JSR PLOT
          DEC POZY
          JSR PLOT
          INC POZY
          INC POZX
          LDA POZX
          CMP #75
          BCC -  
          
          LDA #>LDADR_FILE6-1
          STA PUT
                    
-:
          LDA #30
          STA POZX
          JSR PLOT
          DEC POZX
          JSR PLOT
          LDA #41
          STA POZX
          JSR PLOT
          INC POZX
          JSR PLOT
          INC POZY
          INC PUT
          BNE -
          
          LDA #29
-:        STA POZX
          JSR PLOT
          INC POZY
          JSR PLOT
          DEC POZY
          INC POZX
          LDA POZX
          CMP #43
          BCC -

          LDA #33
          STA POZX
          DEC POZY
          JSR PLOT
          TYA
          CLC
          ADC MAINPLOT
          STA BND1
          LDA MAINPLOT+1
          ADC #$00
          STA BND1+1

          LDA #$46
          STA PUT
          
          LDA #6
          STA POZY
-:
          LDA #62
          STA POZX
          JSR PLOT
          DEC POZX
          JSR PLOT
          LDA #73
          STA POZX
          JSR PLOT
          INC POZX
          JSR PLOT          
          INC POZY
          INC PUT
          BNE -
          
          LDA #61
-:        STA POZX
          JSR PLOT
          INC POZY
          JSR PLOT
          DEC POZY
          INC POZX
          LDA POZX
          CMP #75
          BCC -

          LDA #65
          STA POZX
          DEC POZY
          JSR PLOT
          TYA
          CLC
          ADC MAINPLOT
          STA BND2
          LDA MAINPLOT+1
          ADC #$00
          STA BND2+1
          JSR CLEAR_BOUND
          RTS
MOV_SPRITE

		inc ring_pos
		lda ring_pos
		ldx #0
		
ring_lp
		tay
		
		lda ring_data,y
		sta $d000,x
		inx
		lda ring_data+$0100,y
		sta $d000,x
		tya
		clc
		adc #$20
		
		inx
		cpx #$10
		bcc ring_lp
		rts
;---------
VECADR        !word LDADR_FILE0
STRTTRK       !byte STARTTRCK_FILE0
STRTSEC       !byte STARTSECT_FILE0
LDADFF        !byte 0 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
LENFILE1      !byte LENBLK_FILE1
VECADR1       !word LDADR_FILE1
STRTTRK1      !byte STARTTRCK_FILE1
STRTSEC1      !byte STARTSECT_FILE1
LDADFF1       !byte 0 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
VECADR2       !word LDADR_FILE2
STRTTRK2      !byte STARTTRCK_FILE2
STRTSEC2      !byte STARTSECT_FILE2
LDADFF2       !byte 0 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
LENFILE3      !byte LENBLK_FILE3
VECADR3       !word LDADR_FILE3
STRTTRK3      !byte STARTTRCK_FILE3
STRTSEC3      !byte STARTSECT_FILE3
LDADFF3       !byte 0 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
VECADR4       !word LDADR_FILE4
STRTTRK4      !byte STARTTRCK_FILE4
STRTSEC4      !byte STARTSECT_FILE4
LDADFF4       !byte 0 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
VECADR5       !word $5B00
STRTTRK5      !byte STARTTRCK_FILE5
STRTSEC5      !byte STARTSECT_FILE5
LDADFF5       !byte 1 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
LENFILE6      !byte LENBLK_FILE6
VECADR6       !word LDADR_FILE6
STRTTRK6      !byte STARTTRCK_FILE6
STRTSEC6      !byte STARTSECT_FILE6
LDADFF6       !byte 0 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
LENFILE7      !byte LENBLK_FILE7
VECADR7       !word LDADR_FILE7
STRTTRK7      !byte STARTTRCK_FILE7
STRTSEC7      !byte STARTSECT_FILE7
LDADFF7       !byte 0 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
LENFILE8      !byte LENBLK_FILE8
VECADR8       !word $5B00
STRTTRK8      !byte STARTTRCK_FILE8
STRTSEC8      !byte STARTSECT_FILE8
LDADFF8       !byte 1 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
LENFILE9      !byte LENBLK_FILE9
VECADR9       !word LDADR_FILE9
STRTTRK9      !byte STARTTRCK_FILE9
STRTSEC9      !byte STARTSECT_FILE9
LDADFF9       !byte 0 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
LENFILE10      !byte LENBLK_FILE10
VECADR10       !word $5B00
STRTTRK10      !byte STARTTRCK_FILE10
STRTSEC10      !byte STARTSECT_FILE10
LDADFF10       !byte 1 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
LENFILE11      !byte LENBLK_FILE11
VECADR11       !word LDADR_FILE11
STRTTRK11      !byte STARTTRCK_FILE11
STRTSEC11      !byte STARTSECT_FILE11
LDADFF11       !byte 0 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---
LENFILE12      !byte LENBLK_FILE12
VECADR12       !word $5B00
STRTTRK12      !byte STARTTRCK_FILE12
STRTSEC12      !byte STARTSECT_FILE12
LDADFF12       !byte 1 ; IF = 0 THEN GET LOAD ADDRESS FROM FILE - IF  <> 0 THEN GET MY LOAD ADDRES FROM VECADR
;---

          !SOURCE "data\bongotest.inc"
;*************************
END_CODE
;====================================================================
          *= $1000
          !binary "data\MYMSX.PRG",,2 
END_MSX
;=====================================================================
          * = $1B00
          !binary "data\11ldr1B00.prg",,2   
;=======
          * = $2000
          !binary "data\11inst2000.prg",,2   
;====================================================================
          *= $4000
;---

SETTBADR
         LDX #$00
         LDA #>SCREEN
         STX $FB
         STA $FC
SETTB2
         LDA $FB
         STA TBADLO,X
         LDA $FC
         STA TBADHI,X

         LDA $FB
         CLC
         ADC #$40
         STA $FB

         LDA $FC
         ADC #$01
         STA $FC
         INX
         CPX #25
         BCC SETTB2
;=====

          
          JSR PROC1

          LDX #63
          LDA SPRITE,X
          STA $0340,X
          DEX
          BPL *-7
          
          LDA #$00
          STA $D010
          STA $D01B
          STA $D01D
          STA $D017
          
          
          LDA #$ff
          STA $D015
          ldx #7
          
setspr
          LDA #$0340/64
          STA $07F8,x
          lda #5
          sta $d027,x
		dex
		bpl setspr

          JSR CLRSCR
          LDA #$00
          TAX
          TAY
          JSR $1000         
INITIRQ
          LDA #<NMIEX
          STA $FFFA
          STA $0318
          LDA #>NMIEX
          STA $FFFB
          STA $0319
         
          LDA #<IRQ
          STA $FFFE
          LDA #>IRQ
          STA $FFFF
          sei
		lda #$01
		sta $dc04
		sta $dc05
		bit $d011
		bpl *-3
		bit $d011
		bmi *-3
		bit $d011
		bpl *-3
		bit $d011
		bmi *-3
				          
          LDA #$C7
          STA $DC04
          LDA #$4C
          STA $DC05
		bit $dc0d
		lda #$0b
		sta $d020
		cli

          LDA #$35
          STA $01

          LDA #$03
          STA $DD00
          LDA #$C8
          STA $D016
          LDA #$0B
          STA $D011
          LDA #$18
          STA $D018
          
          BIT $D011
          BMI *-3

          
          LDA #$fb
          LDX #$00
-:        STA $0400,X
          STA $0500,X
          STA $0600,X
          STA $06E8,X
          INX
          BNE -
          
          LDA #$3B
          STA $D011
;=====
         RTS
ring_pos brk
		
ring_data

!byte $da,$dc,$dd,$df,$df,$e0,$e2,$e3,$e3,$e5,$e5,$e6,$e6,$e6,$e8,$e9
!byte $e9,$eb,$eb,$eb,$ec,$ec,$ee,$ee,$ee,$ee,$ef,$ef,$ef,$ef,$ef,$ef
!byte $ef,$ef,$ef,$ef,$ef,$ef,$ef,$ef,$ef,$ee,$ee,$ee,$ee,$ec,$ec,$eb
!byte $eb,$eb,$e9,$e9,$e8,$e6,$e6,$e6,$e5,$e5,$e3,$e3,$e2,$e0,$df,$df
!byte $dd,$dc,$da,$da,$d9,$d7,$d7,$d6,$d4,$d3,$d1,$d0,$ce,$cd,$cb,$ca
!byte $c8,$c7,$c7,$c4,$c2,$c1,$bf,$be,$be,$bb,$bb,$b8,$b6,$b5,$b2,$b2
!byte $b0,$af,$ae,$ac,$ac,$a9,$a8,$a6,$a3,$a3,$a0,$a0,$9f,$9d,$9c,$9a
!byte $97,$97,$96,$94,$93,$91,$90,$8e,$8d,$8b,$8a,$88,$87,$87,$85,$84
!byte $84,$82,$81,$7f,$7f,$7e,$7c,$7b,$7b,$79,$79,$78,$78,$78,$76,$75
!byte $75,$73,$73,$73,$72,$72,$70,$70,$70,$70,$6f,$6f,$6f,$6f,$6f,$6f
!byte $6f,$6f,$6f,$6f,$6f,$6f,$6f,$6f,$6f,$70,$70,$70,$70,$72,$72,$73
!byte $73,$73,$75,$75,$76,$78,$78,$78,$79,$79,$7b,$7b,$7c,$7e,$7f,$7f
!byte $81,$82,$84,$84,$85,$87,$87,$88,$8a,$8b,$8d,$8e,$90,$91,$93,$94
!byte $96,$97,$97,$9a,$9c,$9d,$9f,$a0,$a0,$a3,$a3,$a6,$a8,$a9,$ac,$ac
!byte $ae,$af,$b0,$b2,$b2,$b5,$b6,$b8,$bb,$bb,$be,$be,$bf,$c1,$c2,$c4
!byte $c7,$c7,$c8,$ca,$cb,$cd,$ce,$d0,$d1,$d3,$d4,$d6,$d7,$d7,$d9,$da

!byte $68,$69,$6b,$6b,$6c,$6e,$6e,$6f,$71,$72,$74,$75,$77,$78,$7a,$7b
!byte $7d,$7e,$7e,$81,$83,$84,$86,$87,$87,$8a,$8a,$8d,$8f,$90,$93,$93
!byte $95,$96,$97,$99,$99,$9c,$9d,$9f,$a2,$a2,$a5,$a5,$a6,$a8,$a9,$ab
!byte $ae,$ae,$af,$b1,$b2,$b4,$b5,$b7,$b8,$ba,$bb,$bd,$be,$be,$c0,$c1
!byte $c1,$c3,$c4,$c6,$c6,$c7,$c9,$ca,$ca,$cc,$cc,$cd,$cd,$cd,$cf,$d0
!byte $d0,$d2,$d2,$d2,$d3,$d3,$d5,$d5,$d5,$d5,$d6,$d6,$d6,$d6,$d6,$d6
!byte $d6,$d6,$d6,$d6,$d6,$d6,$d6,$d6,$d6,$d5,$d5,$d5,$d5,$d3,$d3,$d2
!byte $d2,$d2,$d0,$d0,$cf,$cd,$cd,$cd,$cc,$cc,$ca,$ca,$c9,$c7,$c6,$c6
!byte $c4,$c3,$c1,$c1,$c0,$be,$be,$bd,$bb,$ba,$b8,$b7,$b5,$b4,$b2,$b1
!byte $af,$ae,$ae,$ab,$a9,$a8,$a6,$a5,$a5,$a2,$a2,$9f,$9d,$9c,$99,$99
!byte $97,$96,$95,$93,$93,$90,$8f,$8d,$8a,$8a,$87,$87,$86,$84,$83,$81
!byte $7e,$7e,$7d,$7b,$7a,$78,$77,$75,$74,$72,$71,$6f,$6e,$6e,$6c,$6b
!byte $6b,$69,$68,$66,$66,$65,$63,$62,$62,$60,$60,$5f,$5f,$5f,$5d,$5c
!byte $5c,$5a,$5a,$5a,$59,$59,$57,$57,$57,$57,$56,$56,$56,$56,$56,$56
!byte $56,$56,$56,$56,$56,$56,$56,$56,$56,$57,$57,$57,$57,$59,$59,$5a
!byte $5a,$5a,$5c,$5c,$5d,$5f,$5f,$5f,$60,$60,$62,$62,$63,$65,$66,$66

SPRITE         
!byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$03,$ff,$c0,$04
!byte $00,$20,$05,$ff,$a0,$05,$ff,$a0,$04,$00,$20,$04,$18,$20,$04,$24
!byte $20,$04,$24,$20,$04,$18,$20,$04,$00,$20,$04,$18,$20,$04,$18,$20
!byte $04,$18,$20,$03,$ff,$c0,$00,$00,$00,$00,$00,$00,$00,$00,$00,$05
;---