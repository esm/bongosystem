;SOME INFO FOR LOADER...  THIS DATA ADDED ALSO
;ON THE TRACK $12 SECT $02 IN ORDER:
;OFFSET 0 - STARTTRACK FILES
;OFFSET 50 - STARTSECT FILES
;OFFSET 100 - BLOCKS LENGTH FILES
;OFFSET 150 - LO BYTE LOAD ADRESS OF FILE
;OFFSET 200 - HI BYTE LOAD ADRESS OF FILE
;HOW YOU SEE - 50 FILES PER ONE SIDE FOR TRACKMO
;WOULD IT BE SAVED - THATS ENOUGHT I HOPE :)
;YOU CAN USE THIS DATA FROM THIS FILE - OR FROM DISK TRACK 18 SECTOR 2
;GOOD LUCK! :)

TBTRCK

;        .BYTE $01   ; START TRACK FROM : 14.bin
;        .BYTE $01   ; START TRACK FROM : 6a.bin
;        .BYTE $02   ; START TRACK FROM : 10.bin
;        .BYTE $02   ; START TRACK FROM : 6.bin
;        .BYTE $03   ; START TRACK FROM : EXCHGLDR.PRG
;        .BYTE $03   ; START TRACK FROM : 13ldr1B00.prg
;        .BYTE $03   ; START TRACK FROM : PACK1.PRG
;        .BYTE $09   ; START TRACK FROM : EXCHGLDR2.PRG
;        .BYTE $09   ; START TRACK FROM : 14ldr1B00.prg
;        .BYTE $0A   ; START TRACK FROM : exchgldr4.prg
;        .BYTE $0A   ; START TRACK FROM : 16ldr1B00.prg
;        .BYTE $0A   ; START TRACK FROM : exchgldr3.prg
;        .BYTE $0A   ; START TRACK FROM : 15ldr1B00.prg
;        .BYTE $0A   ; START TRACK FROM : doynaxdemo.prg



TBSECT

;        .BYTE $00   ; START SECTOR FROM : 14.bin
;        .BYTE $02   ; START SECTOR FROM : 6a.bin
;        .BYTE $09   ; START SECTOR FROM : 10.bin
;        .BYTE $04   ; START SECTOR FROM : 6.bin
;        .BYTE $0A   ; START SECTOR FROM : EXCHGLDR.PRG
;        .BYTE $0D   ; START SECTOR FROM : 13ldr1B00.prg
;        .BYTE $11   ; START SECTOR FROM : PACK1.PRG
;        .BYTE $04   ; START SECTOR FROM : EXCHGLDR2.PRG
;        .BYTE $08   ; START SECTOR FROM : 14ldr1B00.prg
;        .BYTE $09   ; START SECTOR FROM : exchgldr4.prg
;        .BYTE $03   ; START SECTOR FROM : 16ldr1B00.prg
;        .BYTE $0A   ; START SECTOR FROM : exchgldr3.prg
;        .BYTE $04   ; START SECTOR FROM : 15ldr1B00.prg
;        .BYTE $14   ; START SECTOR FROM : doynaxdemo.prg



TBLENBLCK

;        .BYTE 14   ; LENGTH BLOCKS OF FILE : 14.bin
;        .BYTE 8   ; LENGTH BLOCKS OF FILE : 6a.bin
;        .BYTE 11   ; LENGTH BLOCKS OF FILE : 10.bin
;        .BYTE 17   ; LENGTH BLOCKS OF FILE : 6.bin
;        .BYTE 5   ; LENGTH BLOCKS OF FILE : EXCHGLDR.PRG
;        .BYTE 5   ; LENGTH BLOCKS OF FILE : 13ldr1B00.prg
;        .BYTE 120   ; LENGTH BLOCKS OF FILE : PACK1.PRG
;        .BYTE 5   ; LENGTH BLOCKS OF FILE : EXCHGLDR2.PRG
;        .BYTE 5   ; LENGTH BLOCKS OF FILE : 14ldr1B00.prg
;        .BYTE 4   ; LENGTH BLOCKS OF FILE : exchgldr4.prg
;        .BYTE 3   ; LENGTH BLOCKS OF FILE : 16ldr1B00.prg
;        .BYTE 4   ; LENGTH BLOCKS OF FILE : exchgldr3.prg
;        .BYTE 4   ; LENGTH BLOCKS OF FILE : 15ldr1B00.prg
;        .BYTE 22   ; LENGTH BLOCKS OF FILE : doynaxdemo.prg



;LOADADDRES OF FILES

TBLOWLDAD

;        .BYTE $B0  ; ($32B0) LOW BYTE OF LOAD ADRESS FILE0 -> 14.bin
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE1 -> 6a.bin
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE2 -> 10.bin
;        .BYTE $25  ; ($2F25) LOW BYTE OF LOAD ADRESS FILE3 -> 6.bin
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE4 -> EXCHGLDR.PRG
;        .BYTE $00  ; ($1B00) LOW BYTE OF LOAD ADRESS FILE5 -> 13ldr1B00.prg
;        .BYTE $89  ; ($8889) LOW BYTE OF LOAD ADRESS FILE6 -> PACK1.PRG
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE7 -> EXCHGLDR2.PRG
;        .BYTE $00  ; ($1B00) LOW BYTE OF LOAD ADRESS FILE8 -> 14ldr1B00.prg
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE9 -> exchgldr4.prg
;        .BYTE $00  ; ($1B00) LOW BYTE OF LOAD ADRESS FILE10 -> 16ldr1B00.prg
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE11 -> exchgldr3.prg
;        .BYTE $00  ; ($1B00) LOW BYTE OF LOAD ADRESS FILE12 -> 15ldr1B00.prg
;        .BYTE $01  ; ($0801) LOW BYTE OF LOAD ADRESS FILE13 -> doynaxdemo.prg

TBHILDAD

;        .BYTE $32  ; ($32B0)  HI BYTE OF LOAD ADRESS FILE0 -> 14.bin
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE1 -> 6a.bin
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE2 -> 10.bin
;        .BYTE $2F  ; ($2F25)  HI BYTE OF LOAD ADRESS FILE3 -> 6.bin
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE4 -> EXCHGLDR.PRG
;        .BYTE $1B  ; ($1B00)  HI BYTE OF LOAD ADRESS FILE5 -> 13ldr1B00.prg
;        .BYTE $88  ; ($8889)  HI BYTE OF LOAD ADRESS FILE6 -> PACK1.PRG
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE7 -> EXCHGLDR2.PRG
;        .BYTE $1B  ; ($1B00)  HI BYTE OF LOAD ADRESS FILE8 -> 14ldr1B00.prg
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE9 -> exchgldr4.prg
;        .BYTE $1B  ; ($1B00)  HI BYTE OF LOAD ADRESS FILE10 -> 16ldr1B00.prg
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE11 -> exchgldr3.prg
;        .BYTE $1B  ; ($1B00)  HI BYTE OF LOAD ADRESS FILE12 -> 15ldr1B00.prg
;        .BYTE $08  ; ($0801)  HI BYTE OF LOAD ADRESS FILE13 -> doynaxdemo.prg




STARTTRCK_FILE0 = $01  ; 14.bin
STARTTRCK_FILE1 = $01  ; 6a.bin
STARTTRCK_FILE2 = $02  ; 10.bin
STARTTRCK_FILE3 = $02  ; 6.bin
STARTTRCK_FILE4 = $03  ; EXCHGLDR.PRG
STARTTRCK_FILE5 = $03  ; 13ldr1B00.prg
STARTTRCK_FILE6 = $03  ; PACK1.PRG
STARTTRCK_FILE7 = $09  ; EXCHGLDR2.PRG
STARTTRCK_FILE8 = $09  ; 14ldr1B00.prg
STARTTRCK_FILE9 = $0A  ; exchgldr4.prg
STARTTRCK_FILE10 = $0A  ; 16ldr1B00.prg
STARTTRCK_FILE11 = $0A  ; exchgldr3.prg
STARTTRCK_FILE12 = $0A  ; 15ldr1B00.prg
STARTTRCK_FILE13 = $0A  ; doynaxdemo.prg

STARTSECT_FILE0 = $00  ; 14.bin
STARTSECT_FILE1 = $02  ; 6a.bin
STARTSECT_FILE2 = $09  ; 10.bin
STARTSECT_FILE3 = $04  ; 6.bin
STARTSECT_FILE4 = $0A  ; EXCHGLDR.PRG
STARTSECT_FILE5 = $0D  ; 13ldr1B00.prg
STARTSECT_FILE6 = $11  ; PACK1.PRG
STARTSECT_FILE7 = $04  ; EXCHGLDR2.PRG
STARTSECT_FILE8 = $08  ; 14ldr1B00.prg
STARTSECT_FILE9 = $09  ; exchgldr4.prg
STARTSECT_FILE10 = $03  ; 16ldr1B00.prg
STARTSECT_FILE11 = $0A  ; exchgldr3.prg
STARTSECT_FILE12 = $04  ; 15ldr1B00.prg
STARTSECT_FILE13 = $14  ; doynaxdemo.prg

LDADR_FILE0 = $32B0  ; 14.bin
LDADR_FILE1 = $5000  ; 6a.bin
LDADR_FILE2 = $5000  ; 10.bin
LDADR_FILE3 = $2F25  ; 6.bin
LDADR_FILE4 = $5000  ; EXCHGLDR.PRG
LDADR_FILE5 = $1B00  ; 13ldr1B00.prg
LDADR_FILE6 = $8889  ; PACK1.PRG
LDADR_FILE7 = $5000  ; EXCHGLDR2.PRG
LDADR_FILE8 = $1B00  ; 14ldr1B00.prg
LDADR_FILE9 = $5000  ; exchgldr4.prg
LDADR_FILE10 = $1B00  ; 16ldr1B00.prg
LDADR_FILE11 = $5000  ; exchgldr3.prg
LDADR_FILE12 = $1B00  ; 15ldr1B00.prg
LDADR_FILE13 = $0801  ; doynaxdemo.prg

LENBLK_FILE0 = 14   ; LENGTH BLOCKS OF FILE : 14.bin
LENBLK_FILE1 = 8   ; LENGTH BLOCKS OF FILE : 6a.bin
LENBLK_FILE2 = 11   ; LENGTH BLOCKS OF FILE : 10.bin
LENBLK_FILE3 = 17   ; LENGTH BLOCKS OF FILE : 6.bin
LENBLK_FILE4 = 5   ; LENGTH BLOCKS OF FILE : EXCHGLDR.PRG
LENBLK_FILE5 = 5   ; LENGTH BLOCKS OF FILE : 13ldr1B00.prg
LENBLK_FILE6 = 120   ; LENGTH BLOCKS OF FILE : PACK1.PRG
LENBLK_FILE7 = 5   ; LENGTH BLOCKS OF FILE : EXCHGLDR2.PRG
LENBLK_FILE8 = 5   ; LENGTH BLOCKS OF FILE : 14ldr1B00.prg
LENBLK_FILE9 = 4   ; LENGTH BLOCKS OF FILE : exchgldr4.prg
LENBLK_FILE10 = 3   ; LENGTH BLOCKS OF FILE : 16ldr1B00.prg
LENBLK_FILE11 = 4   ; LENGTH BLOCKS OF FILE : exchgldr3.prg
LENBLK_FILE12 = 4   ; LENGTH BLOCKS OF FILE : 15ldr1B00.prg
LENBLK_FILE13 = 22   ; LENGTH BLOCKS OF FILE : doynaxdemo.prg
