
;================================
;FIND_BY_FILENAME ADDITIONAL FUNCTION FOR:
;THE BONGO LOADERS (NOT FOR DETERMINISTIC)
; (C) BY WEGI
;================================
!TO "find_by_filename.prg" , CBM
;===
          * = $4500
;===
;input:
;acc   #<filename pointer
;yreg  #>filename pointer
;xreg  #filename length

;===
;output
;Carry = 1 - file not found error
;Carry = 0 - acc #start track, yreg #start sector

;===============
FIND_BY_FILENAME
          JMP FBF_2
;=========
SENDDRV       ;SEND ONE BYTE
;---
          SEC
          ROR
          TAX
          LDA $DD02
          ORA #$30
          STA $DD02

          BIT $DD00
          BVC *-3
-:
          LDA $DD02
          EOR #$20
          ORA #$10
          BCC *+4
          AND #$EF
          STA $DD02
          TXA
          LSR
          TAX
          BNE -
          LDA $DD02
          AND #$0F
          ORA #$10
          STA $DD02
;---	
          RTS	
;---
GET_ONE_BYTE
          BIT $DD00
          BVC *-3
          LDX #$3F
          STX $DD02
          STY RCALL
          BIT $EA
          LDY #$37
          NOP
          LDA $DD00
          STY $DD02
          LSR
          LSR
          NOP
          NOP
          BIT $EA
          ORA $DD00
          STX $DD02

          LSR
          LSR
          LSR
          LSR
          STA HALFBYTE
          LDA $DD00
          STY $DD02
;---
RCALL    = *+1
          LDY #$00
;---
          NOP
          LDX #$1F
          LSR
          LSR
MY_MANY   = *+1
          CPY #$FF
          ORA $DD00
          STX $DD02
          AND #$F0
HALFBYTE = *+1
          ORA #$00
;---
          RTS
;---------

FBF_2
          stx drive_data
          sta cp_name+1
          sty cp_name+2
          
          LDA $01
          PHA
          lda #$34
          sta $01

cp_name   lda $1000,x
          sta drive_data+1,x
          dex
          bpl cp_name

          inc $01

          LDY #$00
-:
          LDA CODESEND,Y
          JSR SENDDRV   ;SEND JOB CODE TO LOADER
          INY 
          CPY #$06
          BNE -

          LDY #$00
-:
          LDA CODESEND2,Y  ;SEND OUR EXTRA CODE TO DRIVE
          JSR SENDDRV
          INY 
          BNE -
-:
          LDA CODESEND2+$0100,Y  ;SEND OUR EXTRA CODE TO DRIVE
          JSR SENDDRV
          INY 
          BNE -

-:        jsr GET_ONE_BYTE ;get one block from drive
          sta drive_data,y
          iny
          bne -

          jsr GET_ONE_BYTE
          pha
          jsr GET_ONE_BYTE
          pha


          LDY #$00
-:
          LDA drive_data,Y  ;SEND OUR EXTRA CODE TO DRIVE
          JSR SENDDRV
          INY 
          BNE -

          pla
          tax
          pla
          tay
          
          PLA
          STA $01
          sec
          txa
          beq *+3
          clc
          RTS
;=============
	
;---
CODESEND
;---
;GET_TO_RAM PROTOCOL
!byte 0	;LOW BYTE TARGET ADDRES IN DRIVE TO PUT DATA
!byte 6	;HI BYTE ($0600)
!byte 4	;JOB NR ALLWAYS 4
!byte 0   ;LOW BYTE TO RUN PROG
!byte 6   ;HI BYTE ($0600)
!byte 0   ;BYTES TO SEND 0 = 256
;---
CODESEND2
;---
!pseudopc $0600
;---
;=============
bufer1 = $0300
fvec = $08
;=============
          SEI
          JMP CONT_01         
GET_BT
          LDA #$80
          STA $1800
          LDX $1800
          BNE *-3

-:        CPX $1800
          BEQ *-3
          LDX $1800
          CPX #4
          ROR
          BCC -
		
          LDX #$0D
          STX $1800
          CPX $1800
          BNE *-3
          LDX #$08
          STX $1800
          RTS
;---------
BIN2SER  
          !BYTE $0F,$07,$0D,$05,$0B,$03,$09,$01
          !BYTE $0E,$06,$0C,$04,$0A,$02,$08,$00
;---------
SEND_ONE_BYTE
          TAX
          LSR
          LSR
          LSR
          LSR
          STA MY_BYTE
          TXA
          AND #$0F
          LDX #$00
          STX $1800

          TAX
          LDA BIN2SER,X

          LDX $1800
          BNE *-3
          STA $1800
          ASL
          ORA #$10
          BIT $1800
          BPL *-3

          STA $1800

MY_BYTE = *+1
          LDX #$00
          NOP
          LDA BIN2SER,X

          BIT $1800
          BMI *-3
          STA $1800
          ASL
          ORA #$10
          BIT $1800
          BPL *-3
          STA $1800
          LDA #$18
          BIT $1800
          BMI *-3
          STA $1800
          RTS
;---------
CONT_01
          LDY #$00
-:        JSR GET_BT
          STA $0700,Y
          INY
          BNE -
          
-:        lda $0300,y
          jsr SEND_ONE_BYTE
          INY
          BNE -
          
          LDY #$BA
-:
          LDA $0100,Y ;SAVE DATA FROM $01BA TO $01FF
          STA $0700,Y
          INY
          BNE -

          jsr searchdir

        
          sei
          LDY #$BA
-:
          LDA $0700,Y    ;RESTORE DATA FROM
          STA $0100,Y    ;$01BA - $01FF
          INY
          BNE -
-:        JSR GET_BT
          STA $0300,Y
          INY
          BNE -
;**************
;* EXIT POINT *
;**************
;===
          RTS
;===
;---
;---
;--------
searchdir
         jsr init_disk
         lda #<bufer1
         sta fvec
         lda #>bufer1
         sta fvec+1

         ldy name_len
         cpy #$03
         bcc onlyprg
         dey
         dey
         lda name_data,y
         cmp #','
         bne onlyprg
         sty name_len
         iny
         lda name_data,y
         and #$3f
         ldx #$03
tp1
         cmp tbtype,x
         bne tp2
         stx filetype
         jmp onlyprg
tp2      dex
         bpl tp1

onlyprg
         jsr read_sec
         sei
         lda $1c00
         eor #$08
         sta $1c00
lopfind
         ldy #$02
         lda (fvec),y
         bpl nextf
         and #$0f
         cmp filetype
         beq checknext
nextf
         lda fvec
         clc
         adc #$20
         sta fvec
         bcc lopfind
         lda bufer1+1
         sta $07
         lda bufer1
         bne onlyprg
         jmp fnotf


checknext
         ldx #$00
         ldy #$05

cmpare
         lda name_data,x
         cmp #'*'
         beq finded
         cmp #'?'
         beq nextchar
         cmp (fvec),y
         bne nextf
nextchar iny
         inx
         cpx #$10
         beq finded
         cpx name_len
         bne cmpare
         lda (fvec),y
         cmp #$a0
         bne nextf
finded
         ldy #$03
         lda (fvec),y
         sta strttrk
         iny
         lda (fvec),y
         sta strtsec
fnotf    lda strtsec
         jsr SEND_ONE_BYTE
         lda strttrk
         jmp SEND_ONE_BYTE
;---------------------------------------
;--------
tbtype   !byte $04,$13,$10,$15 ;DSPU
filetype !byte 2
strttrk  !byte 0
strtsec  !byte 0
;--------
;---
read_sec
          lda #$80
          jsr INITER2
          bcs *-3
          rts
init_disk
          lda #$12
          sta $06
          lda #$01
          sta $07
          lda #$b0
          jsr INITER2
          bcs *-3
          rts
;---
INITER2
          SEI
          LDX #0
          STA $0298
          JSR $D57D
          LDA #5
          STA $6A
          CLI
          JSR $D599
          CMP #$02
          RTS
;===
name_len  = *
name_data = *+1

END_NEXT_SIDE
!realpc
drive_data
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0


