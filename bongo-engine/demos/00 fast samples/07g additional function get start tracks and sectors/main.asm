;-------------------------------------------
!TO "main.prg" , CBM
;========
init_msx = $1000
cnt_msx = $1003

          *= $0801
;===================================================================
;= sample 7d - additional functions - GET_START_TRACKS_AND_SECTORS =
;===================================================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------

          cld
          cli
          jsr proc1 ;install drivecode
          jsr init_irq
          jsr enable_hires ;prepare hires mode

;===========================
;= here using a decruncher =		
;===========================
restart

          lda #<tracks_data
          ldy #>tracks_data
;----
;input:
;
;acc/yreg lo/hi byte of pointer to start tracks and sectors table
;
;output:
; carry = 1 - error  0 prg files in dir
; carry = 0 , acc how many prg files, start tracks and sectors data stored in tracks_data buffer
;
;----
          JSR GET_START_TRACKS_AND_SECTORS
          sta how_many_files
          bcs * ; 0 files in dir
          
          lda #1 ;nr of file
          jsr loadfile_by_nr

          lda #2 ;nr of file
          jsr loadfile_by_nr

;===========================
;= 
;===========================	
		
          lda $dc01
          cmp #$ef
          beq restart ;loop if space
          
          jsr disable_loader
          
          lda #$37
          sta $01
     
          lda #$00
          sta $c6
		
          jsr $ffe4
          beq *-3
          lda #$ea
          ldx #$31
          sei
          stx $0314
          sta $0315

          jsr $fda3
          jsr $ff5b ;init vic and return to basic
          cli
          rts

;===================
get_tracks_data
          lda $1111,x
          rts
loadfile_by_nr
          
          ldx #$00
          asl
          bcc +
          inx
+:        clc
          adc #<tracks_data
          sta get_tracks_data+1
          txa
          adc #>tracks_data
          sta get_tracks_data+2
          
          
          ldx #$00
          jsr get_tracks_data
          sta STRTTRK
          inx
          jsr get_tracks_data
          sta STRTSEC
          
          LDA #<infodata
          LDY #>infodata
          JSR loadfile
          BCS *		;LOAD ERROR - DEMO CRASHED  
          rts
;*************************
irq
          pha
          txa
          pha
          tya
          pha

          lda $01
          pha
          lda #$35
          sta $01

          jsr cnt_msx


          bit $dc0d
          pla
          sta $01

          pla
          tay
          pla
          tax
          pla
nmiex
          rti
;--------
;*************************
irq37
          jsr cnt_msx
          jmp $ea31
;*************************

;---
infodata

BASEADR		!WORD $2000 ;load address = $2000
STRTTRK		!BYTE 0
STRTSEC		!BYTE 0
HOW_GET_LD	!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---

;---
;===================
init_irq

          ldx #$00
          txa
          tay
          jsr init_msx
          
          lda #<nmiex
          sta $fffa
          lda #>nmiex
          sta $fffb
         
          lda #<irq
          sta $fffe
          lda #>irq
          sta $ffff



          lda #$c7
          sta $dc04
          lda #$4c
          sta $dc05
          bit $dc0d
          lda #$35
          sta $01
          lda #<irq37
          sta $0314
          lda #>irq37
          sta $0315
          rts
;===================

enable_hires
		
          ldy #$00
          sty $6a
          ldx #$20
          stx $6b
          tya
		
l0        sta ($6a),y
          iny
          bne l0
          inc $6b
          dex
          bne l0

          lda #$fb
          sta $d020
		
          ldx #$04
          stx $6b
		
		
l1        sta ($6a),y
          iny
          bne l1
          inc $6b
          dex
          bne l1

          lda #$3b
          sta $d011

          lda #$03
          sta $dd00
		
          lda #$18
          sta $d018
          lda #$c8
          sta $d016

          rts		
;===================
;-------------------------------
disable_loader
          lda $01
          pha
          lda #$35
          sta $01
          jsr SENDDRIV
          jsr SENDDRIV          
          lda #$00
          jsr SENDDRIV
          pla
          sta $01
          rts
;---
SENDDRIV       ;SEND ONE BYTE
;---
          SEC
          ROR
          TAX
          LDA $DD02
          ORA #$30
          STA $DD02

          BIT $DD00
          BVC *-3
-:
          LDA $DD02
          EOR #$20
          ORA #$10
          BCC *+4
          AND #$EF
          STA $DD02
          TXA
          LSR
          TAX
          BNE -
          LDA $DD02
          AND #$0F
          ORA #$10
          STA $DD02
;---	
          RTS
how_many_files
          !byte 0
tracks_data
;288 possiblity bytes for 144 files start track and sector
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

;===================
          *= $1000
          !binary "MYMSX.PRG",,2 
;===================
		*= $1b00
loadfile
          !binary "4ldr1B00.prg",,2
;===================

;===================
;a stub installer of loader		
;===================
		*= $4000
proc1
;---
		!binary "4inst4000.prg",,2 ;without load address
;===================

;both stubs files generated from relocator of bongo linking engine - page loaders
;of course You can do compile this from source code as well


;================================
;GET_START_TRACKS_AND_SECTORS ADDITIONAL FUNCTION FOR:
;THE BONGO LOADERS (NOT FOR DETERMINISTIC)
; (C) BY WEGI IN
;================================
;!TO "get_start_tracks_and_sectors.prg" , CBM
;===
          * = $4600
;===

;----
;input:
;
;acc/yreg lo/hi byte of pointer to start tracks and sectors table
;===
;
;output:
; carry = 1 - error  0 prg files in dir
; carry = 0 , acc how many prg (or user type selected) files, start tracks and sectors data stored in tracks_data buffer
;
;----

;===============
GET_START_TRACKS_AND_SECTORS
		!binary "get_start_tracks_and_sectors.prg",,2 ;without load address
