;-------------------------------------------
!TO "main.prg" , CBM
;========

init_msx = $1000
cnt_msx = $1003

          *= $0801
;==================================
;= third sample - decrunch in irq =
;==================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------
START

          cld
          cli
          jsr init_irq
          
		;now $01 value is not important
		;when depack is into $e000
		;but allways remember over this !!!
		
		;now lazy clear screen
		lda #<emptybmp
		ldy #>emptybmp
		jsr DECRUNCH
		
		;and lazy fillcolors
		lda #$34
		sta $01
		lda #<hirescolor
		ldy #>hirescolor
		jsr DECRUNCH
				
		jsr enable_hires ;prepare hires mode

;===========================
;= here using a decruncher =		
;===========================
restart

	
		; lazy clear screen
		lda #<emptybmp
		ldy #>emptybmp
		jsr DECRUNCH
		
	
		
		;and lazy fillcolors
		lda #$34
		sta $01
		lda #<hirescolor
		ldy #>hirescolor
		jsr DECRUNCH

		lda #$35
		sta $01
		lda #$c8
		sta $d016	
     
		lda #<jack
		ldy #>jack
		jsr DECRUNCH
          
		;lazy clear screen
		lda #<emptybmp
		ldy #>emptybmp
		jsr DECRUNCH  
        
          ; decrunch will be on the I/O into color RAM
		lda #$35
		sta $01
		lda #$d8
		sta $d016
				
		;lazy copy data to $d800 color RAM via decrunch
		lda #<hated800
		ldy #>hated800
		jsr DECRUNCH  

		;now decrunch under I/O
		lda #$34
		sta $01
		
		lda #<hatedc00
		ldy #>hatedc00
		jsr DECRUNCH  

		lda #$35
		sta $01

		lda #<hate01bmp
		ldy #>hate01bmp
		jsr DECRUNCH  
		
		lda #<hatebmp
		ldy #>hatebmp
		jsr DECRUNCH  
;===========================
;= 
;===========================	

		lda #$35
		sta $01
     
		lda $dc01
		cmp #$ef
		beq restart ;loop if space
          
          lda #$37
          sta $01

		lda #$00
		sta $c6
				
		jsr $ffe4
		beq *-3
          lda #$ea
          ldx #$31
          sei
          stx $0314
          sta $0315
		jsr $fda3
		jsr $ff5b ;init vic and return to basic
		cli
		rts

;===================
;*************************
irq
          pha
          txa
          pha
          tya
          pha

          lda $01
          pha
          lda #$35
          sta $01

          jsr cnt_msx


          bit $dc0d
          pla
          sta $01

          pla
          tay
          pla
          tax
          pla
nmiex
          rti
;--------
;*************************
irq37

          jsr cnt_msx
          jmp $ea31
;===================
init_irq
		lda #$37
		sta $01
		lda #$00
		tax
		tay
		jsr init_msx
          
          lda #<nmiex
          sta $fffa
          lda #>nmiex
          sta $fffb
         
          lda #<irq
          sta $fffe
          lda #>irq
          sta $ffff



          lda #$c7
          sta $dc04
          lda #$4c
          sta $dc05
		bit $dc0d
		lda #$35
		sta $01
          lda #<irq37
          sta $0314
          lda #>irq37
          sta $0315
		rts
;===================
enable_hires

		lda #$35
		sta $01

		lda #$3b
		sta $d011

		lda #$00
		sta $d020
		sta $d021

		lda #$00
		sta $dd00
          lda #$78
          sta $d018
          lda #$c8
          sta $d016

		rts		
;===================
          *= $1000
          !binary "MYMSX.PRG",,2  ;without load address
END_MSX
;===================
;= decruncher stub =		
;===================
DECRUNCH
		ldx #$00
;---
!source "decruncher.asm"
;==================
;check and learn how to use bongo cruncher
;===================
jack
;===================
;simple use cruncher
; ..\..\tools\cmdcruncher.exe -i "jack.bmpC64" -depackadr $2000 -o "jack.bin" -binfile -nosafeldadr
          !binary "jack.bin"   
;===================
emptybmp
; ..\..\tools\cmdcruncher.exe -i "e000 empty.bmpC64" -o "e000 empty.bin" -binfile -nosafeldadr
		!binary "e000 empty.bin" 
;===================
hirescolor
; ..\..\tools\cmdcruncher.exe -i "dc00 fillcolor.clr" -o "dc00 fillcolor.bin" -binfile -nosafeldadr
		!binary "dc00 fillcolor.bin" 
;===================
hate01bmp
; ..\..\tools\cmdcruncher.exe -i "hate01bmp" -depackadr $e000 -o "hate01bmp.bin" -binfile -noldad -nosafeldadr
		!binary "hate01bmp.bin" 
;===================
hatebmp
; ..\..\tools\cmdcruncher.exe -i "hatebmp" -depackadr $e000 -o "hatebmp.bin" -binfile -noldad -nosafeldadr
		!binary "hatebmp.bin" 
;===================
hated800
; ..\..\tools\cmdcruncher.exe -i "hated800" -depackadr $d800 -o "hated800.bin" -binfile -noldad -nosafeldadr
		!binary "hated800.bin" 
;===================
hatedc00
; ..\..\tools\cmdcruncher.exe -i "hatedc00" -depackadr $dc00 -o "hatedc00.bin" -binfile -noldad -nosafeldadr
		!binary "hatedc00.bin" 
;===================
