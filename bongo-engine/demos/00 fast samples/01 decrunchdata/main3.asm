;-------------------------------------------
!TO "main3.prg" , CBM
;========
;decruncher
;Variables..        #Bytes
zp_base	= $6a       ; -
cpl	= zp_base+0 ;1
cur	= zp_base+1 ;1
zp	= zp_base+2 ; -
put	= zp_base+2 ;2
get	= zp_base+4 ;2
cps	= zp_base+6 ;2

          *= $0801
;==================================
;= sample one - using decrunchers =
;==================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------
START

          cld
		jsr enable_hires ;prepare hires mode

;===========================
;= here using a decruncher =		
;===========================
     

          
          ;set pointer to data
          ldy #<cruncheddata
          ldx #>cruncheddata
          
          jsr DECRUNCH

;===========================
;= 
;===========================		
		lda #$00
		sta $c6
		
		lda $dc01
		cmp #$ef
		beq START ;loop if space
		
		jsr $ffe4
		beq *-3
		jmp $ff5b ;init vic and return to basic
;===================
enable_hires
          lda #$37
          sta $01
          lda #$18
          sta $d018
          lda #$c8
          sta $d016

		ldy #$00
		sty $fb
		ldx #$20
		stx $fc
		tya
		
l0		sta ($fb),y
		iny
		bne l0
		inc $fc
		dex
		bne l0
		
		lda #$3b
		sta $d011


          
		lda #$fb
		sta $d020
		ldx #4
		stx $fc
		
l1		sta ($fb),y
		iny
		bne l1
		inc $fc
		dex
		bne l1
		rts		
;===================
;= decruncher stub =		
;===================
DECRUNCH

; ByteBoozer Decruncher    /HCL May.2003





	sty get
	stx get+1

	ldy #2
	lda (get),y
	sta cur,y
	dey
	bpl *-6

	clc
	lda #3
	adc get
	sta get
	bcc *+4
	inc get+1

D_loop
	jsr D_get
Dl_1
	php
	lda #1
Dl_2
	jsr D_get
	bcc Dl_2e
	jsr D_get
	rol
	bpl Dl_2
Dl_2e
	plp
	bcs D_copy

D_plain
	sta cpl

	ldy #0
	lda (get),y
	sta (put),y
	iny
	cpy cpl
	bne *-7

	ldx #get-zp
	jsr D_add
	iny
	beq D_loop
	sec
	bcs Dl_1

D_copy
	adc #0
	beq D_end
	sta cpl
	cmp #3

	lda #0
	sta cps
	sta cps+1

	rol
	jsr D_get
	rol
	jsr D_get
	rol
	tax
Dc_1s
	ldy Tab,x
Dc_1
	jsr D_get
	rol cps
	rol cps+1
	dey
	bne Dc_1
	txa
	dex
	and #3
	beq Dc_1e
	inc cps
	bne Dc_1s
	inc cps+1
	bne Dc_1s
Dc_1e
	sec
	lda put
	sbc cps
	sta cps
	lda put+1
	sbc cps+1
	sta cps+1

	lda (cps),y
	sta (put),y
	iny
	cpy cpl
	bne *-7

	ldx #put-zp
	jsr D_add
	bmi D_loop

D_get
	asl cur
	bne Dg_end
	pha
	tya
	pha
	ldy #0
	lda (get),y
	inc get
	bne *+4
	inc get+1
	sec
	rol
	sta cur
	pla
	tay
	pla
Dg_end
	rts

D_add
	clc
	tya
	adc zp,x
	sta zp,x
	bcc *+4
	inc zp+1,x
	dex
	dex
	bpl D_add
D_end
	rts


Tab	!byte 4,2,2,2
	!byte 5,2,2,3
;===================
cruncheddata
;===================
;simple use cruncher
;..\..\lzwvltest\data\LZWVL.EXE -l   "jack2.bmpC64" "jack.lz"
          !binary "jack.bmpC64.bb",,2 ;without load address
;=======
