;-------------------------------------------
!TO "main2.prg" , CBM
;========
lzwvl_get  = $fb
lzwvl_put  = lzwvl_get+2
lzwvl_temp = lzwvl_get+4

          *= $0801
;==================================
;= sample one - using decrunchers =
;==================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------
START

          cld
		jsr enable_hires ;prepare hires mode

;===========================
;= here using a decruncher =		
;===========================
     
          ;set depack address
          lda #$00
          sta lzwvl_put
          lda #$20
          sta lzwvl_put+1
          
          ;set pointer to data
          lda #<cruncheddata
          sta lzwvl_get
          lda #>cruncheddata
          sta lzwvl_get+1
          
          jsr DECRUNCH

;===========================
;= 
;===========================		
		lda #$00
		sta $c6
		
		lda $dc01
		cmp #$ef
		beq START ;loop if space
		
		jsr $ffe4
		beq *-3
		jmp $ff5b ;init vic and return to basic
;===================
enable_hires
          lda #$37
          sta $01
          lda #$18
          sta $d018
          lda #$c8
          sta $d016

		ldy #$00
		sty $fb
		ldx #$20
		stx $fc
		tya
		
l0		sta ($fb),y
		iny
		bne l0
		inc $fc
		dex
		bne l0
		
		lda #$3b
		sta $d011


          
		lda #$fb
		sta $d020
		ldx #4
		stx $fc
		
l1		sta ($fb),y
		iny
		bne l1
		inc $fc
		dex
		bne l1
		rts		
;===================
;= decruncher stub =		
;===================
DECRUNCH


;decompressor v3, 31-5-2009






; this was for raw data 
        ldy #0
        ldx #$30
        lda (lzwvl_get),y
        ;negative : long mode (bmi, $30)
        ;positive : short mode (bcc, $90)
        bmi *+4
        ldx #$90
        stx delzwvl_short_or_long
        and #$7f
        tax
        bpl delzwvl_start2
        ;----------------


delzwvl_derle
        and #$3f
        sta lzwvl_temp     ;number of bytes to de-rle
        iny
        lda (lzwvl_get),y  ;fetch rle byte
        ldy lzwvl_temp
delzwvl_derle_loop
        dey
        sta (lzwvl_put),y
        bne delzwvl_derle_loop

        ;update 0page

        lda lzwvl_temp
delzwvl_update
        ldx #2              ;update get with encoding byte + rle byte
        ;-------------
delzwvl_update_x_set
        clc
delzwvl_update_noclc
        adc lzwvl_put
        sta lzwvl_put
        bcc *+5
        inc lzwvl_put+1
        clc

        txa
        adc lzwvl_get
        sta lzwvl_get
        bcc *+4
        inc lzwvl_get+1
        ;--------------

delzwvl_start
        ldy #0
;        lax (lzwvl_get),y
        lda (lzwvl_get),y
        tax

        ;if negative -> lz sequence
        bmi delzwvl_copystring
delzwvl_start2
        ;if 0 -> end of file
        beq delzwvl_end

        ;if $01-$3f -> literal
        ;if $40-$7f -> rle sequence
        cmp #$40
        bcs delzwvl_derle
        ;----------------

delzwvl_copybytes
        tay
delzwvl_copybytes_loop
        lda (lzwvl_get),y
        dey
        sta (lzwvl_put),y
        bne delzwvl_copybytes_loop

        ;update 0page, y=0, carry is clear

        txa                       ;update put with the number of bytes copied
        inx                       ;update get with the number of bytes copied + 1
        bne delzwvl_update_noclc
        ;-----------------------

delzwvl_copystring
        and #$7f            ;number of bytes to copy
        sta delzwvl_compare
        iny

        ;calculate adress of lz sequence
        lda (lzwvl_get),y
        clc
delzwvl_short_or_long
        bmi delzwvl_copystring_short   ;long mode : bmi ($30), short mode : bcc ($90)
delzwvl_copystring_long

        pha
        iny
        lda (lzwvl_get),y
        adc lzwvl_put
        sta delzwvl_put+1
        pla
        ora #$80
        adc lzwvl_put+1
        sta delzwvl_put+2
        ldx #3
        bne delzwvl_copystring_start
        ;--------

delzwvl_copystring_short
        adc lzwvl_put
        sta delzwvl_put+1
        lda #$ff
        adc lzwvl_put+1
        sta delzwvl_put+2

        ldx #2
delzwvl_copystring_start
        ldy #0
delzwvl_copystring_loop
delzwvl_put
        lda $dead,y
        sta (lzwvl_put),y
        iny
delzwvl_compare=*+1
        cpy #0
        bne delzwvl_copystring_loop

        ;update 0page

        tya
        bpl delzwvl_update_x_set

delzwvl_end
        rts


;===================
cruncheddata
;===================
;simple use cruncher
;..\..\lzwvltest\data\LZWVL.EXE -l   "jack2.bmpC64" "jack.lz"
          !binary "jack.lz"
;=======
