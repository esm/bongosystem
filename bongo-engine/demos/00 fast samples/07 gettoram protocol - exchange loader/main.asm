;-------------------------------------------
!TO "main.prg" , CBM
;========

init_msx = $1000
cnt_msx = $1003

          *= $0801
;================================
;=  sample 7 - exchange loader  =
;================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------
          cld
          cli
          jsr proc1 ;install drivecode
          jsr init_irq
          jsr enable_hires ;prepare hires mode

;===========================
;= here using a decruncher =		
;===========================
restart

          
          
          jsr replace_to_fast_stream 
          jsr load_demo                      
		jsr replace_to_deterministic


          LDA #<file1info_det 
          LDY #>file1info_det
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED     

                       
          LDA #<file2info_det
          LDY #>file2info_det
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED     

          LDA #<file11info_det 
          LDY #>file11info_det
          JSR proc2
          BCS *		;LOAD ERROR - DEMO CRASHED     

                       
          LDA #<file12info_det
          LDY #>file12info_det
          JSR proc2
          BCS *		;LOAD ERROR - DEMO CRASHED 
		
		jsr replace_to_nointerleave_from_det
		jsr load_demo

		jsr replace_to_shortstream


;===========================
;= 
;===========================	
		
          lda $dc01
          cmp #$ef

          beq restart ;loop if space

endwork          

          
          jsr disable_loader
          
          lda #$37
          sta $01
     
          lda #$00
          sta $c6
		
          jsr $ffe4
          beq *-3
          lda #$ea
          ldx #$31
          sei
          stx $0314
          sta $0315

          jsr $fda3
          jsr $ff5b ;init vic and return to basic
          cli
          rts
;=======================
load_demo
          LDA #<file1info
          LDY #>file1info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file2info
          LDY #>file2info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED 

          LDA #<file11info
          LDY #>file11info
          JSR proc2
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file12info
          LDY #>file12info
          JSR proc2
          BCS *		;LOAD ERROR - DEMO CRASHED 
          rts
;=======================
replace_to_shortstream_from_det
          LDA #<file9info_det
          LDY #>file9info_det
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file10info_det
          LDY #>file10info_det
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED 
          jmp exchg_ldr
;===================
replace_to_shortstream
          LDA #<file9info
          LDY #>file9info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file10info
          LDY #>file10info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED 
          jmp exchg_ldr
;===================
replace_to_deterministic
          LDA #<file7info
          LDY #>file7info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file8info
          LDY #>file8info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED 
          jmp exchg_ldr
;===================
replace_to_nointerleave_from_det
          LDA #<file5info_det
          LDY #>file5info_det
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file6info_det
          LDY #>file6info_det
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED 
          jmp exchg_ldr
;===================
replace_to_nointerleave
          LDA #<file5info
          LDY #>file5info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file6info
          LDY #>file6info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED 
          jmp exchg_ldr
;===================
replace_to_fast_stream_from_det
          LDA #<file3info_det
          LDY #>file3info_det
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file4info_det
          LDY #>file4info_det
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED 
          jmp exchg_ldr
;===================
replace_to_fast_stream
          LDA #<file3info
          LDY #>file3info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file4info
          LDY #>file4info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED 
          jmp exchg_ldr
;===================
exchg_ldr
		lda $01
		pha
		lda #$35
		sta $01
		
		ldx #5
		ldy #0
		lda #$5b
		sta exchg2+2
		lda #$1b
		sta exchg2+5
		
exchg2    lda $5b00,y
		sta $1b00,y
		iny
		bne exchg2
		inc exchg2+2
		inc exchg2+5
		dex
		bne exchg2
		
		jsr $5000
		
		pla
		sta $01
		rts
;===================
;*************************
irq
          pha
          txa
          pha
          tya
          pha

          lda $01
          pha
          lda #$35
          sta $01

          jsr cnt_msx


          bit $dc0d
          pla
          sta $01

          pla
          tay
          pla
          tax
          pla
nmiex
          rti
;--------
;*************************
irq37
          jsr cnt_msx
          jmp $ea31
;*************************
          !source "sample 7.INC" ;infodata from trackmolinker script
;---
file1info_det
!BYTE LENBLK_FILE0
file1info
BASEADR		!WORD LDADR_FILE0 ;$2000 is there
STRTTRK		!BYTE STARTTRCK_FILE0
STRTSEC		!BYTE STARTSECT_FILE0
HOW_GET_LD	!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file2info_det
!BYTE LENBLK_FILE1
file2info
!WORD $2000
!BYTE STARTTRCK_FILE1
!BYTE STARTSECT_FILE1
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file3info_det
!BYTE LENBLK_FILE2
file3info
!WORD $5000
!BYTE STARTTRCK_FILE2
!BYTE STARTSECT_FILE2
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file4info_det
!BYTE LENBLK_FILE3
file4info
!WORD $5b00
!BYTE STARTTRCK_FILE3
!BYTE STARTSECT_FILE3
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file5info_det
!BYTE LENBLK_FILE4
file5info
!WORD $5000
!BYTE STARTTRCK_FILE4
!BYTE STARTSECT_FILE4
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file6info_det
!BYTE LENBLK_FILE5
file6info
!WORD $5b00
!BYTE STARTTRCK_FILE5
!BYTE STARTSECT_FILE5
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file7info_det
!BYTE LENBLK_FILE6
file7info
!WORD $5000
!BYTE STARTTRCK_FILE6
!BYTE STARTSECT_FILE6
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file8info_det
!BYTE LENBLK_FILE7
file8info
!WORD $5b00
!BYTE STARTTRCK_FILE7
!BYTE STARTSECT_FILE7
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file9info_det
!BYTE LENBLK_FILE8
file9info
!WORD $5000
!BYTE STARTTRCK_FILE8
!BYTE STARTSECT_FILE8
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file10info_det
!BYTE LENBLK_FILE9
file10info
!WORD $5b00
!BYTE STARTTRCK_FILE9
!BYTE STARTSECT_FILE9
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file11info_det
!BYTE LENBLK_FILE10
file11info
!WORD LDADR_FILE10
!BYTE STARTTRCK_FILE10
!BYTE STARTSECT_FILE10
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file12info_det
!BYTE LENBLK_FILE11
file12info
!WORD LDADR_FILE11
!BYTE STARTTRCK_FILE11
!BYTE STARTSECT_FILE11
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
;===================
init_irq

         
          lda #$00
          tax
          tay
          jsr init_msx
          
          lda #<nmiex
          sta $fffa
          lda #>nmiex
          sta $fffb
         
          lda #<irq
          sta $fffe
          lda #>irq
          sta $ffff



          lda #$c7
          sta $dc04
          lda #$4c
          sta $dc05
          bit $dc0d
          lda #$35
          sta $01
          lda #<irq37
          sta $0314
          lda #>irq37
          sta $0315
          
          
          rts
;===================

enable_hires
		
          ldy #$00
          sty $6a
          ldx #$20
          stx $6b
          tya
		
l0        sta ($6a),y
          iny
          bne l0
          inc $6b
          dex
          bne l0

          lda #$fb
          sta $d020
		
          ldx #$04
          stx $6b
		
		
l1        sta ($6a),y
          iny
          bne l1
          inc $6b
          dex
          bne l1

          lda #$3b
          sta $d011

          lda #$03
          sta $dd00
		
          lda #$18
          sta $d018
          lda #$c8
          sta $d016

          rts		
;===================
disable_loader
          lda $01
          pha
          lda #$35
          sta $01
          jsr SENDDRIV
          jsr SENDDRIV          
          lda #$00
          jsr SENDDRIV
          pla
          sta $01
          rts
;---
SENDDRIV       ;SEND ONE BYTE
;---
          SEC
          ROR
          TAX
          LDA $DD02
          ORA #$30
          STA $DD02

          BIT $DD00
          BVC *-3
KEX
          LDA $DD02
          EOR #$20
          ORA #$10
          BCC *+4
          AND #$EF
          STA $DD02
          TXA
          LSR
          TAX
          BNE KEX
          LDA $DD02
          AND #$0F
          ORA #$10
          STA $DD02
;---	
          RTS
;===================
          *= $1000
          !binary "MYMSX.PRG",,2 
;===================
; now we use a fast version of decruncher
		*= $1b00
proc2
          !binary "8ldr1b00.prg",,2
;-------------------------------

;===================
;a stub installer of loader		
;===================
		*= $4000
proc1
;---
		!binary "8inst4000.prg",,2 ;without load address
;===================

;===================

;===================
;both stubs files generated from relocator of bongo linking engine - page loaders
;of course You can do compile this from source code as well