;-------------------------------------------
!TO "main.prg" , CBM
;========

init_msx = $1000
cnt_msx = $1003

          *= $0801
;================================
;= sample two - decrunch in irq =
;================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------
START

          cld
          jsr init_irq
		jsr enable_hires ;prepare hires mode

;===========================
;= here using a decruncher =		
;===========================
restart
     lda #$34
     sta $01
     
		lda #<cruncheddata
		ldy #>cruncheddata
		jsr DECRUNCH
          
		lda #<cruncheddata2
		ldy #>cruncheddata2
		jsr DECRUNCH
;===========================
;= 
;===========================	

     lda #$37
     sta $01
     
		lda #$00
		sta $c6
		
		lda $dc01
		cmp #$ef
		beq restart ;loop if space
          

		
		jsr $ffe4
		beq *-3
          lda #$ea
          ldx #$31
          sei
          stx $0314
          sta $0315
     jsr $fda3
		jsr $ff5b ;init vic and return to basic
		cli
		rts

;===================
;*************************
irq
          pha
          txa
          pha
          tya
          pha

          lda $01
          pha
          lda #$35
          sta $01

          jsr cnt_msx


          bit $dc0d
          pla
          sta $01

          pla
          tay
          pla
          tax
          pla
nmiex
          rti
;--------
;*************************
irq37

          jsr cnt_msx
          jmp $ea31
;===================
init_irq
		lda #$37
		sta $01
		lda #$00
		tax
		tay
		jsr init_msx
          
          lda #<nmiex
          sta $fffa
          lda #>nmiex
          sta $fffb
         
          lda #<irq
          sta $fffe
          lda #>irq
          sta $ffff



          lda #$c7
          sta $dc04
          lda #$4c
          sta $dc05
		bit $dc0d
		lda #$35
		sta $01
          lda #<irq37
          sta $0314
          lda #>irq37
          sta $0315
		rts
;===================

enable_hires
		
		lda #$34
		sta $01

		ldy #$00
		sty $6a
		ldx #$1f
		lda #$e0
		sta $6b
		tya
		
l0		sta ($6a),y
		iny
		bne l0
		inc $6b
		dex
		bne l0
		ldy #$3f
		sta ($6a),y
		dey
		bpl *- 3		
      
      iny
		lda #$fb
		
		ldx #>$dc00
		stx $6b
		ldx #$04
		
		
l1		sta ($6a),y
		iny
		bne l1
		inc $6b
		dex
		bne l1

		lda #$35
		sta $01

		lda #$3b
		sta $d011
sta $d020
		lda #$00
		sta $dd00
          lda #$78
          sta $d018
          lda #$c8
          sta $d016

		rts		
;===================


          *= $1000
          !binary "MYMSX.PRG",,2 
END_MSX
;===================
;= decruncher stub =		
;===================
DECRUNCH
		ldx #$00

!source "decruncher.asm"
;===================
cruncheddata
;===================
          !binary "face.bin"
;=======
;===================
cruncheddata2
;===================
;simple use cruncher
; ..\..\tools\cmdcruncher.exe -i "jack.bmpC64" -depackadr $2000 -o "jack.bin" -binfile -nosafeldadr
          !binary "jack.bin"          