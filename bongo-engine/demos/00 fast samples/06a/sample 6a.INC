;SOME INFO FOR LOADER...  THIS DATA ADDED ALSO
;ON THE TRACK $12 SECT $02 IN ORDER:
;OFFSET 0 - STARTTRACK FILES
;OFFSET 50 - STARTSECT FILES
;OFFSET 100 - BLOCKS LENGTH FILES
;OFFSET 150 - LO BYTE LOAD ADRESS OF FILE
;OFFSET 200 - HI BYTE LOAD ADRESS OF FILE
;HOW YOU SEE - 50 FILES PER ONE SIDE FOR TRACKMO
;WOULD IT BE SAVED - THATS ENOUGHT I HOPE :)
;YOU CAN USE THIS DATA FROM THIS FILE - OR FROM DISK TRACK 18 SECTOR 2
;GOOD LUCK! :)

TBTRCK

;        .BYTE $01   ; START TRACK FROM : jack.bin
;        .BYTE $01   ; START TRACK FROM : face.bin
;        .BYTE $02   ; START TRACK FROM : 2000 empty.bin
;        .BYTE $02   ; START TRACK FROM : hate0400.bin
;        .BYTE $02   ; START TRACK FROM : hated800.bin
;        .BYTE $02   ; START TRACK FROM : hate01bmp.bin
;        .BYTE $03   ; START TRACK FROM : hatebmp.bin
;        .BYTE $03   ; START TRACK FROM : 0400 fillcolor.bin
;        .BYTE $03   ; START TRACK FROM : sfx.prg



TBSECT

;        .BYTE $00   ; START SECTOR FROM : jack.bin
;        .BYTE $0C   ; START SECTOR FROM : face.bin
;        .BYTE $04   ; START SECTOR FROM : 2000 empty.bin
;        .BYTE $0C   ; START SECTOR FROM : hate0400.bin
;        .BYTE $07   ; START SECTOR FROM : hated800.bin
;        .BYTE $02   ; START SECTOR FROM : hate01bmp.bin
;        .BYTE $08   ; START SECTOR FROM : hatebmp.bin
;        .BYTE $04   ; START SECTOR FROM : 0400 fillcolor.bin
;        .BYTE $0C   ; START SECTOR FROM : sfx.prg



TBLENBLCK

;        .BYTE 12   ; LENGTH BLOCKS OF FILE : jack.bin
;        .BYTE 20   ; LENGTH BLOCKS OF FILE : face.bin
;        .BYTE 1   ; LENGTH BLOCKS OF FILE : 2000 empty.bin
;        .BYTE 2   ; LENGTH BLOCKS OF FILE : hate0400.bin
;        .BYTE 2   ; LENGTH BLOCKS OF FILE : hated800.bin
;        .BYTE 6   ; LENGTH BLOCKS OF FILE : hate01bmp.bin
;        .BYTE 10   ; LENGTH BLOCKS OF FILE : hatebmp.bin
;        .BYTE 1   ; LENGTH BLOCKS OF FILE : 0400 fillcolor.bin
;        .BYTE 17   ; LENGTH BLOCKS OF FILE : sfx.prg



;LOADADDRES OF FILES

TBLOWLDAD

;        .BYTE $09  ; ($3409) LOW BYTE OF LOAD ADRESS FILE0 -> jack.bin
;        .BYTE $5E  ; ($2C5E) LOW BYTE OF LOAD ADRESS FILE1 -> face.bin
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE2 -> 2000 empty.bin
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE3 -> hate0400.bin
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE4 -> hated800.bin
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE5 -> hate01bmp.bin
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE6 -> hatebmp.bin
;        .BYTE $00  ; ($5000) LOW BYTE OF LOAD ADRESS FILE7 -> 0400 fillcolor.bin
;        .BYTE $01  ; ($0801) LOW BYTE OF LOAD ADRESS FILE8 -> sfx.prg

TBHILDAD

;        .BYTE $34  ; ($3409)  HI BYTE OF LOAD ADRESS FILE0 -> jack.bin
;        .BYTE $2C  ; ($2C5E)  HI BYTE OF LOAD ADRESS FILE1 -> face.bin
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE2 -> 2000 empty.bin
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE3 -> hate0400.bin
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE4 -> hated800.bin
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE5 -> hate01bmp.bin
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE6 -> hatebmp.bin
;        .BYTE $50  ; ($5000)  HI BYTE OF LOAD ADRESS FILE7 -> 0400 fillcolor.bin
;        .BYTE $08  ; ($0801)  HI BYTE OF LOAD ADRESS FILE8 -> sfx.prg




STARTTRCK_FILE0 = $01  ; jack.bin
STARTTRCK_FILE1 = $01  ; face.bin
STARTTRCK_FILE2 = $02  ; 2000 empty.bin
STARTTRCK_FILE3 = $02  ; hate0400.bin
STARTTRCK_FILE4 = $02  ; hated800.bin
STARTTRCK_FILE5 = $02  ; hate01bmp.bin
STARTTRCK_FILE6 = $03  ; hatebmp.bin
STARTTRCK_FILE7 = $03  ; 0400 fillcolor.bin
STARTTRCK_FILE8 = $03  ; sfx.prg

STARTSECT_FILE0 = $00  ; jack.bin
STARTSECT_FILE1 = $0C  ; face.bin
STARTSECT_FILE2 = $04  ; 2000 empty.bin
STARTSECT_FILE3 = $0C  ; hate0400.bin
STARTSECT_FILE4 = $07  ; hated800.bin
STARTSECT_FILE5 = $02  ; hate01bmp.bin
STARTSECT_FILE6 = $08  ; hatebmp.bin
STARTSECT_FILE7 = $04  ; 0400 fillcolor.bin
STARTSECT_FILE8 = $0C  ; sfx.prg

LDADR_FILE0 = $3409  ; jack.bin
LDADR_FILE1 = $2C5E  ; face.bin
LDADR_FILE2 = $5000  ; 2000 empty.bin
LDADR_FILE3 = $5000  ; hate0400.bin
LDADR_FILE4 = $5000  ; hated800.bin
LDADR_FILE5 = $5000  ; hate01bmp.bin
LDADR_FILE6 = $5000  ; hatebmp.bin
LDADR_FILE7 = $5000  ; 0400 fillcolor.bin
LDADR_FILE8 = $0801  ; sfx.prg

LENBLK_FILE0 = 12   ; LENGTH BLOCKS OF FILE : jack.bin
LENBLK_FILE1 = 20   ; LENGTH BLOCKS OF FILE : face.bin
LENBLK_FILE2 = 1   ; LENGTH BLOCKS OF FILE : 2000 empty.bin
LENBLK_FILE3 = 2   ; LENGTH BLOCKS OF FILE : hate0400.bin
LENBLK_FILE4 = 2   ; LENGTH BLOCKS OF FILE : hated800.bin
LENBLK_FILE5 = 6   ; LENGTH BLOCKS OF FILE : hate01bmp.bin
LENBLK_FILE6 = 10   ; LENGTH BLOCKS OF FILE : hatebmp.bin
LENBLK_FILE7 = 1   ; LENGTH BLOCKS OF FILE : 0400 fillcolor.bin
LENBLK_FILE8 = 17   ; LENGTH BLOCKS OF FILE : sfx.prg
