;-------------------------------------------
!TO "main.prg" , CBM
;========

init_msx = $1000
cnt_msx = $1003

          *= $0801
;========================================================
;=  sample 6a - faststream loader use and fly decrunch  =
;========================================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------
          cld
          cli
          jsr proc1 ;install drivecode
          jsr init_irq
          jsr enable_hires ;prepare hires mode

;===========================
;= here using a decruncher =		
;===========================
restart
          LDA #<file1info
          LDY #>file1info
          JSR proc2
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file2info
          LDY #>file2info
          JSR proc2
          BCS *		;LOAD ERROR - DEMO CRASHED 
                       

          LDA #<file1info 
          LDY #>file1info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED     
          lda file1info
          ldy file1info+1
          jsr proc2+6

                       
          LDA #<file2info
          LDY #>file2info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED     
          lda file2info
          ldy file2info+1
          jsr proc2+6

          LDA #<file3info 
          LDY #>file3info
          JSR proc2
          BCS *	  

		lda #$35
		sta $01
		lda #$d8
		sta $d016
		
		; WATCH !!
		lda #$34 ;this is value for decruncher !
		sta $01

          LDA #<file4info 
          LDY #>file4info
          JSR proc2
          BCS *	  

		; !!! WATCH !!!
		lda #$35 ;decruncher value 
		sta $01
		
		;this file will be loaded int $5000
		;and fly decrunched int COLOR RAM !!!
		;taht's why $01 value set to #$35
          LDA #<file5info 
          LDY #>file5info
          JSR proc2
          BCS *	  	
          
          
          lda #$34 ;DON'T NEED - REST FILES IS LOADED TO RAM
          sta $01
          
          LDA #<file6info 
          LDY #>file6info
          JSR proc2
          BCS *	 
           
          LDA #<file7info 
          LDY #>file7info
          JSR proc2
          BCS *	           	
;===========================
;= 
;===========================	
		
		lda #$35
		sta $01
          lda $dc01
          cmp #$ef
          bne endwork
;======================

          LDA #<file3info 
          LDY #>file3info
          JSR proc2
          BCS *	 
          LDA #<file8info 
          LDY #>file8info
          JSR proc2
          BCS *	      
          
          lda #$c8
          sta $d016     

;======================
		jmp restart
endwork          
          
          jsr disable_loader
          
          lda #$37
          sta $01
     
          lda #$00
          sta $c6
		
          jsr $ffe4
          beq *-3
          lda #$ea
          ldx #$31
          sei
          stx $0314
          sta $0315

          jsr $fda3
          jsr $ff5b ;init vic and return to basic
          cli
          rts

;===================
;*************************
irq
          pha
          txa
          pha
          tya
          pha

          lda $01
          pha
          lda #$35
          sta $01

          jsr cnt_msx


          bit $dc0d
          pla
          sta $01

          pla
          tay
          pla
          tax
          pla
nmiex
          rti
;--------
;*************************
irq37
          jsr cnt_msx
          jmp $ea31
;*************************
          !source "sample 6a.INC" ;infodata from trackmolinker script
;---
file1info
BASEADR		!WORD LDADR_FILE0 ;$2000 is there
STRTTRK		!BYTE STARTTRCK_FILE0
STRTSEC		!BYTE STARTSECT_FILE0
HOW_GET_LD	!BYTE $00 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---

;---
file2info
!WORD $5000
!BYTE STARTTRCK_FILE1
!BYTE STARTSECT_FILE1
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
;---
file3info
!WORD LDADR_FILE2 ;$5000 is there
!BYTE STARTTRCK_FILE2
!BYTE STARTSECT_FILE2
!BYTE $00 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file4info
!WORD LDADR_FILE3 ;$5000 is there
!BYTE STARTTRCK_FILE3
!BYTE STARTSECT_FILE3
!BYTE $00 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file5info
!WORD LDADR_FILE4 ;$5000 is there
!BYTE STARTTRCK_FILE4
!BYTE STARTSECT_FILE4
!BYTE $00 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file6info
!WORD LDADR_FILE5 ;$5000 is there
!BYTE STARTTRCK_FILE5
!BYTE STARTSECT_FILE5
!BYTE $00 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file7info
!WORD LDADR_FILE6 ;$5000 is there
!BYTE STARTTRCK_FILE6
!BYTE STARTSECT_FILE6
!BYTE $00 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file8info
!WORD LDADR_FILE7 ;$5000 is there
!BYTE STARTTRCK_FILE7
!BYTE STARTSECT_FILE7
!BYTE $00 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
;===================
init_irq

         
          lda #$00
          tax
          tay
          jsr init_msx
          
          lda #<nmiex
          sta $fffa
          lda #>nmiex
          sta $fffb
         
          lda #<irq
          sta $fffe
          lda #>irq
          sta $ffff



          lda #$c7
          sta $dc04
          lda #$4c
          sta $dc05
          bit $dc0d
          lda #$35
          sta $01
          lda #<irq37
          sta $0314
          lda #>irq37
          sta $0315
          
          
          rts
;===================

enable_hires
		
          ldy #$00
          sty $6a
          ldx #$20
          stx $6b
          tya
		
l0        sta ($6a),y
          iny
          bne l0
          inc $6b
          dex
          bne l0

          lda #$50
          sta $d020
		sta $d021
		
          ldx #$04
          stx $6b
		
		
l1        sta ($6a),y
          iny
          bne l1
          inc $6b
          dex
          bne l1

          lda #$3b
          sta $d011

          lda #$03
          sta $dd00
		
          lda #$18
          sta $d018
          lda #$c8
          sta $d016

          rts		
;===================
disable_loader
          lda $01
          pha
          lda #$35
          sta $01
          jsr SENDDRIV
          jsr SENDDRIV          
          lda #$00
          jsr SENDDRIV
          pla
          sta $01
          rts
;---
SENDDRIV       ;SEND ONE BYTE
;---
          SEC
          ROR
          TAX
          LDA $DD02
          ORA #$30
          STA $DD02

          BIT $DD00
          BVC *-3
KEX
          LDA $DD02
          EOR #$20
          ORA #$10
          BCC *+4
          AND #$EF
          STA $DD02
          TXA
          LSR
          TAX
          BNE KEX
          LDA $DD02
          AND #$0F
          ORA #$10
          STA $DD02
;---	
          RTS
;===================
          *= $1000
          !binary "MYMSX.PRG",,2 
;===================
; now we use a fast version of decruncher
		*= $1b00
proc2
          !binary "7ldr1b00.prg",,2
;-------------------------------

;===================
;a stub installer of loader		
;===================
		*= $4000
proc1
;---
		!binary "7inst4000.prg",,2 ;without load address
;===================

;===================

;===================
;both stubs files generated from relocator of bongo linking engine - page loaders
;of course You can do compile this from source code as well