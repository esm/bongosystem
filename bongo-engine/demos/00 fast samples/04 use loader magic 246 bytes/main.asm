;-------------------------------------------
!TO "main.prg" , CBM
;========
loadfile = $ff00 ;why $ff00? - why not? :-)
init_msx = $1000
cnt_msx = $1003

          *= $0801
;================================
;= sample 4 - simple loader use =
;================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------

          cld
          cli
          jsr proc1 ;install drivecode
          jsr init_irq
          jsr enable_hires ;prepare hires mode

;===========================
;= here using a decruncher =		
;===========================
restart
          LDA #<file1info
          LDY #>file1info
          JSR loadfile
          BCS *		;LOAD ERROR - DEMO CRASHED     
          LDA #<file2info
          LDY #>file2info
          JSR loadfile
          BCS *		;LOAD ERROR - DEMO CRASHED     

;===========================
;= 
;===========================	
		
          lda $dc01
          cmp #$ef
          beq restart ;loop if space
          
          jsr disable_loader
          
          lda #$37
          sta $01
     
          lda #$00
          sta $c6
		
          jsr $ffe4
          beq *-3
          lda #$ea
          ldx #$31
          sei
          stx $0314
          sta $0315

          jsr $fda3
          jsr $ff5b ;init vic and return to basic
          cli
          rts

;===================
;*************************
irq
          pha
          txa
          pha
          tya
          pha

          lda $01
          pha
          lda #$35
          sta $01

          jsr cnt_msx


          bit $dc0d
          pla
          sta $01

          pla
          tay
          pla
          tax
          pla
nmiex
          rti
;--------
;*************************
irq37
          jsr cnt_msx
          jmp $ea31
;*************************
          !source "4th sample.INC" ;infodata from trackmolinker script
;---
file1info
BASEADR		!WORD LDADR_FILE0 ;$2000 is there
STRTTRK		!BYTE STARTTRCK_FILE0
STRTSEC		!BYTE STARTSECT_FILE0
HOW_GET_LD	!BYTE $00 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---

;---
file2info
!WORD $2000
!BYTE STARTTRCK_FILE1
!BYTE STARTSECT_FILE1
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---

;===================
init_irq

          ldx #$00
          
          lda loadercode,x ;relocate loader code into a target place
          sta $ff00,x
          inx
          bne *-7
          
          txa
          tay
          jsr init_msx
          
          lda #<nmiex
          sta $fffa
          lda #>nmiex
          sta $fffb
         
          lda #<irq
          sta $fffe
          lda #>irq
          sta $ffff



          lda #$c7
          sta $dc04
          lda #$4c
          sta $dc05
          bit $dc0d
          lda #$35
          sta $01
          lda #<irq37
          sta $0314
          lda #>irq37
          sta $0315
          rts
;===================

enable_hires
		
          ldy #$00
          sty $6a
          ldx #$20
          stx $6b
          tya
		
l0        sta ($6a),y
          iny
          bne l0
          inc $6b
          dex
          bne l0

          lda #$fb
          sta $d020
		
          ldx #$04
          stx $6b
		
		
l1        sta ($6a),y
          iny
          bne l1
          inc $6b
          dex
          bne l1

          lda #$3b
          sta $d011

          lda #$03
          sta $dd00
		
          lda #$18
          sta $d018
          lda #$c8
          sta $d016

          rts		
;===================
          *= $1000
          !binary "MYMSX.PRG",,2 
;-------------------------------
disable_loader
          lda $01
          pha
          lda #$35
          sta $01
          jsr SENDDRIV
          jsr SENDDRIV          
          lda #$00
          jsr SENDDRIV
          pla
          sta $01
          rts
;---
SENDDRIV       ;SEND ONE BYTE
;---
          SEC
          ROR
          TAX
          LDA $DD02
          ORA #$30
          STA $DD02

          BIT $DD00
          BVC *-3
KEX
          LDA $DD02
          EOR #$20
          ORA #$10
          BCC *+4
          AND #$EF
          STA $DD02
          TXA
          LSR
          TAX
          BNE KEX
          LDA $DD02
          AND #$0F
          ORA #$10
          STA $DD02
;---	
          RTS
;===================
;a stub installer of loader		
;===================
		*= $4000
proc1
;---
		!binary "4inst4000.prg",,2 ;without load address
;===================
loadercode
;===================
          !binary "4ldrFF00.prg",,2
;===================
;both stubs files generated from relocator of bongo linking engine - page loaders
;of course You can do compile this from source code as well