;-------------------------------------------
!TO "main.prg" , CBM
;========
loadfile = $fd00 
init_msx = $1000
cnt_msx = $1003

          *= $0801
;========================================
;= sample 4c - deterministic loader use =
;========================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------

          cld
          cli
          jsr proc1 ;install drivecode
          jsr init_irq
          jsr enable_hires ;prepare hires mode

;===========================
;= here using a decruncher =		
;===========================
restart
          LDA #<lenfile1
          LDY #>lenfile1
          JSR loadfile
          BCS *		;LOAD ERROR - DEMO CRASHED     
          LDA #<lenfile2
          LDY #>lenfile2
          JSR loadfile
          BCS *		;LOAD ERROR - DEMO CRASHED     

;===========================
;= 
;===========================	
		
          lda $dc01
          cmp #$ef
          beq restart ;loop if space
          
          jsr disable_loader
          
          lda #$37
          sta $01
     
          lda #$00
          sta $c6
		
          jsr $ffe4
          beq *-3
          lda #$ea
          ldx #$31
          sei
          stx $0314
          sta $0315

          jsr $fda3
          jsr $ff5b ;init vic and return to basic
          cli
          rts

;===================
;*************************
irq
          pha
          txa
          pha
          tya
          pha

          lda $01
          pha
          lda #$35
          sta $01

          jsr cnt_msx


          bit $dc0d
          pla
          sta $01

          pla
          tay
          pla
          tax
          pla
nmiex
          rti
;--------
;*************************
irq37
          jsr cnt_msx
          jmp $ea31
;*************************
          !source "4c sample.INC" ;infodata from trackmolinker script
;---
file1info
lenfile1		!BYTE LENBLK_FILE0
BASEADR		!WORD LDADR_FILE0 ;$2000 is there
STRTTRK		!BYTE STARTTRCK_FILE0
STRTSEC		!BYTE STARTSECT_FILE0
;---

;---
file2info
lenfile2		
!BYTE LENBLK_FILE1
!WORD $2000
!BYTE STARTTRCK_FILE1
!BYTE STARTSECT_FILE1
;---

;===================
init_irq

          ldx #$00
          
          lda loadercode,x ;relocate loader code into a target place
          sta $fd00,x
          lda loadercode+$0100,x ;relocate loader code into a target place
          sta $fe00,x
          lda loadercode+$0200,x ;relocate loader code into a target place
          sta $ff00,x
          inx
          bne *-19
          
          txa
          tay
          jsr init_msx
          
          lda #<nmiex
          sta $fffa
          lda #>nmiex
          sta $fffb
         
          lda #<irq
          sta $fffe
          lda #>irq
          sta $ffff



          lda #$c7
          sta $dc04
          lda #$4c
          sta $dc05
          bit $dc0d
          lda #$35
          sta $01
          lda #<irq37
          sta $0314
          lda #>irq37
          sta $0315
          rts
;===================

enable_hires
		
          ldy #$00
          sty $6a
          ldx #$20
          stx $6b
          tya
		
l0        sta ($6a),y
          iny
          bne l0
          inc $6b
          dex
          bne l0

          lda #$fb
          sta $d020
		
          ldx #$04
          stx $6b
		
		
l1        sta ($6a),y
          iny
          bne l1
          inc $6b
          dex
          bne l1

          lda #$3b
          sta $d011

          lda #$03
          sta $dd00
		
          lda #$18
          sta $d018
          lda #$c8
          sta $d016

          rts		
;===================
          *= $1000
          !binary "MYMSX.PRG",,2 
;-------------------------------
disable_loader
          lda $01
          pha
          lda #$35
          sta $01
          jsr loadfile+3
          pla
          sta $01
          rts
;---
;===================
;a stub installer of loader		
;===================
		*= $4000
proc1
;---
		!binary "1inst4000.prg",,2 ;without load address
;===================
loadercode
;===================
          !binary "1LdrFd00.prg",,2
;===================
;both stubs files generated from relocator of bongo linking engine - page loaders
;of course You can do compile this from source code as well