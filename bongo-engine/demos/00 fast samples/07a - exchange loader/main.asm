;-------------------------------------------
!TO "main.prg" , CBM
;========

init_msx = $1000
cnt_msx = $1003
disable_loader = proc2+9

          *= $0801
;================================
;=  sample 7a - exchange loader  =
;================================
          !byte $0B,$08,$90,$06,$9E,$32
          !byte $30,$34,$39,$00,$A0,$00
;--------
          cld
          cli
          jsr proc1 ;install drivecode
          jsr init_irq
          jsr enable_hires ;prepare hires mode

;===========================
;= here using a decruncher =		
;===========================
restart

          jsr load_demo                      
		jsr replace_to_nointerleave
		jsr load_demo

;===========================
;= 
;===========================	
		
          lda $dc01
          cmp #$ef
          bne endwork
		jsr copy_stubs
		jsr $5000
          jmp restart ;loop if space

endwork          
          
          jsr disable_loader
          jsr copy_stubs
          
          lda #$37
          sta $01
     
          lda #$00
          sta $c6
		
          jsr $ffe4
          beq *-3
          lda #$ea
          ldx #$31
          sei
          stx $0314
          sta $0315

          jsr $fda3
          jsr $ff5b ;init vic and return to basic
          cli
          rts
;=======================
load_demo
          LDA #<file1info
          LDY #>file1info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file2info
          LDY #>file2info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED 

          rts
;=======================
replace_to_nointerleave
          LDA #<file3info
          LDY #>file3info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED  

          LDA #<file4info
          LDY #>file4info
          JSR proc2+3
          BCS *		;LOAD ERROR - DEMO CRASHED 
          ;jmp exchg_ldr
;===================
exchg_ldr
		lda $01
		pha
		lda #$35
		sta $01
		
		ldx #5
		ldy #0
		lda #$5b
		sta exchg2+2
		lda #$1b
		sta exchg2+5
		
exchg2    lda $5b00,y
		sta $1b00,y
		iny
		bne exchg2
		inc exchg2+2
		inc exchg2+5
		dex
		bne exchg2
		
		jsr $5000
		
		pla
		sta $01
		rts
;===================
;*************************
irq
          pha
          txa
          pha
          tya
          pha

          lda $01
          pha
          lda #$35
          sta $01

          jsr cnt_msx


          bit $dc0d
          pla
          sta $01

          pla
          tay
          pla
          tax
          pla
nmiex
          rti
;--------
;*************************
irq37
          jsr cnt_msx
          jmp $ea31
;*************************
          !source "sample 7a.INC" ;infodata from trackmolinker script
;---
file1info ;jack
BASEADR		!WORD LDADR_FILE0 ;$2000 is there
STRTTRK		!BYTE STARTTRCK_FILE0
STRTSEC		!BYTE STARTSECT_FILE0
HOW_GET_LD	!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file2info ;face
!WORD $2000
!BYTE STARTTRCK_FILE1
!BYTE STARTSECT_FILE1
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file3info ;exchgldr2
!WORD $5000
!BYTE STARTTRCK_FILE2
!BYTE STARTSECT_FILE2
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
file4info ;6ldr1b00
!WORD $5b00
!BYTE STARTTRCK_FILE3
!BYTE STARTSECT_FILE3
!BYTE $01 ; IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---
;===================
init_irq

         
          lda #$00
          tax
          tay
          jsr init_msx
          
          lda #<nmiex
          sta $fffa
          lda #>nmiex
          sta $fffb
         
          lda #<irq
          sta $fffe
          lda #>irq
          sta $ffff



          lda #$c7
          sta $dc04
          lda #$4c
          sta $dc05
          bit $dc0d
          lda #$35
          sta $01
          lda #<irq37
          sta $0314
          lda #>irq37
          sta $0315
          
          
copy_stubs
		
		ldx #4
		ldy #$00
		lda #>stub1
		sta cst1+2
		lda #>$1b00
		sta cst1+5
		
cst1		lda stub1,y
		sta $1b00,y
		iny
		bne cst1
		inc cst1+2
		inc cst1+5
		dex
		bne cst1
		
		ldx #4
		
		
		lda #>stub2
		sta cst2+2
		lda #>$5000
		sta cst2+5
		
cst2		lda stub2,y
		sta $5000,y
		iny
		bne cst2
		inc cst2+2
		inc cst2+5
		dex
		bne cst2
				          
          rts
;===================

enable_hires
		
          ldy #$00
          sty $6a
          ldx #$20
          stx $6b
          tya
		
l0        sta ($6a),y
          iny
          bne l0
          inc $6b
          dex
          bne l0

          lda #$fb
          sta $d020
		
          ldx #$04
          stx $6b
		
		
l1        sta ($6a),y
          iny
          bne l1
          inc $6b
          dex
          bne l1

          lda #$3b
          sta $d011

          lda #$03
          sta $dd00
		
          lda #$18
          sta $d018
          lda #$c8
          sta $d016

          rts		
;===================

;===================
          *= $1000
          !binary "MYMSX.PRG",,2 
;===================
; now we use a fast version of decruncher
		*= $1b00
proc2

;-------------------------------

;===================
;a stub installer of loader		
;===================
		*= $4000
proc1
;---
		!binary "8inst4000.prg",,2 ;without load address
;===================
stub1
          !binary "8ldr1b00.prg",,2          
;===================
stub2
          !binary "exchgldr4.prg",,2
;===================
;both stubs files generated from relocator of bongo linking engine - page loaders
;of course You can do compile this from source code as well