;************
!to "demo.prg" , CBM

proc2 = $0200
flydecrynch = proc2
only_load = proc2+3
only_decrunch = proc2+6
disable_loader = proc2+9

sinus_data = $1b00
tabels = sinus_data-$0100
startloop = sinus_data+$0200 ;a fpp speedcode loop

init_msx = $1000
cnt_msx  = $1003

fpplines = 199
;====


                  *= $0801
;================================
;simple demo from bongo linking engine
; (c) by wegi in 2013.03.24
;================================
         !byte $0b,$08,$90,$06,$9e,$32
         !byte $30,$34,$39,$00,$a0,$00

          sei
          cld
      
          ldx #$ff
          txs
          jsr $fda3
          jsr wait_frame
          lda #$00
          sta $d020
          sta $d021
          sta $d011
          sta $dd00
          cli

          LDA #<NMIEX
          STA $FFFA
          LDA #>NMIEX
          STA $FFFB

         
          LDA #<IRQ
          STA $FFFE
          LDA #>IRQ
          STA $FFFF
          LDA #$C7
          STA $DC04
          LDA #$4C
          STA $DC05


          jsr $1000 ;install loader

          lda #$35
          sta $01
          
          lda #<info_file0
          ldy #>info_file0
          jsr $1b00 ;first load and decrunch loader at $0200

          lda #<info_file1
          ldy #>info_file1
          jsr flydecrynch ;load and decrunch 512bytes sine table at $1b00
          
          lda #<info_file2
          ldy #>info_file2
          jsr flydecrynch ;load and decrunch msx


          dec $01 ;value for decruncher - remember about this when decruch under ROM !!!
          lda #<info_file3
          ldy #>info_file3
          jsr flydecrynch ;load and decrunch empty fpp data into $c000
          inc $01


          lda #<info_file4
          ldy #>info_file4
          jsr flydecrynch ;load and decrunch fpp data into $4000


          lda #$00
          tax
          tay
          jsr init_msx



          sei
          lda #$7f
          sta $dc0d
          sta $dd0d
          bit $dc0d
          bit $dd0d


          jmp runbonus

;--------
;===
loopdata2
          lda tabels 
          sta $d018
          ldx #$18
          stx $d011
          lda #$00
          sta $dd00
;===
loopdata
          lda tabels   
          sta $d018
          ldx #$18
          stx $d011
          lda #$00
          sta $dd00
;===
IRQ
          pha
          lda $01
          pha
          lda #$35
          sta $01
          bit $dc0d
          pla
          sta $01
          pla
NMIEX     rti
;========

;---
info_file0
          !word $8000              ;load address
          !byte STARTTRCK_FILE0    ;start track
          !byte STARTSECT_FILE0    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
info_file1
          !word $8000              ;load address
          !byte STARTTRCK_FILE1    ;start track
          !byte STARTSECT_FILE1    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
info_file2
          !word $8000              ;load address
          !byte STARTTRCK_FILE2    ;start track
          !byte STARTSECT_FILE2    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
info_file3
          !word $8000              ;load address
          !byte STARTTRCK_FILE3    ;start track
          !byte STARTSECT_FILE3    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
info_file4
          !word $8000              ;load address
          !byte STARTTRCK_FILE4    ;start track
          !byte STARTSECT_FILE4    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
info_file5
          !word $8000              ;load address
          !byte STARTTRCK_FILE5    ;start track
          !byte STARTSECT_FILE5    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
info_file6
          !word $8000              ;load address
          !byte STARTTRCK_FILE6    ;start track
          !byte STARTSECT_FILE6    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
info_file7
          !word $8000              ;load address
          !byte STARTTRCK_FILE7    ;start track
          !byte STARTSECT_FILE7    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
info_file8
          !word $8000              ;load address
          !byte STARTTRCK_FILE8    ;start track
          !byte STARTSECT_FILE8    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
info_file9
          !word $8000              ;load address
          !byte STARTTRCK_FILE9    ;start track
          !byte STARTSECT_FILE9    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
info_file10
          !word $8000              ;load address
          !byte STARTTRCK_FILE10    ;start track
          !byte STARTSECT_FILE10    ;start sector
          !byte $01                ;if <> 0 then load addres from infodata - not from file
;---
;========

*= $0a00
runbonus
          
runtimer
          lda #$00
          sta $dc05
          lda #8
          sta $dc04
misio
          ldy #$11
          cpy $d012
          bne *-3
          dey
          nop
mysinc
          jsr opp48
          nop
          iny
          bit $ea
          cpy $d012
          bne mysinc

          bit $ea
          lda #$11
          sta $dc0e
;====
          lda #$09
          ldx #$00
-:
          sta $d800,x
          sta $d900,x
          sta $da00,x
          sta $dae8,x
          inx
          bne -

          lda #<fppirq
          sta $fffe
          lda #>fppirq
          sta $ffff


          bit $dc0d
          
          lda #$00
          sta vbank
          sta loopdata+12
          jsr makespcode

          jsr initgraph
          
          cli



          lda #$00
          sta vbank
          lda #$02
          sta loopdata+12
          jsr makespcode

          lda #<info_file5
          ldy #>info_file5
          dec $01
          jsr flydecrynch ;load and decrunch fpp data
          inc $01
         

          lda #$00
          sta loopdata+12
          jsr makespcode

          lda #<info_file6
          ldy #>info_file6
          dec $01
          jsr flydecrynch ;load and decrunch fpp data
          inc $01
          
          lda #$02
          sta loopdata+12
          jsr makespcode
          
          lda #<info_file7
          ldy #>info_file7
          dec $01
          jsr flydecrynch ;load and decrunch fpp data
          inc $01
          
          lda #$00
          sta loopdata+12
          jsr makespcode

          lda #<info_file4
          ldy #>info_file4
          dec $01
          jsr flydecrynch ;load and decrunch fpp data
          inc $01

          lda #$02
          sta vbank
          jsr makespcode
          
          lda #$00
          sta vbank
          lda #$02
          sta loopdata+12
          jsr makespcode
          
          lda #<info_file5
          ldy #>info_file5
          dec $01
          jsr flydecrynch ;load and decrunch fpp data
          inc $01

          lda #$00
          sta loopdata+12
          jsr makespcode
          
          lda #<info_file8
          ldy #>info_file8
          dec $01
          jsr flydecrynch ;load and decrunch fpp data
          inc $01
          
          lda #$02
          sta vbank
          jsr makespcode
          
          lda #$00
          sta vbank
          lda #$02
          sta loopdata+12
          jsr makespcode
          
          lda #$20
          cmp $d012
          bne *-3
          
          lda #<simpleirq
          sta $fffe
          lda #>simpleirq
          sta $ffff
          
          lda #$c8
          sta $d016
          lda #$3b
          sta $d011
          lda #$08
          sta $d018
          lda #$02
          sta $dd00
          
          lda #$50
          ldx #$00
-:        sta $4000,x
          sta $4100,x
          sta $4200,x
          sta $42e8,x
          inx
          bne -
          
          lda #<info_file9
          ldy #>info_file9
          jsr flydecrynch ;load and decrunch bongo linking engine picture
          
          
          lda #<info_file10
          ldy #>info_file10
          jsr only_load ;load and decrunch jack picture
          
          jsr disable_loader
          
          lda #<$8000
          ldy #>$8000
          jsr only_decrunch
          


          jmp *
end_of_demo        
;***
simpleirq
          pha
          tya
          pha
          txa
          pha
          lda $01
          pha
          lda #$35
          sta $01
          inc $d019
          jsr cnt_msx
          jmp endirq
;***
;===========          
*= $0c00          
fppirq
          pha
          tya
          pha
          txa
          pha
          lda $01
          pha
          lda #$35
          sta $01
          inc $d019

          bit $d011
          bmi *-3

          lda #$1b
          sta $d011
          lda #$00
          sta $dd00
		       
          lda #$2e 

          cmp $d012
          bne *-3

          lda $dc04
          and #$07
          eor #$07
          sta *+4

          bpl *+2
          cmp #$c9
          cmp #$c9
          cmp #$c9
          bit $ea24

          ldy #16
          dey
          bne *-1
          bit $ea
          jmp startloop

loopcount

          lda #$00
          sta $d018
          sta $d011   
          jsr addsinus
          jsr $1003
endirq    pla
          sta $01
          pla
          tax
          pla
          tay
          pla
          rti
;=========
addsinus

          lda $dc01
          cmp #$ef
          bne +
          rts
+:
          ldx #$00
          ldy #$00
-:
mzm2 = *+1
          lda sinus_data,x

mzm3 = *+1
          adc sinus_data,y
          sta tabels,y
          inx
          iny
mzm5 =*+1
          lda sinus_data,x
          adc sinus_data,y
          sta tabels,y
          iny
          cpy #$c8
          bne -

mzm1 = *+1
          lda #$00
          clc
          adc #$02
          sta mzm1
          sta mzm2

mzm4 = *+1
          lda #$00
          clc
          adc #$02
          sta mzm4
          sta mzm5
          rts
;=========
wait_frame
          bit $d011
          bpl *-3
          bit $d011
          bmi *-3
          rts
;---
opp48
          jsr epp
opp36     jsr epp
          jsr epp
epp
          rts
;====
initgraph
          jsr wait_frame
          ldx #$1e
-:
          lda tabvic,x
          sta $d010,x
          dex
          bpl - 
          rts
;---
eof
;===
storedata
          sta $1111
          inc storedata+1
          bne *+5
          inc storedata+2
          rts
;---
vbank	!byte 0
;---
makespcode

          ldx #loopdata-loopdata2-1
          lda loopdata,x
          sta loopdata2,x
          dex
          bpl *-7
          lda #<startloop
          sta storedata +1
          lda #>startloop
          sta storedata +2
          
          ldy #fpplines

msp1
          ldx #$00
msp2      lda loopdata2,x
          jsr storedata
          inx
          cpx #loopdata-loopdata2
          bne msp2
          inc loopdata2+1
          inc loopdata2+7
          lda loopdata2+7
          and #$07
          ora #$18
          sta loopdata2+7
          lda loopdata2+12
          eor vbank
          sta loopdata2+12
		
          dey
          bne msp1
          lda #$4c
          jsr storedata
          lda #<loopcount
          jsr storedata
          lda #>loopcount
          jmp storedata
;=====
;---
tabvic
          ;!byte $00,$00,$00,$00,$00,$00,$00,$00
          ;!byte $00,$00,$00,$00,$00,$00,$00,$00
          !byte $80,$0b,$2b,$00,$00,$00,$d8,$00
          !byte $17,$01,$01,$00,$ff,$00,$00,$00
          !byte $00,$00,$0c,$0f,$03,$03,$0b,$0f
          !byte $0f,$0f,$0f,$0f,$0f,$0f,$0f
;---
end_code
;========

          *= $1000
          !binary "7inst1000.prg",,2
          *= $1b00
          !binary "7ldr1B00.prg",,2
;========
;**********************
end_data




!source "data\09 simple demo.INC"

