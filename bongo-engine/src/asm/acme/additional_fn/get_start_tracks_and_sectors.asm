
;================================
;GET_START_TRACKS_AND_SECTORS ADDITIONAL FUNCTION FOR:
;THE BONGO LOADERS WHERE D64 MAKED TRACKMOLINKER WITH "-DIRENT" OPTION
;NOT FOR DETERMINISTIC LOADER TYPE
; (C) BY WEGI
;================================
!TO "get_start_tracks_and_sectors.prg" , CBM
;===
          * = $4600
;===

;----
;input:
;
;acc/yreg lo/hi byte of pointer to start tracks and sectors table
;===
;
;output:
; carry = 1 - error  0 prg files in dir
; carry = 0 , acc how many prg (or user type selected) files, start tracks and sectors data stored in tracks_data buffer
;
;----

;===============
GET_START_TRACKS_AND_SECTORS
          JMP GSTAS_2
;=========
SENDDRV       ;SEND ONE BYTE
;---
          SEC
          ROR
          TAX
          LDA $DD02
          ORA #$30
          STA $DD02

          BIT $DD00
          BVC *-3
-:
          LDA $DD02
          EOR #$20
          ORA #$10
          BCC *+4
          AND #$EF
          STA $DD02
          TXA
          LSR
          TAX
          BNE -
          LDA $DD02
          AND #$0F
          ORA #$10
          STA $DD02
;---	
          RTS	
;---
GET_ONE_BYTE
          BIT $DD00
          BVC *-3
          LDX #$3F
          STX $DD02
          STY RCALL
          BIT $EA
          LDY #$37
          NOP
          LDA $DD00
          STY $DD02
          LSR
          LSR
          NOP
          NOP
          BIT $EA
          ORA $DD00
          STX $DD02

          LSR
          LSR
          LSR
          LSR
          STA HALFBYTE
          LDA $DD00
          STY $DD02
;---
RCALL    = *+1
          LDY #$00
;---
          NOP
          LDX #$1F
          LSR
          LSR
MY_MANY   = *+1
          CPY #$FF
          ORA $DD00
          STX $DD02
          AND #$F0
HALFBYTE = *+1
          ORA #$00
;---
          RTS
;---------

GSTAS_2

          sta store_vector
          sty store_vector + 1
          lda #$00
          sta many_files
          
          
          LDA $01
          PHA
          lda #$35
          sta $01


          LDY #$00
-:
          LDA CODESEND,Y
          JSR SENDDRV   ;SEND JOB CODE TO LOADER
          INY 
          CPY #$06
          BNE -

          LDY #$00
-:
          LDA CODESEND2,Y  ;SEND OUR EXTRA CODE TO DRIVE
          JSR SENDDRV
          INY 
          BNE -
-:
          LDA CODESEND2+$0100,Y  ;SEND OUR EXTRA CODE TO DRIVE
          JSR SENDDRV
          INY 
          BNE -

-:        jsr GET_ONE_BYTE ;get one block from drive
          sta drive_data,y
          iny
          bne -

-:        jsr GET_ONE_BYTE
          cmp #144
          bcs +
          INC many_files

          jsr GET_ONE_BYTE
          jsr store_data
          jsr GET_ONE_BYTE
          jsr store_data
          jmp -


+:        LDY #$00
-:
          LDA drive_data,Y  ;SEND OUR EXTRA CODE TO DRIVE
          JSR SENDDRV
          INY 
          BNE -

          PLA
          STA $01
          sec
          lda many_files
          beq *+3
          clc
          RTS
;=============
store_data
          dec $01
store_vector = *+1
          sta $1111
          inc store_vector
          bne +
          inc store_vector+1
+:        inc $01
          rts
many_files !byte 0          
;=============
;---
CODESEND
;---
;GET_TO_RAM PROTOCOL
!byte 0	;LOW BYTE TARGET ADDRES IN DRIVE TO PUT DATA
!byte 6	;HI BYTE ($0600)
!byte 4	;JOB NR ALLWAYS 4
!byte 0   ;LOW BYTE TO RUN PROG
!byte 6   ;HI BYTE ($0600)
!byte 0   ;BYTES TO SEND 0 = 256
;---
CODESEND2
;---
!pseudopc $0600
;---
;=============
bufer1 = $0300
fvec = $08
;=============
          SEI
          JMP CONT_01         
GET_BT
          LDA #$80
          STA $1800
          LDX $1800
          BNE *-3

-:        CPX $1800
          BEQ *-3
          LDX $1800
          CPX #4
          ROR
          BCC -
		
          LDX #$0D
          STX $1800
          CPX $1800
          BNE *-3
          LDX #$08
          STX $1800
          RTS
;---------
BIN2SER  
          !BYTE $0F,$07,$0D,$05,$0B,$03,$09,$01
          !BYTE $0E,$06,$0C,$04,$0A,$02,$08,$00
;---------
SEND_ONE_BYTE
          TAX
          LSR
          LSR
          LSR
          LSR
          STA MY_BYTE
          TXA
          AND #$0F
          LDX #$00
          STX $1800

          TAX
          LDA BIN2SER,X

          LDX $1800
          BNE *-3
          STA $1800
          ASL
          ORA #$10
          BIT $1800
          BPL *-3

          STA $1800

MY_BYTE = *+1
          LDX #$00
          NOP
          LDA BIN2SER,X

          BIT $1800
          BMI *-3
          STA $1800
          ASL
          ORA #$10
          BIT $1800
          BPL *-3
          STA $1800
          LDA #$18
          BIT $1800
          BMI *-3
          STA $1800
          RTS
;---------
CONT_01
          LDY #$00
-:        JSR GET_BT
          STA $0700,Y
          INY
          BNE -
          
-:        lda $0300,y
          jsr SEND_ONE_BYTE
          INY
          BNE -
          
          LDY #$BA
-:
          LDA $0100,Y ;SAVE DATA FROM $01BA TO $01FF
          STA $0700,Y
          INY
          BNE -

          jsr searchdir

        
          sei
          LDY #$BA
-:
          LDA $0700,Y    ;RESTORE DATA FROM
          STA $0100,Y    ;$01BA - $01FF
          INY
          BNE -
-:        JSR GET_BT
          STA $0300,Y
          INY
          BNE -
;**************
;* EXIT POINT *
;**************
;===
          RTS
;===
;---
;---
;--------
searchdir
         jsr init_disk
         lda #<bufer1
         sta fvec
         lda #>bufer1
         sta fvec+1

onlyprg
         jsr read_sec
         sei
         lda $1c00
         eor #$08
         sta $1c00
lopfind
         ldy #$02
         lda (fvec),y
         bpl nextf
         and #$0f
         ;cmp filetype
         cmp #$02 ;PRG files ,0 DEL, 1 SEQ, 3 USR 
         beq checknext
         ;cmp #$03
         ;beq checknext
nextf
         lda fvec
         clc
         adc #$20
         sta fvec
         bcc lopfind
         lda bufer1+1
         sta $07
         lda bufer1
         bne onlyprg
         beq eof_dir


checknext
          lda filecntr
          jsr SEND_ONE_BYTE
          inc filecntr
          ldy #$03          
          lda (fvec),y
          jsr SEND_ONE_BYTE
          iny
          lda (fvec),y
          jsr SEND_ONE_BYTE          
          jmp nextf

eof_dir   lda #$ff
          jmp SEND_ONE_BYTE
;---------------------------------------
;--------
;tbtype   !byte $04,$13,$10,$15 ;DSPU
;filetype  !byte 2
filecntr  !byte 0
;--------
;---
read_sec
          lda #$80
          jsr INITER2
          bcs *-3
          rts
init_disk
          lda #$12
          sta $06
          lda #$01
          sta $07
          lda #$b0
          jsr INITER2
          bcs *-3
          rts
;---
INITER2
          SEI
          LDX #0
          STA $0298
          JSR $D57D
          LDA #5
          STA $6A
          CLI
          JSR $D599
          CMP #$02
          RTS
;===

END_NEXT_SIDE
!realpc
drive_data
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
          !byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
