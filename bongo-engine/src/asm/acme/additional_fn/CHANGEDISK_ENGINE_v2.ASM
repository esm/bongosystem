;================================
;CHANGEDISK ENGINE FOR:
;THE BONGO LOADERS 
; (C) BY WEGI IN 2013.02.09
;THIS VERSION BASED ON WRITE PROTECTION TEST
;================================
!TO "CHANGEDISK_ENGINE.PRG" , CBM
;===
TRACK_INDICATOR = $01  ; TRACK AND SECTOR WITH DATA OF IDICATOR SIDE
SECT_INDICATOR = $00
;===
MYCHGDSK = $C000
* = MYCHGDSK

;===============
;SENDDRIV2 = PROC2+12
;=========================
; IF YOU NEED CHANGE THE DISK ENGINE BELOW IS THE PROCEDURE
; YOU MUST PUT YOUR UNIQUE DATA (MAX 20BYTES) TO SECTOR
; AND PUT SECTOR AND TRACK NUMBER TO "TRACK_INDICATOR" AND "SECT_INDICATOR" VARIABLE
; AND CALL "JSR SECONDSIDETEST"
;=========================
SECONDSIDETEST

		LDA $01
		PHA
		LDA #$35
		STA $01

		 LDY #$00
-:
		 LDA CODESEND,Y
		 JSR SENDDRIV   ;SEND JOB CODE TO LOADER
		 INY 
		 CPY #$06
		 BNE -

		 LDY #$00
-:
		 LDA CODESEND2,Y  ;SEND OUR EXTRA CODE TO DRIVE
		 JSR SENDDRIV
		 INY 
		 BNE -
		 BIT $DD00    ;WAIT FOR START WORK
		 BVC *-3
		 BIT $DD00    ;WAIT FOR END OF WORK
		 BVS *-3
		
		PLA
		STA $01

		 RTS
;=============
SENDDRIV       ;SEND ONE BYTE
;---
          SEC
          ROR
          TAX
          LDA $DD02
          ORA #$30
          STA $DD02

          BIT $DD00
          BVC *-3
KEX
          LDA $DD02
          EOR #$20
          ORA #$10
          BCC *+4
          AND #$EF
          STA $DD02
          TXA
          LSR
          TAX
          BNE KEX
          LDA $DD02
          AND #$0F
          ORA #$10
          STA $DD02
;---	
          RTS		
;---
CODESEND
;---
;GET_TO_RAM PROTOCOL
!byte 0	;LOW BYTE TARGET ADDRES IN DRIVE TO PUT DATA
!byte 6	;HI BYTE ($0600)
!byte 4	;JOB NR ALLWAYS 4
!byte 0   ;LOW BYTE TO RUN PROG
!byte 6   ;HI BYTE ($0600)
!byte 0   ;BYTES TO SEND 0 = 256
;---
CODESEND2
;---
!pseudopc $0600
;---
        LDY #$BA
-:
        LDA $0100,Y ;SAVE DATA FROM $01BA TO $01FF
        STA $0600,Y
        INY
        BNE -

        STY $1800    ;START WORK - INFO FOR C64
;===
RESTART
         SEI
         LDA $1C00
         AND #%11111011   ;MOTOR STOP
         STA $1C00

         AND #$10
         STA $85

-:	    JSR WRPTEST   ;WAIT 1ST TIME FOR CHANGE WRITE PROTECT
         BEQ -

-:	    JSR WRPTEST
         BNE -

         LDA $1C00   ;RUN MOTOR AND SETLED ON
         ORA #$0C
         STA $1C00
        
         LDX #$00    ;DELAY FOR MOTOR START
         DEY
         BNE *-1
         DEX
         BNE *-4
 
         STX $1C03   ;HEAD TO READ MODE
;===
STARTTEST
        
        SEI
        LDA #$EE
        STA $1C0C      ;SOE TO READ
        
        LDA $1C01      ;WAIT FOR 1ST SYNC = CLOSE KEY
        BIT $1C00
        BMI *-3
        
        LDX #$10       ;WAIT FOR 16 STABILE SYNC
-:
        LDA #$FF
        STA $1805
        LDA $1C01
CZKN
        BIT $1805
        BPL STARTTEST  ;NO STABILE SYNC REPEAT
        BIT $1C00
        BMI CZKN
        DEX
        BNE -
;===        
        CLI

        JSR $D042      ;HEAD MOVE TO TRACK 18, READ BAM, INIT DISK
        
        JSR READSEC    ;READ OUR TYPED SECTOR

        LDY #$04
-:
        LDA MYCOMPARE,Y
        BEQ TEST_OK   
        CMP $0700,Y    ;TEST DATA
        BNE RESTART    ;NO THIS ONE DISK - RESTART
        INY
        BNE -
;===
TEST_OK
        LDA #$08
        STA $1800      ;THE END OF WORK INFO FOR C64
        JSR READSEC    ;ONLY FOR DELAY
        LDY #$BA
-:
        LDA $0600,Y    ;RESTORE DATA FROM
        STA $0100,Y    ;$01BA - $01FF
        INY
        BNE -
        SEI
;**************
;* EXIT POINT *
;**************
;===
WRPTEST  LDA $1C00   ;WAIT 2ND TIME FOR CHANGE WRITE PROTECT
         AND #$10
         CMP $85
         RTS
;===
READSEC
        LDA #TRACK_INDICATOR
        STA $0E
        LDA #SECT_INDICATOR
        STA $0F
        SEC
        ROL $1C
-:        
        LDA #$80    ;READ SECTOR WITH SIDE INDICATOR
        STA $04     ;TO $0700 BUFFER
        CLI
        LDA $04
        BMI *-2
        CMP #$02
        BCS -
        RTS
;------------------
MYCOMPARE = *-4
;---
;OUR UNIQUE DATA OF SIDE INDICATOR
;NO LONGER THAN 20 BYTES! - THAT'S ENOUGHT
!text "BONGOTEST"
!byte 0
;==============================
;		BELOW
; EXAMPLE ASM FILE FOR OUR INDICATOR
; 3 TEXTLINES
;==============================
;* = $0900 ;NOT IMPORTANT
;!text "BONGO SIDE TEST"
;!byte 0
;==============================
END_NEXT_SIDE
!realpc
 
