
;================================
;2ND PROCEDURE OF:
;THE BONGO FAST-STREAM LOADER 
; INTEGRATED WITH :
;*******************************
;*  MMS LEVEL CRUSHER SPEED 6  *
;*******************************
; (C) BY WEGI IN 2013-02-06
;================================

;**********************
;* VERY IMPORTANT !!! *
;**********************

;USE LEVEL CRUSHER WITH A SPEED 6

;=======================================================
; LEVEL CRUSHER GENERATED FILE WITHOUT LOAD ADDRES
; FOR COMPATIBILITY YOU MUST REMAKE THIS FILE AND ADDED
; AT THE START 2 BYTES OF LOAD ADDRESS
;ALSO I'D REMAKE THE ENTRY POINT AND FOR YOUR COMFORTABLE
;YOU DO CRUNCH A PRG FILE WITH LOAD ADDRESS - THIS WILL BE
;AS DEPACK ADDRESS FOR DECRUNCHER
;=======================================================


;******************
;* TECHNICAL DATA *
;******************



;SUPPORT 1541x , 1570/71 IN 1541 MODE [D64 - NOT D71 FILES (DISKS)]
;SHORT TECHNICAL DATA:
;LOADER USE ATN LINE TO SYNC DATA SEND
;LOADER LOADING UNDER I/O AREA
;FOR FASTER LOAD IN 1570/71 USED 2mhz THIS GAVE A DIFFERENT TIMING THAN 1541
; - GET YOUR ATTENTION ON THIS OR REMAKE SOURCE CODE, OR DISABLE 2mhz FROM GUI

;SPRITES, $D011, BADLINES, IRQ, NMI - ALL OF THEM NOT "NOISING" FOR LOADER


;GENERALLY RECOMMENDED LOAD VIA TRAC AND SECTOR - LOAD VIA NAME IS POSSIBLE WHEN YOU USE 
;ADDITIONAL FUNCTION "FIND_BY_FILENAME", BUT THIS GEAVE A BIG AND UNCONFORMTABLE DELAY
;BY JUMP, READ AND FINDING DIR TO TRACK 18 TIME AFTER TIME SO IN THIS CASE YOU MUST
;WASTED MORE BYTES AND TIME... REALLY I'M NOT RECOMMENDED THIS OPTION

;***************
;* CONSTRAINTS *
;***************

;ON THE IEC - ONLY ONE UNITS CAN BE TURNED ON - YOU DRIVE WITH YOUR DEMO DISC 

;PLEASSE BE CARREFULLY ABOUT WRITE TO $DD00 !!!
;DO THIS OR LIKE BELOW:

; ALLWAYS 6 HIBITS MUST BE SET DOWN TO 0
;	LDA #VICBANK ;#0, #1, #2, #3
;	STA $DD00


;ALSO DON'T TOUCH $DD02 NEVER !!!
;ALL SHOULD BE HAPPY WHEN YOU WILL HAVE A BIG ATTENTION FOR THIS CASES

;************************************
;* A SHORT INFO HOW TO USE THIS ONE *
;************************************

;AT THE VERY FIRST YOU MUST DONE CALL ***JSR PROC1***
;SOMEWHERE IN A BEGINNING OF DEMO - THIS IS A DEDICATED TO THIS CODE
;PROC TO SEND DRIVECODE TO 1541x, 1570/71 IN 1541 MODE


;1. FOR LOAD AND FLY DECRUNCH CALL JSR PROC2 WITH SETUP:
;ACC AND Y MUST HAVE LO/HI BYTE OF POINTER 
;TO 7 BYTES INFO DATA TO WORK IN ORDER:

;---
;BASEADR		WORD - 2 BYTES IN LO/HI FORMAT
;DEPACK_AD	 	WORD - 2 BYTES IN LO/HI FORMAT - IF = 0 THEN DEP. ADR. FROM FILE
;STRTTRK		BYTE - 1 BYTE START TRACK FILE TO LOAD
;STRTSEC		BYTE - 1 BYTE START SECTOR FILE TO LOAD
;HOW_GET_LD	BYTE - 1 BYTE : IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---

;BASEADR = LOAD ADDRESS IF YOU WANNA LOAD FOR OWN LOAD AD.
;THIS INFO YOU SETUP IN HOW_GET_LD :
;HOW_GET_LD  =  0 - LOAD ADDRES FROM FILE - BASEADR IS IGNORED
;HOW_GET_LD <>  0 - LOAD ADDRES FROM BASEADR
;STRTTRK - START TRACK FILE TO LOAD
;STRTSEC - START SECTOR FILE TO LOAD
;DEPACK_AD - ADDRES WHERE DECRUNCHER SHOULD BE DECRUNCH. DATA (EVEN IN FLY DECRUNCH )
;DEPACK_AD IF = 0 THEN DEPACK ADDRES WILL BE TAKEN FROM FILE - REMEMBER ABOUT THIS !!!
;IF YOU WANNA DO DECRUNCH ADDRES FROM FILE THIS 2 BYTEN MUST BE SET TO 0

;START TRACK AND SECTOR VALUES THEY ARE GENERATED
; ***** INTO INCLUDE SCRIPT BY TRACKMOLINKER *****
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;|||||||||||||||||||||||||||||||||||||||||||||||||

;STRONG RECOMMENDED TO USE TRACKMOLINKER V 1.2

; IMPORTANT!!! - $01 VALUE MUST BE SET UP BEFORE JSR PROC2
; THIS VALUE IS RESTORED FOR DECRUNCH FOR POSSIBLITY
; DECRUNCH DATA TO COLOR RAM (IT'S MY OPTION CAUSE I USED THIS)
; OF COURSE - LOAD ADDRES MUST BE RECHANGED FOR DIFFERENT
; THAN I/O AREA IN THIS CASE

;ZERO PAGE USED BY DECRUNCHER - 9 BYTES

;2. FOR ONLY LOAD FILE CALL JSR PROC2+3
; - LIKE BEFORE SETUP YOU DONE 
; DECRUNCHER STILL NOT ACTIVE AND BY LOADER
; *** ZERO PAGE IS UNTOUCHED ***

;3. JUST DECRUNCH - CALL JSR PROC2+6
;ACC/YREG - LO/HI BYTE POINTER TO INFO STREAM DATA
;$01 VALUE YOU MUST BEFORE SETUP FOR DECRUNCH AS YOU NEED
;NOW YOU NEED ONLY FIRST 4 BYTES TO SETUP

;4. JSR PROC2+9 - DISCONECT LOADER AND RELEASE DRIVE

;5. JSR PROC2+12 - SEND ONE BYTE FROM ACC TO DRIVE... FOR OWN CODE, CHANGEDISK ENGINE, ADDITIONAL FUNCTION
;BUT COMPATIBILITY WITH GETTORAM PROTOCOL

;********************
;* A SHORT EXAMPLES *
;********************

;1. -A

; LOADING FILE INTO *** $C000 *** FROM START TRACK 1 AND SECTOR 0
; AND DECRUNCH INTO $4700

;		...
;		LDA #$34	;DECRUNCH VALUE!
;		STA $01
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2
;		BCS *		;LOAD ERROR - DEMO CRASHED

;		NOW IN ACC/XREG LO/HI IS THE POINTER TO END OF DATA+1 FOR OTHER CASE LIKE EXO BACK DECRUNCH...
;		...
;---
;BASEADR		.WORD $C000
;DEPACK_AD 	.WORD $4700
;STRTTRK		.BYTE $01
;STRTSEC		.BYTE $00
;HOW_GET_LD	.BYTE $01
;---

;1. -B

; LOADING FILE INTO *** FILE LOAD ADDRES *** FROM START TRACK 1 AND SECTOR 0
; AND DECRUNCH INTO $4700

;		...
;		LDA #$34	;DECRUNCH VALUE!
;		STA $01
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2
;		BCS *		;LOAD ERROR - DEMO CRASHED

;		NOW IN ACC/XREG LO/HI IS THE POINTER TO END OF DATA+1 FOR OTHER CASE LIKE EXO BACK DECRUNCH...
;		...
;---
;BASEADR		.WORD $0000 ;NOT IMPORTANT
;DEPACK_AD 	.WORD $4700
;STRTTRK		.BYTE $01
;STRTSEC		.BYTE $00
;HOW_GET_LD	.BYTE $00	;BASEAD WILL BE IGNORED TAKE LOAD ADDRES FROM FILE
;---

;================================		
;2. -A
;2. -B 

;SETUP LIKE 1. -A, 1. -B PARAGRAPH
;A DIFFERENT IS - *** JSR PROC2 *** 
;CHANGE TO *** JSR PROC2+3 ***
;DECRUNCHER IS INACTIVE, ZERO PAGE UNTOUCHED

;DEPACK_AD  IN INFOSTREAM DATA	VALUE IN THIS CASE IS IGNORED

;3. -A
; DECRUNCH  DATA IN RAM FROM $C000 INTO $4700


;		...
;		LDA #$34	;FOR DECRUNCHER VALUE
;		STA $01
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2+6
;		...
;--- 
;BASEADR		.WORD $C000 ;HERE ARE CRUNCHED DATA
;DEPACK_AD 	.WORD $4700 ;AND HERE PLEASSE DO DECRUNCHING
;IF YO WANNA DEPACK TO ADDRESS SAVED IN CRUNCHED FILE 
;VARIABLE DEPACK_AD MUST BE SET TO 0
;---
;..................................................

;*****************************************
;* AND I GUESS THAT IS SHOULD BE ENOUGHT *
;* TO YOU OWN HAPPY USE :)               *
;* REGARDS - WEGI                        *
;*****************************************

!TO "Ldr.prg" , CBM

;----- PARAGRAPH @STARTPROC2@ -----

PROC2 = $0900

* = PROC2
;-
;		!ALIGN 256
;-
		;A JUMP TABLE OF THIS -
		;PROC2	- LOAD FILE AND FLY DECRUNCH
		;PROC2+3 	- OLY LOAD WITHOUT DECRUNCH
		;PROC2+6 ONLY DECRUNCH - ACC AND YREG LO/HI BYTE OF START DATA - $01 MUST BE SET HOW YOU NEED !!!
		;PROC2+9 DISCONNECT LOADER
		;PROC2+12 - SEND ONE BYTE TO DRIVE FROM ACC

		JMP FLY_DECR	;LOAD + DECR		
		JMP ONLY_LOAD	;ONLY LOAD WITHOUT DECRUNCH
		JMP ONLY_DECRUNCH
		JMP LOADEROFF
;------------
SENDDRIV2       ;SEND ONE BYTE
;---
         SEC
         ROR
         TAX
         LDA $DD02
         ORA #$30
         STA $DD02

         BIT $DD00
         BVC *-3
KEX
         LDA $DD02
         EOR #$20
         ORA #$10
         BCC *+4
         AND #$EF
         STA $DD02
         TXA
         LSR
         TAX
         BNE KEX
         LDA $DD02
         AND #$0F
         ORA #$10
         STA $DD02
;---		
		RTS
;-------------------
;=========
GET_Y_BYTES
;---
         BIT $DD00
         BVC *-3
POB2     LDX #$3F
         STX $DD02
         ;NOP
         INY
         STY RCALL
         BIT $EA
         LDY #$37
         LDA $DD00
         STY $DD02
         LSR
         LSR
         NOP
         NOP
         BIT $EA
         ORA $DD00
         STX $DD02

         LSR
         LSR
         LSR
         LSR
         STA HALFBYTE
         LDA $DD00
         STY $DD02
;---
RCALL    = *+1
         LDY #$00
;---
         NOP
         LDX #$1F
         LSR
         LSR
MY_MANY   = *+1
         CPY #$FF
         ORA $DD00
         STX $DD02
         AND #$F0
HALFBYTE = *+1
         ORA #$00
;---

		DEC $01
;--		
MY_BUF_LO = *+1
MY_BUF_HI = *+2
;-
		STA $1111,Y
		INC $01
;--
MY_CKSM = *+1
;-
		EOR #$00
		STA MY_CKSM
		BCC POB2
		RTS
;---------
MAIN_LINK	!WORD 0	;LINK OF SECTOR
MAIN_LDAD	!WORD 0	;LOAD ADDRESS FROM FILE
GDZIEHI = *+1
GDZIELO	!WORD 0	;ACTUAL ADDRESS OF DATA TO SAVE
MAIN_DATA	!BYTE 0	;ONE BYTE FOR DATA
;--------

;--------
TRNSM1      ;GET ALL 256 BYTES OF BLOCK
		LDA #<MAIN_LINK
		STA MY_BUF_LO
		LDA #>MAIN_LINK
		STA MY_BUF_HI
		LDY MY_SUBSTRACT
		DEY
		STY MY_MANY

		LDY #0
		STY MY_CKSM
		DEY
		JSR GET_Y_BYTES
;---		
		JSR SETBASE	;SET POINTERS...
;---
		LDY MY_SUBSTRACT ; 2 OR 4
		DEY
		CPY #1
		BEQ +

		LDA HOW_GET_LD
		BNE +
		LDA MAIN_LDAD
		STA BASEADR
		LDA MAIN_LDAD+1
		STA BASEADRHI
		JSR SETBASE
		LDY #3
+:		
		LDA GDZIELO
		STA MY_BUF_LO
;------
MY_SUBSTRACT = *+1
		SBC #$02
;------
		LDA GDZIEHI
		STA MY_BUF_HI
		LDA MAIN_LINK
		BNE +
		LDA MAIN_LINK+1
		STA ILE
		
+:	JSR ACT_ADR
;------	
ILE = *+1		
		LDA #$FF
		STA MY_MANY
;------
		JSR GET_Y_BYTES

	;LAST BLOK - CONTINUE CKSM CALC AND READ DATA AFTER FILE
-:   	LDA ILE
		CMP #$FF
		BEQ +
		JSR GAVE_ONE
		INC ILE
		BNE -
+:	
		RTS
;--------
GAVE_ONE
		LDA #<MAIN_DATA
		STA MY_BUF_LO
		LDA #>MAIN_DATA
		STA MY_BUF_HI
		LDY #$00
		STY MY_MANY
		DEY
		JSR GET_Y_BYTES
		LDA MAIN_DATA
		RTS
;--------
LADUJ	;READ DATA FROM ALL ACTUAL TRACK
;--------

;--------
;GET BLOCK TO BUFFER, NR SECT, CHECKSUM AND CALCULATE IT
;--------
STRTTRS
		JSR TRNSM1		;GET BLOCK
		JSR GAVE_ONE	;GET CHECKSUM
		LDA MY_CKSM
		TAY			;SAVE CHECKSUM
		JSR SENDDRIV2
		TYA			;RECALL CHECKSUM
		BNE STRTTRS	;BAD OF CHKSM - LOOPBACK
		;BNE *
;-------
DECR_WAY
		JMP FIRSTDECR
NO_FLY	
;=======
REFILL_DATA
		LDA MAIN_LINK		;IF IS THIS LAST BLOCK
		BNE LADUJ  	;LOAD LOOP
		RTS
;---
;----

;--------
LOADEROFF
;---
		LDA #$00
		!byte $2C
;---
READ1SEC
;---
		LDA #$02
		PHA
;---
		LDA STRTTRK	;START TRAC FOR DRIVE
		JSR SENDDRIV2
;---
		LDA STRTSEC	;START SECTOR OF FILE
		JSR SENDDRIV2
;---
		PLA			;OPERATION CODE
		JMP SENDDRIV2
;---
;--------
;-------------------------------------
; GET BASE ADRESS OF TRACK AND ADD FOR
; NUMBER OF SECTOR VALUE
;-------------------------------------
SETBASE
		LDA BASEADR
		STA GDZIELO
		STA LOW_ADR
          
		LDA BASEADRHI
		STA GDZIEHI
		STA HI_ADR
          
		LDA #$FF
		STA ILE
		
		LDA GDZIELO
		SEC

		SBC MY_SUBSTRACT
		STA GDZIELO

		BCS +
		DEC GDZIEHI
+:

		RTS
;---
ACT_ADR
		LDA ILE
		SEC
		SBC MY_SUBSTRACT
		;CLC
		
		ADC BASEADR
		STA BASEADR
		BCC +
		INC BASEADRHI
+:   	LDA #$02
		STA MY_SUBSTRACT
		RTS
;--------

;========================
BASEADR		!BYTE 0
BASEADRHI		!BYTE 0
DEPCK		!WORD $4700
STRTTRK		!BYTE 0
STRTSEC		!BYTE 0
HOW_GET_LD	!BYTE 0
;=====================
GET_INIT_DATA
		STA MY_VECT
		STY MY_VECT+1
		LDA #4
		STA MY_SUBSTRACT

		LDA $01
		STA BACKUP01
		JSR SETMY01
		
		;Y = 0
		LDY #6
		
-:
		DEC $01
MY_VECT=*+1
		LDA $1111,Y
		INC $01
		STA BASEADR,Y
		DEY
		BPL -
		RTS
FLY_DECR
		PHA
		LDA #<FIRSTDECR
		LDX #>FIRSTDECR
		BNE GO_1
ONLY_LOAD
		PHA
		LDX #>NO_FLY
		LDA #<NO_FLY
GO_1		JSR SET_DECR_WAY
		PLA
STARTLOAD
		JSR GET_INIT_DATA

FIRSTSECT
		JSR READ1SEC
		JSR LADUJ
ENDLOAD

		JSR RESTORE01
		LDA BASEADR
		LDX BASEADRHI

		;AT THE END - CARRY IS CLEAR AND FOR EXODECRUNCHER
		;IN ACC LOW BYTE OF EOF FILE IN X REG HI BYTE OF EOF FILE

		CLC
		RTS
SETMY01
		LDX #$35
		BNE SET01
RESTORE01
BACKUP01 = *+1
		LDX #$00
SET01	STX $01
		RTS
;=====================
;--------
SEC_DECR
		
		JMP RESTORE01
;----

SET_DECR_WAY
		STA DECR_WAY+1
		STX DECR_WAY+2
		RTS
;--------
FIRSTDECR
		LDA #<SEC_DECR
		LDX #>SEC_DECR
		JSR SET_DECR_WAY
		JSR RESTORE01

LOW_ADR = *+1
          LDA #$00
HI_ADR = *+1
		LDY #$00
  LDX #$4C  ;JSR WE DO
  STX END_DP
		LDX #$08 ;PHP
		BNE JUST_DECRUNCH_2
;---
;=========================
;LEVEL-CRUSHER V1.0/V1.1 DEPACKER	
;(C)1998 TABOO PRODUCTIONS!	
;ALL RIGHTS RESERVED	
;-------------------------------------------	
;ZEROPAGE REGISTERS, THE DECOMPRESSOR USES:	
ZP_BASE	= $02
;---
LNG		= ZP_BASE+0	;BY THE DEPACKER.	
BYTE		= ZP_BASE+1	;ADDITIONAL BYTES USED	

DEST		= ZP_BASE+2	;FIRST-BYTE-POINTER	
DESTH	= DEST+1		;OF DEPACKED DATAS.	
SRC		= ZP_BASE+4	;START OF PACKED FILE.	
SRCH		= SRC+1  ;	

LO		= ZP_BASE+6
HI		= LO+1   ;	

TEMP = HI+1
GET = SRC
;-------------------------------------------	

ONLY_DECRUNCH
          JSR GET_INIT_DATA
          JSR RESTORE01
          LDA BASEADR
          LDY BASEADRHI
JUST_DECRUNCH
    LDX #$60  ;RTS - DON'T NEED COUNTING GET DATA
    STX END_DP
JUST_DECRUNCH_2
    STX TRY_REF
    STA GET
    STY GET+1
DECRUSH
  LDX #6 ;FIRST 252 BYTES - FOR COUNTING GETTING BYTES
		STX TEMP
;===================

          LDY #0
          LDA (SRC),Y
          STA DEST
          INY
          LDA (SRC),Y
          STA DESTH

          CLC
          LDA #2
          ADC SRC
          STA SRC
          BCC *+4
          INC SRCH

          LDA DEPCK+1
          BNE DO_RAW
          LDA DEPCK
          BEQ +
DO_RAW
          LDA DEPCK
          STA DEST
          LDA DEPCK+1
          STA DESTH
+:
          LDX #$00	
          STX BYTE	
JP17      STX HI	
          LDA #$01	
          ASL BYTE	
          BNE *+5	
          JSR GETBIT	
          BCS JP2	
JP4       ASL BYTE	
          BNE *+5	
          JSR GETBIT	
          BCS JP3	
          ASL BYTE	
          BNE *+5	
          JSR GETBIT	
          ROL	
          ROL HI	
          BPL JP4	
JP3       TAX	
          BEQ JP5
;---
JP7       LDY #$00	
JP7A      LDA (SRC),Y	
          STA (DEST),Y
          INC TEMP
          BNE +
          JSR TRY_REF
+:
          INY
          DEX
          BNE JP7A

          CLC
          TYA
          ADC DEST
          STA DEST
          BCC *+4
          INC DESTH
          TYA
          CLC
          ADC SRC
          STA SRC
          BCC *+4
          INC SRCH
;-----------------------------------------	
JP5       CPX HI	
          DEC HI	
          BCC JP7	
          STX HI	
JP2       LDA #$01	
          ASL BYTE	
          BNE *+5	
          JSR GETBIT	
          BCC JP9	
JP8       ASL BYTE	
          BNE *+5	
          JSR GETBIT	
          BCS JP10	
          ASL BYTE	
          BNE *+5	
          JSR GETBIT	
          ROL	
          BCC JP8	
END_DP
          JMP SETMY01

JP9       INX	
JP10      ADC #$01	
          STA LNG	
          TXA	
          ASL BYTE	
          BNE *+5	
          JSR GETBIT	
          ROL	
          ASL BYTE	
          BNE *+5	
          JSR GETBIT	
          ROL	
          TAX	
          LDA #$00	
JP12      LDY TAB,X	
JP11      ASL BYTE	
          BNE *+5	
          JSR GETBIT	
          ROL	
          ROL HI	
          DEY	
          BNE JP11	
          DEX	
          BMI JP13	
          CPX #$03
          CLC
          BEQ JP13		
          ADC #$01	
          BCC JP12	
          INC HI	
          BCS JP12	
JP13     
          ADC LNG
          STA LO	
          BCC JP14
          INC HI	
JP14      SEC	
          LDA DEST	
          SBC LO	
          STA LO	
          LDA DESTH	
          SBC HI	
          STA HI	

          LDX LNG
JP15      LDA (LO),Y	
          STA (DEST),Y	
          INY		
          DEX
          BNE JP15
          TYA
          CLC
          ADC DEST
          STA DEST
          BCC JP16
          INC DESTH
JP16      JMP JP17
;---
GETBIT    PHA	
          STY GB2+1	
          INC TEMP
          BNE +
          JSR TRY_REF
+:
          LDY #$00	
          LDA (SRC),Y	
          INC SRC	
          BNE GB2	
          INC SRCH	
GB2       LDY #$00	
          SEC	
          ROL	
          STA BYTE	
          PLA	
          RTS
;---
TAB       !BYTE 4,3,3,3,4,2,2,2	
;---

;---
TRY_REF
    PHP
     INC TEMP ;NOW 254 WILL BE INCOMING
     INC TEMP
     PHA
     STX MY_XR
     STY MY_YR
     JSR SETMY01
     JSR REFILL_DATA
     PLA
MY_XR =*+1
     LDX #$00
MY_YR =*+1
     LDY #$00
     PLP
     RTS
;---

