;At the start Your demo just do JSR PROC1 for install loader - that's all

;----- PARAGRAPH @STARTPROC2@ -----
;================================
;1ST PROCEDURE OF:
;THE BONGO NOINTERLEAVE LOADER WORKING 1541x, 1570/71 IN 1541 MODE
; (C) BY WEGI IN 2013.01.18
;================================

!TO "ins2.prg" , CBM

PROC1 = $2000

* = PROC1
;-
;.ALIGN 256
;-
VECTR    = $06
VECTR2   = $08

GOTORAM  = $0100 ;A SHORT PROC FOR GETTINGS DATA TO RAM OF DRIVE
;-
		JMP INITLOAD
;--------
;--------
SENDDRIV1 
;---
         SEC
         ROR
         TAX
         LDA $DD02
         ORA #$30
         STA $DD02

         BIT $DD00
         BVC *-3
KEX
         LDA $DD02
         EOR #$20
         ORA #$10
         BCC *+4
         AND #$EF
         STA $DD02
         TXA
         LSR
         TAX
         BNE KEX
         LDA $DD02
         AND #$0F
         ORA #$10
         STA $DD02
;---		
		RTS
;-

;-
TX_DNOT
		!byte 147
		!text "DEVICE NOT PRESENT"
		!byte 13
		!text "PLEASSE TURN ON ONE 1541/70/71 DRIVE"
		!byte 13
		!text "AND PRESS ANY KEY"
		!byte 0

TX_TO_MANY
		!byte 147
		!text "TO MANY UNITS ON THE IEC"
		!byte 13
		!text "PLEASSE TURN OFF UNUSED UNIT"
		!byte 13
		!text "AND PRESS ANY KEY"
		!byte 0
STOS		!byte 0 ;STACK SAVE VALUE
;-
;--------
TBDRIV   !byte 8,9,10,11
;---
IEC_UNITS !byte 0
;---
INITLOAD
		TSX
		STX STOS
		LDA #$00
		STA $D015
		STA IEC_UNITS
		
		LDA #8
		STA $BA
		
		LDX #4
-:	
		JSR LISTEN
		BMI +
		INC IEC_UNITS
		LDA $BA
		STA MY_DRIVE
		JSR $FFAE
		
+:		INC $BA
		DEX
		BNE -
		LDA IEC_UNITS
		CMP #1
		BEQ +
		CMP #0
		BNE TOMANYDR
		LDA #<TX_DNOT
		LDY #>TX_DNOT
		JMP PRT_T
TOMANYDR	LDA #<TX_TO_MANY
		LDY #>TX_TO_MANY
PRT_T	JSR $AB1E
		LDA #$00
		STA $C6
		JSR $FFE4
		BEQ *-3
		JMP INITLOAD
+:	
		
MY_DRIVE = *+1
		LDA #$08
		STA $BA
;-
FASTMW
;--------


		JSR LISTEN
		LDA #'I'
		JSR $FFA8
		JSR $FFAE

		LDA #<DRV
		STA VECTR
		LDA #>DRV
		STA VECTR+1

		LDA #<ADRDRV
		STA VECTR2
		LDA #>ADRDRV
		STA VECTR2+1

		LDA #<ADENDDRV
		STA X3+1
		LDA #>ADENDDRV
		STA X3+2

		JSR MEW

;--------
X0       LDA #$04
		STA $02
		LDY #$00
X3       LDA $1000,Y
		JSR SENDDRIV1
		INY
		BNE X3
		INC X3+2
		DEC $02
		BNE X3
		LDY #<RESTLDR
		STY $02
		LDY #$00

X4A      LDA ADREST,Y
		JSR SENDDRIV1
		INY
		INC $02
		BNE X4A

		CLC
		CLI
		RTS
;========
LISTEN		
		LDA $BA
		JSR $FFB1
		LDA #$00
		STA $90
		LDA #$6F
		JMP $FF93
;--------
;-
MEW      LDX #3
LOOP     
		JSR LISTEN
		BIT $90
		BPL DALEJ
		LDX STOS
		TXS
		LDA #$80  ;DNOTPRESENT
		SEC
		RTS
DALEJ    LDA #$4D
		JSR $FFA8
		LDA #$2D
		JSR $FFA8
		LDA #$57
		JSR $FFA8
		LDA VECTR
		JSR $FFA8
		LDA VECTR+1
		JSR $FFA8
		LDA #$20
		JSR $FFA8
		LDY #$00
LOOP1    LDA (VECTR2),Y
		JSR $FFA8
		INY
		CPY #$20
		BNE LOOP1
		JSR $FFAE
		LDA VECTR
		CLC
		ADC #$20
		STA VECTR
		BCC *+4
		INC VECTR+1
		LDA VECTR2
		CLC
		ADC #$20
		STA VECTR2
		BCC *+4
		INC VECTR2+1
		DEX
		BNE LOOP
;-
MEX      
		
		JSR LISTEN
		LDX #$00
		LDA MEX1,X
		JSR $FFA8
		INX
		CPX #$06
		BNE *-9
		JSR $FFAE
		SEI
     LDA #$03
     STA $DD00
     LDA #$1f
     STA $DD02
		CLI
		BIT $DD00
		BVS *-3
		CLC
		RTS
;-
MEX1     !text "M-E"
		!byte <ABSRUN,>ABSRUN
;-
VBANK1    !byte 3
;--------
;DRIVE VARIABLES
;--------
TRACK	= $06
SECTOR	= $07
TYPEWRK	= $0C

;--------
;---------------------------------------
;-
CODE
ADRDRV
;-
!PSEUDOPC $0146
;------  REDEFINED COS BAD ASSEMBLING
MYDATA   = $01FF
;-
DRV
GETBLK
		LDA #$80
		STA $1800
		LDX $1800
		BNE *-3

EX22     CPX $1800
		BEQ *-3
		LDX $1800
		CPX #4
		ror
		BCC EX22
		
		LDX #$0D
		STX $1800
ADRBLK   = *+2
		STA $0300,Y
		CPX $1800
		BNE *-3

		INY
		BNE GETBLK
		RTS

SETLINE  LDA #$08
STLINE   STA SERIAL
		RTS
CLRLINE  LDA #$01
		BNE STLINE
;-
ABSRUN   SEI
		LDA #$7A
		STA $1802
		JSR SETLINE
		JSR $F5E9
		LDX #$04
		STX $06
DRV1     JSR GETBLK
		INC ADRBLK
		DEC $06
		BNE DRV1
		JMP INITRAM100
;-
RESTLDR

ADREST   = CODE-DRV+RESTLDR
;-

GETJOB   JSR GETONE
		STA TRACK
		JSR GETONE
		STA SECTOR

GETONE   SEI
		LDY #$FF
		JSR GETBLK
		LDA MYDATA
		STA TYPEWRK
		RTS
;-
INITER   STA $00
		CLI
		LDA $00
		BMI *-2
		CMP #$02
ENDINI   RTS

IDLE		SEI
		LDA $1C00
		ORA #$08
		STA $1C00
		
		LDY #150
		
IDD		LDA #$D0
		STA $1805

-:		LDA $1800
		LSR
		BCC +
		BIT $1805
		BMI -
		DEY
		BNE IDD
		
STPMT	LDA $1C00
		AND #$FB
		STA $1C00
+:		RTS
IDLEEND
;---	
!REALPC

;----- PARAGRAPH @DRIVECODE@ -----

;-
ADENDDRV
;-
!PSEUDOPC $0300
;-
TABTRAK	= $0600
TABSEC	= $0620
TABENQ	= $0640 ;
BUFENQ	= $0200 ;USED ON 2 METHOD
;-
NXTRACK	= $08   ;AFTER SCAN 
NXSECTOR	= $09   ;SCAN IS ALLWAYS ENABLED
CNTRSEC	= $0A
ENQN		= $0B   ;ORDER SECTOR IN SEND

		;==========================
		;#$01 WATCH AND WAIT WITHOUT STOP MOTOR
		;#$02 SEND SCANNED TRACK
		;#$03 SEND SELECTED SECTOR
		;#$00 DISCONNECT - END
		;#$04 GET DATA TO RAM
		;===========================
		; WHERE NO COMMENTS
		; - THE LABELS ARE CLEAR

CNTR1    = $0D
CNTR2    = $0E
TMPR1    = $0F
BLKCKSM  = $10

IDHEADER = $24    ;HEADER ID (FROM DOS)
MAXSEC   = $43    ;HOW MANY SECTOR ON THE TRACK (FROM DOS)

GCR1     = $52
GCR2     = $53
GCR3     = $54

FINDSYNC = $F556
HEADBAD  = $F41B
IRQOK    = $F505

LASTLONYBGCR = $07FF

BFHINYBGCR = $0600
BFLONYBGCR = $0700
SERIAL   = $1800  ;THE SERIAL PORT...
;-
RUNLDR   JMP READ
;-
SEND_NEW_ONE
		TAX
		AND #$0F
		TAY
		TXA
		LSR
		LSR
		LSR
		LSR
		TAX
		LDA $F77F,X
		STA BFHINYBGCR+255
		LDA $F77F,Y
GO_ONE_CHK
		STA BFLONYBGCR+255		
		LDY #$FF
		!byte $2C
TRNS0
		LDY #$00
LOOT
		LDA #$00
		STA $1800

		LDX BFLONYBGCR,Y
		LDA GCR2SER,X

		LDX $1800
		BNE *-3
		STA $1800
LOT2
		asl
		ORA #$10
		BIT $1800
		BPL *-3

		STA $1800

		LDX BFHINYBGCR,Y
		LDA GCR2SER,X

		BIT $1800
		BMI *-3
		STA $1800
		asl
		ORA #$10
		BIT $1800
		BPL *-3
		STA $1800
		LDA #$18
		INY
		BEQ ZZZ
		BIT $1800
		BMI *-3
		STA $1800
		BPL LOOT
ZZZ
		BIT $1800
		BMI *-3
		STA $1800
		
		LDA $1C00
		EOR #8
		STA $1C00
		RTS


;-
GCR2SER = *-8 
		!byte $FF,$0E,$0F,$07,$FF,$0A,$0B,$03
		!byte $FF,$FF,$0D,$05,$FF,$00,$09,$01
		!byte $FF,$06,$0C,$04,$FF,$02,$08,$FF
;-
		!text "(C) BY WEGI"
;-
READ     JSR SCANNER
		LDA CNTRSEC
		JSR SEND_NEW_ONE
;-
READL
		JSR HADER

		LDA $1801
		ORA #$20
		STA $1801

		JSR FINDSYNC
;-
K1       BVC *
		CLV
		LDA $1C01
		TAX
		lsr
		lsr
		lsr
		STA BFHINYBGCR-1,Y
		BVC *
		CLV
		LDA $1C01
		lsr
		STA BFHINYBGCR,Y
		TXA
		STA BFLONYBGCR-1,Y
		BVC *
		CLV
		LDA $1C01
		TAX
		ror
		lsr
		lsr
		lsr
		STA BFLONYBGCR,Y
		BVC *
		CLV
		LDA $1C01
		STA BFLONYBGCR+1,Y
		asl
		TXA
		rol
		AND #%00011111
		STA BFHINYBGCR+1,Y
		BVC *
		CLV
		LDA $1C01
		STA BFHINYBGCR+2,Y
		AND #%00011111
		STA BFLONYBGCR+2,Y
		INY
		INY
		INY
		INY
		BNE K1
K2       BVC *
		CLV
		LDA $1C01
		STA GCR1,Y
		INY
		CPY #$03
		BNE K2
		LDY #$00
K3       LDA BFLONYBGCR-1,Y
		STA TMPR1
		LDA BFHINYBGCR,Y
		TAX
		AND #%00011111
		STA BFHINYBGCR,Y
		TXA
		asl
		asl
		ROL TMPR1
		asl
		LDA TMPR1
		rol
		AND #%00011111
		STA BFLONYBGCR-1,Y
		LDA BFLONYBGCR+1,Y
		TAX
		AND #%00000011
		STA TMPR1
		TXA
		lsr
		lsr
		AND #%00011111
		STA BFLONYBGCR+1,Y
		LDA BFHINYBGCR+2,Y
		asl
		ROL TMPR1
		asl
		ROL TMPR1
		asl
		LDA TMPR1
		rol
		STA BFHINYBGCR+2,Y
		INY
		INY
		INY
		INY
		BNE K3
		LDA GCR1
		lsr
		lsr
		lsr
		STA BFLONYBGCR-1
		LDA GCR1
		AND #%00000111
		STA LASTLONYBGCR
		LDA GCR2
		asl
		ROL LASTLONYBGCR
		asl
		ROL LASTLONYBGCR

		LDA BFHINYBGCR+255
		PHA
		LDA BFLONYBGCR+255
		PHA
		
		LDA $1801
		AND #$DF
		STA $1801       

		LDA ENQN
		JSR SEND_NEW_ONE

		PLA
		STA BFLONYBGCR+255
		PLA
		STA BFHINYBGCR+255


		JSR TRNS0
       
		LDA GCR2
		lsr
		AND #$1F
		STA BFHINYBGCR+255
		LDA GCR3
		ROR
		lsr
		lsr
		lsr
		
		JSR GO_ONE_CHK
;--
		LDY #$FF
		JSR GETBLK		;TEST OF LAST CHECKSUM
		CMP #$00
		BNE +			;BAD CHKSM - GOTO READ LOOOP
		LDX SECTOR
		LDA #$FF
		STA BUFENQ,X
		DEC CNTRSEC
		BEQ IREX7
;--
+:		JMP READL
IREX7	LDA NXTRACK
		BNE +
		LDA #$01
		STA TYPEWRK
+:		JMP IRQOK
;-
;----- PARAGRAPH @SCANNER@ -----

SCANNER
		LDX MAXSEC
		STX CNTRSEC   ;HOW MANY SECT.?
CLRTAB                 ;TO SEND
		LDA #$FF
		STA TABENQ,X
		TXA
		STA BUFENQ,X
		DEX
		BPL CLRTAB
		INX
		STX NXTRACK  ;CLEAR
		INX
		CPX TYPEWRK  ;01?
		BEQ ESCN     ;ALL TRACK
		INX
		CPX TYPEWRK  ;02?
		BEQ SCN1     ;WITH SCAN TRACK
		LDX MAXSEC
		LDA #$FF     ;OTHER = SEND
		STA BUFENQ,X ;SELECT SECTOR
		DEX
		BPL *-4
		LDX SECTOR
		LDA #$00
		STA BUFENQ,X
		STA NXTRACK
		LDA #$01
		STA CNTRSEC
		STA TYPEWRK
ESCN		RTS

SCN1		JSR HADER
		JSR FINDSYNC
READ1A	BVC *
		CLV
		LDA $1C01
		STA ($30),Y
		INY
		CPY #$05
		BNE READ1A
		LDY #$00
		JSR $F7E8
		LDX SECTOR
		LDA GCR2
		STA TABTRAK,X
		LDA GCR3
		STA TABSEC,X

		DEC CNTRSEC
		BNE SCN1
		LDX NXSECTOR

SCN2		LDA CNTRSEC
		INC CNTRSEC
		BMI *			;SECTORS LOOP !!
		STA TABENQ,X
		LDA TABTRAK,X
		CMP TRACK
		BNE SCN3
		LDA TABSEC,X
		TAX
		JMP SCN2
SCN3		STA NXTRACK
		LDA TABSEC,X
		STA NXSECTOR
		LDX MAXSEC
SCN4		LDA TABENQ,X
		STA BUFENQ,X
SCN5		DEX
		BPL SCN4
		RTS
;------
 
LENSCAN = * - SCANNER
;-
HADER	LDY #$00
		STY CNTR1
		STY CNTR2
		LDA #$07
		STA $31
HLOOP	DEC CNTR2
		BEQ *		;HEADERS BAD !!
FINDH	JSR FINDSYNC
		BVC *
		CLV
		LDA $1C01
		CMP IDHEADER
		BNE HLOOP
		INY
HREAD	BVC *
		CLV
		LDA $1C01
		STA ($30),Y
		INY
		CPY #$04
		BNE HREAD
		LDY #$00
		JSR $f7e8
		LDX GCR3
		CPX MAXSEC
		BCS *		;HEADERS BAD !!
		LDA BUFENQ,X
		BPL HEND
		INC CNTR1
		BPL FINDH
		BMI *		;HEADERS BAD !!
HEND		STX SECTOR
		STA ENQN
		RTS
;-
		;WATCH AND WAIT BY IDLE LOOP
MTORSTOP
STRTLDR
		JSR IDLE
;-
;STRTLDR
WITHOUT_STOP
		JSR GETJOB
		LDA SECTOR
		STA NXSECTOR
		LDA TRACK
		STA NXTRACK
		;==========================
		;#$01 WATCH AND WAIT THROUGH IDLE LOOP
		;#$02 SEND SCANNED TRACK
		;#$03 SEND SELECTED SECTOR
		;#$00 DISCONNECT - END
		;#$04 GET DATA TO RAM - MAYBE FOR DRIVECALC, CHANGEDISK ENGINE...
		;===========================
		
ELO      LDA TYPEWRK
		BEQ EOFI
		CMP #$01
		BEQ MTORSTOP
		CMP #$03
		BEQ ELO2
		CMP #$04
		BNE SENDST ;SO $02 CODE LAST ONE POSSIBLITY
		JSR GOTORAM
		JMP STRTLDR
;---
SENDST		
		LDA NXTRACK
		BEQ ELO
		STA TRACK
		LDA NXSECTOR
		STA SECTOR
ELO2
		JSR MOTORSTART
		LDA #$E0
		JSR INITER
		BCC SENDST
BAD1

		JSR EOFI
		LDA #$05
		
BAD2     JSR DELAY
		TAX
		DEX
		TXA
		BNE BAD2
		JSR SETLINE
		JSR DELAY
		JMP STRTLDR
EOFI
		JSR CLRLINE
     STA $1C
     RTS
;---
MOTORSTART
		SEI
		LDA $1C00
		TAX
		ORA #$04
		STA $1C00
		TXA
		AND #$04
		BNE MOTOROK
DELAY
		LDX #$00
		DEY
		BNE *-1
		DEX
		BNE *-4
MOTOROK
 		RTS
EOFDRIVECODE
;=====================
INITRAM100
		LDA #1
		STA ADRBLK
		LDA $1801
		AND #$DF
		STA $1801		 
		LDA #$10
		STA $1C07

		LDA #$01  ; GO NEAR START OF DATA BEFORE
		STA $06
		LDA #$00
		STA $07
	
		LDA #$B0
		STA $00
		CLI
		LDA $00
		BMI *-2
   
		SEI

		LDY #<RESTLDR
		JSR GETBLK
		        
		LDX #$22
-:
		LDA GETTORAM,X
		STA GOTORAM,X
		DEX
		BPL -
		JMP STRTLDR 
;==========		
GETTORAM
		LDA TRACK		;SHORT PROCEDURE
		STA $08		  ;FOR GETTING DATA TO
		LDA SECTOR        ;RAMDRIVE
		STA $09
		LDA #$00
		STA $0A
		JSR GETJOB
		STA $0B
-:
		JSR GETONE
		LDY $0A
		STA ($08),Y
		INC $0A
		DEC $0B
		BNE -
		
		JMP ($0006) ;RUN
;==========
;-
ENDLDR
!REALPC
;===========================
END_PROC1


