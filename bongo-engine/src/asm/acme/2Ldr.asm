
;================================
;2ND PROCEDURE OF:
;THE BONGO NOINTERLEAVE LOADER
; (C) BY WEGI IN 2013.01.18
;================================


;******************
;* TECHNICAL DATA *
;******************

;SUPPORT 1541x , 1570/71 IN 1541 MODE [D64 - NOT D71 FILES (DISKS)]
;LOADER USE ATN LINE TO SYNC DATA SEND
;LOADER LOADING UNDER I/O AREA
;FOR FASTER LOAD IN 1570/71 USED 2mhz THIS GAVE A DIFFERENT TIMING THAN 1541
; - GET ATTENTION ON THIS OR REMAKE SOURCE CODE - or use option "no 2MHZ" from GUI

;SPRITES, $D011, BADLINES, IRQ, NMI - ALL OF THEM NOT "NOISING" FOR LOADER


;GENERALLY RECOMMENDED LOAD VIA TRAC AND SECTOR - LOAD VIA NAME IS POSSIBLE WHEN YOU USE 
;ADDITIONAL FUNCTION "FIND_BY_FILENAME", BUT THIS GEAVE A BIG AND UNCONFORMTABLE DELAY
;BY JUMP, READ AND FINDING DIR TO TRACK 18 TIME AFTER TIME SO IN THIS CASE YOU MUST
;WASTED MORE BYTES AND TIME... REALLY I'M NOT RECOMMENDED THIS OPTION

;***************
;* CONSTRAINTS *
;***************

;ON THE IEC - ONLY ONE UNITS CAN BE TURNED ON - YOU DRIVE WITH YOUR DEMO DISC 

;PLEASSE BE CARREFULLY ABOUT WRITE TO $DD00 !!!
;DO THIS OR LIKE BELOW:

; ALLWAYS 6 HIBITS MUST BE SET DOWN TO 0
;	LDA #VICBANK ;#0, #1, #2, #3
;	STA $DD00


;ALSO DON'T TOUCH $DD02 NEVER !!!
;ALL SHOULD BE HAPPY WHEN YOU WILL HAVE A BIG ATTENTION FOR THIS CASES

;************************************
;* A SHORT INFO HOW TO USE THIS ONE *
;************************************

;AT THE VERY FIRST YOU MUST DONE CALL ***JSR PROC1***
;SOMEWHERE IN A BEGINNING OF DEMO - THIS IS A DEDICATED TO THIS CODE
;PROC TO SEND DRIVECODE TO 1541x, 1570/71 IN 1541 MODE


;1. FOR LOAD CALL JSR PROC2 WITH SETUP:
;ACC AND Y MUST HAVE LO/HI BYTE OF POINTER 
;TO 5 BYTES INFO DATA TO WORK IN ORDER:

;---
;BASEADR		WORD - 2 BYTES IN LO/HI FORMAT
;STRTTRK		BYTE - 1 BYTE START TRACK FILE TO LOAD
;STRTSEC		BYTE - 1 BYTE START SECTOR FILE TO LOAD
;HOW_GET_LD	BYTE - 1 BYTE : IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---

;BASEADR = LOAD ADDRESS IF YOU WANNA LOAD FOR OWN LOAD AD.
;THIS INFO YOU SETUP IN HOW_GET_LD :
;HOW_GET_LD  =  0 - LOAD ADDRES FROM FILE - BASEADR IS IGNORED
;HOW_GET_LD <>  0 - LOAD ADDRES FROM BASEADR
;STRTTRK - START TRACK FILE TO LOAD
;STRTSEC - START SECTOR FILE TO LOAD

;START TRACK AND SECTOR VALUES THEY ARE GENERATED
; ***** INTO INCLUDE SCRIPT BY TRACKMOLINKER *****
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;|||||||||||||||||||||||||||||||||||||||||||||||||

;STRONG RECOMMENDED TO USE TRACKMOLINKER V 1.2


; *** ZERO PAGE IS UNTOUCHED ***


;********************
;* A SHORT EXAMPLES *
;********************

;1. -A

; LOADING FILE INTO *** $C000 *** FROM START TRACK 1 AND SECTOR 0

;		...
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2
;		BCS *		;LOAD ERROR - DEMO CRASHED

;		NOW IN ACC/XREG LO/HI IS THE POINTER TO END OF DATA+1 FOR OTHER CASE LIKE EXO BACK DECRUNCH...
;		...
;---
;BASEADR		!word $C000
;STRTTRK		!byte $01
;STRTSEC		!byte $00
;HOW_GET_LD	!byte $01
;---

;1. -B

; LOADING FILE INTO *** FILE LOAD ADDRES *** FROM START TRACK 1 AND SECTOR 0

;		...
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2
;		BCS *		;LOAD ERROR - DEMO CRASHED

;		NOW IN ACC/XREG LO/HI IS THE POINTER TO END OF DATA+1 FOR OTHER CASE LIKE EXO BACK DECRUNCH...
;		...
;---
;BASEADR		!word $0000 ;NOT IMPORTANT
;STRTTRK		!byte $01
;STRTSEC		!byte $00
;HOW_GET_LD	!byte $00	;BASEAD WILL BE IGNORED TAKE LOAD ADDRES FROM FILE
;---


; ZERO PAGE UNTOUCHED
;..................................................

;2. JSR PROC2+3 - DISCONECT LOADER AND RELEASE DRIVE

;3. JSR PROC2+6 - SEND ONE BYTE FROM ACC TO DRIVE... FOR OWN CODE, CHANGEDISK ENGINE, ADDITIONAL FUNCTION
;BUT COMPATIBILITY WITH GETTORAM PROTOCOL

;*****************************************
;* AND I GUESS THAT IS SHOULD BE ENOUGHT *
;* TO YOU OWN HAPPY USE :)               *
;* REGARDS - WEGI                        *
;*****************************************

!TO "ldr2.prg" , CBM
;----- PARAGRAPH @STARTPROC2@ -----

PROC2 = $1b00

* = PROC2
;-
		;A JUMP TABLE OF THIS
		;PROC2    = LOAD FILE AND STOP MOTOR
		;PROC2+3  = DISCONNECT LOADER
		;PROC2+6 = SEND BYTE FROM ACC TO THE DRIVE 
		
		JMP STARTLOAD		 ;LOAD AND AFTER THIS STOP MOTOR BY IDLE LOOP
		JMP LOADEROFF		 ;DISCONNECT LOADER
;------------
SENDDRIV2       ;SEND ONE BYTE

;---
         SEC
         ROR
         TAX
         LDA $DD02
         ORA #$30
         STA $DD02

         BIT $DD00
         BVC *-3
KEX
         LDA $DD02
         EOR #$20
         ORA #$10
         BCC *+4
         AND #$EF
         STA $DD02
         TXA
         LSR
         TAX
         BNE KEX
         LDA $DD02
         AND #$0F
         ORA #$10
         STA $DD02
;---	
		RTS
;-------------------
;=========
GET_Y_BYTES
         BIT $DD00
         BVC *-3
POB2     LDX #$3F
         STX $DD02
         iny
         STY RCALL
         BIT $EA
         LDY #$37
         ;nop
         LDA $DD00
         STY $DD02
         LSR
         LSR
         NOP
         NOP
         BIT $EA
         ORA $DD00
         STX $DD02

         LSR
         LSR
         LSR
         LSR
         STA HALFBYTE
         LDA $DD00
         STY $DD02
;---
RCALL    = *+1
         LDY #$00
;---
         NOP
         LDX #$1F
         LSR
         LSR
MY_MANY   = *+1
         CPY #$FF
         ORA $DD00
         STX $DD02
         AND #$F0
HALFBYTE = *+1
         ORA #$00
;---

		DEC $01
;--		
MY_BUF_LO = *+1
MY_BUF_HI = *+2
;-
		STA $1111,Y
		INC $01
;--
MY_CKSM = *+1
;-
		EOR #$00
		STA MY_CKSM
		BCC POB2
		RTS
;---------
MAIN_LINK	!word 0	;LINK OF SECTOR
MAIN_LDAD	!word 0	;LOAD ADDRESS FROM FILE
GDZIEHI = *+1
GDZIELO	!word 0	;ACTUAL ADDRESS OF DATA TO SAVE
MAIN_DATA	!byte 0	;ONE BYTE FOR DATA
;--------

;--------
TRNSM1      ;GET ALL 256 BYTES OF BLOCK
		LDA #<MAIN_LINK
		STA MY_BUF_LO
		LDA #>MAIN_LINK
		STA MY_BUF_HI
		LDY #1
		STY MY_MANY
		DEY
		STY MY_CKSM
		DEY
		JSR GET_Y_BYTES
;---		
		JSR SETBASE	;SET POINTERS...
;---
		LDY MY_SUBSTRACT ; 2 OR 4
		DEY
		CPY #1
		BEQ +
		STY MY_MANY ;Y = 3
		LDY #1
		JSR GET_Y_BYTES ;GET LOAD ADDRESS 2 BYTES
HOW_GET_LD = *+1
		LDA #$00
		BNE +
		STA WASFIRST
		LDA MAIN_LDAD
		STA BASEADR
		LDA MAIN_LDAD+1
		STA BASEADRHI
		JSR SETBASE
		SEC
		LDY #3
+:		
		LDA GDZIELO
		;SEC			;AFTER PROC GET_Y_BYTES C=1
MY_SUBSTRACT = *+1
		SBC #$02
		STA MY_BUF_LO
		LDA GDZIEHI
		SBC #$00
		STA MY_BUF_HI
ILE = *+1		
		LDA #$FF
		STA MY_MANY
		JSR GET_Y_BYTES

	;LAST BLOK - CONTINUE CKSM CALC AND READ DATA AFTER FILE
-:		LDA ILE
		CMP #$FF
		BEQ +
		JSR GAVE_ONE
		INC ILE
		BNE -
+:	
		RTS
;--------
GAVE_ONE
		LDA #<MAIN_DATA
		STA MY_BUF_LO
		LDA #>MAIN_DATA
		STA MY_BUF_HI
		LDY #$00
		STY MY_MANY
		DEY
		JSR GET_Y_BYTES
		LDA MAIN_DATA
		RTS
;--------
LADUJ	;READ DATA FROM ALL ACTUAL TRACK
;--------
		JSR GAVE_ONE	;GET HOW MANY SECTOR OF FILE ON THIS TRACK
		STA LICZSEC
		STA LICZSEC2
		CMP #22		;IF MORE THAN 21
		BCS *			;SOMETHING IS WRONG - ERROR!

;--------
;GET BLOCK TO BUFFER, NR SECT, CHECKSUM AND CALCULATE IT
;--------
STRTTRS
		JSR GAVE_ONE	;GET WHICH ONE SECTOR
		STA SECTNR		;ON THE TRACK IS NOW

		JSR TRNSM1		;GET BLOCK
		JSR GAVE_ONE	;GET CHECKSUM
		LDA MY_CKSM
		TAY			;SAVE CHECKSUM
		JSR SENDDRIV2
		TYA			;RECALL CHECKSUM
		BNE STRTTRS	;BAD OF CHKSM - LOOPBACK
		;BNE *
     LDA MAIN_LINK
     BEQ *+5
MY_JUMP	BIT READ_REST_SEC
		LDA #$2C
		STA MY_JUMP
;-------
		LDA MAIN_LINK		;IF IS THIS LAST BLOCK
		BNE +
		STA MARKER		;SAVE THIS TO OUR INDICATOR
+:
		DEC LICZSEC	;HOW MANY SECTOR ON THE TRACK...
		BNE STRTTRS

		INC WASFIRST	;AFTER THIS LOOP REWRITE INDICATOR OF FIRST TRACK
MARKER   =*+1   		;INDICATOR - LAST SECTOR WE HAVE ALLREADY?? | 0 = YES | <> 0 = NO |
		LDA #$01
		BNE LADUJ  	;LOAD LOOP
		RTS
;--------
LOADEROFF
;---
		LDA #$00
		!byte $2C
;---
READ_REST_SEC
;---
		LDA #$02
		!byte $2C
;---
READ1SEC
;---
		LDA #$03
		PHA
STRTTRK = *+1
		LDA #$00		;START TRAC FOR DRIVE
		JSR SENDDRIV2
;---
STRTSEC  =*+1
-:		LDA #$00		;START SECTOR OF FILE
		JSR SENDDRIV2
;---
		PLA		;SELECTED SECTOR SEND
		JMP SENDDRIV2

;---
;--------
;-------------------------------------
; GET BASE ADRESS OF TRACK AND ADD FOR
; NUMBER OF SECTOR VALUE
;-------------------------------------
SETBASE
BASEADR = *+1		;ADRESS START OF DATA IN THE C64 FROM ACTUAL TRACK
		LDY #$00
		STY GDZIELO
BASEADRHI = *+1
		LDA #$00
		STA GDZIEHI
		
SECTNR   =*+1			;SECTOR NR OF ORDER ON THE TRACK
		LDX #$00
		BEQ SETB3
;--------------------------------
; BELLOW BIT STRANGE MAYBE BUT:
; X * $FE = X * 254 <=> ((X*256) - (2*X))
; WITHOUT LOOP - FASTER
;--------------------------------
SETB2
		CLC
		ADC SECTNR  ;ADC X*256
		STA GDZIEHI ;OK ADRESS 
		TXA		;X -> A
		ASL		;X*2
		EOR #$FF    ;HERE X * (-1)
		;CLC        ;AFTER ASL C=0
		ADC #$01
		;CLC		;X FROM RANGE 1 TO 22 IN THE NEGATIVE + 1 WILL BE NEVER OVERFLOV
		ADC GDZIELO  ;SUBSTRACT BY ADD
		STA GDZIELO  ;CAUSE X < 0
		BCS SETB3
		DEC GDZIEHI
SETB3    
;=====================================
; OK NOW WE HAVE ALL FOR REWRITE DATA
; TO RAM COMMODORE 64 - SO LET'S BEGIN
;=====================================
PREPI
;=========  DEFAULT VALUES        
		LDA #$FF
		STA ILE		;IF NOT FIRST SECTOR $FE BYTES OF DATA
					;LABEL 'ILE' <=> 'HOW MANY'
		LDA #$02
		STA MY_SUBSTRACT	;IF NOT FIRST SECTOR - OFFSET 2

		LDX MAIN_LINK+1	;OFSET OF LAST BYTE - IF IS THIS LAST SECTOR
		LDY MAIN_LINK		;INDICATOR FROM LINK - IF = 0 THEN THIS IS THE LAST SECTOR
		BNE +
		STX ILE		;LAST BYTE FROM FILE IN THE SECTOR
;=========		
+:					;IT'S THE 1ST TRACK?
WASFIRST =*+1   		;INDICATOR - IS THE FIRST TRACK WHICH WE READING | 0 = YES | <> 0 = NO |
		LDA #$00
		BNE NOFIRSTTRACK

		LDA SECTNR		;IT'S THE FIRST SECTOR?
		BNE NOFIRSTSEC
		LDA BASEADR

		CLC
		ADC #$FC       ;IF FIRST SECTOR IS ONE ON FIRST TRACK
		STA NEXTBASE   ;CALCULATE NEXTBASE ADRESS
		LDA BASEADRHI
		ADC #0
		STA NEXTBASEHI
		LDA MAIN_LINK
		BEQ +
		STA STRTTRK
		LDA MAIN_LINK+1
		STA STRTSEC

+:		LDA #$04		;1ST SECTOR - OFFSET 4
		STA MY_SUBSTRACT		
		BNE NOFIRSTTRACK
;-----------------------------------------------------------------
;EVERY ONE SECTOR ON THE 1ST TRACK - MUS BE HAVE CORRECTED ADRESS
;-----------------------------------------------------------------
NOFIRSTSEC  
		LDA GDZIELO
		SEC
		SBC #$02		;CORRECT ON THE 1ST TRACK
		STA GDZIELO	;OFFSET MUST BE LESS 2
		BCS NOFIRSTTRACK
		DEC GDZIEHI
;--------
NOFIRSTTRACK
;--------
; IF IS THE LAST SECTOR
; CALCULATE END ADRESS
;--------
		LDA MAIN_LINK		;IT'S THE LAST SECTOR?
		BNE PREPI4

		LDA ILE
		SEC
		SBC MY_SUBSTRACT
		SEC	;ONE MORE... BYTE
		ADC GDZIELO 
		STA EOF
		LDA GDZIEHI	;OF FILE
		ADC #$00
		STA EOF+1
;----------------------------------------------
;ON EVERY ONE LAST SECTOR ARRIVING TO C64
;MUST CALCULATED BASEADRES FOR NEXT TRACK DATA
;----------------------------------------------
PREPI4
LICZSEC2 =*+1			;VALUES OF SECTORS ON THE TRACK OUR FILE
		LDX #$00		;LAST SECTOR NUMBER FROM TRACK
		DEX
		CPX SECTNR		;IS HERE NOW THE LAST SECTOR?
		BNE PREPI3		;NO
		CPX #$00
		BNE PREPI4A	;THAT WAS ONLY ONE SECTOR ON THE TRACK?
		CPX WASFIRST	;YES ONLY ONE BUT THAT WAS THE FIRST SECTOR OF FILE?
		BEQ PREPI3		;YES - DON'T CALC NEXTBASE - WE HAVE GOTTA HIS BEFORE!!
PREPI4A		    
		LDA GDZIELO	;YES THAT WAS THE LAST SECTOR
		CLC			;FROM TRACK
		ADC #$FE
		STA NEXTBASE	;SO WITHOUT BIG CALCULATIONC
		LDA GDZIEHI	;GET THE ADRESS FOR NEW DATA FROM
		ADC #$00		;NEW (NEXT) TRACK
		STA NEXTBASEHI
;------
PREPI3
		LDX LICZSEC	;WAS THAT THE LAST SECTOR FORM TRACK READING NOW?
		CPX #1
		BNE +			;NO
PREPI3A
NEXTBASE = *+1		;ADRESS OF DATA FOR NEXT TRACK
		LDA #$00    ;YES MAKE BASEADR FOR NEW TRACK
		STA BASEADR
NEXTBASEHI = *+1
		LDA #$00
		STA BASEADRHI
+:    
		RTS
;--------
EOF      !word 0		;END OF FILE ADRESS
;--------
LICZSEC  !byte 0		;VALUES OF SECTORS ON THE TRACK OUR FILE
;--------
GET_START_DATA
		DEC $01
MY_VECT=*+1
		LDA $1111,Y
		INC $01
		INY
		RTS
;========================
STARTLOAD
		STA MY_VECT
		STY MY_VECT+1
		LDA #$20
		STA MY_JUMP
		LDA $01
		STA BACKUP01
		LDA #$35
		STA $01
		
		LDY #$01
		STY MARKER
		DEY
		STY WASFIRST	;FIRST TRACK - INDICATOR FOR CORRECT #2 ADRESS OF SECTOR

		
		;Y = 0
		JSR GET_START_DATA
		STA BASEADR

		JSR GET_START_DATA
		STA BASEADRHI
FROMTRACK
		JSR GET_START_DATA
		STA STRTTRK

		JSR GET_START_DATA
		STA STRTSEC

		JSR GET_START_DATA
		STA HOW_GET_LD


FIRSTSECT
		JSR READ1SEC
		JSR LADUJ
ENDLOAD

BACKUP01 = *+1
		LDA #$00
		STA $01

		LDA EOF
		LDX EOF+1

		;AT THE END - CARRY IS CLEAR AND FOR EXODECRUNCHER
		;IN ACC LOW BYTE OF EOF FILE IN X REG HI BYTE OF EOF FILE

		CLC
		RTS
;=====================
;--------
END_PROC2

