; BONGO SFX RECURSIVE DE-CRUNCHER - WEGI 2013.02.13
; WITH GOLDEN SEQUENCES SERVICE
; FAST VERSION
; MEASSURE DECRUNCH TIME EXAMPLE	
; optimize for upspeed help me Bitbreaker
; thanks a lot Toby
;---	

;---
LENGTH		= $02 
STREAM_BYTE	= LENGTH +1
PUT			= LENGTH+2
COPY_SEQ		= LENGTH+4
;---
IRQCNTR 		= LENGTH +6

;=========================
		*= $0801

;BASICLINE	1680 SYS2059 !!!! 080b, not 0801!
		.BYTE $0B,$08,$90,$06,$9E,$32
		.BYTE $30,$35,$39,$00
;--------
		LDY #$00
;----
LDA #<IRQ
STA $FFFE
LDA #>IRQ
STA $FFFF
LDA #$C6
STA $DC04
LDA #$4C
STA $DC05


		
	-	LDA CODEX,Y
		STA DECRUNCH,Y		
		LDA CODEX +    (<LENGTH_DECRUNCHER),Y	
		STA DECRUNCH + (<LENGTH_DECRUNCHER),Y		
		INY 	
		BNE -	
;=================		
		LDA #$38
		STA $01

LDA #$20
STA $0650,Y
INY
BNE *-4

;-----
;-----	

		LDX #$00

		LDY #>FILE_LENGTH+256
-
ADR_GT = *+2
		LDA FINISH-$0100,X
ADR_PT = *+2
		STA $FEFE,X
		INX
		BNE -
		DEC ADR_GT	
		DEC ADR_PT
		DEY
		BNE -

		STX IRQCNTR
		STX IRQCNTR+1

;--- JMP $080D VIA STACK
		LDA #$08
		PHA
		LDA #$0C
		PHA
;-----

		LDA #<START_DATA
		LDY #>START_DATA 
		JMP DECRUNCH	
;---

;============================================================
CODEX		
.LOGICAL		$0400
DECRUNCH	

        ;---------------------------------------------
        ; FIRST ENTRY POINT TO DECRUNCHER
        ; IN ACC LO BYTE DATA ADDRESS IN Y HI BYTE
        ;---------------------------------------------


                sta COPY_SEQ      ;LO BYTE START DATA
                sty COPY_SEQ+1    ;HI BYTE

                ldy #$00
                sty STREAM_BYTE
                lda (COPY_SEQ),y
			STA ITERATOR
        ;----------------------------
        ; BELLOW IF YOU MAKE DECRUNCH
        ; INTO YOUR AREA BEFORE YOU
        ; MUST SET THE PUT VECTOR
        ; AND GET2 VECTOR
        ;----------------------------

                pha ;remember A

                and #$3f
                sta LENGTH
                bne +
-
                lda (COPY_SEQ),y
                sta SEQENCES-1,y
+
                iny
                cpy LENGTH
                bne -

                pla ;restore A
                bpl +
                lda (COPY_SEQ),y
                sta PUT
                STA GET2LO
                iny
                lda (COPY_SEQ),y
                sta PUT+1
                STA GET2HI
;                iny
+
                tya 
                ;clc
                SEC
                adc COPY_SEQ     ;move all offset to x and keep lowbyte to zero! then no page boundaries will be crossed on lda $1111,x and also we can store x easily as lowbyte instead of index
                tax
                lda #$00
                adc COPY_SEQ+1
                sta MGET1HI
                sta MGET2HI
;**********************
;* MAIN DECRUNCH LOOP *
;**********************
DECRUNCH_LOOP
                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE      ;A can be trashed
                bcc IS_UNCRUNCH

                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE      ;A can be trashed
                bcs COPY_ONLY_1          ;never zero, rol would set zero flag in case

                jsr GET3_OR_6BITS        ;sets Y to 0
                sta LENGTH
                cmp #$00
                bne LOOP_COPY
                beq CHECK_PAGE
;---
COPY_ONLY_1
                ldy #$01
                sty LENGTH
                dey
                sty COPY_SEQ+1
;=================
LOOP_COPY       ;Y = #$00
MGET2HI = *+2
                lda $1000,x
                sta (PUT),y
                inx
                bne +
                jsr BIG_INCR
+
                inc PUT
                bne *+4
                inc PUT+1
                dec LENGTH
                bne LOOP_COPY
;---
CHECK_PAGE
                lda COPY_SEQ+1
                beq IS_UNCRUNCH
                dec COPY_SEQ+1
                jmp LOOP_COPY

IS_UNCRUNCH

                ldy #$03    ;used in SHORT_C and as A later on

                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE    ;A can be trashed
                lda #$02    ;2 BYTES SEQ?
                bcc SHORT_C

                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE    ;A can be trashed
                tya         ;A = Y = 3
                bcc START_UNCRUNCH

                jsr GET3MANYBITS
                tay
                beq CHECK_SEQ

                lda #$01
                jsr GETCRUNCHBYTES
                cmp #4
                bcs START_UNCRUNCH

;=====
EOF
ITERATOR = *+1
			LDA #$00
			ASL
			BPL +
GET2LO = *+1
			LDA #$00
GET2HI = *+1
			LDY #$00
			LDX #$00
			JMP DECRUNCH
+               sei
                DEC $01
                LDA #$EF
                CMP $DC01
                BNE *-3
                RTS
                ;clc       ;MAYBE FOR PARTIAL LOAD INDICATOR
                ;rts

;-----------------------------
;HERE SERVICE GOLDEN SEQUENCES
;-----------------------------
CHECK_SEQ
                ldy #4
                jsr GETMANYBITS ;GET4BITS

                ;NR OF SEQ *3
                sta LENGTH
                asl
                adc LENGTH
                tay
;===
                lda SEQENCES,Y
                sta LENGTH_+1

                lda SEQENCES+2,Y ;OFFSET HI BYTE
                sta COPY_SEQ+1
                lda SEQENCES+1,Y ;OFFSET LO BYTE
                ldy #$00
                beq SHORT_WAY

;===================================================
;=============      UNCRUNCHING      ===============
;===================================================
START_UNCRUNCH
                ldy #$04         ;4 BITS FOR >2 BYTES SEQ
SHORT_C
                sta LENGTH_+1
                jsr GET_Y_BYTES  ;GET 3 OR 4 BITS
;***************************************************
SHORT_WAY
                ;SUBSTRACT OFFSET
                eor #$ff         ;-COPY_SEQ + PUT = PUT - COPY_SEQ :-) -> COPY_SEQ ^ $ff + 1 (carry!) + PUT
                sec
                adc PUT
                sta CP+1
                lda PUT+1
                sbc COPY_SEQ+1
                sta CP+2
;---
                ;Y = 0
-
CP              lda $1000,y
                sta (PUT),y
                iny
LENGTH_         cpy #$00
                bne -

                tya
                clc
                adc PUT
                sta PUT
                bcc *+4
                inc PUT+1
                jmp DECRUNCH_LOOP
;==========================
GET_STREAM_BYTE
                ;A is trashed, but in most cases this is no problem
MGET1HI = *+2
                lda $1000,x   ;we could do lda #$01 + slo $1000,x instead of the next three commands but slo takes 7 cycles, dammit :-)
                sec
                rol
                sta STREAM_BYTE
                inx
                beq BIG_INCR
                rts
;---
BIG_INCR
                inc MGET1HI
                inc MGET2HI
+
                ;sec ; FOR FUTURE USE
                rts
;============
GET3MANYBITS
                ldy #3             ;fetch 3 bits
GETMANYBITS                        ;Y holds the number of bits to fetch
                lda #$00
GETCRUNCHBYTES
-
                asl STREAM_BYTE
                bne *+7
                pha
                jsr GET_STREAM_BYTE
                pla
                rol
                dey
                bne -
                rts
;===
GET3_OR_6BITS
                jsr GET3MANYBITS    ;sets Y to 0
                cmp #7
                bne GETBYTESTOCOPY

                jsr GET3MANYBITS    ;IN ACC #BITS - 7 , C=0, sets Y to 0
                adc #7
                bne GETBYTESTOCOPY
GET_Y_BYTES
                jsr GETMANYBITS     ;sets Y to 0, A is already 0, actually no need to set it to 0 again @GETMANYBITS
-
GETBYTESTOCOPY  ;why can it happen that we shall copy 0 bytes? can't we avoid that case while packing?!
                ;ldy #$00 y is always 0
                sty COPY_SEQ+1
                tay
                beq LOWER

                lda #$01
                cpy #$08
                bcc GETCRUNCHBYTES  ;we fetch too less bits to be able to rol anything into COPY_SEQ+1, so we take the faster version
-
                asl STREAM_BYTE
                bne *+7
                pha
                jsr GET_STREAM_BYTE
                pla
                rol
                rol COPY_SEQ+1
                dey
                bne -
                rts
LOWER
                lda #$01
                rts
;---
_DECRUNCHER_LENGTH = * -DECRUNCH
;====
IRQ
		PHA
		LDA #$35
		STA $01
INC $D020

;====
STY IRQ_Y
		BIT $DC0D
		INC IRQCNTR
		BNE *+4
		INC IRQCNTR+1
		
		LDY #$28		
		LDA IRQCNTR+1
		JSR PRH
		LDA IRQCNTR
		JSR PRH
		
DEC $D020		

		LDA #$38
		STA $01
IRQ_Y =*+1
		LDY #$FF
;===
		PLA
		RTI
;=======================
PRH      PHA
         LSR A
         LSR A
         LSR A
         LSR A
         JSR CONV1
         PLA
CONV1    AND #$0F
         ORA #$30
         CMP #$3A
         BCC CONVEND
         ADC #$06
STCHAR   AND #$3F
CONVEND  STA $0680,Y
		INY
		RTS
;=======

SEQENCES = *
;---------------------------------------------
; 48 BYTES MAX FOR MY LOVELLY GOLDEN SEQUENCES
; OF COURSE - YOU CAN CHANGE SEQ ADDRESS
;---------------------------------------------		
ENDCRUSH	
LENGTH_DECRUNCHER = * -DECRUNCH	
.HERE
MYPOINT	
FILE_LENGTH = FINISH-MYPOINT
START_DATA = $FFFF - FINISH+MYPOINT-1
;.BINARY "YOU_CRUNCHED_DATA_NAME",2 ;WITHOUT LOAD ADDRESS
FINISH	
