
;================================
;2ND PROCEDURE OF:
;THE BONGO NOINTERLEAVE LOADER
; INTEGRATED WITH BONGO DECRUNCHER NO-RECURSIVE 
;WITH GOLDEN SEQ
; (C) BY WEGI IN 2013.01.18
;================================


;******************
;* TECHNICAL DATA *
;******************



;SUPPORT 1541x , 1570/71 IN 1541 MODE [D64 - NOT D71 FILES (DISKS)]
;SHORT TECHNICAL DATA:
;LOADER USE ATN LINE TO SYNC DATA SEND
;LOADER LOADING UNDER I/O AREA
;FOR FASTER LOAD IN 1570/71 USED 2mhz THIS GAVE A DIFFERENT TIMING THAN 1541
; - GET YOUR ATTENTION ON THIS OR REMAKE SOURCE CODE, OR DISABLE 2mhz FROM GUI

;SPRITES, $D011, BADLINES, IRQ, NMI - ALL OF THEM NOT "NOISING" FOR LOADER


;GENERALLY RECOMMENDED LOAD VIA TRAC AND SECTOR - LOAD VIA NAME IS POSSIBLE WHEN YOU USE 
;ADDITIONAL FUNCTION "FIND_BY_FILENAME", BUT THIS GEAVE A BIG AND UNCONFORMTABLE DELAY
;BY JUMP, READ AND FINDING DIR TO TRACK 18 TIME AFTER TIME SO IN THIS CASE YOU MUST
;WASTED MORE BYTES AND TIME... REALLY I'M NOT RECOMMENDED THIS OPTION

;***************
;* CONSTRAINTS *
;***************

;ON THE IEC - ONLY ONE UNITS CAN BE TURNED ON - YOU DRIVE WITH YOUR DEMO DISC 

;PLEASSE BE CARREFULLY ABOUT WRITE TO $DD00 !!!
;DO THIS OR LIKE BELOW:

; ALLWAYS 6 HIBITS MUST BE SET DOWN TO 0
;	LDA #VICBANK ;#0, #1, #2, #3
;	STA $DD00


;ALSO DON'T TOUCH $DD02 NEVER !!!
;ALL SHOULD BE HAPPY WHEN YOU WILL HAVE A BIG ATTENTION FOR THIS CASES


;************************************
;* A SHORT INFO HOW TO USE THIS ONE *
;************************************

;AT THE VERY FIRST YOU MUST DONE CALL ***JSR PROC1***
;SOMEWHERE IN A BEGINNING OF DEMO - THIS IS A DEDICATED TO THIS CODE
;PROC TO SEND DRIVECODE TO 1541x, 1570/71 IN 1541 MODE


;1. FOR LOAD AND FLY DECRUNCH CALL JSR PROC2 WITH SETUP:
;ACC AND Y MUST HAVE LO/HI BYTE OF POINTER 
;TO 5 BYTES INFO DATA TO WORK IN ORDER:

;---
;BASEADR		WORD - 2 BYTES IN LO/HI FORMAT
;STRTTRK		BYTE - 1 BYTE START TRACK FILE TO LOAD
;STRTSEC		BYTE - 1 BYTE START SECTOR FILE TO LOAD
;HOW_GET_LD	BYTE - 1 BYTE : IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---

;BASEADR = LOAD ADDRESS IF YOU WANNA LOAD FOR OWN LOAD AD.
;THIS INFO YOU SETUP IN HOW_GET_LD :
;HOW_GET_LD  =  0 - LOAD ADDRES FROM FILE - BASEADR IS IGNORED
;HOW_GET_LD <>  0 - LOAD ADDRES FROM BASEADR
;STRTTRK - START TRACK FILE TO LOAD
;STRTSEC - START SECTOR FILE TO LOAD

;START TRACK AND SECTOR VALUES THEY ARE GENERATED
; ***** INTO INCLUDE SCRIPT BY TRACKMOLINKER *****
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;|||||||||||||||||||||||||||||||||||||||||||||||||

;STRONG RECOMMENDED TO USE TRACKMOLINKER V 1.2


;... IF YOU WANNA DECRUNCHING RAW DATA INTO YOUR DEPACK ADDRESS 
;YOU MUST BEFORE SET UP THE "PUT" VECTOR OF DECRUNCHER 
;AND LOADING SAVED RAW DATA MAKED BY BONGO CRUNCHER

; IMPORTANT!!! - $01 VALUE MUST BE SET UP BEFORE JSR PROC2
; THIS VALUE IS RESTORED FOR DECRUNCH FOR POSSIBLITY
; DECRUNCH DATA TO COLOR RAM (IT'S MY OPTION CAUSE I USED THIS)
; OF COURSE - LOAD ADDRES MUST BE RECHANGED FOR DIFFERENT
; THAN I/O AREA 

;ZERO PAGE USED ONLY BY DECRUNCHER - 6 BYTES

;2. FOR ONLY LOAD FILE CALL JSR PROC2+3
; - LIKE BEFORE SETUP YOU DONE 
; DECRUNCHER STILL NOT ACTIVE AND BY LOADER
; *** ZERO PAGE IS UNTOUCHED ***

;3. JUST DECRUNCH - CALL JSR PROC2+6
;ACC/YREG - LO/HI BYTE POINTER TO DATA FOR DECRUNCH
;$01 VALUE YOU MUST BEFORE SETUP FOR DECRUNCH AS YOU NEED
;AND ABOUT RAW DATA THE RULE IS THIS SAME LIKE IN POINT 1

;4. JSR PROC2+9 - DISCONECT LOADER AND RELEASE DRIVE

;5. JSR PROC2+12 - SEND ONE BYTE FROM ACC TO DRIVE... FOR OWN CODE, CHANGEDISK ENGINE, ADDITIONAL FUNCTION
;BUT COMPATIBILITY WITH GETTORAM PROTOCOL

;********************
;* A SHORT EXAMPLES *
;********************

;1. -A

; LOADING FILE INTO *** $C000 *** FROM START TRACK 1 AND SECTOR 0
; AND DECRUNCH INTO DEPACK ADDRES FROM CRUNCHED DATA

;		...
;		LDA #$34	;DECRUNCH VALUE!
;		STA $01
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2
;		BCS *		;LOAD ERROR - DEMO CRASHED

;		NOW IN ACC/XREG LO/HI IS THE POINTER TO END OF DATA+1 FOR OTHER CASE LIKE EXO BACK DECRUNCH...
;		...
;---
;BASEADR		.WORD $C000
;STRTTRK		.BYTE $01
;STRTSEC		.BYTE $00
;HOW_GET_LD	.BYTE $01
;---

;1. -B

; LOADING FILE INTO *** FILE LOAD ADDRES *** FROM START TRACK 1 AND SECTOR 0
; AND DECRUNCH INTO DEPACK ADDRES FROM CRUNCHED DATA

;		...
;		LDA #$34	;DECRUNCH VALUE!
;		STA $01
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2
;		BCS *		;LOAD ERROR - DEMO CRASHED

;		NOW IN ACC/XREG LO/HI IS THE POINTER TO END OF DATA+1 FOR OTHER CASE LIKE EXO BACK DECRUNCH...
;		...
;---
;BASEADR		.WORD $0000 ;NOT IMPORTANT
;STRTTRK		.BYTE $01
;STRTSEC		.BYTE $00
;HOW_GET_LD	.BYTE $00	;BASEAD WILL BE IGNORED TAKE LOAD ADDRES FROM FILE
;---


;1. -C

; LOADING FILE INTO *** $C000 *** FROM START TRACK 1 AND SECTOR 0
; AND DECRUNCH INTO *** $4000 *** FROM CRUNCHED DATA
; DATA MUST BE DONE AS RAW DATA BY BONGO CRUNCHER

;		...
;		LDA #<$4000 ;BONGO DEPACK ADDRESS SETUP
;		STA PUT
;		LDA #>$4000
;		STA PUT+1
;		LDA #$34	;DECRUNCH VALUE!
;		STA $01
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2
;		BCS *		;LOAD ERROR - DEMO CRASHED

;		NOW IN ACC/XREG LO/HI IS THE POINTER TO END OF DATA+1 FOR OTHER CASE LIKE EXO BACK DECRUNCH...
;		...

;---
;BASEADR		.WORD $C000
;STRTTRK		.BYTE $01
;STRTSEC		.BYTE $00
;HOW_GET_LD	.BYTE $01
;---
;================================		
;2. -A
;2. -B 

;SETUP LIKE 1. -A, 1. -B PARAGRAPH
;A DIFFERENT IS - *** JSR PROC2 *** CHANGE TO *** JSR PROC2+3 ***
;DECRUNCHER IS INACTIVE, ZERO PAGE UNTOUCHED

;3. -A

; DECRUNCH DATA IN RAM FROM $C000
; INTO DEPACK ADDRES FROM CRUNCHED DATA

;		...
;		LDA #$34	;FOR DECRUNCHER VALUE
;		STA $01
;		LDA #<$C000
;		LDY #>$C000
;		JSR PROC2+6
;		...

;3. -B

; DECRUNCH ***RAW*** DATA IN RAM FROM $C000 INTO $4000
; DATA MUST BE DONE AS ***RAW*** DATA BY BONGO CRUNCHER

;		...
;		LDA #$34	;FOR DECRUNCHER VALUE
;		STA $01
;		LDA #<$4000
;		STA PUT
;		LDA #>$4000
;		STA PUT +1
;		LDA #<$C000
;		LDY #>$C000
;		JSR PROC2+6
;		...

;..................................................

;*****************************************
;* AND I GUESS THAT IS SHOULD BE ENOUGHT *
;* TO YOU OWN HAPPY USE :)               *
;* REGARDS - WEGI                        *
;*****************************************


;----- PARAGRAPH @STARTPROC2@ -----
PROC2 = $0900

* = PROC2
;-
		.ALIGN 256
;-
		;A JUMP TABLE OF THIS -
		;PROC2	- LOAD FILE AND FLY DECRUNCH
		;PROC2+3 	- OLY LOAD WITHOUT DECRUNCH
		;PROC2+6 ONLY DECRUNCH - ACC AND YREG LO/HI BYTE OF START DATA - $01 MUST BE SET HOW YOU NEED !!!
		;PROC2+9 DISCONNECT LOADER
		;PROC2+12 - SEND ONE BYTE TO DRIVE FROM ACC		

		JMP FLY_DECR
		JMP ONLY_LOAD		 ;LOAD AND AFTER THIS STOP MOTOR BY IDLE LOOP
		JMP JUST_DECRUNCH
		JMP LOADEROFF		 ;DISCONNECT LOADER
;------------
SENDDRIV2       ;SEND ONE BYTE

;---
         SEC
         ROR
         TAX
         LDA $DD02
         ORA #$30
         STA $DD02

         BIT $DD00
         BVC *-3
KEX
         LDA $DD02
         EOR #$20
         ORA #$10
         BCC *+4
         AND #$EF
         STA $DD02
         TXA
         LSR
         TAX
         BNE KEX
         LDA $DD02
         AND #$0F
         ORA #$10
         STA $DD02
;---
		
		RTS
;-------------------
;=========
GET_Y_BYTES
;---
         BIT $DD00
         BVC *-3
POB2     LDX #$3F
         STX $DD02
         ;NOP
         INY
         STY RCALL
         BIT $EA
         LDY #$37
         LDA $DD00
         STY $DD02
         LSR
         LSR
         NOP
         NOP
         BIT $EA
         ORA $DD00
         STX $DD02

         LSR
         LSR
         LSR
         LSR
         STA HALFBYTE
         LDA $DD00
         STY $DD02
;---
RCALL    = *+1
         LDY #$00
;---
         NOP
         LDX #$1F
         LSR
         LSR
MY_MANY   = *+1
         CPY #$FF
         ORA $DD00
         STX $DD02
         AND #$F0
HALFBYTE = *+1
         ORA #$00
;---


		DEC $01
;--		
MY_BUF_LO = *+1
MY_BUF_HI = *+2
;-
		STA $1111,Y
		INC $01
;--
MY_CKSM = *+1
;-
		EOR #$00
		STA MY_CKSM
		BCC POB2
		RTS
;---------
MAIN_LINK	.WORD 0	;LINK OF SECTOR
MAIN_LDAD	.WORD 0	;LOAD ADDRESS FROM FILE
GDZIEHI = *+1
GDZIELO	.WORD 0	;ACTUAL ADDRESS OF DATA TO SAVE
MAIN_DATA	.BYTE 0	;ONE BYTE FOR DATA
;--------

;--------
TRNSM1      ;GET ALL 256 BYTES OF BLOCK
		LDA #<MAIN_LINK
		STA MY_BUF_LO
		LDA #>MAIN_LINK
		STA MY_BUF_HI
		LDY #1
		STY MY_MANY
		DEY
		STY MY_CKSM
		DEY
		JSR GET_Y_BYTES
;---		
		JSR SETBASE	;SET POINTERS...
;---
		LDY MY_SUBSTRACT ; 2 OR 4
		DEY
		CPY #1
		BEQ +
		STY MY_MANY ;Y = 3
		LDY #1
		JSR GET_Y_BYTES ;GET LOAD ADDRESS 2 BYTES
HOW_GET_LD = *+1
		LDA #$00
		BNE +
		STA WASFIRST
		LDA MAIN_LDAD
		STA BASEADR
		LDA MAIN_LDAD+1
		STA BASEADRHI
		JSR SETBASE
		SEC
		LDY #3
	+		
		LDA GDZIELO
		;SEC			;AFTER PROC GET_Y_BYTES C=1
MY_SUBSTRACT = *+1
		SBC #$02
		STA MY_BUF_LO
		LDA GDZIEHI
		SBC #$00
		STA MY_BUF_HI
ILE = *+1		
		LDA #$FF
		STA MY_MANY
		JSR GET_Y_BYTES

	;LAST BLOK - CONTINUE CKSM CALC AND READ DATA AFTER FILE
	-	LDA ILE
		CMP #$FF
		BEQ +
		JSR GAVE_ONE
		INC ILE
		BNE -
	+	
		RTS
;--------
GAVE_ONE
		LDA #<MAIN_DATA
		STA MY_BUF_LO
		LDA #>MAIN_DATA
		STA MY_BUF_HI
		LDY #$00
		STY MY_MANY
		DEY
		JSR GET_Y_BYTES
		LDA MAIN_DATA
		RTS
;--------
LADUJ	;READ DATA FROM ALL ACTUAL TRACK
;--------
		JSR GAVE_ONE	;GET HOW MANY SECTOR OF FILE ON THIS TRACK
		STA LICZSEC
		STA LICZSEC2

		CMP #22		;IF MORE THAN 21
		BCS *			;SOMETHING IS WRONG - ERROR!

;--------
;GET BLOCK TO BUFFER, NR SECT, CHECKSUM AND CALCULATE IT
;--------
STRTTRS
		JSR GAVE_ONE	;GET WHICH ONE SECTOR
		STA SECTNR		;ON THE TRACK IS NOW

		JSR TRNSM1		;GET BLOCK
		JSR GAVE_ONE	;GET CHECKSUM
		LDA MY_CKSM
		TAY			;SAVE CHECKSUM
		JSR SENDDRIV2
		TYA			;RECALL CHECKSUM
		BNE STRTTRS	;BAD OF CHKSM - LOOPBACK
		;BNE *
		LDA MAIN_LINK
		BEQ *+5
MY_JUMP	BIT READ_REST_SEC
		LDA #$2C
		STA MY_JUMP

;-------
		LDA LICZSEC3
		BEQ NO_FLY
		LDA #<READ_SEC
		STA WHERE+1
		LDA #>READ_SEC
		STA WHERE+2
DECR_WAY	JMP FIRSTDECR
READ_SEC
NO_FLY

		LDA MAIN_LINK		;IF IS THIS LAST BLOCK
		BNE +
		STA MARKER		;SAVE THIS TO OUR INDICATOR
	+
		DEC LICZSEC	;HOW MANY SECTOR ON THE TRACK...
		BNE STRTTRS
		INC WASFIRST	;AFTER THIS LOOP REWRITE INDICATOR OF FIRST TRACK
;----- Paragraph @decrway@ -----
		LDA LICZSEC2
		CLC
		ADC LICZSEC3
		STA LICZSEC3		

		LDA #<RSS2
		STA WHERE+1
		LDA #>RSS2
		STA WHERE+2
RSS3		BIT SEC_DECR
RSS2

MARKER   =*+1   		;INDICATOR - LAST SECTOR WE HAVE ALLREADY?? | 0 = YES | <> 0 = NO |
		LDA #$01
		STA MARKER2
		BNE LADUJ  	;LOAD LOOP
		RTS
;--------
LOADEROFF
;---
		LDA #$00
		.BYTE $2C
;---
READ_REST_SEC
;---
		LDA #$02
		.BYTE $2C
;---
READ1SEC
;---
		LDA #$03
		PHA
STRTTRK = *+1
		LDA #$00		;START TRAC FOR DRIVE
		JSR SENDDRIV2
;---
STRTSEC  =*+1
	-	LDA #$00		;START SECTOR OF FILE
		JSR SENDDRIV2
;---
		PLA		;SELECTED SECTOR SEND
		JMP SENDDRIV2

;---
;--------
;-------------------------------------
; GET BASE ADRESS OF TRACK AND ADD FOR
; NUMBER OF SECTOR VALUE
;-------------------------------------
SETBASE
BASEADR = *+1		;ADRESS START OF DATA IN THE C64 FROM ACTUAL TRACK
		LDY #$00
		STY GDZIELO
BASEADRHI = *+1
		LDA #$00
		STA GDZIEHI
		
SECTNR   =*+1			;SECTOR NR OF ORDER ON THE TRACK
		LDX #$00
		BEQ SETB3
;--------------------------------
; BELLOW BIT STRANGE MAYBE BUT:
; X * $FE = X * 254 <=> ((X*256) - (2*X))
; WITHOUT LOOP - FASTER
;--------------------------------
SETB2
		CLC
		ADC SECTNR  ;ADC X*256
		STA GDZIEHI ;OK ADRESS 
		TXA		;X -> A
		ASL		;X*2
		EOR #$FF    ;HERE X * (-1)
		;CLC        ;AFTER ASL C=0
		ADC #$01
		;CLC		;X FROM RANGE 1 TO 22 IN THE NEGATIVE + 1 WILL BE NEVER OVERFLOV
		ADC GDZIELO  ;SUBSTRACT BY ADD
		STA GDZIELO  ;CAUSE X < 0
		BCS SETB3
		DEC GDZIEHI
SETB3    
;=====================================
; OK NOW WE HAVE ALL FOR REWRITE DATA
; TO RAM COMMODORE 64 - SO LET'S BEGIN
;=====================================
PREPI
;=========  DEFAULT VALUES        
		LDA #$FF
		STA ILE		;IF NOT FIRST SECTOR $FE BYTES OF DATA
					;LABEL 'ILE' <=> 'HOW MANY'
		LDA #$02
		STA MY_SUBSTRACT	;IF NOT FIRST SECTOR - OFFSET 2

		LDX MAIN_LINK+1	;OFSET OF LAST BYTE - IF IS THIS LAST SECTOR
		LDY MAIN_LINK		;INDICATOR FROM LINK - IF = 0 THEN THIS IS THE LAST SECTOR
		BNE +
		STX ILE		;LAST BYTE FROM FILE IN THE SECTOR
;=========		
	+				;IT'S THE 1ST TRACK?
WASFIRST =*+1   		;INDICATOR - IS THE FIRST TRACK WHICH WE READING | 0 = YES | <> 0 = NO |
		LDA #$00
		BNE NOFIRSTTRACK

		LDA SECTNR		;IT'S THE FIRST SECTOR?
		BNE NOFIRSTSEC
		LDA BASEADR
		SEC
		SBC #4
		STA LOW_ADR
		LDA BASEADRHI
		SBC #0
		STA HI_ADR
		LDA BASEADR

		CLC
		ADC #$FC       ;IF FIRST SECTOR IS ONE ON FIRST TRACK
		STA NEXTBASE   ;CALCULATE NEXTBASE ADRESS
		LDA BASEADRHI
		ADC #0
		STA NEXTBASEHI
		LDA MAIN_LINK
		BEQ +
		STA STRTTRK
		LDA MAIN_LINK+1
		STA STRTSEC

	+	LDA #$04		;1ST SECTOR - OFFSET 4
		STA MY_SUBSTRACT		
		BNE NOFIRSTTRACK
;-----------------------------------------------------------------
;EVERY ONE SECTOR ON THE 1ST TRACK - MUS BE HAVE CORRECTED ADRESS
;-----------------------------------------------------------------
NOFIRSTSEC  
		LDA GDZIELO
		SEC
		SBC #$02		;CORRECT ON THE 1ST TRACK
		STA GDZIELO	;OFFSET MUST BE LESS 2
		BCS NOFIRSTTRACK
		DEC GDZIEHI
;--------
NOFIRSTTRACK
;--------
; IF IS THE LAST SECTOR
; CALCULATE END ADRESS
;--------
		LDA MAIN_LINK		;IT'S THE LAST SECTOR?
		BNE PREPI4

		LDA ILE
		SEC
		SBC MY_SUBSTRACT
		SEC	;ONE MORE... BYTE
		ADC GDZIELO 
		STA EOF
		LDA GDZIEHI	;OF FILE
		ADC #$00
		STA EOF+1
;----------------------------------------------
;ON EVERY ONE LAST SECTOR ARRIVING TO C64
;MUST CALCULATED BASEADRES FOR NEXT TRACK DATA
;----------------------------------------------
PREPI4
LICZSEC2 =*+1			;VALUES OF SECTORS ON THE TRACK OUR FILE
		LDX #$00		;LAST SECTOR NUMBER FROM TRACK
		DEX
		CPX SECTNR		;IS HERE NOW THE LAST SECTOR?
		BNE PREPI3		;NO
		CPX #$00
		BNE PREPI4A	;THAT WAS ONLY ONE SECTOR ON THE TRACK?
		CPX WASFIRST	;YES ONLY ONE BUT THAT WAS THE FIRST SECTOR OF FILE?
		BEQ PREPI3		;YES - DON'T CALC NEXTBASE - WE HAVE GOTTA HIS BEFORE!!
PREPI4A		    
		LDA GDZIELO	;YES THAT WAS THE LAST SECTOR
		CLC			;FROM TRACK
		ADC #$FE
		STA NEXTBASE	;SO WITHOUT BIG CALCULATIONC
		LDA GDZIEHI	;GET THE ADRESS FOR NEW DATA FROM
		ADC #$00		;NEW (NEXT) TRACK
		STA NEXTBASEHI
;------
PREPI3
		LDX LICZSEC	;WAS THAT THE LAST SECTOR FORM TRACK READING NOW?
		CPX #1
		BNE +			;NO
PREPI3A
NEXTBASE = *+1		;ADRESS OF DATA FOR NEXT TRACK
		LDA #$00    ;YES MAKE BASEADR FOR NEW TRACK
		STA BASEADR
NEXTBASEHI = *+1
		LDA #$00
		STA BASEADRHI
	+    
		RTS
;--------
EOF      .WORD 0		;END OF FILE ADRESS
;--------
LICZSEC  .BYTE 0		;VALUES OF SECTORS ON THE TRACK OUR FILE
LICZSEC3 .BYTE 0
;--------
GET_START_DATA
		DEC $01
MY_VECT=*+1
		LDA $1111,Y
		INC $01
		INY
		RTS
;========================
FLY_DECR
		PHA
		LDA #<FIRSTDECR
		LDX #>FIRSTDECR
		BNE GO_1
ONLY_LOAD
		PHA
		LDX #>NO_FLY
		LDA #<NO_FLY
GO_1		JSR SET_DECR_WAY
		PLA
;---
STARTLOAD
;----- Paragraph @STARTLOAD@ -----
		STA MY_VECT
		STY MY_VECT+1
		LDA #$20
		STA MY_JUMP
		LDA #$60
		STA END_DP
		LDA $01
		STA BACKUP01
		JSR SETMY01
		LDA #$2C
		STA RSS3
		LDY #$01
		STY MARKER
		STY MARKER2
		DEY
		STY WASFIRST	;FIRST TRACK - INDICATOR FOR CORRECT #2 ADRESS OF SECTOR
		STY LICZSEC3
		
		;Y = 0
		JSR GET_START_DATA
		STA BASEADR

		JSR GET_START_DATA
		STA BASEADRHI
FROMTRACK
		JSR GET_START_DATA
		STA STRTTRK

		JSR GET_START_DATA
		STA STRTSEC

		JSR GET_START_DATA
		STA HOW_GET_LD


FIRSTSECT
		JSR READ1SEC

		JSR LADUJ
ENDLOAD


		JSR RESTORE01

		LDA EOF
		LDX EOF+1

		;AT THE END - CARRY IS CLEAR AND FOR EXODECRUNCHER
		;IN ACC LOW BYTE OF EOF FILE IN X REG HI BYTE OF EOF FILE

		CLC
		RTS
;=====================
;--------
_END_PROC2


;=====================
;---
SETMY01	LDX #$35
		BNE SET01
RESTORE01
BACKUP01 = *+1
		LDX #$00
SET01	STX $01
		RTS
;----
REFILL_DATA

		LDA #$FE
		CLC
		ADC LOW_ADR
		STA LOW_ADR
		BCC +
		INC HI_ADR
	+	DEC LICZSEC3

MARKER2 = *+1
		LDA #$00
		BEQ DPR_LOOP
		JSR SETMY01
	+	BIT $DD00
		BVC +
;--- MY LOOP-BACK TO READSECTORS, REFILL DATA
BACK_LOOP	
		JSR SETMY01
WHERE	JMP READ_SEC
;---
	+	LDA LICZSEC3
		BEQ BACK_LOOP
		JMP SEC_DECR
;---
DPR_LOOP	
		LDA LOW_ADR
		LDY HI_ADR
		LDX #$00
		CLC
		ADC #$02
		BCC +
		INY
	+	JMP STOREPARAMS

;--------
;--------
;BONGO NO-RECURSIVE DE-CRUNCHER - WEGI 2013.01.01
	
;----- Paragraph @decruncher@ -----

;---
LENGTH		= $02
STREAM_BYTE	= LENGTH +1
PUT			= LENGTH+2
COPY_SEQ		= LENGTH+4
;---
;=========================
SET_SEC_DECR
		LDA #<SEC_DECR
		LDX #>SEC_DECR
SET_DECR_WAY
		STA DECR_WAY+1
		STX DECR_WAY+2
		RTS
FIRSTDECR

		JSR SET_SEC_DECR
		LDA #$4C
		STA END_DP
		STA RSS3
		JSR RESTORE01
		JSR FIRST_ADR_SET
		.BYTE $2C

JUST_DECRUNCH
		LDX #0
DECRUNCH	
	; A #<START_DATA ; Y #>START_DATA ; X #OFFSET TO DATA	
	;---------------------------------------------
	; FIRST ENTRY POINT TO DECRUNCHER
	; IN ACC LO BYTE DATA ADRESS IN Y HI BYTE
	; X REGISTER MUST BE 0 !!!
	; OR IF YOU DO PARTIAL DECRUNCH X MUST BE
	; HAVE OFFSET TO DATA FOR EXAMPLE
	; IF YOU USE 256 BYTES BUFFER BLOCK FOR SECTOR
	; X SHOULD BE #4 FOR FIRST BLOCK
	; AND #2 FOR NEXT OTHER
	;---------------------------------------------
	
		JSR STOREPARAMS

		LDY #$00
		STY STREAM_BYTE
		JSR GET_DATA_BYTE

	;----------------------------
	; BELLOW IF YOU MAKE DECRUNCH
	; INTO YOUR AREA BEFORE YOU
	; MUST SET THE PUT VECTOR !!!
	;----------------------------
		
		CMP #$80		
		PHP ;RAW DATA? - MAKE ACTIVE IF TRUE
		
		AND #$3F 
		STA LENGTH
		BNE +
	-
		JSR GET_DATA_BYTE
		STA SEQUENCES-1,Y
	+	INY
		CPY LENGTH
		BCC -
		
		
	;----------------------------------
	; IF YOU USE RAW DATA 2 LINES BELOW
	; MUST BE ACTIVE AND BEFORE PHP
	;----------------------------------
		PLP
		BCC +
		
		JSR GET_DATA_BYTE
		STA PUT

		JSR GET_DATA_BYTE
		STA PUT+1	
	+	
		
		
;**********************
;* MAIN DECRUNCH LOOP *
;**********************			
DECRUNCH_LOOP
                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE      ;A can be trashed
                bcc IS_UNCRUNCH

                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE      ;A can be trashed
                bcs COPY_ONLY_1          ;never zero, rol would set zero flag in case

                jsr GET3_OR_6BITS        ;sets Y to 0
                sta LENGTH
                cmp #$00
                bne LOOP_COPY
                beq CHECK_PAGE
;---
COPY_ONLY_1
                ldy #$01
                sty LENGTH
                dey
                sty COPY_SEQ+1
;=================
LOOP_COPY       ;Y = #$00
MGET2HI = *+2
                lda $1000,x
                sta (PUT),y
                inx
                bne +
                jsr TRY_REF
+
                inc PUT
                bne *+4
                inc PUT+1
                dec LENGTH
                bne LOOP_COPY
;---
CHECK_PAGE
                lda COPY_SEQ+1
                beq IS_UNCRUNCH
                dec COPY_SEQ+1
                jmp LOOP_COPY

IS_UNCRUNCH

                ldy #$03    ;used in SHORT_C and as A later on

                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE    ;A can be trashed
                lda #$02    ;2 BYTES SEQ?
                bcc SHORT_C

                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE    ;A can be trashed
                tya         ;A = Y = 3
                bcc START_UNCRUNCH

                jsr GET3MANYBITS
                tay
                beq CHECK_SEQ

                lda #$01
                jsr GETCRUNCHBYTES
                cmp #4
                bcs START_UNCRUNCH
		
;----- Paragraph @EOF@ -----		
;=====		
END_DP
		JMP BACK_LOOP
;-----------------------------
;HERE SERVICE GOLDEN SEQUENCES
;-----------------------------
CHECK_SEQ
                ldy #4
                jsr GETMANYBITS ;GET4BITS

                ;NR OF SEQ *3
                sta LENGTH
                asl
                adc LENGTH
                tay
;===
                lda SEQUENCES,Y
                sta LENGTH_+1

                lda SEQUENCES+2,Y ;OFFSET HI BYTE
                sta COPY_SEQ+1
                lda SEQUENCES+1,Y ;OFFSET LO BYTE
                ldy #$00
                beq SHORT_WAY

;===================================================
;=============      UNCRUNCHING      ===============
;===================================================
START_UNCRUNCH
                ldy #$04         ;4 BITS FOR >2 BYTES SEQ
SHORT_C
                sta LENGTH_+1
                jsr GET_BYTES_Y  ;GET 3 OR 4 BITS
;***************************************************
SHORT_WAY
                ;SUBSTRACT OFFSET
                eor #$ff         ;-COPY_SEQ + PUT = PUT - COPY_SEQ :-) -> COPY_SEQ ^ $ff + 1 (carry!) + PUT
                sec
                adc PUT
                sta CP+1
                lda PUT+1
                sbc COPY_SEQ+1
                sta CP+2
;---
                ;Y = 0
-
CP              lda $1000,y
                sta (PUT),y
                iny
LENGTH_         cpy #$00
                bne -

                tya
                clc
                adc PUT
                sta PUT
                bcc *+4
                inc PUT+1
                jmp DECRUNCH_LOOP

;==========================
GET_STREAM_BYTE		


MGET1HI = *+2

		LDA $1111,X
		SEC
		ROL
		STA STREAM_BYTE

     INX
		BEQ TRY_REF
     RTS
;---
;----- Paragraph @TRY_REF@ -----

TRY_REF

DEP_OFFS = *+1
		LDX #$00
		BEQ BIG_INCR
		PHP
		PHA
		TYA
		PHA
		JSR REFILL_DATA
		PLA
		TAY
		PLA
		PLP
		RTS
;---
BIG_INCR
		INC MGET1HI
		INC MGET2HI
;============
+		RTS
;============
;---
GET3MANYBITS
                ldy #3             ;fetch 3 bits
GETMANYBITS                        ;Y holds the number of bits to fetch
                lda #$00
GETCRUNCHBYTES
-
                asl STREAM_BYTE
                bne *+7
                pha
                jsr GET_STREAM_BYTE
                pla
                rol
                dey
                bne -
                rts
;===
GET3_OR_6BITS
                jsr GET3MANYBITS    ;sets Y to 0
                cmp #7
                bne GETBYTESTOCOPY

                jsr GET3MANYBITS    ;IN ACC #BITS - 7 , C=0, sets Y to 0
                adc #7
                bne GETBYTESTOCOPY
GET_BYTES_Y
                jsr GETMANYBITS     ;sets Y to 0, A is already 0, actually no need to set it to 0 again @GETMANYBITS
-
GETBYTESTOCOPY  ;why can it happen that we shall copy 0 bytes? can't we avoid that case while packing?!
                ;ldy #$00 y is always 0
                sty COPY_SEQ+1
                tay
                beq LOWER

                lda #$01
                cpy #$08
                bcc GETCRUNCHBYTES  ;we fetch too less bits to be able to rol anything into COPY_SEQ+1, so we take the faster version
-
                asl STREAM_BYTE
                bne *+7
                pha
                jsr GET_STREAM_BYTE
                pla
                rol
                rol COPY_SEQ+1
                dey
                bne -
                rts
LOWER
                lda #$01
                rts
;---

;---
GET_DATA_BYTE
MGET3HI = *+2
		LDA $1111,X
		INX
		RTS
;---
;---
SEC_DECR
		LDX LICZSEC3
		BNE +
		JMP BACK_LOOP
+		JSR RESTORE01
		LDX #$02
		.BYTE $2C
FIRST_ADR_SET
		LDX #$04
LOW_ADR = *+1
		LDA #$00
HI_ADR = *+1
		LDY #$00


STOREPARAMS		
		STA MGET3HI-1	;LO BYTE START DATA
		STA MGET1HI-1
		STA MGET2HI-1
		
		STY MGET1HI
		STY MGET2HI
		STY MGET3HI
		STX DEP_OFFS	;INDICATOR 0 = STREAM CRUNCH <> 0 PARTIAL CRUNCH
		RTS
;---
SEQUENCES = *
.BYTE 0,0,0,0,0,0,0,0
.BYTE 0,0,0,0,0,0,0,0
.BYTE 0,0,0,0,0,0,0,0
.BYTE 0,0,0,0,0,0,0,0
.BYTE 0,0,0,0,0,0,0,0
.BYTE 0,0,0,0,0,0,0,0

;---------------------------------------------
; 48 BYTES MAX FOR MY LOVELLY GOLDEN SEQUENCES
; OF COURSE - YOU CAN CHANGE SEQ ADDRESS
;---------------------------------------------		

