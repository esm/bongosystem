
;================================
;2ND PROCEDURE OF:
;THE BONGO SHORT STREAM LOADER INTEGRATED WITH BONGO DECRUNCHER NO-RECURSIVE WITH GOLDEN SEQ
; (C) BY WEGI IN 2013.01.21 
;================================


;******************
;* TECHNICAL DATA *
;******************

;SUPPORT 1541x , 1570/71 IN 1541 MODE [D64 - NOT D71 FILES (DISKS)]
;SHORT TECHNICAL DATA:
;LOADER USE ATN LINE TO SYNC DATA SEND
;LOADER LOADING UNDER I/O AREA
;FOR FASTER LOAD IN 1570/71 USED 2mhz THIS GAVE A DIFFERENT TIMING THAN 1541
; - GET ATTENTION ON THIS OR REMAKE SOURCE CODE

;SPRITES, $D011, BADLINES, IRQ, NMI - ALL OF THEM NOT "NOISING" FOR LOADER


;GENERALLY RECOMMENDED LOAD VIA TRAC AND SECTOR - LOAD VIA NAME IS POSSIBLE WHEN YOU USE 
;ADDITIONAL FUNCTION "FIND_BY_FILENAME", BUT THIS GEAVE A BIG AND UNCONFORMTABLE DELAY
;BY JUMP, READ AND FINDING DIR TO TRACK 18 TIME AFTER TIME SO IN THIS CASE YOU MUST
;WASTED MORE BYTES AND TIME... REALLY I'M NOT RECOMMENDED THIS OPTION

;***************
;* CONSTRAINTS *
;***************

;ON THE IEC - ONLY ONE UNITS CAN BE TURNED ON - YOU DRIVE WITH YOUR DEMO DISC 

;PLEASSE BE CARREFULLY ABOUT WRITE TO $DD00 !!!
;DO THIS OR LIKE BELOW:

; ALLWAYS 6 HIBITS MUST BE SET DOWN TO 0
;	LDA #VICBANK ;#0, #1, #2, #3
;	STA $DD00


;ALSO DON'T TOUCH $DD02 NEVER !!!
;ALL SHOULD BE HAPPY WHEN YOU WILL HAVE A BIG ATTENTION FOR THIS CASES

;************************************
;* A SHORT INFO HOW TO USE THIS ONE *
;************************************

;AT THE VERY FIRST YOU MUST DONE CALL ***JSR PROC1***
;SOMEWHERE IN A BEGINNING OF DEMO - THIS IS A DEDICATED TO THIS CODE
;PROC TO SEND DRIVECODE TO 1541x, 1570/71 IN 1541 MODE


;1. FOR LOAD AND FLY DECRUNCH CALL JSR PROC2 WITH SETUP:
;ACC AND Y MUST HAVE LO/HI BYTE OF POINTER 
;TO 5 BYTES INFO DATA TO WORK IN ORDER:

;---
;BASEADR		WORD - 2 BYTES IN LO/HI FORMAT
;STRTTRK		BYTE - 1 BYTE START TRACK FILE TO LOAD
;STRTSEC		BYTE - 1 BYTE START SECTOR FILE TO LOAD
;HOW_GET_LD	BYTE - 1 BYTE : IF = 0 LOAD ADDRES FROM FILE , IF <> 0 LOAD ADDRES FROM BASEADR
;---

;BASEADR = LOAD ADDRESS IF YOU WANNA LOAD FOR OWN LOAD AD.
;THIS INFO YOU SETUP IN HOW_GET_LD :
;HOW_GET_LD  =  0 - LOAD ADDRES FROM FILE - BASEADR IS IGNORED
;HOW_GET_LD <>  0 - LOAD ADDRES FROM BASEADR
;STRTTRK - START TRACK FILE TO LOAD
;STRTSEC - START SECTOR FILE TO LOAD

;START TRACK AND SECTOR VALUES THEY ARE GENERATED
; ***** INTO INCLUDE SCRIPT BY TRACKMOLINKER *****
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;|||||||||||||||||||||||||||||||||||||||||||||||||

;STRONG RECOMMENDED TO USE TRACKMOLINKER V 1.2


;... IF YOU WANNA DECRUNCHING RAW DATA INTO YOUR DEPACK ADDRESS 
;YOU MUST BEFORE SET UP THE "PUT" VECTOR OF DECRUNCHER 
;AND LOADING SAVED RAW DATA MAKED BY BONGO CRUNCHER

; IMPORTANT!!! - $01 VALUE MUST BE SET UP BEFORE JSR PROC2
; THIS VALUE IS RESTORED FOR DECRUNCH FOR POSSIBLITY
; DECRUNCH DATA TO COLOR RAM (IT'S MY OPTION CAUSE I USED THIS)
; OF COURSE - LOAD ADDRES MUST BE RECHANGED FOR DIFFERENT
; THAN I/O AREA 

;ZERO PAGE USED ONLY BY DECRUNCHER - 6 BYTES

;2. FOR ONLY LOAD FILE CALL JSR PROC2+3
; - LIKE BEFORE SETUP YOU DONE 
; DECRUNCHER STILL NOT ACTIVE AND BY LOADER
; *** ZERO PAGE IS UNTOUCHED ***

;3. JUST DECRUNCH - CALL JSR PROC2+6
;ACC/YREG - LO/HI BYTE POINTER TO DATA FOR DECRUNCH
;$01 VALUE YOU MUST BEFORE SETUP FOR DECRUNCH AS YOU NEED
;AND ABOUT RAW DATA THE RULE IS THIS SAME LIKE IN POINT 1

;4. JSR PROC2+9 - DISCONECT LOADER AND RELEASE DRIVE

;5. JSR PROC2+12 - SEND ONE BYTE FROM ACC TO DRIVE... FOR OWN CODE, CHANGEDISK ENGINE, ADDITIONAL FUNCTION
;BUT COMPATIBILITY WITH GETTORAM PROTOCOL

;********************
;* A SHORT EXAMPLES *
;********************

;1. -A

; LOADING FILE INTO *** $C000 *** FROM START TRACK 1 AND SECTOR 0
; AND DECRUNCH INTO DEPACK ADDRES FROM CRUNCHED DATA

;		...
;		LDA #$34	;DECRUNCH VALUE!
;		STA $01
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2
;		BCS *		;LOAD ERROR - DEMO CRASHED

;		NOW IN ACC/XREG LO/HI IS THE POINTER TO END OF DATA+1 FOR OTHER CASE LIKE EXO BACK DECRUNCH...
;		...
;---
;BASEADR		.WORD $C000
;STRTTRK		.BYTE $01
;STRTSEC		.BYTE $00
;HOW_GET_LD	.BYTE $01
;---

;1. -B

; LOADING FILE INTO *** FILE LOAD ADDRES *** FROM START TRACK 1 AND SECTOR 0
; AND DECRUNCH INTO DEPACK ADDRES FROM CRUNCHED DATA

;		...
;		LDA #$34	;DECRUNCH VALUE!
;		STA $01
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2
;		BCS *		;LOAD ERROR - DEMO CRASHED

;		NOW IN ACC/XREG LO/HI IS THE POINTER TO END OF DATA+1 FOR OTHER CASE LIKE EXO BACK DECRUNCH...
;		...
;---
;BASEADR		.WORD $0000 ;NOT IMPORTANT
;STRTTRK		.BYTE $01
;STRTSEC		.BYTE $00
;HOW_GET_LD	.BYTE $00	;BASEAD WILL BE IGNORED TAKE LOAD ADDRES FROM FILE
;---


;1. -C

; LOADING FILE INTO *** $C000 *** FROM START TRACK 1 AND SECTOR 0
; AND DECRUNCH INTO *** $4000 *** FROM CRUNCHED DATA
; DATA MUST BE DONE AS RAW DATA BY BONGO CRUNCHER

;		...
;		LDA #<$4000 ;BONGO DEPACK ADDRESS SETUP
;		STA PUT
;		LDA #>$4000
;		STA PUT+1
;		LDA #$34	;DECRUNCH VALUE!
;		STA $01
;		LDA #<BASEADR
;		LDY #>BASEADR
;		JSR PROC2
;		BCS *		;LOAD ERROR - DEMO CRASHED

;		NOW IN ACC/XREG LO/HI IS THE POINTER TO END OF DATA+1 FOR OTHER CASE LIKE EXO BACK DECRUNCH...
;		...

;---
;BASEADR		.WORD $C000
;STRTTRK		.BYTE $01
;STRTSEC		.BYTE $00
;HOW_GET_LD	.BYTE $01
;---
;================================		
;2. -A
;2. -B 

;SETUP LIKE 1. -A, 1. -B PARAGRAPH
;A DIFFERENT IS - *** JSR PROC2 *** CHANGE TO *** JSR PROC2+3 ***
;DECRUNCHER IS INACTIVE, ZERO PAGE UNTOUCHED

;3. -A

; DECRUNCH DATA IN RAM FROM $C000
; INTO DEPACK ADDRES FROM CRUNCHED DATA

;		...
;		LDA #$34	;FOR DECRUNCHER VALUE
;		STA $01
;		LDA #<$C000
;		LDY #>$C000
;		JSR PROC2+6
;		...

;3. -B

; DECRUNCH ***RAW*** DATA IN RAM FROM $C000 INTO $4000
; DATA MUST BE DONE AS ***RAW*** DATA BY BONGO CRUNCHER

;		...
;		LDA #$34	;FOR DECRUNCHER VALUE
;		STA $01
;		LDA #<$4000
;		STA PUT
;		LDA #>$4000
;		STA PUT +1
;		LDA #<$C000
;		LDY #>$C000
;		JSR PROC2+6
;		...

;..................................................

;*****************************************
;* AND I GUESS THAT IS SHOULD BE ENOUGHT *
;* TO YOU OWN HAPPY USE :)               *
;* REGARDS - WEGI                        *
;*****************************************

;----- PARAGRAPH @STARTPROC2@ -----
PROC2 = $0a00 ;YOR LOADER ADDRESS

* = PROC2
;-
		.ALIGN 256
;-
		;A JUMP TABLE OF THIS -
		;PROC2	- LOAD FILE AND FLY DECRUNCH
		;PROC2+3 	- OLY LOAD WITHOUT DECRUNCH
		;PROC2+6 ONLY DECRUNCH - ACC AND YREG LO/HI BYTE OF START DATA - $01 MUST BE SET HOW YOU NEED !!!
		;PROC2+9 DISCONNECT LOADER
		;PROC2+12 - SEND ONE BYTE TO DRIVE FROM ACC
		
		JMP FLY_DECR	;LOAD + DECR
		JMP ONLY_LOAD	;ONLY LOAD WITHOUT DECRUNCH
		JMP JUST_DECRUNCH
		JMP LOADEROFF
		
;------------
SENDDRIV2       ;SEND ONE BYTE
;---
         SEC
         ROR
         TAX
         LDA $DD02
         ORA #$30
         STA $DD02

         BIT $DD00
         BVC *-3
KEX
         LDA $DD02
         EOR #$20
         ORA #$10
         BCC *+4
         AND #$EF
         STA $DD02
         TXA
         LSR
         TAX
         BNE KEX
         LDA $DD02
         AND #$0F
         ORA #$10
         STA $DD02
;---		
		RTS
;-------------------
;=========
GET_ONE_BYTE
;---
         BIT $DD00
         BVC *-3
POB2     LDX #$3F
         STX $DD02
         NOP
         ;INY
         STY RCALL
         BIT $EA
         LDY #$37
         LDA $DD00
         STY $DD02
         LSR
         LSR
         NOP
         NOP
         BIT $EA
         ORA $DD00
         STX $DD02

         LSR
         LSR
         LSR
         LSR
         STA HALFBYTE
         LDA $DD00
         STY $DD02
;---
RCALL    = *+1
         LDY #$00
;---
         NOP
         LDX #$1F
         LSR
         LSR
MY_MANY   = *+1
         CPY #$FF
         ORA $DD00
         STX $DD02
         AND #$F0
HALFBYTE = *+1
         ORA #$00
;---
		RTS
;---------
;---
LOADEROFF LDA #$FF
		.BYTE $2C
LOADER_ON
		LDA #$02
		PHA
		LDA STRTTRK	;START TRAC FOR DRIVE
		JSR SENDDRIV2
;---
		LDA STRTSEC	;START SECTOR OF FILE
		JSR SENDDRIV2
;---
		PLA			;OPERATION CODE
		JMP SENDDRIV2
;---
;-----------
FLY_DECR
		PHA
		LDA #<FIRSTDECR
		LDX #>FIRSTDECR
		BNE GO_1
ONLY_LOAD
		PHA
		LDX #>NO_FLY
		LDA #<NO_FLY
GO_1		JSR SET_DECR_WAY
		PLA
;----------------
STARTLOAD
		STA MY_VECT
		STY MY_VECT+1

		LDA $01
		STA BACKUP01
		JSR SETMY01
		
		LDY #4
		DEC $01		
	-
MY_VECT=*+1
		LDA $1111,Y
		STA BASEADR,Y
		DEY
		BPL -
		INC $01
		JSR LOADER_ON
		JSR GET_ONE_BYTE
		TAY
		DEY
		DEY

		JSR GET_ONE_BYTE
		STA OUT_VEC
		JSR GET_ONE_BYTE
		STA OUT_VEC+1

		LDX HOW_GET_LD
		BEQ +

     LDA BASEADR
     STA OUT_VEC
     LDA BASEADRHI
     STA OUT_VEC+1
+   STA BASEADRHI
     LDA OUT_VEC
     STA BASEADR
     JMP NORELOAD_Y
;---

READ_SEC	JSR GET_ONE_BYTE
		BNE +
		JSR RESTORE01
		;AT THE END - CARRY IS CLEAR AND FOR EXODECRUNCHER
		;IN ACC LOW BYTE OF EOF FILE IN X REG HI BYTE OF EOF FILE
		LDA OUT_VEC
		LDX OUT_VEC+1
		CLC
		RTS
	+	TAY
NORELOAD_Y	
	-	JSR GET_ONE_BYTE
		DEC $01
OUT_VEC = *+1
		STA $1111
		INC $01
		INC OUT_VEC
		BNE +
		INC OUT_VEC+1
	+	DEY
		BNE -
DECR_WAY
		JMP FIRSTDECR
NO_FLY	BEQ READ_SEC
;--------




;-------------------------------------
; GET BASE ADRESS OF TRACK AND ADD FOR
; NUMBER OF SECTOR VALUE
;-------------------------------------
;--------
;========================
BASEADR		.BYTE 0
BASEADRHI		.BYTE 0
STRTTRK		.BYTE 0
STRTSEC		.BYTE 0
HOW_GET_LD	.BYTE 0
;=====================
FIRSTDECR
		LDA #<SEC_DECR
		LDX #>SEC_DECR
		JSR SET_DECR_WAY

		LDA BASEADR
		SEC
		SBC #4
		LDY BASEADRHI
		BCS +
		DEY
	+	
		JSR RESTORE01
		LDX #4
		JMP DECRUNCH
;---
SETMY01	LDX #$35
		BNE SET01
RESTORE01
BACKUP01 = *+1
		LDX #$00
SET01	STX $01
		RTS
;---
SEC_DECR
		JSR RESTORE01
		LDX #2
		RTS;;;
;----
SET_DECR_WAY
		STA DECR_WAY+1
		STX DECR_WAY+2
		RTS
;----
REFILL_DATA
		LDA OUT_VEC
		SEC
		SBC #2
		LDY OUT_VEC+1
		BCS +
		DEY
	+	JSR SETMY01
		JSR STOREPARAMS
		JMP READ_SEC
;--------
;BONGO NO-RECURSIVE DE-CRUNCHER - WEGI 2013.01.01
	

;---
LENGTH		= $02
STREAM_BYTE	= LENGTH +1
PUT			= LENGTH+2
COPY_SEQ		= LENGTH+4
;---
;=========================
JUST_DECRUNCH
		LDX #0
DECRUNCH	
	; A #<START_DATA ; Y #>START_DATA ; X #OFFSET TO DATA	
	;---------------------------------------------
	; FIRST ENTRY POINT TO DECRUNCHER
	; IN ACC LO BYTE DATA ADRESS IN Y HI BYTE
	; X REGISTER MUST BE 0 !!!
	; OR IF YOU DO PARTIAL DECRUNCH X MUST BE
	; HAVE OFFSET TO DATA FOR EXAMPLE
	; IF YOU USE 256 BYTES BUFFER BLOCK FOR SECTOR
	; X SHOULD BE #4 FOR FIRST BLOCK
	; AND #2 FOR NEXT OTHER
	;---------------------------------------------
	
		JSR STOREPARAMS

		LDY #$00
		STY STREAM_BYTE
		JSR GET_DATA_BYTE

	;----------------------------
	; BELLOW IF YOU MAKE DECRUNCH
	; INTO YOUR AREA BEFORE YOU
	; MUST SET THE PUT VECTOR !!!
	;----------------------------
		
		CMP #$80		
		PHP ;RAW DATA? - MAKE ACTIVE IF TRUE
		
		AND #$3F 
		STA LENGTH
		BNE +
	-
		JSR GET_DATA_BYTE
		STA SEQUENCES-1,Y
	+	INY
		CPY LENGTH
		BCC -
		
		
	;----------------------------------
	; IF YOU USE RAW DATA 2 LINES BELOW
	; MUST BE ACTIVE AND BEFORE PHP
	;----------------------------------
		PLP
		BCC +
		
		JSR GET_DATA_BYTE
		STA PUT

		JSR GET_DATA_BYTE
		STA PUT+1	
	+	
		
		
;**********************
;* MAIN DECRUNCH LOOP *
;**********************			
DECRUNCH_LOOP
                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE      ;A can be trashed
                bcc IS_UNCRUNCH

                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE      ;A can be trashed
                bcs COPY_ONLY_1          ;never zero, rol would set zero flag in case

                jsr GET3_OR_6BITS        ;sets Y to 0
                sta LENGTH
                cmp #$00
                bne LOOP_COPY
                beq CHECK_PAGE
;---
COPY_ONLY_1
                ldy #$01
                sty LENGTH
                dey
                sty COPY_SEQ+1
;=================
LOOP_COPY       ;Y = #$00
MGET2HI = *+2
                lda $1000,x
                sta (PUT),y
                inx
                bne +
                jsr TRY_REF
+
                inc PUT
                bne *+4
                inc PUT+1
                dec LENGTH
                bne LOOP_COPY
;---
CHECK_PAGE
                lda COPY_SEQ+1
                beq IS_UNCRUNCH
                dec COPY_SEQ+1
                jmp LOOP_COPY

IS_UNCRUNCH

                ldy #$03    ;used in SHORT_C and as A later on

                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE    ;A can be trashed
                lda #$02    ;2 BYTES SEQ?
                bcc SHORT_C

                asl STREAM_BYTE
                bne *+5
                jsr GET_STREAM_BYTE    ;A can be trashed
                tya         ;A = Y = 3
                bcc START_UNCRUNCH

                jsr GET3MANYBITS
                tay
                beq CHECK_SEQ

                lda #$01
                jsr GETCRUNCHBYTES
                cmp #4
                bcs START_UNCRUNCH
		
;----- Paragraph @EOF@ -----		
;=====		
END_DP
		LDA DEP_OFFS
		BNE +
		RTS
	+	
		JSR SETMY01
		JMP READ_SEC ;READ LAST BYTE 0 VALUE
;-----------------------------
;HERE SERVICE GOLDEN SEQUENCES
;-----------------------------
CHECK_SEQ
                ldy #4
                jsr GETMANYBITS ;GET4BITS

                ;NR OF SEQ *3
                sta LENGTH
                asl
                adc LENGTH
                tay
;===
                lda SEQUENCES,Y
                sta LENGTH_+1

                lda SEQUENCES+2,Y ;OFFSET HI BYTE
                sta COPY_SEQ+1
                lda SEQUENCES+1,Y ;OFFSET LO BYTE
                ldy #$00
                beq SHORT_WAY

;===================================================
;=============      UNCRUNCHING      ===============
;===================================================
START_UNCRUNCH
                ldy #$04         ;4 BITS FOR >2 BYTES SEQ
SHORT_C
                sta LENGTH_+1
                jsr GET_BYTES_Y  ;GET 3 OR 4 BITS
;***************************************************
SHORT_WAY
                ;SUBSTRACT OFFSET
                eor #$ff         ;-COPY_SEQ + PUT = PUT - COPY_SEQ :-) -> COPY_SEQ ^ $ff + 1 (carry!) + PUT
                sec
                adc PUT
                sta CP+1
                lda PUT+1
                sbc COPY_SEQ+1
                sta CP+2
;---
                ;Y = 0
-
CP              lda $1000,y
                sta (PUT),y
                iny
LENGTH_         cpy #$00
                bne -

                tya
                clc
                adc PUT
                sta PUT
                bcc *+4
                inc PUT+1
                jmp DECRUNCH_LOOP

;==========================
GET_STREAM_BYTE		


MGET1HI = *+2

		LDA $1111,X
		SEC
		ROL
		STA STREAM_BYTE

     INX
		BEQ TRY_REF
     RTS
;---
;----- Paragraph @TRY_REF@ -----

TRY_REF

DEP_OFFS = *+1
		LDX #$00
		BEQ BIG_INCR
		PHP
		PHA
		TYA
		PHA
		JSR REFILL_DATA
		PLA
		TAY
		PLA
		PLP
		RTS
;---
BIG_INCR
		INC MGET1HI
		INC MGET2HI
;============
+		RTS
;============
;---
GET3MANYBITS
                ldy #3             ;fetch 3 bits
GETMANYBITS                        ;Y holds the number of bits to fetch
                lda #$00
GETCRUNCHBYTES
-
                asl STREAM_BYTE
                bne *+7
                pha
                jsr GET_STREAM_BYTE
                pla
                rol
                dey
                bne -
                rts
;===
GET3_OR_6BITS
                jsr GET3MANYBITS    ;sets Y to 0
                cmp #7
                bne GETBYTESTOCOPY

                jsr GET3MANYBITS    ;IN ACC #BITS - 7 , C=0, sets Y to 0
                adc #7
                bne GETBYTESTOCOPY
GET_BYTES_Y
                jsr GETMANYBITS     ;sets Y to 0, A is already 0, actually no need to set it to 0 again @GETMANYBITS
-
GETBYTESTOCOPY  ;why can it happen that we shall copy 0 bytes? can't we avoid that case while packing?!
                ;ldy #$00 y is always 0
                sty COPY_SEQ+1
                tay
                beq LOWER

                lda #$01
                cpy #$08
                bcc GETCRUNCHBYTES  ;we fetch too less bits to be able to rol anything into COPY_SEQ+1, so we take the faster version
-
                asl STREAM_BYTE
                bne *+7
                pha
                jsr GET_STREAM_BYTE
                pla
                rol
                rol COPY_SEQ+1
                dey
                bne -
                rts
LOWER
                lda #$01
                rts
;---
GET_DATA_BYTE
MGET3HI = *+2
		LDA $1111,X
		INX
		RTS
;---
STOREPARAMS		
		STA MGET3HI-1	;LO BYTE START DATA
		STA MGET1HI-1
		STA MGET2HI-1
		
		STY MGET1HI
		STY MGET2HI
		STY MGET3HI
		STX DEP_OFFS	;INDICATOR 0 = STREAM CRUNCH <> 0 PARTIAL CRUNCH
		RTS
;---
SEQUENCES = *
.BYTE 0,0,0,0,0,0,0,0
.BYTE 0,0,0,0,0,0,0,0
.BYTE 0,0,0,0,0,0,0,0
.BYTE 0,0,0,0,0,0,0,0
.BYTE 0,0,0,0,0,0,0,0
.BYTE 0,0,0,0,0,0,0,0

;---------------------------------------------
; 48 BYTES MAX FOR MY LOVELLY GOLDEN SEQUENCES
; OF COURSE - YOU CAN CHANGE SEQ ADDRESS
;---------------------------------------------		

