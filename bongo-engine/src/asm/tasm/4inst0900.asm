;At the start Your demo just do JSR PROC1 for install loader - that's all

;----- PARAGRAPH @STARTPROC2@ -----
;================================
;1ST PROCEDURE OF:
;THE BONGO STREAM LOADER WORKING 1541x, 1570/71 IN 1541 MODE
;SHORT VERSION
; (C) BY WEGI IN 2013.01.21
;================================
PROC1 = $0900

* = PROC1
;-
.ALIGN 256
;-
VECTR    = $06
VECTR2   = $08

GOTORAM  = $0100 ;A SHORT PROC FOR GETTINGS DATA TO RAM OF DRIVE
;-
		JMP INITLOAD
;--------
;--------
SENDDRIV1 
;---
         SEC
         ROR
         TAX
         LDA $DD02
         ORA #$30
         STA $DD02

         BIT $DD00
         BVC *-3
KEX
         LDA $DD02
         EOR #$20
         ORA #$10
         BCC *+4
         AND #$EF
         STA $DD02
         TXA
         LSR
         TAX
         BNE KEX
         LDA $DD02
         AND #$0F
         ORA #$10
         STA $DD02
;---		
		RTS
;-

;-
TX_DNOT
		.BYTE 147
		.TEXT "DEVICE NOT PRESENT"
		.BYTE 13
		.TEXT "PLEASSE TURN ON ONE 1541/70/71 DRIVE"
		.BYTE 13
		.TEXT "AND PRESS ANY KEY"
		.BYTE 0

TX_TO_MANY
		.BYTE 147
		.TEXT "TO MANY UNITS ON THE IEC"
		.BYTE 13
		.TEXT "PLEASSE TURN OFF UNUSED UNIT"
		.BYTE 13
		.TEXT "AND PRESS ANY KEY"
		.BYTE 0
STOS		.BYTE 0 ;STACK SAVE VALUE
;-
;--------
TBDRIV   .BYTE 8,9,10,11
;---
IEC_UNITS .BYTE 0
;---
INITLOAD
		TSX
		STX STOS
		LDA #$00
		STA $D015
		STA IEC_UNITS
		
		LDA #8
		STA $BA
		
		LDX #4
	-	
		JSR LISTEN
		BMI +
		INC IEC_UNITS
		LDA $BA
		STA MY_DRIVE
		JSR $FFAE
		
	+	INC $BA
		DEX
		BNE -
		LDA IEC_UNITS
		CMP #1
		BEQ +
		CMP #0
		BNE TOMANYDR
		LDA #<TX_DNOT
		LDY #>TX_DNOT
		JMP PRT_T
TOMANYDR	LDA #<TX_TO_MANY
		LDY #>TX_TO_MANY
PRT_T	JSR $AB1E
		LDA #$00
		STA $C6
		JSR $FFE4
		BEQ *-3
		JMP INITLOAD
	+	
		
MY_DRIVE = *+1
		LDA #$08
		STA $BA
;-
FASTMW   


;--------

		JSR LISTEN
		LDA #"I"
		JSR $FFA8
		JSR $FFAE

		LDA #<DRV
		STA VECTR
		LDA #>DRV
		STA VECTR+1

		LDA #<ADRDRV
		STA VECTR2
		LDA #>ADRDRV
		STA VECTR2+1

		LDA #<ADENDDRV
		STA X3+1
		LDA #>ADENDDRV
		STA X3+2

		JSR MEW

;--------
X0       LDA #$04
		STA $02
		LDY #$00
X3       LDA $1000,Y
		JSR SENDDRIV1
		INY
		BNE X3
		INC X3+2
		DEC $02
		BNE X3
		LDY #<RESTLDR
		STY $02
		LDY #$00

X4A      LDA ADREST,Y
		JSR SENDDRIV1
		INY
		INC $02
		BNE X4A

		CLC
		CLI
		RTS
;========
LISTEN		
		LDA $BA
		JSR $FFB1
		LDA #$00
		STA $90
		LDA #$6F
		JMP $FF93
;--------
;-
MEW      LDX #3
LOOP     
		JSR LISTEN
		BIT $90
		BPL DALEJ
		LDX STOS
		TXS
		LDA #$80  ;DNOTPRESENT
		SEC
		RTS
DALEJ    LDA #$4D
		JSR $FFA8
		LDA #$2D
		JSR $FFA8
		LDA #$57
		JSR $FFA8
		LDA VECTR
		JSR $FFA8
		LDA VECTR+1
		JSR $FFA8
		LDA #$20
		JSR $FFA8
		LDY #$00
LOOP1    LDA (VECTR2),Y
		JSR $FFA8
		INY
		CPY #$20
		BNE LOOP1
		JSR $FFAE
		LDA VECTR
		CLC
		ADC #$20
		STA VECTR
		BCC *+4
		INC VECTR+1
		LDA VECTR2
		CLC
		ADC #$20
		STA VECTR2
		BCC *+4
		INC VECTR2+1
		DEX
		BNE LOOP
;-
MEX      
		
		JSR LISTEN
		LDX #$00
		LDA MEX1,X
		JSR $FFA8
		INX
		CPX #$06
		BNE *-9
		JSR $FFAE
		SEI
		SEI
     LDA #$03
     STA $DD00
     LDA #$1f
     STA $DD02
		CLI
		BIT $DD00
		BVS *-3
		CLC
		RTS
;-
MEX1     .TEXT "M-E"
		.BYTE <ABSRUN,>ABSRUN
;-
VBANK1    .BYTE 3
;--------

;--------
;---------------------------------------
;-
CODE
ADRDRV
;-
		.LOGICAL $0146
;------  REDEFINED COS BAD ASSEMBLING
MYDATA   = $01FF
;-
DRV
GETBLK
		LDA #$80
		STA $1800
		LDX $1800
		BNE *-3

EX22     CPX $1800
		BEQ *-3
		LDX $1800
		CPX #4
		ROR A
		BCC EX22
		
		LDX #$0D
		STX $1800
ADRBLK   = *+2
		STA $0300,Y
		CPX $1800
		BNE *-3

		INY
		BNE GETBLK
		RTS

SETLINE  LDA #$08
STLINE   STA SERIAL
		RTS
CLRLINE  LDA #$01
		BNE STLINE
;-
ABSRUN   SEI
		LDA #$7A
		STA $1802
		JSR SETLINE
		JSR $F5E9
		LDX #$04
		STX $06
DRV1     JSR GETBLK
		INC ADRBLK
		DEC $06
		BNE DRV1
		JMP INITRAM100
;-
RESTLDR

ADREST   = CODE-DRV+RESTLDR
;-

GETJOB   JSR GETONE
		STA TRACK
		JSR GETONE
		STA SECTOR

GETONE   SEI
		LDY #$FF
		JSR GETBLK
		LDA MYDATA
		STA TYPEWRK
		RTS
;-
INITER   STA $00
		CLI
		LDA $00
		BMI *-2
		CMP #$02
ENDINI   RTS
;---

.HERE

;----- PARAGRAPH @DRIVECODE@ -----

;-
ADENDDRV
;-
		.LOGICAL $0300
;-
TRACK	= $06
SECTOR	= $07
NXTRACK	= $08   ;AFTER SCAN 
NXSECTOR	= $09   ;SCAN IS ALLWAYS ENABLED

TYPEWRK	= $0C
		;==========================
		;#$01 WATCH AND WAIT WITHOUT STOP MOTOR
		;#$02 SEND SCANNED TRACK
		;#$03 SEND SELECTED SECTOR
		;#$00 DISCONNECT - END
		;#$04 GET DATA TO RAM
		;===========================
		; WHERE NO COMMENTS
		; - THE LABELS ARE CLEAR

CNTR1    = $0D
CNTR2    = $0E
TMPR1    = $0F
BLKCKSM  = $10

IDHEADER = $24    ;HEADER ID (FROM DOS)
MAXSEC   = $43    ;HOW MANY SECTOR ON THE TRACK (FROM DOS)

GCR1     = $52
GCR2     = $53
GCR3     = $54

FINDSYNC = $F556
IRQOK    = $F505

LASTLONYBGCR = $07FF

BFHINYBGCR = $0600
BFLONYBGCR = $0700

BITTABLO = $0600
BITTABHI = $0700


SERIAL   = $1800  ;THE SERIAL PORT...
;-
RUNLDR   JMP READ
;-
SEND_ONE_BYTE
		TAX
		LDA #$00
		STA $1800

		STX MY_BYTE
		LDA BITTABLO,X

		LDX $1800
		BNE *-3
		STA $1800
LOT2
		ASL A
		ORA #$10
		BIT $1800
		BPL *-3

		STA $1800

MY_BYTE = *+1
		LDX #$00
		NOP
		LDA BITTABHI,X

		BIT $1800
		BMI *-3
		STA $1800
		ASL A
		ORA #$10
		BIT $1800
		BPL *-3
		STA $1800
		LDA #$18
		BIT $1800
		BMI *-3
		STA $1800
		RTS
;-
BIN2SER  
		.BYTE $0F,$07,$0D,$05,$0B,$03,$09,$01
		.BYTE $0E,$06,$0C,$04,$0A,$02,$08,$00		
		.TEXT "(C) BY WEGI"
;-
READ
		LDA #$05
		STA $31		
;-
READL
		LDA $1801
		ORA #$20
		STA $1801

		JSR $F50A

	-	BVC *
		CLV
		LDA $1C01
		STA $0500,Y
		INY
		BNE -
		LDY #$BA
	-	BVC *
		CLV
		LDA $1C01
		STA $0100,Y
		INY
		BNE -

		JSR $F8E0
		
		LDX #$3F
		LDA $3A
		
	-	EOR $0500,X
		EOR $0540,X
		EOR $0580,X
		EOR $05C0,X
		DEX
		BPL -
		CMP #$00

		BNE READL
		STA CMPR_Y

		LDA $1801
		AND #$DF
		STA $1801
		
		LDA $1C00
		EOR #8
		STA $1C00

;--
		LDA #$FE
		STA BYT_TO_SEND
		LDX $0501
		STX NXSECTOR
		STX SECTOR
		LDA $0500
		BNE TEST_TWRK
		INX
		STX CMPR_Y
		DEX
		DEX
		STX BYT_TO_SEND
;--------
TEST_TWRK
		LDA TYPEWRK
		CMP #3
		BNE +
		LDY #$00
	-	LDA $0500,Y
		JSR SEND_ONE_BYTE
		INY
		BNE -
		TYA
		BEQ IREX7
;--------
	+
BYT_TO_SEND = *+1
		LDA #$FE
		JSR SEND_ONE_BYTE
		
		LDY #$02
	-	LDA $0500,Y
		JSR SEND_ONE_BYTE
		INY
CMPR_Y = *+1
		CPY #$00
		BNE -
		
		
		LDA $0500
		bne +
		JSR SEND_ONE_BYTE
		LDA #$00
	+	CMP TRACK
		BNE IREX7
;--
RLOOP	JMP READL
	
IREX7	STA NXTRACK
		TAX
		BNE +
		LDA #$01
		STA TYPEWRK
	+	JMP IRQOK
;-
;---
TABGEN
         LDX #0
-
         TXA
         AND #$0F
         TAY
         LDA BIN2SER,Y
         STA BITTABLO,X
         TXA
         LSR A
         LSR A
         LSR A
         LSR A
         TAY
         LDA BIN2SER,Y
         STA BITTABHI,X
         INX
         BNE -
         RTS
;---
;-----
IDLE		SEI

		LDA #$08
		STA $1800
		ORA $1C00
		STA $1C00
		
		LDY #150
		
	-	LDA #$D0
		STA $1805

	-	LDA $1800
		LSR
		BCC +
		BIT $1805
		BMI -
		DEY
		BNE --
		
STPMT	LDA $1C00
		AND #$FB
		STA $1C00
	+	RTS
IDLEEND
;---	
		;WATCH AND WAIT BY IDLE LOOP
MTORSTOP
STRTLDR
		JSR IDLE
;-
;STRTLDR
WITHOUT_STOP
		JSR GETJOB
		LDA SECTOR
		STA NXSECTOR
		LDA TRACK
		STA NXTRACK
		;==========================
		;#$01 WATCH AND WAIT THROUGH IDLE LOOP
		;#$02 LOAD FILE
		;#$03 SEND SELECTED SECTOR
		;#$00 DISCONNECT - END
		;#$04 GET DATA TO RAM - MAYBE FOR DRIVECALC, CHANGEDISK ENGINE...
		;===========================
		
ELO      LDA TYPEWRK
		BEQ EOFI
		CMP #$01
		BEQ MTORSTOP
		CMP #$03
		BEQ ELO2
		CMP #$04
		BNE SENDST ;SO $02 CODE LAST ONE POSSIBLITY
		JSR GOTORAM
		JSR TABGEN
		JMP STRTLDR
;---
SENDST		
		LDA NXTRACK
		BEQ ELO
		STA TRACK
		LDA NXSECTOR
		STA SECTOR
ELO2
		JSR MOTORSTART
		LDA #$E0
		JSR INITER
		BCC SENDST
BAD1

		JSR EOFI
		LDA #$05
		
BAD2     JSR DELAY
		TAX
		DEX
		TXA
		BNE BAD2
		JSR SETLINE
		JSR DELAY
		JMP STRTLDR
EOFI
		JSR CLRLINE
		STA $1C
		RTS
;---
MOTORSTART
		SEI
		LDA $1C00
		TAX
		ORA #$04
		STA $1C00
		TXA
		AND #$04
		BNE MOTOROK
DELAY
		LDX #$00
		DEY
		BNE *-1
		DEX
		BNE *-4
MOTOROK
		RTS
EOFDRIVECODE
;=====================
INITRAM100
		LDA #1
		STA ADRBLK
		LDA $1801
		AND #$DF
		STA $1801		 
		LDA #$10
		STA $1C07

		LDA #$01  ; GO NEAR START OF DATA BEFORE
		STA $06
		LDA #$00
		STA $07
	
		LDA #$B0
		STA $00
		CLI
		LDA $00
		BMI *-2
   
		SEI

		LDY #<RESTLDR
		JSR GETBLK
		        
		LDX #$22
		-
		LDA GETTORAM,X
		STA GOTORAM,X
		DEX
		BPL -
		JSR TABGEN
		JMP STRTLDR 
;==========		
GETTORAM
		LDA TRACK		;SHORT PROCEDURE
		STA $08		  ;FOR GETTING DATA TO
		LDA SECTOR        ;RAMDRIVE
		STA $09
		LDA #$00
		STA $0A
		JSR GETJOB
		STA $0B
   -
		JSR GETONE
		LDY $0A
		STA ($08),Y
		INC $0A
		DEC $0B
		BNE -
		
		JMP ($0006) ;RUN
;==========
;-
ENDLDR
.HERE
;===========================
END_PROC1


